﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

namespace FTdi260Wrapper
{
    public enum FT260_FLOW
    {
        OFF = 0,
        RTS_CTS_MODE = 1,                                           // hardware flow control RTS, CTS mode
        DTR_DSR_MODE = 2,                                           // hardware flow control DTR, DSR mode
        XON_XOFF_MODE = 3,                                          // software flow control mode
        NO_FLOW_CTRL_MODE = 4,                                      // no flow control mode
    }
}
// Copyright (c) 2022 Zilico Limited
