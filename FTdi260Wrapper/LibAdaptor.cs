﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Runtime.InteropServices;

namespace FTdi260Wrapper
{
    /// <summary>
    /// LibFT260 adaptor
    /// </summary>
    internal class LibAdaptor
    {
        //FT260_GetLibVersion(LPDWORD lpdwLibVersion);
        [DllImport("LibFT260.dll", EntryPoint = "FT260_GetLibVersion")]
        internal static extern FT260_STATUS GetLibraryVersion(out UInt32 lpdwLibVersion);

        //FT260_CreateDeviceList(LPDWORD lpdwNumDevs);
        [DllImport("LibFT260.dll", EntryPoint = "FT260_CreateDeviceList")]
        public static extern FT260_STATUS CreateDeviceList(out UInt32 lpdwNumDevs);


        //FT260_GetDevicePath(WCHAR* pDevicePath, DWORD bufferLength, DWORD deviceIndex);
        [DllImport("LibFT260.dll", EntryPoint = "FT260_GetDevicePath", 
                   CallingConvention = CallingConvention.Winapi, CharSet = CharSet.Unicode)]
        public static extern FT260_STATUS GetDevicePath(IntPtr pDevicePath, Int32 bufferLength, Int32 deviceIndex);



        //FT260_OpenByVidPid(WORD vid, WORD pid, DWORD deviceIndex, FT260_HANDLE* pFt260Handle);
        [DllImport("LibFT260.dll", EntryPoint = "FT260_OpenByVidPid")]
        public static extern FT260_STATUS OpenByVidPid(UInt16 vid, UInt16 pid, UInt32 deviceIndex, out IntPtr handle);

        //FT260_OpenByDevicePath(WCHAR* pDevicePath, FT260_HANDLE* pFt260Handle);
        [DllImport("LibFT260.dll", EntryPoint = "FT260_OpenByDevicePath")]
        public static extern FT260_STATUS OpenByDevicePath(IntPtr pDevicePath, out IntPtr handle);


        //FT260_GetChipVersion(FT260_HANDLE ft260Handle, LPDWORD lpdwChipVersion);
        [DllImport("LibFT260.dll", EntryPoint = "FT260_GetChipVersion")]
        internal static extern FT260_STATUS GetChipVersion(IntPtr handle, out UInt32 lpdwChipVersion);


        //FT260_UART_Init(FT260_HANDLE ft260Handle);
        [DllImport("LibFT260.dll", EntryPoint = "FT260_UART_Init")]
        public static extern FT260_STATUS Initialise(IntPtr handle);


        //FT260_UART_SetBaudRate(FT260_HANDLE ft260Handle, ULONG baudRate);
        [DllImport("LibFT260.dll", EntryPoint = "FT260_UART_SetBaudRate")]
        public static extern FT260_STATUS SetBaudRate(IntPtr handle, UInt32 baudRate);


        //FT260_UART_SetFlowControl(FT260_HANDLE ft260Handle, FT260_UART_Mode flowControl);
        [DllImport("LibFT260.dll", EntryPoint = "FT260_UART_SetFlowControl")]
        public static extern FT260_STATUS SetFlowControl(IntPtr handle, UInt32 flowControl);


        //FT260_Close(FT260_HANDLE ft260Handle);
        [DllImport("LibFT260.dll", EntryPoint = "FT260_Close")]
        public static extern FT260_STATUS Close(IntPtr handle);
    }
}
// Copyright (c) 2022 Zilico Limited
