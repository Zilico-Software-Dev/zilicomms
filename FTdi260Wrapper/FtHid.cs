﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using System.Linq;
//using System.Text;
using System.Threading.Tasks;

namespace FTdi260Wrapper
{
    /// <summary>
    /// LibFT260 
    /// </summary>
    public class FtHid
    {
        #region fields

        private string _Message;

        private bool _TraceEnabled;

        private IntPtr _Ft260Handle;

        #endregion

        #region properties

        public string Message
        { get { return _Message; } }

        /// <summary>
        /// Enable the trace debug output
        /// </summary>
        internal bool TraceEnabled
        {
            get { return _TraceEnabled; }
            set { _TraceEnabled = value; }
        }

        #endregion



        public FtHid() : this(false) { }

        public FtHid(bool traceEnable)
        {
            _TraceEnabled = traceEnable;

            _Ft260Handle = IntPtr.Zero;
        }



        /// <summary>
        /// Get libFT260 version number returned in Message property
        /// </summary>
        /// <returns></returns>
        public bool GetLibFT260Version()
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                UInt32 versionNumber = 0;

                FT260_STATUS status = LibAdaptor.GetLibraryVersion(out versionNumber);

                if (!status.Equals(FT260_STATUS.OK))
                    throw new ApplicationException(status.ToString());
                
                string version = $"{versionNumber:x8}";

                while (version.Length > 1)
                {
                    _Message += version.Substring(0, 2);

                    version = version.Substring(2);

                    if (version.Length > 0)
                        _Message += ".";
                }

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"LibFT260 version {_Message}");

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        /// <summary>
        /// Leaky as fuck. Use at own risk
        /// </summary>
        /// <returns></returns>
        public bool CreateDeviceList()
        {
            bool result = false;

            _Message = string.Empty;

            IntPtr devicePathPtr = IntPtr.Zero;

            try
            {
                UInt32 deviceCount = 0;

                FT260_STATUS status = LibAdaptor.CreateDeviceList(out deviceCount);

                if (!status.Equals(FT260_STATUS.OK))
                    throw new ApplicationException(status.ToString());

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"LibFT260 HID device count {deviceCount}");

                devicePathPtr = System.Runtime.InteropServices.Marshal.AllocHGlobal(0x80);

                if (deviceCount > 0)
                {
                    for (int i = 0; i < deviceCount; i++)
                    {
                        status = LibAdaptor.GetDevicePath(devicePathPtr, 128, i);

                        if (status.Equals(FT260_STATUS.OK))
                        {
                            string path = System.Runtime.InteropServices.Marshal.PtrToStringUni(devicePathPtr);

                            System.Diagnostics.Trace.WriteLine(path);
                        }
                    }
                }

                System.Diagnostics.Trace.WriteLine("____________________________________");

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }
            finally
            {
                if (devicePathPtr != IntPtr.Zero)
                    System.Runtime.InteropServices.Marshal.FreeHGlobal(devicePathPtr);
            }

            return result;
        }


        /// <summary>
        /// Open handle to FT260 using Vid and Pid
        /// </summary>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        /// <param name="deviceIndex"></param>
        /// <returns></returns>
        public bool Open(UInt16 vid, UInt16 pid, UInt32 deviceIndex)
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                if (_Ft260Handle != IntPtr.Zero)
                {
                    // close
                }

                FT260_STATUS status = LibAdaptor.OpenByVidPid(vid, pid, deviceIndex, out _Ft260Handle);

                if (!status.Equals(FT260_STATUS.OK))
                    throw new ApplicationException(status.ToString());

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        /// <summary>
        /// Open handle to FT260 using device path
        /// </summary>
        /// <param name="devicePath"></param>
        /// <returns></returns>
        public bool Open(string devicePath)
        {
            bool result = false;

            _Message = string.Empty;

            IntPtr devicePathPtr = IntPtr.Zero;

            try
            {
                if (_Ft260Handle != IntPtr.Zero)
                {
                    // close
                }

                devicePathPtr = System.Runtime.InteropServices.Marshal.StringToHGlobalUni(devicePath);

                FT260_STATUS status = LibAdaptor.OpenByDevicePath(devicePathPtr, out _Ft260Handle);

                if (!status.Equals(FT260_STATUS.OK))
                    throw new ApplicationException(status.ToString());

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }
            finally
            {
                if (devicePathPtr != IntPtr.Zero)
                    System.Runtime.InteropServices.Marshal.FreeHGlobal(devicePathPtr);
            }

            return result;
        }

        /// <summary>
        /// Get FT260 chip version number returned in Message property
        /// </summary>
        /// <returns></returns>
        public bool GetChipVersion()
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                UInt32 versionNumber = 0;

                FT260_STATUS status = LibAdaptor.GetChipVersion(_Ft260Handle, out versionNumber);

                if (!status.Equals(FT260_STATUS.OK))
                    throw new ApplicationException(status.ToString());

                string version = $"{versionNumber:x8}";

                while (version.Length > 3)
                {
                    _Message += version.Substring(0, 4);

                    version = version.Substring(4);

                    if (version.Length > 0)
                        _Message += ".";
                }

                if (!string.IsNullOrEmpty(version))
                {
                    _Message += "-";
                    _Message += version;
                }

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"Chip version {_Message}");

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }


        /// <summary>
        /// Initialise FT260 UART 
        /// </summary>
        /// <returns></returns>
        public bool Initialise()
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                if (_Ft260Handle.Equals(IntPtr.Zero))
                    throw new ApplicationException("Port not open");

                FT260_STATUS status = LibAdaptor.Initialise(_Ft260Handle);

                if (!status.Equals(FT260_STATUS.OK))
                    throw new ApplicationException(status.ToString());

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        /// <summary>
        /// Set FT260 UART Baud Rate
        /// </summary>
        /// <param name="baud"></param>
        /// <returns></returns>
        public bool SetBaudRate(UInt32 baud)
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                if (_Ft260Handle.Equals(IntPtr.Zero))
                    throw new ApplicationException("Port not open");

                FT260_STATUS status = LibAdaptor.SetBaudRate(_Ft260Handle, baud);

                if (!status.Equals(FT260_STATUS.OK))
                    throw new ApplicationException(status.ToString());

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        /// <summary>
        /// Set FT260 UART Flow Control
        /// </summary>
        /// <param name="flow"></param>
        /// <returns></returns>
        public bool SetFlowControl(FT260_FLOW flow)
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                if (_Ft260Handle.Equals(IntPtr.Zero))
                    throw new ApplicationException("Port not open");

                FT260_STATUS status = LibAdaptor.SetFlowControl(_Ft260Handle,(UInt32) flow);

                if (!status.Equals(FT260_STATUS.OK))
                    throw new ApplicationException(status.ToString());

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        /// <summary>
        /// Close FT260 connection
        /// </summary>
        /// <returns></returns>
        public bool Close()
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                if (!_Ft260Handle.Equals(IntPtr.Zero))
                {
                    FT260_STATUS status = LibAdaptor.Close(_Ft260Handle);

                    if (!status.Equals(FT260_STATUS.OK))
                        throw new ApplicationException(status.ToString());

                    _Ft260Handle = IntPtr.Zero;
                }

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

    }
}
// Copyright (c) 2022 Zilico Limited
