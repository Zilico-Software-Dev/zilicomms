﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;

namespace ZiliComms.Protobuff
{
    /// <summary>
    /// Single float
    /// </summary>
    internal class Bit32Coder : CoderBase, ICoder
    {
        private Single _Value;

        /// <summary>
        /// Single float
        /// </summary>
        public object Value
        { get { return _Value; } }


        /// <summary>
        /// Encode value
        /// </summary>
        /// <param name="value"></param>
        public Bit32Coder(Single value)
        {
            _Value = value;

            _Data = new List<byte>();

            _Data.AddRange(BitConverter.GetBytes(_Value));
        }

        /// <summary>
        /// Decode bytes
        /// </summary>
        /// <param name="data"></param>
        public Bit32Coder(List<byte> data)
        {
            _Data = data;

            int dataSize = 4;                                       // 32 bit

            int length = _Data.Count - dataSize;

            if (length > 0)
                _Data.RemoveRange(dataSize, length);

            _Value = BitConverter.ToSingle(_Data.ToArray(), 0);
        }



        public override string ToString()
        {
            return _Value.ToString();
        }
    }
}
// Copyright (c) 2022 Zilico Limited
