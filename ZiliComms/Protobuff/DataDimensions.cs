﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

namespace ZiliComms.Protobuff
{
    /// <summary>
    /// Encoded and decoded byte lengths
    /// </summary>
    internal class DataDimensions
    {
        private int _Start;
        private int _Length;

        /// <summary>
        /// Index of first data byte
        /// </summary>
        public int Start
        {
            get { return _Start; }
            set { _Start = value; }
        }

        /// <summary>
        /// Number of data bytes
        /// </summary>
        public int Length
        {
            get { return _Length; }
            set { _Length = value; }
        }


        /// <summary>
        /// Encoded and decoded byte lengths
        /// </summary>
        public DataDimensions()
        {
            _Start = -1;
            _Length = -1;
        }


        public override string ToString()
        {
            return $"{_Start} Encoded bytes, {_Length} Data bytes";
        }
    }
}
// Copyright (c) 2021 Zilico Limited
