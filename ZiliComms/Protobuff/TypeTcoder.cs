﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using ZiliComms.Handset;

namespace ZiliComms.Protobuff
{
    public interface ITypeTcoder<T>
    {
        T Value { get; }

        List<byte> Data { get; }


        string ToString();
    }

    public class TcoderBase<T> : ITypeTcoder<T>
    {
        protected T _Value;

        protected List<byte> _Data;



        public T Value
        { get { return _Value; } }

        public List<byte> Data
        { get { return _Data; } }



        // Unable to cast object of type
        // 'ZiliComms.Protobuff.TcoderBase`1[ZiliComms.Handset.Session]'
        // to type
        // 'ZiliComms.Protobuff.TypeTcoder`1[ZiliComms.Handset.Session]'.
        public static implicit operator TcoderBase<T>(TypeTcoder<Session> v)
        {
            return (TcoderBase<T>)new TcoderBase<Session>();
        }
    }




    public class TypeTcoder<T> : TcoderBase<T>
    {
        public TypeTcoder(List<byte> data)
        {
            _Data = data;

            _Value = (T)Activator.CreateInstance(typeof(T));

            if ((_Data != null) && (_Data.Count > 1))
            {
                DataDimensions dd = FindLength();

                int dataSize = dd.Start + dd.Length;

                if (_Data.Count < dataSize)
                    throw new ApplicationException("LengthDelimitCoder: Data length too short");

                int length = _Data.Count - dataSize;                // trim the end off Data

                if (length > 0)
                    _Data.RemoveRange(dataSize, length);

                //if (!ParseResponse(_Data.ToArray(), _Value))
                //{ }
            }
            else
                throw new ApplicationException("LengthDelimitCoder: Insufficient data");
        }



        internal DataDimensions FindLength()
        {
            DataDimensions result = new DataDimensions();

            result.Length = _Data[0];

            result.Start = 0;

            if (_Data[0] > 0x80)                                // MSB set = longer than 127 characters
            {
                List<byte> buffer = new List<byte>();           // decode string length

                while (_Data[result.Start] > 0x80)
                {
                    buffer.Add(_Data[result.Start]);

                    result.Start++;
                }

                buffer.Add(_Data[result.Start]);

                VarIntCoder32 decodeLength = new VarIntCoder32(buffer);

                result.Length = (Int32)decodeLength.Value;
            }

            result.Start++;

            return result;
        }


        public override string ToString()
        {
            return _Value.ToString();
        }
    }

    public class StringTCoder<T> : TcoderBase<T>
    {
        /// <summary>
        /// Instantiate a new String coder with encoded data
        /// </summary>
        /// <param name="data">Encoded byte collection</param>
        public StringTCoder(List<byte> data)
        {
            _Data = data;

            if ((_Data != null) && (_Data.Count > 1))
            {
                DataDimensions dd = FindLength();

                int dataSize = dd.Start + dd.Length;

                if (_Data.Count < dataSize)
                    throw new ApplicationException("StringCoder: Data length too short");

                string buffer = System.Text.Encoding.UTF8.GetString(_Data.ToArray(), dd.Start, dd.Length);
                _Value = (T)Convert.ChangeType(buffer, typeof(T));

                int length = _Data.Count - dataSize;                // trim the end off Data

                if (length > 0)
                    _Data.RemoveRange(dataSize, length);
            }
            else
                throw new ApplicationException("StringCoder: Insufficient data");
        }

        /// <summary>
        /// Instantiate a new String coder with a string to encode
        /// </summary>
        /// <param name="message">String to be encoded</param>
        public StringTCoder(string message)
        {
            _Value = (T)Convert.ChangeType(message, typeof(T));

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(Convert.ToString(_Value));

            VarIntCoder32 encodeLength = new VarIntCoder32(buffer.Length);

            _Data = new List<byte>();

            _Data.AddRange(encodeLength.Data);
            _Data.AddRange(buffer);
        }



        /// <summary>
        /// Find string length and starting index in Data
        /// </summary>
        /// <returns></returns>
        internal DataDimensions FindLength()
        {
            DataDimensions result = new DataDimensions();

            result.Length = _Data[0];

            result.Start = 0;

            if (_Data[0] > 0x80)                                // MSB set = longer than 127 characters
            {
                List<byte> buffer = new List<byte>();           // decode string length

                while (_Data[result.Start] > 0x80)
                {
                    buffer.Add(_Data[result.Start]);

                    result.Start++;
                }

                buffer.Add(_Data[result.Start]);

                VarIntCoder32 decodeLength = new VarIntCoder32(buffer);

                result.Length = (Int32)decodeLength.Value;
            }

            result.Start++;

            return result;
        }


        public override string ToString()
        {
            return Convert.ToString(_Value);
        }
    }



    public static class Trial
    {
        public static void TryThis(List<byte> buffer)
        {
            Protobuff.TypeTcoder<Handset.Session> testCoder = new Protobuff.TypeTcoder<Handset.Session>(buffer);
            Protobuff.StringTCoder<string> testStringCoder = new Protobuff.StringTCoder<string>(buffer);


            //Protobuff.TcoderBase<Handset.IBaseService> testBaseT =
            //    new Protobuff.TypeTcoder<Handset.Session>(buffer);


            //Protobuff.ITypeTcoder<Handset.IBaseService> testItypeT = 
            //    new Protobuff.TypeTcoder<Handset.Session>(buffer);

            //System.Diagnostics.Trace.WriteLine($"{testItypeT.Value} {testItypeT.Data}");



            Protobuff.TcoderBase<Handset.Session> testTCoder = new Protobuff.TypeTcoder<Handset.Session>(buffer);
            Protobuff.TcoderBase<string> testSCoder = new Protobuff.StringTCoder<string>(buffer);

            System.Diagnostics.Trace.WriteLine($"{testTCoder.Value} {testTCoder.Data}");
            System.Diagnostics.Trace.WriteLine($"{testSCoder.Value} {testSCoder.Data}");
        }
    }
}
// Copyright (c) 2022 Zilico Limited
