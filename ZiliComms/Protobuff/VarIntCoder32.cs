﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;

namespace ZiliComms.Protobuff
{
    /// <summary>
    /// Int32
    /// </summary>
    internal class VarIntCoder32 : CoderBase, ICoder
    {
        private Int32 _Value;

        /// <summary>
        /// 32 bit integer
        /// </summary>
        public object Value
        { get { return _Value; } }



        /// <summary>
        /// Encode value
        /// </summary>
        /// <param name="value"></param>
        public VarIntCoder32(Int32 value)
        {
            _Value = value;

            Encode(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Decode bytes
        /// </summary>
        /// <param name="data"></param>
        public VarIntCoder32(List<byte> data)
        {
            _Data = data;

            _Value = BitConverter.ToInt32(Decode(32), 0);
        }



        public override string ToString()
        {
            return _Value.ToString();
        }
    }
}
// Copyright (c) 2021 Zilico Limited
