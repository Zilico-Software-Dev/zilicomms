﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

namespace ZiliComms.Protobuff
{
    /// <summary>
    /// 3 bit variable Type encoding
    /// </summary>
    internal enum WireType
    {
        /// <summary>
        /// int32, int64, uint32, uint64, sint32, sint64, bool, enum
        /// </summary>
        VarInt = 0,

        /// <summary>
        /// fixed64, sfixed64, double
        /// </summary>
        bit64 = 1,

        /// <summary>
        /// string, bytes, embedded messages, packed repeated fields
        /// </summary>
        LengthDelimit = 2,

        /// <summary>
        /// groups (deprecated)
        /// </summary>
        GroupStart = 3,
        /// <summary>
        /// groups (deprecated)
        /// </summary>
        GroupEnd = 4,

        /// <summary>
        /// fixed32, sfixed32, float
        /// </summary>
        bit32 = 5,

        /// <summary>
        /// Cannot be set as 4 bits (0000 1000)
        /// </summary>
        Unknown = 8
    }
}
// Copyright (c) 2021 Zilico Limited
