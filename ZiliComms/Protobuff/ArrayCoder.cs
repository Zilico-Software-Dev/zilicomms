﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using System.Linq;
//using System.Text;
using System.Threading.Tasks;

namespace ZiliComms.Protobuff
{
    internal class ArrayCoder : CoderBase, ICoder
    {
        private byte[] _Value;                                      // ToDo: ArrayCoder - generic data type

        /// <summary>
        /// Object being encoded or decoded
        /// </summary>
        public object Value
        { get { return _Value; } }



        public ArrayCoder(List<byte> data, object target)
        {
            _Data = data;

            if ((_Data != null) && (_Data.Count > 1))
            {
                DataDimensions dd = FindLength(true);

                int dataSize = dd.Start + dd.Length;

                if (_Data.Count < dataSize)
                    throw new ApplicationException("ArrayCoder: Data length too short");

                int length = _Data.Count - dataSize;                // trim the end off Data

                if (length > 0)
                    _Data.RemoveRange(dataSize, length);

                Type type = ((Type)target).GetElementType();

                int elementsize = 0;

                switch (Type.GetTypeCode(type))
                {
                    //case TypeCode.Empty:
                    //    break;
                    //case TypeCode.Object:
                    //    break;
                    //case TypeCode.DBNull:
                    //    break;
                    //case TypeCode.Boolean:
                    //    break;
                    case TypeCode.Char:
                        elementsize = 1;
                        break;
                    //case TypeCode.SByte:
                    //    break;
                    case TypeCode.Byte:
                        elementsize = 1;
                        break;
                    case TypeCode.Int16:
                        elementsize = 2;
                        break;
                    case TypeCode.UInt16:
                        elementsize = 2;
                        break;
                    case TypeCode.Int32:
                        elementsize = 4;
                        break;
                    case TypeCode.UInt32:
                        elementsize = 4;
                        break;
                    case TypeCode.Int64:
                        elementsize = 8;
                        break;
                    case TypeCode.UInt64:
                        elementsize = 8;
                        break;
                    //case TypeCode.Single:
                    //    break;
                    //case TypeCode.Double:
                    //    break;
                    //case TypeCode.Decimal:
                    //    break;
                    //case TypeCode.DateTime:
                    //    break;
                    //case TypeCode.String:
                    //    break;
                    default:
                        break;
                }

                if ((elementsize > 0) && (dd.Length % elementsize == 0))
                    length = dd.Length / elementsize;

                _Value = (byte[])Activator.CreateInstance((Type)target, length);

                for (int i = 0; i < length; i++)                    // extract data bytes
                {
                    _Value[i] = data[dd.Start + i];
                }
            }
            //else
            //    throw new ApplicationException("ArrayCoder: Insufficient data");
        }

    }
}
// Copyright (c) 2022 Zilico Limited
