﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;

namespace ZiliComms.Protobuff
{
    internal static class CoderFactory
    {
        public static ICoder NewDecoder(Type type, List<byte> buffer)
        {
            ICoder coder = null;

            switch (Type.GetTypeCode(type))
            {
                //case TypeCode.Empty:
                //    break;

                case TypeCode.Object:
                    if (type.IsArray)
                        coder = new ArrayCoder(buffer, type);
                    else
                        coder = new LengthDelimitCoder(buffer, Activator.CreateInstance(type));
                    break;

                //case TypeCode.DBNull:
                //    break;

                case TypeCode.Boolean:
                    coder = new BoolCoder(buffer);
                    break;

                //case TypeCode.Char:
                //    break;
                //case TypeCode.SByte:
                //    break;
                //case TypeCode.Byte:
                //    break;

                case TypeCode.Int16:
                    coder = new VarIntCoder16(buffer);
                    break;
                case TypeCode.UInt16:
                    coder = new VarIntCoderU16(buffer);
                    break;

                case TypeCode.Int32:
                    coder = new VarIntCoder32(buffer);
                    break;
                case TypeCode.UInt32:
                    coder = new VarIntCoderU32(buffer);
                    break;
                case TypeCode.Int64:
                    coder = new VarIntCoder64(buffer);
                    break;
                case TypeCode.UInt64:
                    coder = new VarIntCoderU64(buffer);
                    break;

                case TypeCode.Single:
                    coder = new Bit32Coder(buffer);
                    break;

                //case TypeCode.Double:
                //    break;
                //case TypeCode.Decimal:
                //    break;
                case TypeCode.DateTime:
                    coder = new VarIntCoderU32(buffer);
                    break;

                case TypeCode.String:
                    coder = new StringCoder(buffer);
                    break;

                default:
                    throw new ApplicationException($"Unsupported type [{Type.GetTypeCode(type)}]");
                    //break;
            }

            return coder;
        }
    }
}
// Copyright (c) 2022 Zilico Limited
