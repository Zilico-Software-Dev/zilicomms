﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;
using System.Text;

namespace ZiliComms.Protobuff
{
    /// <summary>
    /// string, bytes, embedded messages, packed repeated fields
    /// </summary>
    internal class StringCoder : CoderBase, ICoder
    {
        private string _Value;

        /// <summary>
        /// String value
        /// </summary>
        public object Value
        { get { return _Value; } }



        /// <summary>
        /// Instantiate a new String coder with encoded data
        /// </summary>
        /// <param name="data">Encoded byte collection</param>
        public StringCoder(List<byte> data)
        {
            _Data = data;

            if ((_Data != null) && (_Data.Count > 1))
            {
                DataDimensions dd = FindLength(true);

                int dataSize = dd.Start + dd.Length;

                if (_Data.Count < dataSize)
                    throw new ApplicationException("StringCoder: Data length too short");

                _Value = Encoding.UTF8.GetString(_Data.ToArray(), dd.Start, dd.Length);

                int length = _Data.Count - dataSize;                // trim the end off Data

                if (length > 0)
                    _Data.RemoveRange(dataSize, length);
            }
            else
                throw new ApplicationException("StringCoder: Insufficient data");
        }

        /// <summary>
        /// Instantiate a new String coder with a string to encode
        /// </summary>
        /// <param name="message">String to be encoded</param>
        public StringCoder(string message)
        {
            _Value = message;

            byte[] buffer = Encoding.UTF8.GetBytes(_Value);

            VarIntCoder32 encodeLength = new VarIntCoder32(buffer.Length);

            _Data = new List<byte>();

            _Data.AddRange(encodeLength.Data);
            _Data.AddRange(buffer);
        }



        public override string ToString()
        {
            return _Value;
        }
    }
}
// Copyright (c) 2021 Zilico Limited
