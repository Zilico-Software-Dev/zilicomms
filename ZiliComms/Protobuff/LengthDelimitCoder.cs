﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;

namespace ZiliComms.Protobuff
{
    /// <summary>
    /// Container for value and data byte collection
    /// </summary>
    internal class LengthDelimitCoder : CoderBase, ICoder
    {
        private object _Value;

        /// <summary>
        /// Object being encoded or decoded
        /// </summary>
        public object Value
        { get { return _Value; } }



        /// <summary>
        /// Container for value and data byte collection
        /// </summary>
        /// <param name="value"></param>
        public LengthDelimitCoder(object value)
        {
            _Value = value;

            _Data = new List<byte>();
        }

        /// <summary>
        /// Decode bytes
        /// </summary>
        /// <param name="data"></param>
        public LengthDelimitCoder(List<byte> data, object target)
        {
            _Data = data;
            _Value = target;

            if ((_Data != null) && (_Data.Count > 1))
            {
                DataDimensions dd = FindLength(true);

                int dataSize = dd.Start + dd.Length;

                if (_Data.Count < dataSize)
                    throw new ApplicationException("LengthDelimitCoder: Data length too short");

                int length = _Data.Count - dataSize;                // trim the end off Data

                if (length > 0)
                    _Data.RemoveRange(dataSize, length);
            }
            else
                throw new ApplicationException("LengthDelimitCoder: Insufficient data");
        }



        public override string ToString()
        {
            return _Value.ToString();
        }
    }
}
// Copyright (c) 2022 Zilico Limited
