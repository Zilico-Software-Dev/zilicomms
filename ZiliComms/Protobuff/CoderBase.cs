﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;
using System.Text;

namespace ZiliComms.Protobuff
{
    /// <summary>
    /// String Class   encode and decode string, bytes, embedded messages, packed repeated fields
    /// VarInt Classes encode and decode Int32, Int64, UInt32, UInt64, sint32, sint64, bool, enum
    /// </summary>
    internal class CoderBase
    {
        /// <summary>
        /// Encoded byte array
        /// </summary>
        protected List<byte> _Data;


        /// <summary>
        /// Variable representation byte array
        /// </summary>
        public List<byte> Data
        { get { return _Data; } }


        /// <summary>
        /// <para>Find number of encoded data bytes in Data.</para>
        /// <para>If delimited find length and starting index</para>
        /// </summary>
        /// <param name="lengthDelimit"></param>
        /// <returns></returns>
        internal virtual DataDimensions FindLength(bool lengthDelimit)
        {
            DataDimensions result = new DataDimensions();

            result.Start = 0;

            if (lengthDelimit)
            {
                result.Length = _Data[0];

                if (MoreBytes(_Data[0]))                            // MSB set = longer than 127 characters
                {
                    List<byte> buffer = new List<byte>();           // decode string length

                    while (MoreBytes(_Data[result.Start]))
                    {
                        buffer.Add(_Data[result.Start]);

                        result.Start++;
                    }

                    buffer.Add(_Data[result.Start]);

                    VarIntCoder32 decodeLength = new VarIntCoder32(buffer);

                    result.Length = (Int32)decodeLength.Value;
                }

                result.Start++;
            }
            else
            {
                result.Length = 0;

                while (MoreBytes(_Data[result.Length]))             // MSB set = another byte to follow
                {
                    result.Length++;
                }

                result.Length++;
            }

            return result;
        }

        /// <summary>
        /// If MSB is set, then there is another byte to follow
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private bool MoreBytes(byte data)
        {
            return (data & 0x80) > 0;
        }


        /// <summary>
        /// convert 7 bit byte array to full bit pattern string
        /// </summary>
        /// <returns></returns>
        protected string To7BitPattern()
        {
            string result = string.Empty;

            StringBuilder sb = new StringBuilder();

            foreach (byte encoded in _Data)
            {
                result = Convert.ToString(encoded, 2);
                sb.Insert(0, result.PadLeft(8, '0').Substring(1));
            }

            result = sb.ToString();

            int length = result.Length / 8;

            if ((result.Length % 8) > 0)
                length++;

            length *= 8;

            result = result.PadLeft(length, '0');                   // fully populated binary string

            return result;
        }

        /// <summary>
        /// convert byte array to full bit pattern string
        /// </summary>
        /// <returns></returns>
        protected string ToBitPattern(byte[] data)
        {
            string result = string.Empty;

            StringBuilder sb = new StringBuilder();

            foreach (byte encoded in data)
            {
                result = Convert.ToString(encoded, 2);
                sb.Insert(0, result.PadLeft(8, '0'));
            }

            result = sb.ToString();

            return result;
        }


        /// <summary>
        /// encode byte array to Data property
        /// </summary>
        /// <returns></returns>
        protected void Encode(byte[] data)
        {
            _Data = new List<byte>();

            #region convert to binary string trimmed to byte sizes

            string bitPattern = string.Empty;

            StringBuilder sb = new StringBuilder();

            foreach (byte encoded in data)
            {
                bitPattern = Convert.ToString(encoded, 2);
                sb.Insert(0, bitPattern.PadLeft(8, '0'));
            }

            bitPattern = sb.ToString().TrimStart('0');              // if value is zer0 bitPattern will be empty

            int length = bitPattern.Length / 8;

            // round up to whole 8 bit bytes or add eight zer0s if bitPattern empty
            if (((bitPattern.Length % 8) > 0) || (bitPattern.Length < 1))
                length++;

            length = length * 8;

            bitPattern = bitPattern.PadLeft(length, '0');

            //System.Diagnostics.Trace.WriteLine(bitPattern);

            #endregion

            #region transform to 7 bit and reflect

            List<string> buffer = new List<string>();

            int count = length - 7;

            while (count > 0)
            {
                buffer.Add(bitPattern.Substring(count, 7));
                bitPattern = bitPattern.Remove(count, 7);
                count -= 7;
            }

            if (bitPattern.Length > 0)
            {
                if (bitPattern.Contains("1"))
                    buffer.Add(bitPattern.PadLeft(7, '0'));
            }

            #endregion

            #region encode buffer

            count = buffer.Count;

            count--;

            for (int i = 0; i < buffer.Count; i++)
            {
                if (count > i)
                    buffer[i] = buffer[i].Insert(0, "1");
                else
                    buffer[i] = buffer[i].Insert(0, "0");
            }

            #endregion

            #region bits to value

            foreach (string item in buffer)
            {
                count = 0;

                int sum = 0;

                for (int i = 7; i > -1; i--)
                {
                    if (item.Substring(i, 1).Equals("1"))
                        sum |= (1 << count);

                    count++;
                }

                _Data.Add((byte)sum);
            }

            #endregion
        }

        /// <summary>
        /// decode Data into byte array for BitConvertion
        /// </summary>
        /// <param name="bitLength"></param>
        /// <returns></returns>
        protected byte[] Decode(int bitLength)
        {
            List<byte> decoded = new List<byte>();

            DataDimensions dd = FindLength(false);

            string bitPattern = string.Empty;

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < dd.Length; i++)
            {
                bitPattern = Convert.ToString(_Data[i], 2);
                sb.Insert(0, bitPattern.PadLeft(8, '0').Substring(1));          // pad left to full 8 bit byte then drop MSB
            }

            int endIndex = dd.Length;
            int remainder = _Data.Count - endIndex;

            if (remainder > 0)
                _Data.RemoveRange(endIndex, remainder);


            bitPattern = sb.ToString();

            bitPattern = bitPattern.PadLeft(bitLength, '0');                    // fully populated binary string

            bitPattern = bitPattern.Substring(bitPattern.Length - bitLength);   // trim excess at start of string

            bitLength -= 8;                                                     // index of last byte in string

            for (int index = bitLength; index > -1; index = index - 8)          // step through bit string one byte at a time
            {
                string pattern = bitPattern.Substring(index, 8);

                int count = 0;

                int sum = 0;

                for (int i = 7; i > -1; i--)
                {
                    if (pattern.Substring(i, 1).Equals("1"))
                        sum |= (1 << count);

                    count++;
                }

                decoded.Add((byte)sum);
            }

            return decoded.ToArray();
        }
    }
}
// Copyright (c) 2021 Zilico Limited
