﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;

namespace ZiliComms.Protobuff
{
    /// <summary>
    /// UInt32
    /// </summary>
    internal class VarIntCoderU32 : CoderBase, ICoder
    {
        private UInt32 _Value;

        /// <summary>
        /// Unsigned 32 bit integer
        /// </summary>
        public object Value
        { get { return _Value; } }



        /// <summary>
        /// Encode value
        /// </summary>
        /// <param name="value"></param>
        public VarIntCoderU32(UInt32 value)
        {
            _Value = value;

            Encode(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Decode bytes
        /// </summary>
        /// <param name="data"></param>
        public VarIntCoderU32(List<byte> data)
        {
            _Data = data;

            _Value = BitConverter.ToUInt32(Decode(32), 0);
        }



        public override string ToString()
        {
            return _Value.ToString();
        }
    }
}
// Copyright (c) 2021 Zilico Limited
