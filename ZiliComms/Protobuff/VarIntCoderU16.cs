﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 20221010

using System;
using System.Collections.Generic;

namespace ZiliComms.Protobuff
{
    internal class VarIntCoderU16 : CoderBase, ICoder
    {
        private UInt16 _Value;

        /// <summary>
        /// Unsigned 16 bit integer
        /// </summary>
        public object Value
        { get { return _Value; } }



        /// <summary>
        /// Encode value
        /// </summary>
        /// <param name="value"></param>
        public VarIntCoderU16(UInt16 value)
        {
            _Value = value;

            Encode(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Decode bytes
        /// </summary>
        /// <param name="data"></param>
        public VarIntCoderU16(List<byte> data)
        {
            _Data = data;

            _Value = BitConverter.ToUInt16(Decode(16), 0);
        }



        public override string ToString()
        {
            return _Value.ToString();
        }
    }
}
// Copyright (c) 2022 Zilico Limited
