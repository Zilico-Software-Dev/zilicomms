﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;

namespace ZiliComms.Protobuff
{
    /// <summary>
    /// Int64
    /// </summary>
    internal class VarIntCoder64 : CoderBase, ICoder
    {
        private Int64 _Value;

        /// <summary>
        /// 64 bit integer
        /// </summary>
        public object Value
        { get { return _Value; } }



        /// <summary>
        /// Encode value
        /// </summary>
        /// <param name="value"></param>
        public VarIntCoder64(Int64 value)
        {
            _Value = value;

            Encode(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Decode bytes
        /// </summary>
        /// <param name="data"></param>
        public VarIntCoder64(List<byte> data)
        {
            _Data = data;

            _Value = BitConverter.ToInt64(Decode(64), 0);
        }



        public override string ToString()
        {
            return _Value.ToString();
        }
    }
}
// Copyright (c) 2021 Zilico Limited
