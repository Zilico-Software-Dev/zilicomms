﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;

namespace ZiliComms.Protobuff
{
    /// <summary>
    /// Boolean
    /// </summary>
    internal class BoolCoder : CoderBase, ICoder
    {
        private bool _Value;

        /// <summary>
        /// Boolean
        /// </summary>
        public object Value
        { get { return _Value; } }



        /// <summary>
        /// Encode value
        /// </summary>
        /// <param name="value"></param>
        public BoolCoder(bool value)
        {
            _Value = value;

            Encode(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Decode bytes
        /// </summary>
        /// <param name="data"></param>
        public BoolCoder (List<byte> data)
        {
            _Data = data;

            _Value = false;

            if (BitConverter.ToInt32(Decode(32), 0) > 0)
                _Value = true;
        }



        public override string ToString()
        {
            return _Value.ToString();
        }
    }
}
// Copyright (c) 2022 Zilico Limited
