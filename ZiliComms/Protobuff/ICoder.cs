﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System.Collections.Generic;

namespace ZiliComms.Protobuff
{
    /// <summary>
    /// Encoder Decoder Interface
    /// </summary>
    internal interface ICoder
    {
        /// <summary>
        /// Encoded data bytes
        /// </summary>
        List<byte> Data { get; }

        /// <summary>
        /// Decoded value
        /// </summary>
        object Value { get; }
    }
}