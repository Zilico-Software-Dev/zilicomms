﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;

namespace ZiliComms.Protobuff
{
    /// <summary>
    /// UInt64
    /// </summary>
    internal class VarIntCoderU64 : CoderBase, ICoder
    {
        private UInt64 _Value;

        /// <summary>
        /// Unsigned 64 bit integer
        /// </summary>
        public object Value
        { get { return _Value; } }



        /// <summary>
        /// Encode value
        /// </summary>
        /// <param name="value"></param>
        public VarIntCoderU64(UInt64 value)
        {
            _Value = value;

            Encode(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Decode bytes
        /// </summary>
        /// <param name="data"></param>
        public VarIntCoderU64(List<byte> data)
        {
            _Data = data;

            _Value = BitConverter.ToUInt64(Decode(64), 0);
        }



        public override string ToString()
        {
            return _Value.ToString();
        }
    }
}
// Copyright (c) 2021 Zilico Limited
