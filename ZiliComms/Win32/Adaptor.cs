﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Runtime.InteropServices;

namespace ZiliComms.Win32
{
    #region File Access enum

    /// <summary>
    /// Generic access rights
    /// </summary>
    internal enum FileAccess : uint
    {
        ALL = 0x10000000,
        EXECUTE = 0x20000000,
        WRITE = 0x40000000,
        READ = 0x80000000,
    }

    #endregion

    #region FileShare enum

    /// <summary>
    /// File share mode
    /// </summary>
    internal enum FileShare
    {
        /// <summary>
        /// Prevents other processes from opening a file or device if they request delete, read, or write access.
        /// </summary>
        NONE = 0x00000000,
        READ = 0x00000001,
        WRITE = 0x00000002,
        DELETE = 0x00000004,
    }

    #endregion

    #region File Disposition enum

    /// <summary>
    /// File creation disposition
    /// </summary>
    internal enum FileDisposition : uint
    {
        CREATE_NEW = 0x00000001,
        CREATE_ALWAYS = 0x00000002,
        OPEN_EXISTING = 0x00000003,
        OPEN_ALWAYS = 0x00000004,
        TRUNCATE_EXISTING = 0x00000005,
    }

    #endregion

    #region File Flag and Attributes enum

    /// <summary>
    /// File Flags and attributes
    /// </summary>
    internal enum FileFlagAttribs : uint
    {
        ATTRIBUTE_READONLY = 0x00000001,
        ATTRIBUTE_HIDDEN = 0x00000002,
        ATTRIBUTE_SYSTEM = 0x00000004,
        ATTRIBUTE_ARCHIVE = 0x00000020,
        ATTRIBUTE_NORMAL = 0x00000080,
        ATTRIBUTE_TEMPORARY = 0x00000100,
        ATTRIBUTE_OFFLINE = 0x00001000,
        ATTRIBUTE_ENCRYPTED = 0x00004000,

        FLAG_OPEN_NO_RECALL = 0x00100000,
        FLAG_OPEN_REPARSE_POINT = 0x00200000,
        FLAG_SESSION_AWARE = 0x00800000,
        FLAG_POSIX_SEMANTICS = 0x0100000,
        FLAG_BACKUP_SEMANTICS = 0x02000000,
        FLAG_DELETE_ON_CLOSE = 0x04000000,
        FLAG_SEQUENTIAL_SCAN = 0x08000000,
        FLAG_RANDOM_ACCESS = 0x10000000,
        FLAG_NO_BUFFERING = 0x20000000,
        FLAG_OVERLAPPED = 0x40000000,
        FLAG_WRITE_THROUGH = 0x80000000,
    }

    #endregion

    /// <summary>
    /// kernel32 API
    /// </summary>
    internal static class StreamAdaptor
    {
        #region kernel32 CreateFile constants

        // https://msdn.microsoft.com/en-us/library/windows/desktop/aa363858(v=vs.85).aspx

        #endregion

        #region Safe handle to file, serial port, USB device etc. KERNAL32 calls

        /// <summary>
        /// Creates/opens a file, serial port, USB device etc.
        /// </summary>
        /// <param name="fileName">Path to object to open</param>
        /// <param name="dwDesiredAccess">Access mode. e.g. Read, Write</param>
        /// <param name="dwShareMode">Sharing mode</param>
        /// <param name="securityAttrs_MustBeZero">Security details (can be null)</param>
        /// <param name="dwCreationDisposition">Specifies if the file is to be created or opened</param>
        /// <param name="dwFlagsAndAttributes">Any extra attributes e.g. open overlapped</param>
        /// <param name="hTemplateFile_MustBeZero">Not used</param>
        /// <returns>Pointer to file</returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern Microsoft.Win32.SafeHandles.SafeFileHandle CreateFile
        (
            String fileName,
            uint dwDesiredAccess,
            uint dwShareMode,
            IntPtr securityAttrs_MustBeZero,
            uint dwCreationDisposition,
            uint dwFlagsAndAttributes,
            IntPtr hTemplateFile_MustBeZero
        );

        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern bool WriteFile
        (
            Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid hFile,
            byte[] lpBuffer,
            uint dwNumberOfBytesToWrite,
            out uint lpNumberOfBytesWriten,
            uint lpOverlapped
        );

        /// <summary>
        /// Closes a window handle. File handles, event handles, mutex handles etc.
        /// </summary>
        /// <param name="hObject">Handle to close</param>
        /// <returns>True if successful</returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern bool CloseHandle(Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid hObject);



        /// <summary>
        /// String representation of the last Win32 API calls return code
        /// </summary>
        /// <returns></returns>
        public static ReturnCode GetLastError()
        {
            return (ReturnCode)Marshal.GetLastWin32Error();
        }

        #endregion
    }
}
// Copyright (c) 2021 Zilico Limited
