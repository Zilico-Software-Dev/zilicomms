﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021


namespace ZiliComms.Win32
{
    /// <summary>
    /// Errors
    /// </summary>
    internal enum ReturnCode
    {
        SUCCESS = 0,
        INVALID_FUNCTION = 1,
        FILE_NOT_FOUND = 2,
        PATH_NOT_FOUND = 3,
        TOO_MANY_OPEN_FILES = 4,
        ACCESS_DENIED = 5,
        INVALID_HANDLE = 6,
        ARENA_TRASHED = 7,
        NOT_ENOUGH_MEMORY = 8,
        INVALID_BLOCK = 9,
        BAD_ENVIRONMENT = 10,
        BAD_FORMAT = 11,
        INVALID_ACCESS = 12,
        INVALID_DATA = 13,
        OUTOFMEMORY = 14,
        INSUFFICIENT_BUFFER = 122,
        MORE_DATA = 234,
        NO_MORE_ITEMS = 259,
        SERVICE_SPECIFIC_ERROR = 1066,
        INVALID_USER_BUFFER = 1784
    }
}
// Copyright (c) 2021 Zilico Limited
