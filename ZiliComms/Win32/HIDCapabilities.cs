﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System.Runtime.InteropServices;

namespace ZiliComms.Win32
{
	/// <summary>
	/// Provides the capabilities of a HID device
	/// </summary>
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	internal struct HidCapabilities
	{
		public short Usage;
		public short UsagePage;
		public short InputReportByteLength;
		public short OutputReportByteLength;
		public short FeatureReportByteLength;
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 17)]
		public short[] Reserved;
		public short NumberLinkCollectionNodes;
		public short NumberInputButtonCaps;
		public short NumberInputValueCaps;
		public short NumberInputDataIndices;
		public short NumberOutputButtonCaps;
		public short NumberOutputValueCaps;
		public short NumberOutputDataIndices;
		public short NumberFeatureButtonCaps;
		public short NumberFeatureValueCaps;
		public short NumberFeatureDataIndices;
	}
}
// Copyright (c) 2021 Zilico Limited
