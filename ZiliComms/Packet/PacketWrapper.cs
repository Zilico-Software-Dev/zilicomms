﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 2022

using System;
using System.Collections.Generic;

namespace ZiliComms
{
    /// <summary>
    /// Wrapper for Communication packets
    /// </summary>
    public static class PacketWrapper
    {
        #region fields

        private static string _Message = string.Empty;

        private static List<byte> _PayLoad;

        private static byte _MessageId = 0xff;                      // NOP

        #endregion

        #region properties

        /// <summary>
        /// Status message
        /// </summary>
        public static string Message
        { get { return _Message; } }

        /// <summary>
        /// Encoded or decoded data
        /// </summary>
        public static byte[] PayLoad
        {
            get { return _PayLoad.ToArray(); }
            set
            {
                if (_PayLoad != null)
                    _PayLoad.Clear();
                else
                    _PayLoad = new List<byte>();

                _PayLoad.AddRange(value);
            }
        }

        /// <summary>
        /// Instruction message identification number (OpCode)
        /// </summary>
        public static byte MessageId
        { get { return _MessageId; } }

        #endregion


        /// <summary>
        /// Packet decode a byte collection with the result in PayLoad
        /// </summary>
        /// <param name="rxBuffer"></param>
        /// <returns></returns>
        public static bool Decode(List<byte> rxBuffer)
        {
            bool result = false;

            CommsState commsState = CommsState.Normal;
            MessageState messageState = MessageState.Idle;


            _Message = string.Empty;

            if (_PayLoad != null)
                _PayLoad.Clear();
            else
                _PayLoad = new List<byte>();

            UInt32 checkSum = 0;

            if ((rxBuffer != null) && (rxBuffer.Count > 0))
            {
                // Packets must start with an Escape value (0xfe) sometimes first byte is zero on freshly opened port
                if (rxBuffer[0].Equals((byte)PacketSignal.None))
                    rxBuffer.RemoveAt(0);

                int chkSmCount = 0;

                foreach (byte data in rxBuffer)
                {
                    byte chrBuffer = data;

                    if (data.Equals((byte)PacketSignal.Escape))         // sequential Escape (0xfe) ignored
                    {
                        commsState = CommsState.Escaped;
                    }
                    else
                    {
                        if (commsState.Equals(CommsState.Escaped))
                        {
                            // if not yet Extracting, Escape followed by 'Message Id'
                            if (data.Equals((byte)PacketSignal.EscapeAlt))
                            {
                                chrBuffer = (byte)PacketSignal.Escape;  // Message Id can not be 0xff (NOP)
                                commsState = CommsState.Normal;
                            }
                            else if (data.Equals((byte)PacketSignal.End) && messageState.Equals(MessageState.Extracting))
                            {
                                messageState = MessageState.End;
                            }
                            else                                        // Start of Payload
                            {
                                _MessageId = data;

                                commsState = CommsState.Normal;

                                if (!messageState.Equals((byte)MessageState.End))
                                    messageState = MessageState.Extracting;
                                else
                                    break;                              // Escape followed by illegal byte while in MessageState.End
                            }
                        }

                        if (commsState.Equals(CommsState.Normal))
                        {
                            if (messageState.Equals(MessageState.Extracting))
                            {
                                _PayLoad.Add(chrBuffer);
                            }
                            else if (messageState.Equals(MessageState.End)) // accumulate checksum
                            {
                                checkSum <<= 8;
                                checkSum += chrBuffer;

                                chkSmCount++;

                                if (chkSmCount > 3)
                                {
                                    messageState = MessageState.Complete;
                                    break;
                                }
                            }
                        }

                        commsState = CommsState.Normal;
                    }
                }
            }

            if (messageState.Equals(MessageState.Complete))
            {
                if (CheckSum.CalculateCRC(_PayLoad).Equals(checkSum))
                    result = true;
                else
                    _Message = "Failed checksum";
            }
            else if ((rxBuffer != null) && (rxBuffer.Count > 0))
                _Message = "Incomplete packet";
            else
                _Message = "No response";

            return result;
        }


        /// <summary>
        /// Packet encode a byte array with the result in PayLoad
        /// </summary>
        /// <param name="sendData"></param>
        /// <returns></returns>
        public static bool Encode(byte[] sendData)
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                // calculate checksum on the data before escape values applied
                UInt32 checkSum = CheckSum.CalculateCRC(sendData);

                if (_PayLoad != null)
                    _PayLoad.Clear();
                else
                    _PayLoad = new List<byte>();

                EncodeData(sendData);

                _PayLoad.Insert(0, (byte)PacketSignal.Escape);          // packets start with an Escape value

                _PayLoad.Add((byte)PacketSignal.Escape);                // packet data ends with an Escape value
                _PayLoad.Add((byte)PacketSignal.End);                   // and an End value

                byte[] checkSumBytes = BitConverter.GetBytes(checkSum);

                Array.Reverse(checkSumBytes, 0, checkSumBytes.Length);

                EncodeData(checkSumBytes);

                result = true;
            }
            catch (Exception e)
            {
                _Message = $"Encode message {e.Message}";
            }

            return result;
        }

        private static void EncodeData(byte[] data)
        {
            foreach (byte item in data)
            {
                _PayLoad.Add(item);

                if (item.Equals((byte)PacketSignal.Escape))         // indicate valid data value
                    _PayLoad.Add((byte)PacketSignal.EscapeAlt);
            }
        }
    }
}
// Copyright (c) 2022 Zilico Limited
