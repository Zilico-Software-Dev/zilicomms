﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

namespace ZiliComms
{
    /// <summary>
    /// Escape Characters
    /// </summary>
    internal enum PacketSignal : byte
    {
        None = 0x00,
        End = 0xFD,
        Escape = 0xFE,
        EscapeAlt = 0xFF,
    }
}
// Copyright (c) 2021 Zilico Limited
