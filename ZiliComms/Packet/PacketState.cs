﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;

namespace ZiliComms
{
    // Packet decode states

    internal enum CommsState
    {
        Normal = 0,
        Escaped = 1,
    }

    internal enum MessageState
    {
        Idle = 0,
        Extracting = 1,
        End = 2,
        Complete = 3,
    }

    [Flags]
    internal enum PacketState                                         // Not implemented yet
    {
        Normal = 0x00,
        Escaped = 0x01,

        Extracting = 0x10,
        End = 0x20,
        Complete = 0x40,
    }
}
// Copyright (c) 2021 Zilico Limited
