﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;

namespace ZiliComms
{
    public static class CheckSum
    {
        private static UInt32[] _CRC32Table = CRC32();



        // https://stackoverflow.com/questions/53438815/how-to-build-crc32-table-for-ogg#53442047

        /// <summary>
        /// MPEG2 CRC32 Table generation
        /// </summary>
        private static UInt32[] CRC32()
        {
            const UInt32 polynomial = 0x04c11db7;

            uint[] checksumTable = new uint[0x100];                 // 256

            UInt32 crc = 0;
            for (UInt32 c = 0; c < 0x100; c++)
            {
                crc = c << 24;

                for (int i = 0; i < 8; i++)
                {
                    UInt32 b = crc >> 31;
                    crc <<= 1;
                    crc ^= (0 - b) & polynomial;
                }

                checksumTable[c] = crc;
            }

            return checksumTable;
        }

        /// <summary>
        /// Calculate checksum on data collection
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static UInt32 CalculateCRC(IEnumerable<byte> data)
        {
            UInt32 accumulator = 0xffffffff;

            foreach (byte element in data)
            {
                accumulator = _CRC32Table[(accumulator >> 24) ^ element] ^ (accumulator << 8);
            }

            return accumulator;
        }
    }
}

// Copyright (c) 2021 Zilico Limited
