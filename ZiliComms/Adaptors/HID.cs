﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;

namespace ZiliComms
{
    /// <summary>
    /// HID Communication port and data stream
    /// </summary>
    public class HID : AdaptorBase, IUSBPort
    {
        #region fields

        private Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid _HandleWrite;
        private Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid _HandleRead;

        private int _InputReportLength;
        private int _OutputReportLength;


        private AsyncState _AsyncState;


        //private bool _Disposed;

        #endregion


        #region ~ctor

        public HID(string devicePath) : this(devicePath, false) { }

        public HID(string devicePath, bool traceEnabled)
        {
            _Message = string.Empty;

            _TraceEnabled = traceEnabled;

            _DevicePath = devicePath;

            _TimeOut = 1000;

            _Disposed = false;
            _IsOpen = false;
            _RxError = false;

            _RxBuffer = new List<byte>();

            _HandleWrite = null;
            _HandleRead = null;

            _InputReportLength = -1;
            _OutputReportLength = -1;

            _AsyncState = null;
        }


        ~HID()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of readability and maintainability.
            this.Dispose(false);
        }


        public virtual void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to take this object off the finalization queue
            // and prevent finalization code for this object from executing a second time.
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine($"ZiliComms Dispose Disposing : {disposing}");

            // Check to see if Dispose has already been called.
            if (!_Disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                }

                // Call the appropriate methods to clean up unmanaged resources

                // Close stream handles
                Close();

                // Note disposing has been done.
                _Disposed = true;
            }
        }

        #endregion


        /// <summary>
        /// Open communications
        /// </summary>
        /// <returns></returns>
        public override bool Open()
        {
            bool result = false;

            _Message = string.Empty;

            IntPtr parsedData = IntPtr.Zero;

            try
            {
                if (_IsOpen)
                {
                    if (!Close())
                        throw new ApplicationException(_Message);
                }

                if (string.IsNullOrEmpty(_DevicePath))
                    throw new ApplicationException("Device path not set");

                _HandleWrite = Win32.StreamAdaptor.CreateFile(_DevicePath,
                                                              (uint)Win32.FileAccess.WRITE,
                                                              (uint)(Win32.FileShare.READ | Win32.FileShare.WRITE),
                                                              IntPtr.Zero, (uint)Win32.FileDisposition.OPEN_EXISTING,
                                                              0, IntPtr.Zero);

                if (_HandleWrite.IsInvalid)
                {
                    Win32.ReturnCode returnCode = Win32.StreamAdaptor.GetLastError();

                    throw new ApplicationException($"Write Handle {returnCode}");
                }

                #region HID Capabilities

                // Retrieve Report Lengths
                if (Win32.HidApi.HidD_GetPreparsedData(_HandleWrite, out parsedData))
                {
                    Win32.HidCapabilities hidCapabilities;

                    Win32.HidApi.HidP_GetCaps(parsedData, out hidCapabilities);

                    _InputReportLength = hidCapabilities.InputReportByteLength;
                    _OutputReportLength = hidCapabilities.OutputReportByteLength;
                }

                #endregion

                _HandleRead = Win32.StreamAdaptor.CreateFile(_DevicePath,
                                                             (uint)Win32.FileAccess.READ,
                                                             (uint)(Win32.FileShare.READ | Win32.FileShare.WRITE),
                                                             IntPtr.Zero, (uint)Win32.FileDisposition.OPEN_EXISTING,
                                                             (uint)Win32.FileFlagAttribs.FLAG_OVERLAPPED, IntPtr.Zero);

                if (_HandleRead.IsInvalid)
                {
                    Win32.ReturnCode returnCode = Win32.StreamAdaptor.GetLastError();

                    throw new ApplicationException($"Read Handle {returnCode}");
                }

                if (!(_HandleWrite.IsClosed || _HandleRead.IsClosed))
                    result = true;

                _IsOpen = result;
            }
            catch (Exception e)
            {
                if (string.IsNullOrEmpty(_Message))
                    _Message = $"Open {e.Message}";

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"ZiliComms {_Message}");
            }
            finally
            {
                if (!parsedData.Equals(IntPtr.Zero))
                    Win32.HidApi.HidD_FreePreparsedData(ref parsedData);

                bool closeWriteHandle = false;

                if (_HandleWrite != null)
                {
                    if (_HandleRead != null)
                        closeWriteHandle = _HandleRead.IsClosed;
                    else
                        closeWriteHandle = true;
                }

                if (closeWriteHandle)
                {
                    Win32.StreamAdaptor.CloseHandle(_HandleWrite);
                    _HandleWrite.SetHandleAsInvalid();
                }
            }

            return result;
        }

        /// <summary>
        /// Close communications
        /// </summary>
        /// <returns></returns>
        public override bool Close()
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                if ((_HandleRead != null) && (!_HandleRead.IsInvalid))
                {
                    if (_AsyncState != null)
                    {
                        try
                        {
                            _AsyncState.Stream.Close();
                        }
                        catch (Exception)                           // Continue rest of clean up if stream broken
                        { }
                    }

                    if (!_HandleRead.IsClosed)
                    {
                        Win32.StreamAdaptor.CloseHandle(_HandleRead);
                        _HandleRead.SetHandleAsInvalid();
                    }

                    _AsyncState = null;
                }

                if ((_HandleWrite != null) && (!_HandleWrite.IsInvalid) && (!_HandleWrite.IsClosed))
                {
                    Win32.StreamAdaptor.CloseHandle(_HandleWrite);
                    _HandleWrite.SetHandleAsInvalid();
                }

                _IsOpen = false;

                result = true;
            }
            catch (Exception e)
            {
                _Message = $"Close {e.Message}";

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"ZiliComms {_Message}");
            }

            return result;
        }

        /// <summary>
        /// Transmit sendData and receive into ReturnedPacket
        /// </summary>
        /// <param name="sendData"></param>
        /// <returns></returns>
        public override bool Transceive(byte[] sendData)
        {
            // http://msdn.microsoft.com/en-us/library/7db28s3c(v=vs.90).aspx

            bool result = false;

            _Message = string.Empty;

            _RxError = false;

            try
            {
                _RxBuffer.Clear();

                if (!_IsOpen)
                {
                    //if (_AsyncState != null)                      // this needed?
                    //{
                    //    _AsyncState.Dispose();
                    //    _AsyncState = null;
                    //}

                    if (!Open())
                        throw new ApplicationException(_Message);
                }

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"Tx : {ToHex(sendData)}");


                if (_AsyncState != null)
                    _AsyncState.Reset();
                else
                {
                    _AsyncState = new AsyncState(_InputReportLength);

                    _AsyncState.Stream = new System.IO.FileStream((Microsoft.Win32.SafeHandles.SafeFileHandle)_HandleRead,
                                                                  System.IO.FileAccess.ReadWrite, _InputReportLength, true);
                }

                if (BeginAsyncRead(_AsyncState) && Transmit(sendData))
                    result = _AsyncState.WaitHandle.WaitOne(_TimeOut, false);
                else
                    throw new ApplicationException(_Message);

                if (result)
                {
                    if (_TraceEnabled)
                        System.Diagnostics.Trace.WriteLine($"Rx : {ToHex(_RxBuffer.ToArray())}");
                }
                else
                {
                    if (!_RxError)                                  // End stream error
                        throw new ApplicationException("Timeout");
                }
            }
            catch (Exception e)
            {
                if (string.IsNullOrEmpty(_Message))
                    _Message = $"Transceive {e.Message}";

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"ZiliComms {_Message}");

                string message = _Message;

                Close();

                _Message = message;
            }

            return result;
        }


        // SetBaudRate(230400);

        /// <summary>
        /// Set Baud Rate and disable flow control
        /// </summary>
        /// <param name="baud"></param>
        /// <returns></returns>
        public override bool SetBaudRate(int baud)
        {
            bool result = false;

            _Message = string.Empty;

            _RxError = false;

            FTdi260Wrapper.FtHid ftHid = null;

            try
            {
                ftHid = new FTdi260Wrapper.FtHid(true);

                //result = ftHid.CreateDeviceList();

                if (!ftHid.Open(_DevicePath))
                    throw new ApplicationException($"LibFT260 Open {ftHid.Message}");

                //result = ftHid.GetChipVersion();

                if (!ftHid.Initialise())
                    throw new ApplicationException($"LibFT260 Initialise {ftHid.Message}");

                if (!ftHid.SetFlowControl(FTdi260Wrapper.FT260_FLOW.NO_FLOW_CTRL_MODE))
                    throw new ApplicationException($"LibFT260 Set Flow Control {ftHid.Message}");

                if (baud > 0)
                {
                    if (!ftHid.SetBaudRate((uint)baud))
                        throw new ApplicationException($"LibFT260 Set Baud Rate {ftHid.Message}");
                }

                result = true;
            }
            catch (Exception e)
            {
                if (string.IsNullOrEmpty(_Message))
                    _Message = $"SetBaudRate {e.Message}";

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"SetBaudRate {_Message}");
            }
            finally
            {
                if (ftHid != null)
                {
                    string message = _Message;
                    if (!ftHid.Close())
                    {
                        if (!string.IsNullOrEmpty(message))
                            _Message = message;
                    }
                }
            }

            return result;
        }



        private bool BeginAsyncRead(AsyncState asyncState)
        {
            bool result = asyncState.BeginRead(new AsyncCallback(EndAsyncRead));

            if (!result)
                _Message = $"BeginRead {asyncState.Message}";

            return result;
        }

        private void EndAsyncRead(IAsyncResult asyncResult)
        {
            const int HidReportLength = 2;                          // block count and data length
            const int TailLength = 6;                               // Escape, End and CheckSum

            try
            {
                AsyncState readState = (AsyncState)asyncResult.AsyncState;

                if (readState.Stream.EndRead(asyncResult) > 0)
                {
                    byte reportId = readState.DataBuffer[0];        // # 4 byte blocks : f0 = 1; f1 = 2; f2 = 3

                    byte dataLength = readState.DataBuffer[1];      // data length in HID packet

                    byte[] dataBuffer = new byte[dataLength];

                    Array.Copy(readState.DataBuffer, HidReportLength, dataBuffer, 0, dataBuffer.Length);

                    _RxBuffer.AddRange(dataBuffer);

                    if ((_RxBuffer.Count > TailLength) &&
                        _RxBuffer[_RxBuffer.Count - 6].Equals((byte)PacketSignal.Escape) &&
                        _RxBuffer[_RxBuffer.Count - 5].Equals((byte)PacketSignal.End))
                    {
                        readState.Signal();
                    }
                    else
                        BeginAsyncRead(readState);
                }
            }
            catch (Exception e)
            {
                _RxError = true;

                if (string.IsNullOrEmpty(_Message))
                    _Message = $"EndRead {e.Message}";
            }
        }

        /// <summary>
        /// Apply HID Packet wrapper
        /// </summary>
        /// <param name="sendData"></param>
        /// <returns></returns>
        private bool Transmit(byte[] sendData)
        {
            const int hidHeaderSize = 2;                            // reportId and data length

            bool result = false;

            byte[] outputPacketBuffer = null;

            int sendPtr = 0;

            int maxDataLength = _OutputReportLength;

            if (_SingleByteMode)
                maxDataLength = hidHeaderSize + 1;

            maxDataLength -= hidHeaderSize;

            while (sendPtr < sendData.Length)                       // EOF Data
            {
                int sendDataLength = sendData.Length - sendPtr;

                if (!(sendDataLength < maxDataLength))              // max packet data = 60 => 0xfe
                    sendDataLength = maxDataLength;

                #region calculate HID reportId and packetSize

                int reportId = sendDataLength;
                reportId /= 4;                                      // 4 byte increments

                int packetSize = reportId * 4;

                if (packetSize < sendDataLength)
                    reportId++;

                packetSize = reportId * 4;
                packetSize += hidHeaderSize;

                reportId--;                                         // 0xf0 => 4 bytes

                reportId |= 0xf0;

                #endregion

                outputPacketBuffer = new byte[packetSize];

                outputPacketBuffer[0] = (byte)reportId;
                outputPacketBuffer[1] = (byte)sendDataLength;

                Array.Copy(sendData, sendPtr, outputPacketBuffer, hidHeaderSize, sendDataLength);

                sendPtr += sendDataLength;

                uint bytesWritten = 0;

                result = Win32.StreamAdaptor.WriteFile(_HandleWrite, outputPacketBuffer,
                                                       (uint)outputPacketBuffer.Length, out bytesWritten, 0);

                if (!_OutputReportLength.Equals((int)bytesWritten))
                    _Message = "Not all bytes were transmitted";
            }

            return result;
        }
    } 
}
// Copyright (c) 2021 Zilico Limited
