﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;

namespace ZiliComms
{
    /// <summary>
    /// Communications interface
    /// </summary>
    public interface IUSBPort
    {
        /// <summary>
        /// Status message
        /// </summary>
        string Message { get; }

        /// <summary>
        /// Debug trace output enabled
        /// </summary>
        bool TraceEnabled { get; set; }

        /// <summary>
        /// Amount of time in milliseconds for the command to respond
        /// </summary>
        Int32 TimeOut { get; set; }

        /// <summary>
        /// Path to the device
        /// </summary>
        string DevicePath { get; }

        /// <summary>
        /// Communication established
        /// </summary>
        bool IsOpen { get; }

        /// <summary>
        /// Transmit one byte at a time
        /// </summary>
        bool SingleByteMode { get; set; }

        /// <summary>
        /// Bytes returned
        /// </summary>
        List<byte> ReturnedPacket { get; }

        /// <summary>
        /// Port has been disposed
        /// </summary>
        bool IsDisposed { get; }



        /// <summary>
        /// Close communication port and dispose of resources
        /// </summary>
        void Dispose();


        /// <summary>
        /// Open communications
        /// </summary>
        /// <returns></returns>
        bool Open();

        /// <summary>
        /// Close communications
        /// </summary>
        /// <returns></returns>
        bool Close();

        /// <summary>
        /// Transmit sendData and receive into ReturnedPacket
        /// </summary>
        /// <param name="sendData"></param>
        /// <returns></returns>
        bool Transceive(byte[] sendData);

        /// <summary>
        /// Set Baud Rate
        /// </summary>
        /// <param name="rate"></param>
        /// <returns></returns>
        bool SetBaudRate(Baud rate);

        /// <summary>
        /// Set Baud Rate
        /// </summary>
        /// <param name="baud"></param>
        /// <returns></returns>
        bool SetBaudRate(int baud);

        /// <summary>
        /// Convert byte array to hex string
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        string ToHex(byte[] buffer);
    }
}