﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;

namespace ZiliComms
{
    /// <summary>
    /// serial Communication port and data stream
    /// </summary>
    public class Serial : AdaptorBase, IUSBPort
    {
        #region fields

        private string _PortNumber;

        private System.IO.Ports.SerialPort _CommPort;

        private int _InputReportLength;
        private int _OutputReportLength;


        private AsyncState _AsyncState;


        //private bool _Disposed;

        #endregion


        #region ~ctor

        public Serial(string devicePath) : this(devicePath, false) { }

        public Serial(string devicePath, bool traceEnabled)
        {
            _Message = string.Empty;

            _TraceEnabled = traceEnabled;

            _DevicePath = devicePath;

            _PortNumber = string.Empty;

            _TimeOut = 1000;

            _Disposed = false;
            _IsOpen = false;
            _RxError = false;

            _RxBuffer = new List<byte>();

            _CommPort = null;

            _InputReportLength = -1;
            _OutputReportLength = -1;

            _AsyncState = null;
        }


        ~Serial()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of readability and maintainability.
            this.Dispose(false);
        }


        public virtual void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to take this object off the finalization queue
            // and prevent finalization code for this object from executing a second time.
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine($"ZiliComms Dispose Disposing : {disposing}");

            // Check to see if Dispose has already been called.
            if (!_Disposed)
            {
                // If disposing equals true, dispose all managed and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                }

                // Call the appropriate methods to clean up unmanaged resources

                // Close stream handles
                Close();

                // Note disposing has been done.
                _Disposed = true;
            }
        }

        #endregion


        /// <summary>
        /// Open communications
        /// </summary>
        /// <returns></returns>
        public override bool Open()
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                if (_IsOpen)
                {
                    if (!Close())
                        throw new ApplicationException(_Message);
                }

                if (string.IsNullOrEmpty(_DevicePath))
                    throw new ApplicationException("Device path not set");

                if (!GetCommPortNumber())
                    throw new ApplicationException("Can't find comm port");

                if (_CommPort != null)
                    _CommPort.PortName = _PortNumber;
                else
                {
                    _CommPort = new System.IO.Ports.SerialPort(_PortNumber);

                    _InputReportLength = _CommPort.ReadBufferSize;                      // 4096
                    _OutputReportLength = _CommPort.WriteBufferSize;                    // 2048
                }

                _CommPort.Open();

                _IsOpen = _CommPort.IsOpen;

                result = _IsOpen;
            }
            catch (Exception e)
            {
                if (string.IsNullOrEmpty(_Message))
                    _Message = $"Open {e.Message}";

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"ZiliComms {_Message}");
            }

            return result;
        }

        /// <summary>
        /// Close communications
        /// </summary>
        /// <returns></returns>
        public override bool Close()
        {
            if ((_CommPort != null) && (_CommPort.IsOpen))
            {
                _CommPort.Close();

                _IsOpen = _CommPort.IsOpen;
            }
            else
                _IsOpen = false;

            return !_IsOpen;
        }

        /// <summary>
        /// Transmit sendData and receive into ReturnedPacket
        /// </summary>
        /// <param name="sendData"></param>
        /// <returns></returns>
        public override bool Transceive(byte[] sendData)
        {
            bool result = false;

            _Message = string.Empty;

            _RxError = false;

            try
            {
                _RxBuffer.Clear();

                if (!_IsOpen)
                {
                    if (_AsyncState != null)
                    {
                        _AsyncState.Dispose();
                        _AsyncState = null;
                    }

                    if (!Open())
                        throw new ApplicationException(_Message);
                }

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"Tx : {ToHex(sendData)}");

                if (_AsyncState != null)
                    _AsyncState.Reset();
                else
                {
                    _AsyncState = new AsyncState(_InputReportLength);

                    _AsyncState.Stream = _CommPort.BaseStream;
                }

                if (BeginAsyncRead(_AsyncState))
                {
                    if (_SingleByteMode)
                    {
                        foreach (var item in sendData)
                        {
                            _CommPort.BaseStream.Write(new byte[] { item }, 0, 1);
                            _CommPort.BaseStream.Flush();
                        }
                    }
                    else
                        _CommPort.BaseStream.Write(sendData, 0, sendData.Length);

                    result = _AsyncState.WaitHandle.WaitOne(_TimeOut, false);
                }
                else
                    throw new ApplicationException(_Message);

                if (result)
                {
                    if (_TraceEnabled)
                        System.Diagnostics.Trace.WriteLine($"Rx : {ToHex(_RxBuffer.ToArray())}");
                }
                else
                {
                    if (!_RxError)                                  // End stream error
                        throw new ApplicationException("Timeout");
                }
            }
            catch (Exception e)
            {
                if (string.IsNullOrEmpty(_Message))
                    _Message = $"Transceive {e.Message}";

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"ZiliComms {_Message}");

                string message = _Message;

                Close();

                _Message = message;
            }

            return result;
        }

        /// <summary>
        /// Set Baud Rate
        /// </summary>
        /// <param name="baud"></param>
        /// <returns></returns>
        public override bool SetBaudRate(int baud)
        {
            bool result = false;

            try
            {
                if (_CommPort != null)
                {
                    if (_CommPort.BaudRate != baud)
                    {
                        bool wasOpen = false;

                        if (_IsOpen)
                        {
                            wasOpen = true;

                            if (!Close())
                                throw new ApplicationException(_Message);
                        }

                        if (baud > 0)
                        {
                            _CommPort.BaudRate = baud;

                            _CommPort.DataBits = 8;
                            _CommPort.Handshake = System.IO.Ports.Handshake.None;
                            _CommPort.Parity = System.IO.Ports.Parity.None;
                            _CommPort.StopBits = System.IO.Ports.StopBits.One;
                        }
                        else
                        {
                            // dispose and re-instantiate
                            _CommPort.Dispose();
                            _CommPort = null;
                        }

                        if (wasOpen)
                            result = Open();
                        else
                            result = true;
                    }
                    else
                        result = true;
                }
                else
                    throw new ApplicationException("Comm port not set");
            }
            catch (Exception e)
            {
                if (string.IsNullOrEmpty(_Message))
                    _Message = $"SetBaudRate {e.Message}";

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"ZiliComms {_Message}");
            }

            return result;
        }


        private bool BeginAsyncRead(AsyncState asyncState)
        {
            bool result = asyncState.BeginRead(new AsyncCallback(EndAsyncRead));

            if (!result)
                _Message = $"BeginRead {asyncState.Message}";

            return result;
        }

        private void EndAsyncRead(IAsyncResult asyncResult)
        {
            const int TailLength = 6;                               // Escape, End and CheckSum

            try
            {
                AsyncState readState = (AsyncState)asyncResult.AsyncState;

                int bytesRead = readState.Stream.EndRead(asyncResult);

                if (bytesRead > 0)
                {
                    byte[] dataBuffer = new byte[bytesRead];

                    Array.Copy(readState.DataBuffer, 0, dataBuffer, 0, dataBuffer.Length);

                    _RxBuffer.AddRange(dataBuffer);

                    if ((_RxBuffer.Count > TailLength) &&
                        _RxBuffer[_RxBuffer.Count - 6].Equals((byte)PacketSignal.Escape) &&
                        _RxBuffer[_RxBuffer.Count - 5].Equals((byte)PacketSignal.End))
                    {
                        readState.Signal();
                    }
                    else
                        BeginAsyncRead(readState);
                }
            }
            catch (Exception e)
            {
                _RxError = true;

                if (string.IsNullOrEmpty(_Message))
                    _Message = $"EndRead {e.Message}";
            }
        }

        /// <summary>
        /// Set _PortNumber
        /// </summary>
        /// <returns></returns>
        private bool GetCommPortNumber()
        {
            bool result = false;

            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine("GetCommPortNumber");

            int firstDelimiter = _DevicePath.IndexOf("#");

            string path = _DevicePath.Substring(firstDelimiter, _DevicePath.IndexOf("{") - (firstDelimiter + 1));
            path = path.Replace("#", "\\").ToUpper();

            string query = "SELECT * FROM WIN32_SerialPort";
            query = $"SELECT * FROM Win32_PnPEntity WHERE Caption like '%(COM%'";

            System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher(query);

            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine("searching");

            System.Management.ManagementObjectCollection matches = searcher.Get();

            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine("parsing");

            // PNPDeviceID  USB\\VID_0483&PID_5740\\48E570563135
            foreach (var item in matches)
            {
                var p = item.GetPropertyValue("PNPDeviceID");
                if ((p != null) && p.ToString().ToUpper().Contains(path))
                {
                    _PortNumber = item.GetPropertyValue("DeviceID").ToString();

                    if (!_PortNumber.Contains("COM"))               // ftdibus#vid_0403+pid_6001+a10kvbbqa#0000
                    {
                        _PortNumber = item.GetPropertyValue("Name").ToString(); // USB Serial Port (COM8)

                        _PortNumber = _PortNumber.Substring(_PortNumber.IndexOf('(') + 1);

                        _PortNumber = _PortNumber.Substring(0, _PortNumber.IndexOf(')'));
                    }

                    result = true;

                    break;
                }
            }

            return result;
        }
    }
}
// Copyright (c) 2021 Zilico Limited
