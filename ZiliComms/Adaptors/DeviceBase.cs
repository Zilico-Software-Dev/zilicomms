﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;

namespace ZiliComms
{
    /// <summary>
    /// Communications interface device
    /// </summary>
    public abstract class AdaptorBase
    {
        #region fields

        protected string _Message;

        protected bool _TraceEnabled;

        protected string _DevicePath;

        protected Int32 _TimeOut;

        protected bool _IsOpen;

        /// <summary>
        /// End stream error
        /// </summary>
        protected bool _RxError;

        protected List<byte> _RxBuffer;

        protected bool _SingleByteMode;


        protected bool _Disposed;

        #endregion

        #region properties

        /// <summary>
        /// Status message
        /// </summary>
        public string Message
        { get { return _Message; } }

        /// <summary>
        /// Debug trace output enabled
        /// </summary>
        public bool TraceEnabled
        {
            get { return _TraceEnabled; }
            set { _TraceEnabled = value; }
        }

        /// <summary>
        /// Path to the device
        /// </summary>
        public string DevicePath
        { get { return _DevicePath; } }

        /// <summary>
        /// Port has been disposed
        /// </summary>
        public bool IsDisposed
        { get { return _Disposed; } }

        /// <summary>
        /// Communication established
        /// </summary>
        public bool IsOpen
        { get { return _IsOpen; } }

        /// <summary>
        /// Transmit one byte at a time
        /// </summary>
        public bool SingleByteMode
        {
            get { return _SingleByteMode; }
            set { _SingleByteMode = value; }
        }

        /// <summary>
        /// Bytes returned minus HID headers
        /// </summary>
        public List<byte> ReturnedPacket
        { get { return _RxBuffer; } }

        /// <summary>
        /// Maximum time for a complete return response
        /// </summary>
        public Int32 TimeOut
        {
            get { return _TimeOut; }
            set { _TimeOut = value; }
        }

        #endregion



        /// <summary>
        /// Open communications
        /// </summary>
        /// <returns></returns>
        public abstract bool Open();

        /// <summary>
        /// Close communications
        /// </summary>
        /// <returns></returns>
        public abstract bool Close();

        /// <summary>
        /// Transmit sendData and receive into ReturnedPacket
        /// </summary>
        /// <param name="sendData"></param>
        /// <returns></returns>
        public abstract bool Transceive(byte[] sendData);


        /// <summary>
        /// Set Baud Rate and disable flow control
        /// </summary>
        /// <param name="rate"></param>
        /// <returns></returns>
        public bool SetBaudRate(Baud rate)
        {
            return SetBaudRate((int)rate);
        }

        /// <summary>
        /// Set Baud Rate and disable flow control
        /// </summary>
        /// <param name="baud"></param>
        /// <returns></returns>
        public virtual bool SetBaudRate(int baud)
        {
            return false;
        }

        /// <summary>
        /// Convert byte array to hex string
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public virtual string ToHex(byte[] buffer)
        {
            string result = string.Empty;

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (byte item in buffer)
            {
                sb.Append($"{item:X2} ");
            }

            result = sb.ToString();

            return result.Trim();
        }
    }
}