﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;

namespace ZiliComms
{
    /// <summary>
    /// Stream container
    /// </summary>
    internal class AsyncState: IDisposable
    {
        #region Fields

        private string _Message;

        System.IO.Stream _Stream;

        byte[] _DataBuffer;

        System.Threading.ManualResetEvent _ManualWaitHandle;

        private bool _Disposed;

        #endregion

        #region Properties

        /// <summary>
        /// Status message
        /// </summary>
        public string Message
        { get { return _Message; } }

        /// <summary>
        /// Signal packet received
        /// </summary>
        public System.Threading.ManualResetEvent WaitHandle
        { get { return _ManualWaitHandle; } }

        /// <summary>
        /// Stream read buffer
        /// </summary>
        public byte[] DataBuffer
        { get { return _DataBuffer; } }

        /// <summary>
        /// The communication stream
        /// </summary>
        public System.IO.Stream Stream
        {
            get { return _Stream; }
            set { _Stream = value; }
        }

        #endregion



        /// <summary>
        /// Initialises DataBuffer to bufferSize
        /// </summary>
        /// <param name="bufferSize"></param>
        public AsyncState(Int32 bufferSize)
        {
            _Message = string.Empty;

            _DataBuffer = new byte[bufferSize];

            _ManualWaitHandle = new System.Threading.ManualResetEvent(false);

            _Stream = null;

            _Disposed = false;
        }

        // override finaliser only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~AsyncSerial()
        // {
        //     // Do not change this code. Put clean up code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }


        protected virtual void Dispose(bool disposing)
        {
            if (!_Disposed)
            {
                if (disposing)                                      // dispose managed state (managed objects)
                {
                    if(_ManualWaitHandle!=null)
                        _ManualWaitHandle.Dispose();

                    if (_Stream != null)
                        _Stream.Dispose();
                }

                // free unmanaged resources (unmanaged objects) here and override finaliser

                _DataBuffer = null;                                 // set large fields to null

                _Disposed = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put clean up code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }




        /// <summary>
        /// Begin asynchronous stream read
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public bool BeginRead(AsyncCallback callback)
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                _Stream.BeginRead(_DataBuffer, 0, _DataBuffer.Length, callback, this);

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        /// <summary>
        /// Indicate read complete
        /// </summary>
        public void Signal()
        {
            if (_ManualWaitHandle != null)
                _ManualWaitHandle.Set();
        }

        /// <summary>
        /// Clear the Data buffer and resets WaitHandle
        /// </summary>
        /// <param name="bufferSize"></param>
        public void Reset()
        {
            Array.Clear(_DataBuffer, 0, _DataBuffer.Length);

            if (_ManualWaitHandle != null)
                _ManualWaitHandle.Reset();
        }
    }
}
// Copyright (c) 2021 Zilico Limited
