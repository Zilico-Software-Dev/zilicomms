﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 20221012

using System;

namespace ZiliComms
{
    public delegate void DataTransferEventHandler(object sender, DataTransferEventArgs e);



    public class DataTransferEventArgs : EventArgs
    {

        private readonly byte _PercentageComplete = 0;


        public byte PercentageComplete
        { get { return _PercentageComplete; } }


        public DataTransferEventArgs(int percentageComplete)
        {
            byte percent = 0;

            if (percentageComplete > 100)
                percent = 100;
            else
                percent = (byte)percentageComplete;

            _PercentageComplete = percent;
        }
    }
}
// Copyright (c) 2022 Zilico Limited
