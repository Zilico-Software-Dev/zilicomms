﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 20221020

using System;

namespace ZiliComms.Handset
{
    public class BlockEnd
    {
        #region fields

        private UInt32 _Size;

        private byte[] _Signature;

        #endregion

        #region properties


        [PropId(1)]
        public UInt32 Size
        { get { return _Size; } }


        [PropId(2)]
        public byte[] Signature
        { get { return _Signature; } }

        #endregion



        public BlockEnd(UInt32 size, byte[] signature)
        {
            _Size = size;

            _Signature = signature;
        }

    }
}
// Copyright (c) 2022 Zilico Limited
