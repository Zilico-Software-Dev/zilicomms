﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

namespace ZiliComms.Handset
{
    /// <summary>
    /// Referral Types
    /// </summary>
    public enum ReferralType : int
    {
        //Modified BSCC 2008

        /// <summary>
        /// No Referral
        /// </summary>
        BSCC_No_Referral = 0,

        /// <summary>
        /// Borderline
        /// </summary>
        BSCC_Borderline = 1,

        /// <summary>
        /// Borderline (B) Glandular
        /// </summary>
        BSCC_Borderline_Glandular = 2,

        /// <summary>
        /// Low Grade
        /// </summary>
        BSCC_Low_Grade = 3,

        /// <summary>
        /// Possible (Q) High Grade
        /// </summary>
        BSCC_Possible_High_Grade = 4,

        /// <summary>
        /// High Grade
        /// </summary>
        BSCC_High_Grade = 5,

        /// <summary>
        /// Possible (Q) Invasive
        /// </summary>
        BSCC_Possible_Invasive = 6,

        /// <summary>
        /// Glandular Neoplasia
        /// </summary>
        BSCC_Glandular_Neoplasia = 7,

        //TBS 2001

        /// <summary>
        /// No Referral
        /// </summary>
        TBS_No_Referral = 8,

        /// <summary>
        /// ASC-US
        /// </summary>
        TBS_ASC_US = 9,

        /// <summary>
        /// LSIL
        /// </summary>
        TBS_LSIL = 10,

        /// <summary>
        /// ASC-H
        /// </summary>
        TBS_Atypical_Squamous_Possible_HSIL = 11,

        /// <summary>
        /// HSIL
        /// </summary>
        TBS_HSIL = 12,

        /// <summary>
        /// Squamous_Carcinoma
        /// </summary>
        TBS_Squamous_Cell_Carcinoma = 13,

        /// <summary>
        /// AGCUS
        /// </summary>
        TBS_Atypical_Glandular = 14,

        /// <summary>
        /// AIS
        /// </summary>
        TBS_Adenocarcinoma_in_Situ = 15,

        //ECTP

        /// <summary>
        /// No Referral
        /// </summary>
        ECTP_No_Referral = 16,

        /// <summary>
        /// Koilocytes
        /// </summary>
        ECTP_Koilocytes = 17,

        /// <summary>
        /// CIN 1
        /// </summary>
        ECTP_Mild_Dysplasia = 18,

        /// <summary>
        /// CIN 2
        /// </summary>
        ECTP_Moderate_Dysplasisa = 19,

        /// <summary>
        /// CIN 3
        /// </summary>
        ECTP_Severe_Dysplasia = 20,

        /// <summary>
        /// Possible Invasive
        /// </summary>
        ECTP_Severe_Dysplasia_Possible_Invasive = 21,

        /// <summary>
        /// Atypical Glandular
        /// </summary>
        ECTP_Atypical_Glandular = 22,

        /// <summary>
        /// AIS
        /// </summary>
        ECTP_Adenocarcinoma_in_Situ = 23,

        //AMBS 2004

        /// <summary>
        /// No Referral
        /// </summary>
        AMBS_No_Referral = 24,

        /// <summary>
        /// Possible LSIL
        /// </summary>
        AMBS_Possible_Low_Grade_SIL = 25,

        /// <summary>
        /// LSIL
        /// </summary>
        AMBS_Low_Grade_SIL = 26,

        /// <summary>
        /// Possible HSIL
        /// </summary>
        AMBS_Possible_High_Grade_SIL = 27,

        /// <summary>
        /// HSIL
        /// </summary>
        AMBS_High_Grade_SIL = 28,

        /// <summary>
        /// Squamous Carcinoma
        /// </summary>
        AMBS_Squamous_Carcinoma = 29,

        /// <summary>
        /// AGCUS
        /// </summary>
        AMBS_Atypical_Glandular = 30,

        /// <summary>
        /// AIS
        /// </summary>
        AMBS_Adenocarcinoma_in_Situ = 31,

        //additions

        /// <summary>
        /// HR-HPV cytology negative
        /// </summary>
        TBS_HR_HPV = 32,

        /// <summary>
        /// HR-HPV cytology negative
        /// </summary>
        ECTP_HR_HPV = 33,

        /// <summary>
        /// HR-HPV cytology negative
        /// </summary>
        AMBS_HR_HPV = 34,

        //changes

        /// <summary>
        /// No cytology abnormality
        /// </summary>
        BSCC_No_Cytology = 35,

        /// <summary>
        /// No cytology abnormality
        /// </summary>
        TBS_No_Cytology = 36,

        /// <summary>
        /// No cytology abnormality
        /// </summary>
        ECTP_No_Cytology = 37,

        /// <summary>
        /// No cytology abnormality
        /// </summary>
        AMBS_No_Cytology = 38,


        /// <summary>
        /// HR-HPV cytology negative
        /// </summary>
        BSCC_HR_HPV_Cytology_Negative = 39,

        /// <summary>
        /// Borderline Endocervical
        /// </summary>
        BSCC_Borderline_Endocervical = 40,

        /// <summary>
        /// Borderline Squamous
        /// </summary>
        BSCC_Borderline_Squamous = 41,

        /// <summary>
        /// Low Grade
        /// </summary>
        BSCC_New_Low_Grade = 42,



        /// <summary>
        /// Default when not set
        /// </summary>
        Not_Set = 255,
    }
}
// Copyright (c) 2021 Zilico Limited
