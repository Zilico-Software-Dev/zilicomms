﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

namespace ZiliComms.Handset
{
    public enum AnalysisResult : int
    {
        None = 0,
        Below = 1,
        Above = 2,
        Peak = 3
    }
}
// Copyright (c) 2022 Zilico Limited
