﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;

namespace ZiliComms.Handset
{
    /// <summary>
    /// Handset device information container base properties for ZedScan Mk1
    /// </summary>
    public class InformationBase : Iinformation
    {
        #region fields

        private string _SoftwareVersion;
        private string _HardwareVersion;
        private string _SerialNumber;

        private byte[] _ErrorLog;
        //private List<byte> _ErrorLog;

        #endregion

        #region properties

        /// <summary>
        /// Handset software version
        /// </summary>
        [PropId(1)]
        public string SoftwareVersion
        {
            get { return _SoftwareVersion; }
            internal set { _SoftwareVersion = value; }
        }

        /// <summary>
        /// Handset hardware version
        /// </summary>
        [PropId(2)]
        public string HardwareVersion
        {
            get { return _HardwareVersion; }
            internal set { _HardwareVersion = value; }
        }

        /// <summary>
        /// <para>Handset serial number. Format ZS-YYYY-XXXX,</para> 
        /// <para>where YYYY is the year of manufacture and XXXX will be a sequential number of the device</para>
        /// </summary>
        [PropId(3)]
        public string SerialNumber
        {
            get { return _SerialNumber; }
            internal set { _SerialNumber = value; }
        }

        /// <summary>
        /// Handset error log
        /// </summary>
        [PropId(4)]
        public byte[] ErrorLog
        {
            get { return _ErrorLog; }
            internal set { _ErrorLog = value; }
        }
        //public List<byte> ErrorLog
        //{
        //    get { return _ErrorLog; }
        //    internal set { _ErrorLog = value; }
        //}

        #endregion

        /// <summary>
        /// Instantiate an empty handset device information container
        /// </summary>
        public InformationBase() : this(string.Empty, string.Empty, string.Empty, null) { }

        /// <summary>
        /// Instantiate a handset device information container
        /// </summary>
        /// <param name="softwareVersion"></param>
        /// <param name="hardwareVersion"></param>
        /// <param name="serialNumber"></param>
        /// <param name="errorLog"></param>
        public InformationBase(string softwareVersion, string hardwareVersion, string serialNumber, byte[] errorLog)
        //public Information(string softwareVersion, string hardwareVersion, string serialNumber, List<byte> errorLog)
        {
            _SoftwareVersion = softwareVersion;
            _HardwareVersion = hardwareVersion;
            _SerialNumber = serialNumber;
            _ErrorLog = errorLog;
        }
    }
}
// Copyright (c) 2021 Zilico Limited
