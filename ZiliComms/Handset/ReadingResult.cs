﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;

namespace ZiliComms.Handset
{
    public class ReadingResult
    {
        #region fields

        private Session _SessionInfo;

        private string _SerialNumber;

        private float _ScanThreshold;

        private float _SingleThreshold;

        private List<Calibration> _CalibrationData;

        private List<PointReading> _Readings;

        private UInt32 _HighGradePresent;

        private UInt32 _ZedScanOpinion;

        #endregion

        #region properties

        [PropId(1)]
        public Session SessionInfo
        {
            get { return _SessionInfo; }
            set { _SessionInfo = value; }
        }

        [PropId(2)]
        public string SerialNumber
        {
            get { return _SerialNumber; }
            set { _SerialNumber = value; }
        }

        [PropId(3)]
        public float ScanThreshold
        {
            get { return _ScanThreshold; }
            set { _ScanThreshold = value; }
        }

        [PropId(4)]
        public float SingleThreshold
        {
            get { return _SingleThreshold; }
            set { _SingleThreshold = value; }
        }

        [PropId(5)]
        public List<Calibration> CalibrationData
        {
            get { return _CalibrationData; }
            set { _CalibrationData = value; }
        }

        [PropId(6)]
        public List<PointReading> Readings
        {
            get { return _Readings; }
            set { _Readings = value; }
        }

        [PropId(7)]
        public UInt32 HighGradePresent
        {
            get { return _HighGradePresent; }
            set { _HighGradePresent = value; }
        }

        [PropId(8)]
        public UInt32 ZedScanOpinion
        {
            get { return _ZedScanOpinion; }
            set { _ZedScanOpinion = value; }
        }

        #endregion

        public ReadingResult()
        {
            _SessionInfo = null;

            _SerialNumber = string.Empty;

            _ScanThreshold = 0.0f;
            _SingleThreshold = 0.0f;

            _CalibrationData = new List<Calibration>();

            _Readings = new List<PointReading>();

            _HighGradePresent = 0;

            _ZedScanOpinion = 0;
        }
    }
}
// Copyright (c) 2022 Zilico Limited
