﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 20221011

using System;
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;

namespace ZiliComms.Handset
{
    public class LogEventRecord
    {
        public UInt16 TimeIndex { get; private set; }

        public LogEvent Type { get; private set; }

        public byte ParameterFirst { get; private set; }
        public UInt16 ParameterSecond { get; private set; }



        public LogEventRecord(byte[] data, int pointer)
        {
            TimeIndex = BitConverter.ToUInt16(data, pointer);
            pointer += 2;

            Type = (LogEvent)data[pointer];
            pointer++;

            ParameterFirst = data[pointer];
            pointer++;

            ParameterSecond = BitConverter.ToUInt16(data, pointer);
        }



        public override string ToString()
        {
            return $"[{TimeIndex:00000}] {Type} ({ParameterFirst:000}) ({ParameterSecond:00000})";
        }
    }
}
// Copyright (c) 2022 Zilico Limited
