﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

namespace ZiliComms.Handset
{
    /// <summary>
    /// Frequency Component
    /// </summary>
    public class FreqComponent
    {
        #region fields

        private float _Frequency;

        private float _Real;
        private float _SdReal;

        private float _Imag;
        private float _SdImag;

        #endregion

        #region properties

        [PropId(1)]
        public float Frequency
        {
            get { return _Frequency; }
            set { _Frequency = value; }
        }

        [PropId(2)]
        public float Real
        {
            get { return _Real; }
            set { _Real = value; }
        }

        [PropId(3)]
        public float SdReal
        {
            get { return _SdReal; }
            set { _SdReal = value; }
        }

        [PropId(4)]
        public float Imag
        {
            get { return _Imag; }
            set { _Imag = value; }
        }

        [PropId(5)]
        public float SdImag
        {
            get { return _SdImag; }
            set { _SdImag = value; }
        }

        #endregion

        public FreqComponent()
        {
            _Frequency = 0.0f;

            _Real = 0.0f;
            _SdReal = 0.0f;

            _Imag = 0.0f;
            _SdImag = 0.0f;
        }



        public override string ToString()
        {
            return $"{_Frequency}Hz [{_Real} : i {_Imag}]";
        }
    }
}
// Copyright (c) 2022 Zilico Limited
