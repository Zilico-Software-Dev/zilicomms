﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 20220928

namespace ZiliComms.Handset
{
    public enum ResultsStatus
    {
        None = 0,
        Available = 1,
    }
}
// Copyright (c) 2022 Zilico Limited
