﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 20221021

using System;

namespace ZiliComms.Handset
{
    public class BlockStartFirmware
    {

        private UInt32 _Size;


        /// <summary>
        /// Total number of data bytes
        /// </summary>
        [PropId(1)]
        public UInt32 Size
        { get { return _Size; } }


        public BlockStartFirmware(UInt32 size)
        {
            _Size = size;
        }



    }
}
// Copyright (c) 2022 Zilico Limited
