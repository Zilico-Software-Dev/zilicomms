﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using System.Linq;
//using System.Text;
using System.Threading.Tasks;

namespace ZiliComms.Handset
{
    public enum PointType : int
    {
        Regular = 0,
        Singular = 1,
    }
}
// Copyright (c) 2022 Zilico Limited
