﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 20221020

using System;

namespace ZiliComms.Handset
{
    public class BlockData
    {
        #region fields

        private UInt32 _Offset;

        private UInt32 _Size;

        private byte[] _Data;

        #endregion

        #region properties

        /// <summary>
        /// The start position for this packet
        /// </summary>
        [PropId(1)]
        public UInt32 Offset
        { get { return _Offset; } }

        /// <summary>
        /// Size of the data in this packet
        /// </summary>
        [PropId(2)]
        public UInt32 Size
        { get { return _Size; } }

        /// <summary>
        /// The packet payload
        /// </summary>
        [PropId(3)]
        public byte[] Data
        { get { return _Data; } }

        #endregion


        public BlockData(UInt32 offset, byte[] data) : this(offset, (UInt32)data.Length, data) { }


        public BlockData(UInt32 offset, UInt32 size, byte[] data)
        {
            _Offset = offset;

            _Size = size;

            _Data = data;
        }



    }
}
// Copyright (c) 2022 Zilico Limited
