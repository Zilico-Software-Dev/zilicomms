﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 20221020

using System;

namespace ZiliComms.Handset
{
    public class BlockStartLanguage
    {
        #region fields

        private UInt32 _Slot;

        private UInt32 _Size;

        #endregion

        #region properties

        /// <summary>
        /// Specifies the slot for this language
        /// </summary>
        [PropId(1)]
        public UInt32 Slot
        { get { return _Slot; } }

        /// <summary>
        /// Total number of data bytes in the pack
        /// </summary>
        [PropId(2)]
        public UInt32 Size
        { get { return _Size; } }

        #endregion


        /// <summary>
        /// Language start block with Slot set to 1
        /// </summary>
        /// <param name="size"></param>
        public BlockStartLanguage(UInt32 size) : this(1, size) { }

        /// <summary>
        /// Language start block for given slot with a total data size specified
        /// </summary>
        /// <param name="slot"></param>
        /// <param name="size"></param>
        public BlockStartLanguage(UInt32 slot, UInt32 size)
        {
            _Slot = slot;

            _Size = size;
        }

    }
}
// Copyright (c) 2022 Zilico Limited
