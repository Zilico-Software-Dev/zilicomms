﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;

namespace ZiliComms.Handset
{
    /// <summary>
    /// Derived Handset device information container properties for ZedScan Mk2
    /// </summary>
    public class Information : InformationBase
    {
        #region fields

        private ChargingState _ChargeState;
        private UInt16 _BatteryCharge;
        private ResultsStatus _ReadingStatus;
        private string _Language;                                   // current language in use

        #endregion

        #region properties

        [PropId(4)]
        public ChargingState ChargeState
        {
            get { return _ChargeState; }
            internal set { _ChargeState = value; }
        }

        [PropId(5)]
        public UInt16 BatteryCharge
        {
            get { return _BatteryCharge; }
            internal set { _BatteryCharge = value; }
        }

        [PropId(6)]
        public ResultsStatus ReadingStatus
        {
            get { return _ReadingStatus; }
            internal set { _ReadingStatus = value; }
        }

        [PropId(7)]
        public string LanguageCode
        {
            get { return _Language; }
            internal set { _Language = value; }
        }

        /// <summary>
        /// Handset error log
        /// </summary>
        [PropId(8)]
        public new byte[] ErrorLog
        {
            get { return base.ErrorLog; }
            internal set { base.ErrorLog = value; }
        }

        /// <summary>
        /// Decoded handset error log
        /// </summary>
        public LogRecord LogRecord
        { get { return new LogRecord(base.ErrorLog); } }

        #endregion

        /// <summary>
        /// Instantiate an empty handset device information container
        /// </summary>
        public Information() : this(string.Empty, string.Empty, string.Empty, null) { }

        /// <summary>
        /// Instantiate a handset device information container with base values
        /// </summary>
        /// <param name="softwareVersion"></param>
        /// <param name="hardwareVersion"></param>
        /// <param name="serialNumber"></param>
        /// <param name="errorLog"></param>
        public Information(string softwareVersion, string hardwareVersion, string serialNumber, byte[] errorLog)
            : this(softwareVersion, hardwareVersion, serialNumber, string.Empty, errorLog) { }

        /// <summary>
        /// Instantiate a handset device information container
        /// </summary>
        /// <param name="softwareVersion"></param>
        /// <param name="hardwareVersion"></param>
        /// <param name="serialNumber"></param>
        /// <param name="languageCode"></param>
        /// <param name="errorLog"></param>
        public Information(string softwareVersion, string hardwareVersion, string serialNumber, string languageCode, byte[] errorLog)
        {
            base.SoftwareVersion = softwareVersion;
            base.HardwareVersion = hardwareVersion;
            base.SerialNumber = serialNumber;
            base.ErrorLog = errorLog;

            _Language = languageCode;
        }
    }
}
// Copyright (c) 2021 Zilico Limited
