﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System.Collections.Generic;

namespace ZiliComms.Handset
{
    public class PointReading
    {
        #region fields

        private PointType _Type;

        private uint _PointNumber;

        private float _MeasuredT;
        private float _MeasuredTr;
        private float _AnalysisValue;

        private AnalysisResult _AnalysisResult;

        private List<FreqComponent> _Frequencies;

        #endregion

        #region properties

        [PropId(1)]
        public PointType Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        [PropId(2)]
        public uint PointNumber
        {
            get { return _PointNumber; }
            set { _PointNumber = value; }
        }

        [PropId(3)]
        public float MeasuredT
        {
            get { return _MeasuredT; }
            set { _MeasuredT = value; }
        }

        [PropId(4)]
        public float MeasuredTr
        {
            get { return _MeasuredTr; }
            set { _MeasuredTr = value; }
        }

        [PropId(5)]
        public float AnalysisValue
        {
            get { return _AnalysisValue; }
            set { _AnalysisValue = value; }
        }

        [PropId(6)]
        public AnalysisResult AnalysisResult
        {
            get { return _AnalysisResult; }
            set { _AnalysisResult = value; }
        }

        [PropId(7)]
        public List<FreqComponent> Frequencies
        {
            get{return _Frequencies;}
            set{_Frequencies = value;}
        }

        #endregion



        public PointReading()
        {
            _Type = PointType.Regular;

            _PointNumber = 0;

            _MeasuredT = 0.0f;
            _MeasuredTr = 0.0f;
            _AnalysisValue = 0.0f;

            _AnalysisResult = AnalysisResult.None;

            _Frequencies = new List<FreqComponent>();
        }



        public override string ToString()
        {
            return $"{_Type} {_PointNumber} {_AnalysisResult} [{_AnalysisValue}]";
        }
    }
}
// Copyright (c) 2022 Zilico Limited
