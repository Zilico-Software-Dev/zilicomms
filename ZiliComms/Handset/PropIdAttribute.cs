﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;

namespace ZiliComms.Handset
{
    /// <summary>
    ///  Property Identifier for Protobuff
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class PropIdAttribute : Attribute
    {
        private int _Index;

        /// <summary>
        /// Field Number
        /// </summary>
        public int Index
        { get { return _Index; } }

        /// <summary>
        /// Property Identifier Index (Protobuff Field Number)
        /// </summary>
        /// <param name="index"></param>
        public PropIdAttribute(int index)
        {
            _Index = index;
        }
    }
}
// Copyright (c) 2021 Zilico Limited
