﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

namespace ZiliComms.Handset
{
    public class Calibration
    {
        #region fields

        private float _Frequency;
        private float _Real;
        private float _Imag;

        #endregion

        #region properties

        /// <summary>
        /// Frequency
        /// </summary>
        [PropId(1)]
        public float Frequency
        {
            get { return _Frequency; }
            set { _Frequency = value; }
        }

        /// <summary>
        /// Z Real
        /// </summary>
        [PropId(2)]
        public float Real
        {
            get { return _Real; }
            set { _Real = value; }
        }

        /// <summary>
        /// Z Imaginary
        /// </summary>
        [PropId(3)]
        public float Imag
        {
            get { return _Imag; }
            set { _Imag = value; }
        }

        #endregion

        public Calibration()
        {
            _Frequency = 0.0f;
            _Real = 0.0f;
            _Imag = 0.0f;
        }


        public override string ToString()
        {
            return $"{_Frequency}Hz [{_Real} : i {_Imag}]";
        }
    }
}
// Copyright (c) 2022 Zilico Limited
