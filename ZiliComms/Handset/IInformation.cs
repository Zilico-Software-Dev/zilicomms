﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 20220808

namespace ZiliComms.Handset
{
    public interface Iinformation
    {
        string SoftwareVersion { get; }
        string HardwareVersion { get; }
        string SerialNumber { get; }
        byte[] ErrorLog { get; }
    }
}
// Copyright (c) 2022 Zilico Limited
