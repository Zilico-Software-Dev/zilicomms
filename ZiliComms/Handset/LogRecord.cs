﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 20221010

using System;
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;

namespace ZiliComms.Handset
{
    public class LogRecord
    {
        #region fields

        private string _Message;

        // Log header (0x422304).

        private byte _Version;                                      // 0x02

        private byte _FirmwareVersionLength;                        // In bytes
        private string _FirmwareVersion;                            // 5 bytes currently

        private string _DeviceID;                                   // 8 bytes

        private byte _UniqueIdLength;                               // In DWORDs
        private string _UniqueId;

        private byte _AlertCode;                                    // (if any) ? Not sure what this means.
        private byte _TrainingModeStatus;

        // Section Break Indicator (0x1111).

        private UInt16 _ClockSeconds;
        private UInt16 _ClockMinutes;
        private UInt16 _ClockHours;
        private UInt16 _ClockDays;
        private UInt16 _ClockMonths;
        private UInt16 _ClockYears;

        private byte _CriticalErrorFlag;

        private byte _CriticalErrorLength;
        private string _CriticalErrorFileName;                      // If length of critical error source file name > 0

        private UInt16 _LineNumber;                                 // File line number at which critical error occurred.
        private byte _ErrorCode;                                    // (if any) ? Not sure what this means.

        private UInt16 _BatteryPercentage;

        private DockState _DockState;

        // Section Break Indicator (0x2222).
        // Section Break Indicator (0x3333).

        private UInt16 _PointsScanned;
        private UInt16 _PointsRead;

        private byte _QualityChecks;                                // Were reading QC checks in progress when error record created? (1 = yes)
        private byte _Measuring;                                    // Was a reading measurement in progress when error record created? (1 = yes)

        // Section Break Indicator (0x4444).

        private byte _EventQueueLength;                             // Number of event log items (less than or equal to 88)
        private List<LogEventRecord> _EventRecords;                 // Event log items (6 bytes per entry)

        // Section Break Indicator (0x5555).

        private byte[] _FailCounts;                                 // QC fail counts for point measurements (12 points plus 4 single points) 16 * 16 = 256

        private string _Measurements;

        // Section Break Indicator (0x6666).

        #endregion

        #region properties

        public byte Version
        { get { return _Version; } }

        public string FirmwareVersion
        { get { return _FirmwareVersion; } }

        public string DeviceID
        { get { return _DeviceID; } }

        public string UniqueId
        { get { return _UniqueId; } }

        public byte AlertCode
        { get { return _AlertCode; } }

        public byte TrainingModeStatus
        { get { return _TrainingModeStatus; } }

        public UInt16 ClockSeconds
        { get { return _ClockSeconds; } }

        public UInt16 ClockMinutes
        { get { return _ClockMinutes; } }

        public UInt16 ClockHours
        { get { return _ClockHours; } }

        public UInt16 ClockDays
        { get { return _ClockDays; } }

        public UInt16 ClockMonths
        { get { return _ClockMonths; } }

        public UInt16 ClockYears
        { get { return _ClockYears; } }

        public byte CriticalErrorFlag
        { get { return _CriticalErrorFlag; } }

        public string CriticalErrorFileName
        { get { return _CriticalErrorFileName; } }

        public UInt16 LineNumber
        { get { return _LineNumber; } }

        /// <summary>
        /// 
        /// </summary>
        public byte ErrorCode
        { get { return _ErrorCode; } }

        public UInt16 BatteryPercentage
        { get { return _BatteryPercentage; } }

        public DockState DockState
        { get { return _DockState; } }

        public UInt16 PointsScanned
        { get { return _PointsScanned; } }

        public UInt16 PointsRead
        { get { return _PointsRead; } }

        public byte QualityChecks
        { get { return _QualityChecks; } }

        public byte Measuring
        { get { return _Measuring; } }

        public List<LogEventRecord> EventRecords
        { get { return _EventRecords; } }

        public byte[] FailCounts
        { get { return _FailCounts; } }

        public string Measurements
        { get { return _Measurements; } }


        #endregion



        public LogRecord()
        {
            _Version = 0;

            _FirmwareVersionLength = 0;
            _FirmwareVersion = string.Empty;

            _DeviceID = string.Empty;

            _UniqueIdLength = 0;
            _UniqueId = string.Empty;

            _AlertCode = 0;
            _TrainingModeStatus = 0;

            _ClockSeconds = 0;
            _ClockMinutes = 0;
            _ClockHours = 0;
            _ClockDays = 0;
            _ClockMonths = 0;
            _ClockYears = 0;

            _CriticalErrorFlag = 0;

            _CriticalErrorLength = 0;
            _CriticalErrorFileName = string.Empty;

            _LineNumber = 0;
            _ErrorCode = 0;

            _BatteryPercentage = 0;

            _DockState = 0;

            _PointsScanned = 0;
            _PointsRead = 0;

            _QualityChecks = 0;
            _Measuring = 0;

            _EventQueueLength = 0;
            _EventRecords = null;

            _FailCounts = null;
        }

        public LogRecord(byte[] data)
        {
            _Message = string.Empty;

            try
            {
                if (data != null)
                {
                    if (data.Length < 5)                            // Start of Firmware version
                        throw new ApplicationException("Not enough data");

                    if (!(data[0].Equals(0x42) && data[1].Equals(0x23) && data[2].Equals(0x04)))
                        throw new ApplicationException("Not header signature");

                    int pointer = 3;

                    _Version = data[pointer];
                    pointer++;

                    _FirmwareVersionLength = data[pointer];
                    pointer++;

                    _FirmwareVersion = System.Text.Encoding.UTF8.GetString(data, pointer, _FirmwareVersionLength);
                    pointer += _FirmwareVersionLength;

                    System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
                    for (int i = 0; i < 8; i++)
                    {
                        stringBuilder.Append($"{data[pointer + i]:x2} ");
                    }
                    _DeviceID = stringBuilder.ToString().Trim();
                    pointer += 8;

                    _UniqueIdLength = data[pointer];
                    pointer ++;

                    stringBuilder.Clear();                          // DWORDS
                    for (int i = 0; i < _UniqueIdLength; i++)
                    {
                        stringBuilder.Append(System.Text.Encoding.UTF32.GetString(data, pointer, 4));
                        pointer += 4;
                    }
                    _UniqueId = stringBuilder.ToString();

                    _AlertCode = data[pointer];
                    pointer++;

                    _TrainingModeStatus = data[pointer];
                    pointer++;

                    #region Section check

                    if (!data[pointer].Equals(0x11))
                        throw new ApplicationException("Bad section check 0x11");
                    pointer++;
                    if (!data[pointer].Equals(0x11))
                        throw new ApplicationException("Bad section check 0x1111");
                    pointer++;

                    #endregion

                    _ClockSeconds = BitConverter.ToUInt16(data, pointer);
                    pointer += 2;

                    _ClockMinutes = BitConverter.ToUInt16(data, pointer);
                    pointer += 2;

                    _ClockHours = BitConverter.ToUInt16(data, pointer);
                    pointer += 2;

                    _ClockDays = BitConverter.ToUInt16(data, pointer);
                    pointer += 2;

                    _ClockMonths = BitConverter.ToUInt16(data, pointer);
                    pointer += 2;

                    _ClockYears = BitConverter.ToUInt16(data, pointer);
                    pointer += 2;

                    _CriticalErrorFlag = data[pointer];
                    pointer++;

                    _CriticalErrorLength = data[pointer];
                    pointer++;

                    _CriticalErrorFileName = System.Text.Encoding.UTF8.GetString(data, pointer, _CriticalErrorLength);
                    pointer += _CriticalErrorLength;

                    _LineNumber = BitConverter.ToUInt16(data, pointer);
                    pointer += 2;

                    _ErrorCode = data[pointer];
                    pointer++;

                    _BatteryPercentage = BitConverter.ToUInt16(data, pointer);
                    pointer += 2;

                    _DockState = (DockState)data[pointer];
                    pointer++;

                    #region Section check

                    if (!data[pointer].Equals(0x22))
                        throw new ApplicationException("Bad section check 0x22");
                    pointer++;
                    if (!data[pointer].Equals(0x22))
                        throw new ApplicationException("Bad section check 0x2222");
                    pointer++;

                    #endregion

                    #region Section check

                    if (!data[pointer].Equals(0x33))
                        throw new ApplicationException("Bad section check 0x33");
                    pointer++;
                    if (!data[pointer].Equals(0x33))
                        throw new ApplicationException("Bad section check 0x3333");
                    pointer++;

                    #endregion

                    _PointsScanned = BitConverter.ToUInt16(data, pointer);
                    pointer += 2;

                    _PointsRead = BitConverter.ToUInt16(data, pointer);
                    pointer += 2;

                    _QualityChecks = data[pointer];
                    pointer++;

                    _Measuring = data[pointer];
                    pointer++;

                    #region Section check

                    if (!data[pointer].Equals(0x44))
                        throw new ApplicationException("Bad section check 0x44");
                    pointer++;
                    if (!data[pointer].Equals(0x44))
                        throw new ApplicationException("Bad section check 0x4444");
                    pointer++;

                    #endregion

                    _EventQueueLength = data[pointer];
                    pointer++;

                    if (_EventQueueLength > 0)
                    {
                        _EventRecords = new List<LogEventRecord>();

                        for (int i = 0; i < _EventQueueLength; i++)
                        {
                            _EventRecords.Add(new LogEventRecord(data, pointer));

                            pointer += 6;                           // Size of LogEventRecord = 6 bytes
                        }
                    }

                    #region Section check

                    if (!data[pointer].Equals(0x55))
                        throw new ApplicationException("Bad section check 0x55");
                    pointer++;
                    if (!data[pointer].Equals(0x55))
                        throw new ApplicationException("Bad section check 0x55555");
                    pointer++;

                    #endregion

                    // QC fail counts for point measurements (12 points plus 4 single points) 16 * 16 = 256
                    _FailCounts = new byte[256];
                    
                    // ToDo: 8 WORDs

                    int index = 0;

                    stringBuilder.Clear();
                    for (int o = 0; o < 16; o++)
                    {
                        System.Text.StringBuilder line = new System.Text.StringBuilder();
                        for (int i = 0; i < 16; i++)
                        {
                            _FailCounts[index] = data[pointer + i];

                            line.Append($"{_FailCounts[index]:x2} ");

                            if (i == 7)
                                line.Append(" ");

                            index++;
                        }
                        stringBuilder.AppendLine(line.ToString().Trim());
                        pointer += 16;
                    }
                    _Measurements = stringBuilder.ToString();

                    #region Section check

                    if (!data[pointer].Equals(0x66))
                        throw new ApplicationException("Bad section check 0x66");
                    pointer++;
                    if (!data[pointer].Equals(0x66))
                        throw new ApplicationException("Bad section check 0x6666");
                    pointer++;

                    #endregion
                }
                else
                    throw new ApplicationException("No data available");
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }
        }
    }
}
// Copyright (c) 2022 Zilico Limited
