﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;
//using System.Runtime.Serialization;

namespace ZiliComms.Handset
{
    /// <summary>
    /// Session data object exchanged with the handset
    /// </summary>
    //[DataContract]
    public class Session : IEquatable<Session>
    {
        #region fields

        private string _PatientId;
        private string _OperatorId;
        private ReferralType _ReferralType;                         // TwosComplement
        private DateTime _StartTime;                                // TwosComplement
        private bool _TrainingMode;                                 // TwosComplement : TrainingSelect

        private int _ZedScanVersion;

        #endregion

        #region properties

        /// <summary>
        /// Patient Id
        /// </summary>
        [PropId(1)]
        public string PatientId
        {
            get { return _PatientId; }
            internal set { _PatientId = value; }
        }

        /// <summary>
        /// Operator Id
        /// </summary>
        [PropId(2)]
        public string OperatorId
        {
            get { return _OperatorId; }
            internal set { _OperatorId = value; }
        }

        /// <summary>
        /// Referral type
        /// </summary>
        [PropId(3)]
        public ReferralType ReferralType
        {
            get { return _ReferralType; }
            internal set { _ReferralType = value; }
        }

        /// <summary>
        /// Time session started
        /// </summary>
        [PropId(4)]
        public DateTime StartTime
        {
            get { return _StartTime; }
            internal set { _StartTime = value; }
        }

        /// <summary>
        /// Training mode
        /// </summary>
        [PropId(5)]
        public bool TrainingMode
        {
            get { return _TrainingMode; }
            internal set { _TrainingMode = value; }
        }


        /// <summary>
        /// ZedScan Mk 1 or ZedScan Mk 2
        /// </summary>
        internal int ZedScanVersion
        {
            get { return _ZedScanVersion; }
            set { _ZedScanVersion = value; }
        }

        #endregion


        /// <summary>
        /// Empty Session communication object, starting Now
        /// </summary>
        public Session() : this(string.Empty, string.Empty, ReferralType.Not_Set, DateTime.Now, true) { }

        /// <summary>
        /// Populated Session communication object, starting Now
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="operatorId"></param>
        /// <param name="referralType"></param>
        /// <param name="trainingMode"></param>
        public Session(string patientId, string operatorId, ReferralType referralType, bool trainingMode)
            : this(patientId, operatorId, referralType, DateTime.Now, trainingMode) { }

        /// <summary>
        /// Populated Session communication object
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="operatorId"></param>
        /// <param name="referralType"></param>
        /// <param name="currentTime"></param>
        /// <param name="trainingMode"></param>
        public Session(string patientId, string operatorId, ReferralType referralType,
                       DateTime currentTime, bool trainingMode)
        {
            _PatientId = patientId;
            _OperatorId = operatorId;
            _ReferralType = referralType;
            _StartTime = currentTime;
            _TrainingMode = trainingMode;
        }




        public override string ToString()
        {
            string training = string.Empty;
            if (_TrainingMode)
                training = "Training Mode";


            return $"{PatientId} {_ReferralType} {training}";
        }




        public override bool Equals(object obj)                     // auto generated
        {
            return Equals(obj as Session);
        }

        public bool Equals(Session other)                           // rewritten
        {
            bool result = false;

            // StartTime will have changed when read back from handset - Mk1 only

            if (other != null &&
                other.PatientId.Equals(_PatientId) &&
                other.OperatorId.Equals(_OperatorId) &&
                other.ReferralType.Equals(_ReferralType) &&
                CheckDateTime(other.StartTime)/* &&
                other.TrainingMode.Equals(_TrainingMode)*/)         // Training mode only returned as true when results are read
            {
                result = true;
            }

            if (object.ReferenceEquals(this, other))
                result = true;

            return result;
        }

        private bool CheckDateTime(DateTime other)
        {
            bool result = false;

            if (_ZedScanVersion > 1)
            {
                if (_StartTime.Year.Equals(other.Year) && _StartTime.Month.Equals(other.Month) && _StartTime.Day.Equals(other.Day) &&
                    _StartTime.Hour.Equals(other.Hour) && _StartTime.Minute.Equals(other.Minute) && _StartTime.Second.Equals(other.Second))
                    result = true;
            }

            return result;
        }

        public override int GetHashCode()                           // auto generated
        {
            int hashCode = 976605956;

            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_PatientId);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_OperatorId);
            hashCode = hashCode * -1521134295 + _ReferralType.GetHashCode();

            return hashCode;
        }



        // == and != cause stack overflows through recursive calls

        //public static bool operator ==(Session item1, Session item2)
        //{
        //    bool result = false;

        //    if (object.ReferenceEquals(item1, item2))
        //        result = true;

        //    if ((item1 != null) && (item2 != null))
        //    {
        //        if ((item1.PatientId == item2.PatientId) &&
        //            (item1.OperatorId == item2.OperatorId) &&
        //            (item1.ReferralType == item2.ReferralType))
        //            result = true;
        //    }

        //    return result;
        //}

        //public static bool operator !=(Session item1, Session item2)
        //{
        //    return !(item1 == item2);
        //}
    }
}
// Copyright (c) 2021 Zilico Limited
