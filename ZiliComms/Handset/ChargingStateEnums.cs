﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 20220805

namespace ZiliComms.Handset
{
    public enum ChargingState
    {
        Off = 0,
        Charging = 1,
        Charged = 2,
        Suspended = 3
    }
}
// Copyright (c) 2022 Zilico Limited
