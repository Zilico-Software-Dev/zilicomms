﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 20221011

namespace ZiliComms.Handset
{
    public enum LogEvent
    {
        POWER_ON = 1,
        NEW_SESSION = 2,
        DOCK = 3,
        UNDOCK = 4,
        TAKE_READING = 5,
        BAD_BOUNDARY = 6,
        TAKE_READING_SP = 7,
        TAKE_READING_FAILED = 8,
        SEE_AND_TREAT = 9,
        POWER_DOWN = 10,
        SYSTEM_STATE_CHANGE = 11,
        SYSTEM_STATE_MOVE = 12,
        SYSTEM_TIMER_WRAP = 13,
        KEY_PRESS = 14,
        ALERT_STATE_CHANGE = 15,
        ALERT_STATE_MOVE = 16,
        BAD_CAL = 17,
        CRITICAL_ERROR = 255,
    }


    public enum DockState
    {
        Undocked = 100,
        Docked = 200,
    }
}
// Copyright (c) 2022 Zilico Limited
