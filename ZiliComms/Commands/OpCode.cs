﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

namespace ZiliComms.Commands
{
    /// <summary>
    /// Message Id
    /// </summary>
    public enum OpCode
    {
        /// <summary>
        /// 0
        /// </summary>
        REQUEST_DEVICE_INFO = 0x00,
        /// <summary>
        /// 1
        /// </summary>
        DEVICE_INFORMATION = 0x01,

        /// <summary>
        /// 2
        /// </summary>
        REQUEST_RESULTS = 0x02,
        /// <summary>
        /// 3
        /// </summary>
        READING_RESULT = 0x03,

        /// <summary>
        /// 4
        /// </summary>
        REQUEST_SESSION_INFORMATION = 0x04,
        /// <summary>
        /// 5
        /// </summary>
        SESSION_INFORMATION = 0x05,

        /// <summary>
        /// 6
        /// </summary>
        CODE_UPDATE_START_BLOCK = 0x06,
        /// <summary>
        /// 7
        /// </summary>
        CODE_UPDATE_BLOCK = 0x07,
        /// <summary>
        /// 8
        /// </summary>
        CODE_UPDATE_END_BLOCK = 0x08,

        /// <summary>
        /// 9
        /// </summary>
        LANGUAGE_UPDATE_START_BLOCK = 0x09,
        /// <summary>
        /// 10
        /// </summary>
        LANGUAGE_UPDATE_BLOCK = 0x0A,
        /// <summary>
        /// 11
        /// </summary>
        LANGUAGE_UPDATE_END_BLOCK = 0x0B,

        /// <summary>
        /// 12
        /// </summary>
        DELETE_SESSION_RESULTS = 0x0C,

        /// <summary>
        /// 13
        /// </summary>
        REQUEST_CONFIGURATION = 0x0D,
        /// <summary>
        /// 14
        /// </summary>
        CONFIGURATION = 0x0E,

        /// <summary>
        /// 15
        /// </summary>
        LANGUAGE_SELECTION = 0x0F,

        /// <summary>
        /// 16
        /// </summary>
        ACK = 0x10,

        /// <summary>
        /// 17
        /// </summary>
        SET_UNIQUE_ID = 0x11,

        /// <summary>
        /// 18
        /// </summary>
        NAK = 0x12,

        /*
            NUM_VALID_MESSAGES,
            MESSAGE_INCOMPLETE
        */

        // Do not use enum value higher than 0x7F (127) unless updating ZedScan.ParseResponse -> startIndex

        /// <summary>
        /// 255
        /// </summary>
        NOP = 0xFF,
    }
}
// Copyright (c) 2021 Zilico Limited
