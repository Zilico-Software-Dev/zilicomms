﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;
using System.Linq;
//using System.Text;
using System.Threading.Tasks;

namespace ZiliComms.Protobuff
{
    public class Message
    {
        private OpCode _Instruction;

        private List<PropertyItem> _Properties;


        /// <summary>
        /// Identification
        /// </summary>
        public OpCode Instruction
        { get { return _Instruction; } }

        public List<PropertyItem> Properties
        {
            get { return _Properties; }
            set { _Properties = value; }
        }



        public Message() : this(OpCode.NOP) { }

        public Message(byte[] payLoad)
        {
            _Instruction = (OpCode)payLoad[0];

            GenerateProperties();

            // decode payload
        }

        public Message(OpCode instruction)
        {
            _Instruction = instruction;

            GenerateProperties();



        }



        private void GenerateProperties()
        {
            _Properties = new List<PropertyItem>();

            switch (_Instruction)
            {
                case OpCode.REQUEST_DEVICE_INFO:                    // No properties to send
                    break;
                case OpCode.DEVICE_INFORMATION:
                    AddDeviceInfoProperties();
                    break;

                case OpCode.REQUEST_RESULTS:
                    break;
                case OpCode.READING_RESULT:
                    AddResultProperties();
                    break;

                case OpCode.REQUEST_SESSION_INFORMATION:
                    break;
                case OpCode.SESSION_INFORMATION:
                    AddSessionInfoProperties();
                    break;

                case OpCode.CODE_UPDATE_START_BLOCK:
                    break;
                case OpCode.CODE_UPDATE_BLOCK:
                    break;
                case OpCode.CODE_UPDATE_END_BLOCK:
                    break;

                case OpCode.LANGUAGE_UPDATE_START_BLOCK:
                    break;
                case OpCode.LANGUAGE_UPDATE_BLOCK:
                    break;
                case OpCode.LANGUAGE_UPDATE_END_BLOCK:
                    break;

                case OpCode.DELETE_SESSION_RESULTS:
                    break;

                case OpCode.REQUEST_CONFIGURATION:
                    break;
                case OpCode.CONFIGURATION:
                    break;

                case OpCode.LANGUAGE_SELECTION:
                    break;
                case OpCode.UPDATE_ACK:
                    break;
                case OpCode.SET_UNIQUE_ID:
                    break;
                case OpCode.NOP:
                    break;
                default:
                    break;
            }
        }

        private void AddDeviceInfoProperties()
        {
            string softwareVersion = string.Empty;
            _Properties.Add(new PropertyItem(1, softwareVersion, "Software Version"));
            string hardwareVersion = string.Empty;
            _Properties.Add(new PropertyItem(2, hardwareVersion, "Hardware Version"));
            string serialNumber = string.Empty;
            _Properties.Add(new PropertyItem(3, serialNumber, "Serial Number"));
            byte[] errorLog = null;
            _Properties.Add(new PropertyItem(4, errorLog, "Error Log"));
        }

        private void AddResultProperties()
        {
            int sessionInfo = 0;                                                   // SessionInformation
            _Properties.Add(new PropertyItem(1, sessionInfo, "Session Info"));
            string serialNumber = string.Empty;
            _Properties.Add(new PropertyItem(2, serialNumber, "Serial Number"));
            float scanThreshold = 0.0f;
            _Properties.Add(new PropertyItem(3, scanThreshold, "Scan Threshold"));
            float singleThreshold = 0.0f;
            _Properties.Add(new PropertyItem(4, singleThreshold, "Single Threshold"));

            //List<Cal> calData;
            //_Properties.Add(new PropertyItem(5, calData, "Calibration Data"));

            //List<PointReading> readings;
            //_Properties.Add(new PropertyItem(6, readings, "Readings"));

            uint highGradePresent = 0;
            _Properties.Add(new PropertyItem(7, highGradePresent, "High Grade Present"));
            long RF_ID = 0;
            _Properties.Add(new PropertyItem(8, RF_ID, "RF ID"));
        }

        private void AddSessionInfoProperties()
        {
            string patientId = string.Empty;
            _Properties.Add(new PropertyItem(1, patientId, "Patient Id"));
            string operatorID = string.Empty;
            _Properties.Add(new PropertyItem(2, operatorID, "Operator Id"));
            int referralType = 0;                                                   // ReferralType
            _Properties.Add(new PropertyItem(3, referralType, "Referral Type"));
            uint currentTime = 0;
            _Properties.Add(new PropertyItem(4, currentTime, "Current Time"));
            bool trainingMode = false;                                              //TrainingSelect
            _Properties.Add(new PropertyItem(5, trainingMode, "Training Mode"));
        }

    }
}
// Copyright (c) 2021 Zilico Limited
