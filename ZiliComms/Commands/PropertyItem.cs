﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;

namespace ZiliComms.Protobuff
{
    public class PropertyItem
    {
        #region fields

        private int _Index;

        private object _Value;

        private string _Name;

        #endregion

        #region properties

        /// <summary>
        /// Field Number
        /// </summary>
        public int Index
        { get { return _Index; } }

        public object Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        #endregion



        public PropertyItem(int index) : this(index, null, string.Empty) { }

        public PropertyItem(int index, object value) : this(index, value, string.Empty) { }

        public PropertyItem(int index, object value, string name)
        {
            _Index = index;

            _Value = value;

            _Name = name;
        }



        public override string ToString()
        {
            return $"{_Name} [{_Value}]";
        }
    }
}
// Copyright (c) 2021 Zilico Limited
