﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
//using System.Text;
using System.Threading.Tasks;
using ZiliComms.Commands;

namespace ZiliComms
{
    public class ZedScan
    {
        #region fields

        private string _Message;

        private bool _TraceEnabled;

        private IUSBPort _Port;

        private int _ZedScanVersion;


        private Handset.Iinformation _Information;

        private Handset.Session _Session;

        private Handset.ReadingResult _Results;

        #endregion

        #region properties

        /// <summary>
        /// Return message
        /// </summary>
        public string Message
        { get { return _Message; } }

        /// <summary>
        /// Debug trace output
        /// </summary>
        public bool TraceEnabled
        {
            get { return _TraceEnabled; }
            set { _TraceEnabled = value; }
        }

        /// <summary>
        /// Communication port
        /// </summary>
        public IUSBPort Port
        {
            get { return _Port; }
            set { _Port = value; }
        }

        /// <summary>
        /// ZedScan Mk 1 or ZedScan Mk 2
        /// </summary>
        public int ZedScanVersion
        {
            get { return _ZedScanVersion; }
            set { _ZedScanVersion = value; }
        }



        /// <summary>
        /// Handset Information object
        /// </summary>
        public Handset.Iinformation DeviceInformation
        { get { return _Information; } }

        /// <summary>
        /// Handset Session object
        /// </summary>
        public Handset.Session Session
        { get { return _Session; } }

        /// <summary>
        /// Handset Reading Result object
        /// </summary>
        public Handset.ReadingResult Results
        { get { return _Results; } }

        #endregion

        #region Data Transfer Event

        public delegate void DataTransferEventHandler(DataTransferEventArgs eventArgs);

        public event DataTransferEventHandler DataTransferEvent;

        protected virtual void OnDataTransferEvent(DataTransferEventArgs e)
        {
            if (DataTransferEvent != null) DataTransferEvent(e);
        }

        private void RaiseDataTransferEvent(int percentComplete)
        {
            DataTransferEventArgs eventArgs = new DataTransferEventArgs(percentComplete);

            Task.Run(() => OnDataTransferEvent(eventArgs));
        }

        #endregion

        #region ~ctor

        public ZedScan() : this(null) { }

        public ZedScan(IUSBPort port)
        {
            _Message = string.Empty;

            _TraceEnabled = false;

            _Information = null;

            _Session = null;
            _Results = null;

            _Port = port;

            _ZedScanVersion = 1;
        }

        #endregion

        #region Test Methods

        /// <summary>
        /// test method - does not transmit
        /// </summary>
        /// <param name="payLoad"></param>
        /// <returns></returns>
        public bool TestStartSession(byte[] payLoad)
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                PacketWrapper.PayLoad = payLoad;

                OpCode MessageId = (OpCode)PacketWrapper.PayLoad[0];

                if (!MessageId.Equals(OpCode.SESSION_INFORMATION))
                    throw new ApplicationException($"Incorrect response, message {MessageId}");

                _Session = new Handset.Session();

                result = ParseResponse(PacketWrapper.PayLoad, _Session);
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        /// <summary>
        /// test method - does not transmit
        /// </summary>
        /// <param name="payLoad"></param>
        /// <returns></returns>
        public bool TestGetResults(byte[] payLoad)
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                PacketWrapper.PayLoad = payLoad;

                OpCode MessageId = (OpCode)PacketWrapper.PayLoad[0];

                if (!MessageId.Equals(OpCode.READING_RESULT))
                    throw new ApplicationException($"Incorrect response, message {MessageId}");

                _Results = new Handset.ReadingResult();

                result = ParseResponse(PacketWrapper.PayLoad, _Results);
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        /// <summary>
        /// test method - does not transmit
        /// </summary>
        /// <param name="payLoad"></param>
        /// <returns></returns>
        public bool TestSetResult(Handset.ReadingResult results)
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                List<byte> sendData = new List<byte>();

                sendData.Add((byte)OpCode.READING_RESULT);

                PropertyInfo[] propInfos = results.GetType().GetProperties(BindingFlags.Public |
                                                                           BindingFlags.Instance);

                foreach (PropertyInfo prop in propInfos)
                {
                    sendData.AddRange(EncodeProperty(results, prop));
                }

                //if (!Send(sendData))
                //{
                //    if (_ZedScanVersion > 1)                    // No ACK sent with ZedScan 1
                //        throw new ApplicationException(_Message);
                //}

                result = true;

                if ((PacketWrapper.PayLoad != null) && !PacketWrapper.PayLoad.Count().Equals(0))
                {
                    for (int i = 0; i < PacketWrapper.PayLoad.Count(); i++)
                    {
                        if (PacketWrapper.PayLoad[i] != sendData[i])
                        {
                            _Message += $"Mismatch {i} [PacketWrapper.PayLoad[i]] [sendData[i]] ";
                            result = false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        #endregion

        #region public commands

        /// <summary>
        /// Read device information for handset
        /// </summary>
        /// <returns></returns>
        public bool GetDeviceInformation()
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                if (_Port != null)
                {
                    if (!Send(OpCode.REQUEST_DEVICE_INFO))
                        throw new ApplicationException(_Message);

                    OpCode MessageId = (OpCode)PacketWrapper.MessageId;

                    if (!MessageId.Equals(OpCode.DEVICE_INFORMATION))
                        throw new ApplicationException($"Incorrect response, message {MessageId}");

                    if (_ZedScanVersion < 2)
                        _Information = new Handset.InformationBase();
                    else if (_ZedScanVersion.Equals(2))
                        _Information = new Handset.Information();

                    result = ParseResponse(PacketWrapper.PayLoad, _Information);
                }
                else
                    throw new ApplicationException("Port not found");
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        /// <summary>
        /// Send passed Handset.Information information
        /// </summary>
        /// <param name="information"></param>
        /// <returns></returns>
        public bool SetDeviceInformation(Handset.Information information)
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                //if (_Port != null)
                result = Send(OpCode.READING_RESULT, information);
                //else
                //    throw new ApplicationException("Port not found");
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        /// <summary>
        /// Send and verify Session
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public bool StartSession(Handset.Session session)
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                if (_Port != null)
                {
                    if (!Send(OpCode.SESSION_INFORMATION, session))
                        throw new ApplicationException(_Message);

                    if (!GetSession())
                        throw new ApplicationException(_Message);

                    // Signal version to _Session Mk1 / Mk2
                    _Session.ZedScanVersion = _ZedScanVersion;

                    if (!_Session.Equals(session))
                        throw new ApplicationException("Session verification failure");

                    result = true;
                }
                else
                    throw new ApplicationException("Port not found");
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        /// <summary>
        /// Reads Session Information from HandSet and populates a new Session class
        /// </summary>
        /// <returns></returns>
        public bool GetSession()
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                if (_Port != null)
                {
                    if (!Send(OpCode.REQUEST_SESSION_INFORMATION))
                        throw new ApplicationException(_Message);

                    OpCode MessageId = (OpCode)PacketWrapper.MessageId;

                    if (!MessageId.Equals(OpCode.SESSION_INFORMATION))
                        throw new ApplicationException($"Incorrect response, message {MessageId}");

                    _Session = new Handset.Session();

                    result = ParseResponse(PacketWrapper.PayLoad, _Session);
                }
                else
                    throw new ApplicationException("Port not found");
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }


        public bool GetResults()
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                if (_Port != null)
                {
                    if (!Send(OpCode.REQUEST_RESULTS))
                        throw new ApplicationException(_Message);

                    OpCode MessageId = (OpCode)PacketWrapper.MessageId;

                    if (!MessageId.Equals(OpCode.READING_RESULT))
                        throw new ApplicationException($"Incorrect response, message {MessageId}");

                    _Results = new Handset.ReadingResult();

                    result = ParseResponse(PacketWrapper.PayLoad, _Results);
                }
                else
                    throw new ApplicationException("Port not found");
            }
            catch (Exception e)
            {

                _Message = e.Message;
            }

            return result;
        }

        /// <summary>
        /// Send passed ReadingResult results
        /// </summary>
        /// <param name="results"></param>
        /// <returns></returns>
        public bool SetResults(Handset.ReadingResult results)
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                //if (_Port != null)
                result = Send(OpCode.READING_RESULT, results);
                //else
                //    throw new ApplicationException("Port not found");
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }


        /// <summary>
        /// Instruct the HandSet to delete all Session data
        /// </summary>
        /// <returns></returns>
        public bool DeleteSessionResults()
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                if (_Port != null)
                {
                    if (!Send(OpCode.DELETE_SESSION_RESULTS))
                    {
                        if (_ZedScanVersion > 1)                    // No ACK sent with ZedScan 1
                            throw new ApplicationException(_Message);
                    }
                }
                else
                    throw new ApplicationException("Port not found");

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }


        public bool SendFirmware(string fileName)
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                byte[] signature = ExtractSignature(fileName);

                if (signature != null)
                {
                    byte[] data = System.IO.File.ReadAllBytes(fileName);

                    result = SendFirmware(data, signature);
                }
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        public bool SendFirmware(byte[] data, byte[] signature)
        {
            bool result = false;

            _Message = string.Empty;

            int _UploadDataSize = 1024;

            try
            {
                if (_Port != null)
                {
                    int currentComplete = 0;
                    RaiseDataTransferEvent(currentComplete);

                    int increments = data.Length / _UploadDataSize;
                    increments += 2;                                // Start and End

                    if (!Send(OpCode.CODE_UPDATE_START_BLOCK, new Handset.BlockStartFirmware((uint)data.Length)))
                        throw new ApplicationException(_Message);

                    currentComplete += 1;
                    RaiseDataTransferEvent((int)((((float)currentComplete) / increments) * 100));

                    byte[] transfer = new byte[_UploadDataSize];

                    int dataPointer = 0;

                    do
                    {
                        if ((dataPointer + _UploadDataSize) < data.Length)
                        {
                            Array.ConstrainedCopy(data, dataPointer, transfer, 0, _UploadDataSize);
                        }
                        else
                        {
                            int remainder = data.Length;
                            remainder -= dataPointer;

                            transfer = new byte[remainder];

                            Array.ConstrainedCopy(data, dataPointer, transfer, 0, remainder);
                        }

                        //Task.Delay(100).Wait();
                        if (!Send(OpCode.CODE_UPDATE_BLOCK, new Handset.BlockData((UInt32)dataPointer, transfer)))
                            throw new ApplicationException(_Message);

                        dataPointer += _UploadDataSize;

                        currentComplete += 1;
                        RaiseDataTransferEvent((int)((((float)currentComplete) / increments) * 100));
                    }
                    while (dataPointer < data.Length);

                    if (!Send(OpCode.CODE_UPDATE_END_BLOCK, new Handset.BlockEnd((uint)data.Length, signature)))
                        throw new ApplicationException(_Message);

                    currentComplete += 1;
                    RaiseDataTransferEvent((int)((((float)currentComplete) / increments) * 100));

                    result = true;
                }
                else
                    throw new ApplicationException("Port not found");
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }


        public bool SendLanguagePack(string fileName)
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                byte[] signature = ExtractSignature(fileName);

                if (signature != null)
                {
                    byte[] data = System.IO.File.ReadAllBytes(fileName);

                    result = SendLanguagePack(data, signature);
                }
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        public bool SendLanguagePack(byte[] data, byte[] signature)
        {
            bool result = false;

            _Message = string.Empty;

            int _UploadDataSize = 1024;

            try
            {
                if (_Port != null)
                {
                    int currentComplete = 0;
                    RaiseDataTransferEvent(currentComplete);

                    int increments = data.Length / _UploadDataSize;
                    increments += 2;                                // Start and End

                    if (!Send(OpCode.LANGUAGE_UPDATE_START_BLOCK, new Handset.BlockStartLanguage((uint)data.Length)))
                        throw new ApplicationException(_Message);

                    currentComplete += 1;
                    RaiseDataTransferEvent((int)((((float)currentComplete) / increments) * 100));

                    byte[] transfer = new byte[_UploadDataSize];

                    int dataPointer = 0;

                    do
                    {
                        if ((dataPointer + _UploadDataSize) < data.Length)
                        {
                            Array.ConstrainedCopy(data, dataPointer, transfer, 0, _UploadDataSize);
                        }
                        else
                        {
                            int remainder = data.Length;
                            remainder -= dataPointer;

                            transfer = new byte[remainder];

                            Array.ConstrainedCopy(data, dataPointer, transfer, 0, remainder);
                        }

                        if (!Send(OpCode.LANGUAGE_UPDATE_BLOCK, new Handset.BlockData((UInt32)dataPointer, transfer)))
                            throw new ApplicationException(_Message);

                        dataPointer += _UploadDataSize;

                        currentComplete += 1;
                        RaiseDataTransferEvent((int)((((float)currentComplete) / increments) * 100));
                    }
                    while (dataPointer < data.Length);

                    if (!Send(OpCode.LANGUAGE_UPDATE_BLOCK, new Handset.BlockEnd((uint)data.Length, signature)))
                        throw new ApplicationException(_Message);

                    currentComplete += 1;
                    RaiseDataTransferEvent((int)((((float)currentComplete) / increments) * 100));

                    result = true;
                }
                else
                    throw new ApplicationException("Port not found");
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }



        /// <summary>
        /// Parse signature from file name
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public byte[] ExtractSignature(string fileName)
        {
            byte[] result = null;

            _Message = string.Empty;

            try
            {
                string fullName = System.IO.Path.GetFileNameWithoutExtension(fileName);
                int underScore = fullName.LastIndexOf("_");
                if (underScore < 0)
                    throw new ApplicationException("Missing signature");
                underScore++;                                   // _

                underScore += 3;                                // MD5

                if (fullName.Length < (underScore + 32))        // signature is 16 bytes saved as hex
                    throw new ApplicationException("Missing signature");

                fullName = fullName.Substring(underScore);

                if (fullName.Length < 32)
                    throw new ApplicationException("Missing signature");

                result = new byte[16];
                for (int i = 0; i < 16; i++)
                {
                    result[i] = Convert.ToByte(fullName.Substring((i * 2), 2), 16);
                }
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        #endregion

        // communications

        /// <summary>
        /// Encode object and send message
        /// </summary>
        /// <param name="opCode"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool Send(OpCode opCode, object obj)
        {
            bool result = false;

            try
            {
                List<byte> sendData = new List<byte>();

                sendData.Add((byte)opCode);

                PropertyInfo[] propInfos = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                                        .Where(a => Attribute.IsDefined(a, typeof(Handset.PropIdAttribute), false)).ToArray();

                foreach (PropertyInfo prop in propInfos)
                {
                    sendData.AddRange(EncodeProperty(obj, prop));
                }

                if (!Transceive(sendData.ToArray()))
                {
                    if (_ZedScanVersion > 1)                        // No ACK sent with ZedScan 1
                        throw new ApplicationException(_Message);
                }

                result = true;
            }
            catch (Exception e)
            {
                if (string.IsNullOrEmpty(_Message))
                    _Message = e.Message;
            }

            return result;
        }

        /// <summary>
        /// Send the command
        /// </summary>
        /// <param name="opCode"></param>
        /// <returns></returns>
        private bool Send(OpCode opCode)
        {
            return Transceive(new byte[] { (byte)opCode });
        }


        /// <summary>
        /// Encode and transmit data, receive and decode response
        /// </summary>
        /// <param name="sendData"></param>
        /// <returns></returns>
        private bool Transceive(byte[] sendData)
        {
            bool result = false;

            _Message = string.Empty;

            try
            {
                if (!PacketWrapper.Encode(sendData))
                    throw new ApplicationException(PacketWrapper.Message);

                if (!_Port.Transceive(PacketWrapper.PayLoad))
                    throw new ApplicationException(_Port.Message);

                if (!PacketWrapper.Decode(_Port.ReturnedPacket))
                    throw new ApplicationException(PacketWrapper.Message);

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }



        // encoding

        private List<byte> EncodeProperty(object obj, PropertyInfo propInfo)
        {
            int fieldNumber = propInfo.GetCustomAttribute<Handset.PropIdAttribute>().Index;
            fieldNumber = fieldNumber << 3;

            Protobuff.WireType wireType = Protobuff.WireType.Unknown;

            Protobuff.ICoder coder = null;

            TypeCode typeCode = Type.GetTypeCode(propInfo.PropertyType);

            switch (typeCode)
            {
                //case TypeCode.Empty:
                //    break;
                case TypeCode.Object:
                    wireType = Protobuff.WireType.LengthDelimit;

                    coder = new Protobuff.LengthDelimitCoder(propInfo.GetValue(obj));

                    if (propInfo.PropertyType.IsGenericType &&
                       (propInfo.PropertyType.GetInterface(nameof(System.Collections.ICollection)) != null))
                    {
                        if (propInfo.PropertyType.GenericTypeArguments[0].Name.Equals("Calibration"))
                        {
                            EncodeCollection<Handset.Calibration>(coder, (byte)(fieldNumber | (int)wireType));
                        }
                        else if (propInfo.PropertyType.GenericTypeArguments[0].Name.Equals("PointReading"))
                        {
                            EncodeCollection<Handset.PointReading>(coder, (byte)(fieldNumber | (int)wireType));
                        }
                        else if (propInfo.PropertyType.GenericTypeArguments[0].Name.Equals("FreqComponent"))
                        {
                            EncodeCollection<Handset.FreqComponent>(coder, (byte)(fieldNumber | (int)wireType));
                        }
                        else
                            throw new ApplicationException($"Unsupported type [{propInfo}]");
                    }
                    else if (propInfo.PropertyType.IsArray && propInfo.PropertyType.Name.Equals("Byte[]"))
                    {
                        byte[] buffer = (byte[])coder.Value;

                        if (buffer != null)
                        {
                            coder.Data.Add((byte)buffer.Length);

                            coder.Data.AddRange(buffer);
                        }
                        else
                            coder.Data.Add(0);
                    }
                    else
                    {
                        if (coder.Value != null)
                        {
                            PropertyInfo[] propInfos = coder.Value.GetType().GetProperties(BindingFlags.Public |
                                                                                           BindingFlags.Instance);

                            foreach (PropertyInfo property in propInfos)
                            {
                                coder.Data.AddRange(EncodeProperty(coder.Value, property));
                            }

                            Protobuff.ICoder coderSize = new Protobuff.VarIntCoder32(coder.Data.Count);

                            coder.Data.InsertRange(0, coderSize.Data);
                        }

                    //    //EncodeElement<Handset.Session>(coder.Value, coder);
                    }

                    coder = EncodeObject(obj, propInfo, (byte)(fieldNumber | (int)wireType));

                    break;
                //case TypeCode.DBNull:
                //    break;
                case TypeCode.Boolean:
                    wireType = Protobuff.WireType.VarInt;

                    coder = new Protobuff.BoolCoder((bool)propInfo.GetValue(obj));

                    break;
                //case TypeCode.Char:
                //    break;
                //case TypeCode.SByte:
                //    break;
                case TypeCode.Byte:
                    wireType = Protobuff.WireType.LengthDelimit;
                    break;
                //case TypeCode.Int16:
                //    break;
                //case TypeCode.UInt16:
                //    break;
                case TypeCode.Int32:
                    wireType = Protobuff.WireType.VarInt;

                    coder = new Protobuff.VarIntCoder32((Int32)propInfo.GetValue(obj));

                    break;
                case TypeCode.UInt32:
                    wireType = Protobuff.WireType.VarInt;

                    coder = new Protobuff.VarIntCoderU32((UInt32)propInfo.GetValue(obj));

                    break;
                case TypeCode.Int64:
                    wireType = Protobuff.WireType.VarInt;

                    coder = new Protobuff.VarIntCoder64((Int64)propInfo.GetValue(obj));

                    break;
                case TypeCode.UInt64:
                    wireType = Protobuff.WireType.VarInt;

                    coder = new Protobuff.VarIntCoderU64((UInt64)propInfo.GetValue(obj));

                    break;
                case TypeCode.Single:
                    wireType = Protobuff.WireType.bit32;

                    coder = new Protobuff.Bit32Coder((Single)propInfo.GetValue(obj));

                    break;
                case TypeCode.Double:
                    wireType = Protobuff.WireType.bit64;
                    break;
                //case TypeCode.Decimal:
                //    break;
                case TypeCode.DateTime:

                    UInt32 encodedDateTime = EncodeDate((DateTime)propInfo.GetValue(obj));

                    wireType = Protobuff.WireType.VarInt;
                    coder = new Protobuff.VarIntCoderU32(encodedDateTime);

                    break;
                case TypeCode.String:
                    wireType = Protobuff.WireType.LengthDelimit;

                    coder = new Protobuff.StringCoder(propInfo.GetValue(obj).ToString());

                    break;
                default:
                    throw new ApplicationException($"Unsupported type [{typeCode}]");
                    //break;
            }

            List<byte> sendData = new List<byte>();

            sendData.Add((byte)(fieldNumber | (int)wireType));

            sendData.AddRange(coder.Data);

            return sendData;
        }


        // Diminished
        /// <summary>
        /// Parse returned data into class property
        /// </summary>
        /// <param name="target">Class who's property to populate</param>
        /// <returns></returns>
        private bool ParseResponse(object target)
        {
            bool result = false;

            try
            {
                PropertyInfo[] propInfos = target.GetType().GetProperties(BindingFlags.Public |
                                                                          BindingFlags.Instance);

                bool start = true;

                int fieldNumber = 0;

                Protobuff.WireType wireType = Protobuff.WireType.Unknown;
                Protobuff.ICoder coder = null;

                for (int i = 1; i < PacketWrapper.PayLoad.Length; i++)
                {
                    if (start)                                      // 7 bit value mask 0x7f
                    {
                        #region determine field number and wire type

                        start = false;

                        fieldNumber = 0x78 & PacketWrapper.PayLoad[i];  // 0111 1000
                        fieldNumber = fieldNumber >> 3;                 // 0000 1111

                        wireType = (Protobuff.WireType)(0x07 & PacketWrapper.PayLoad[i]);   // 0000 0111

                        #endregion
                    }
                    else
                    {
                        // which property are we looking at and what Type is it

                        PropertyInfo[] pi = (propInfos.Where(p => p.GetCustomAttribute<Handset.PropIdAttribute>().Index.Equals(fieldNumber))).ToArray();

                        if ((pi == null) || (pi.Length < 1))
                            throw new ApplicationException($"Property {fieldNumber} not found");

                        Type proptype = pi[0].PropertyType;         // first match in list - there should be only one

                        List<byte> buffer = new List<byte>();       // copy data to buffer
                        buffer.AddRange(PacketWrapper.PayLoad);
                        buffer.RemoveRange(0, i);                   // trim to pointer

                        switch (wireType)
                        {
                            case Protobuff.WireType.VarInt:
                                #region data types: int32, int64, uint32, uint64, sint32, sint64, bool, enum

                                if (proptype.Equals(typeof(Int32)) || proptype.BaseType.Equals(typeof(Enum)))
                                {
                                    coder = new Protobuff.VarIntCoder32(buffer);

                                    pi[0].SetValue(target, coder.Value);

                                }
                                else if (proptype.Equals(typeof(UInt32)))
                                {
                                    coder = new Protobuff.VarIntCoderU32(buffer);

                                    pi[0].SetValue(target, coder.Value);
                                }
                                else if (proptype.Equals(typeof(Int64)))
                                {
                                    coder = new Protobuff.VarIntCoder64(buffer);

                                    pi[0].SetValue(target, coder.Value);
                                }
                                else if (proptype.Equals(typeof(UInt64)))
                                {
                                    coder = new Protobuff.VarIntCoderU64(buffer);

                                    pi[0].SetValue(target, coder.Value);
                                }
                                else if (proptype.Equals(typeof(bool)))
                                {
                                    coder = new Protobuff.VarIntCoder32(buffer);

                                    bool value = false;
                                    if (((Int32)coder.Value) > 0)
                                        value = true;

                                    pi[0].SetValue(target, value);
                                }
                                else // unsupported type (sint32, sint64)
                                {

                                }

                                i += coder.Data.Count;
                                i--;

                                break;

                            #endregion

                            case Protobuff.WireType.bit64:
                                break;

                            case Protobuff.WireType.LengthDelimit:

                                if (!buffer[0].Equals(0))           // 'LSB' length
                                {
                                    if (proptype.Equals(typeof(string)))
                                    {
                                        //Protobuff.StringCoder sdc = new Protobuff.StringCoder(buffer);

                                        coder = new Protobuff.StringCoder(buffer);

                                        //pi[0].SetValue(target, sdc.Value);
                                        pi[0].SetValue(target, coder.Value);

                                        //i += sdc.Data.Count;
                                        i += coder.Data.Count;
                                        i--;
                                    }
                                    else
                                    {
                                        // other data types: bytes, embedded messages, packed repeated fields


                                        if (proptype.Equals(typeof(Handset.Session)))
                                        {
                                            Handset.Session sesh = new Handset.Session();

                                            coder = new Protobuff.LengthDelimitCoder(buffer, sesh);

                                            ParseResponse(coder.Data.ToArray(), coder.Value);

                                            pi[0].SetValue(target, coder.Value);

                                            i += coder.Data.Count;
                                            i--;
                                        }
                                    }
                                }

                                break;

                            //case Protobuff.WireType.GroupStart:
                            //    break;
                            //case Protobuff.WireType.GroupEnd:
                            //    break;

                            case Protobuff.WireType.bit32:
                                break;

                            default:
                                break;
                        }

                        start = true;
                    }
                }

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }

        /// <summary>
        /// Decode payload byte array in to target object
        /// </summary>
        /// <param name="payload"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        private bool ParseResponse(byte[] payload, object target)
        {
            bool result = false;

            try
            {
                PropertyInfo[] propInfos = target.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                                 .Where(a => Attribute.IsDefined(a, typeof(Handset.PropIdAttribute), false)).ToArray();

                bool start = true;

                int fieldNumber = 0;

                Protobuff.WireType wireType = Protobuff.WireType.Unknown;
                Protobuff.ICoder coder = null;

                int startIndex = 1;

                for (int i = 0; i < payload.Length; i++)
                {
                    if ((payload[i] & 0x80) > 0)
                        startIndex++;
                    else
                        break;
                }

                for (int i = startIndex; i < payload.Length; i++)
                {
                    if (start)                                      // 7 bit value mask 0x7f
                    {
                        #region determine field number and wire type

                        start = false;

                        fieldNumber = 0x78 & payload[i];            // 0111 1000
                        fieldNumber = fieldNumber >> 3;             // 0000 1111

                        wireType = (Protobuff.WireType)(0x07 & payload[i]); // 0000 0111

                        #endregion
                     }
                    else
                    {
                        // which property are we looking at and what Type is it
                        PropertyInfo property = null;

                        try
                        {
                            property = propInfos.First(p => p.GetCustomAttribute<Handset.PropIdAttribute>().Index.Equals(fieldNumber));
                        }
                        catch (Exception e)
                        {
                            if (_TraceEnabled)
                                System.Diagnostics.Trace.WriteLine($"ZiliComms.ZedScan.ParseResponse select property : {e.Message}");
                        }

                        if (property == null)
                            throw new ApplicationException($"Property {fieldNumber} not found");

                        Type propType = property.PropertyType;      // first match in list - there should be only one

                        List<byte> buffer = new List<byte>();       // copy data to buffer
                        buffer.AddRange(payload);
                        buffer.RemoveRange(0, i);                   // trim to pointer

                        if (wireType.Equals(Protobuff.WireType.LengthDelimit) && !propType.Equals(typeof(string)))
                        {
                            if (propType.IsGenericType &&
                               (propType.GetInterface(nameof(System.Collections.ICollection)) != null))
                            {
                                if (propType.GenericTypeArguments[0].Name.Equals("Calibration"))
                                {
                                    DecodeElement<Handset.Calibration>(target, property, out coder, buffer);
                                }
                                else if (propType.GenericTypeArguments[0].Name.Equals("PointReading"))
                                {
                                    DecodeElement<Handset.PointReading>(target, property, out coder, buffer);
                                }
                                else if (propType.GenericTypeArguments[0].Name.Equals("FreqComponent"))
                                {
                                    DecodeElement<Handset.FreqComponent>(target, property, out coder, buffer);
                                }
                                //else
                                //{
                                //    DecodeElement <List<byte>>(target, property, out coder, buffer);
                                //}
                            }
                            else
                            {
                                coder = Protobuff.CoderFactory.NewDecoder(propType, buffer);

                                if (!propType.IsArray)
                                    ParseResponse(coder.Data.ToArray(), coder.Value);   // recursively populate

                                property.SetValue(target, coder.Value);
                            }
                        }
                        else
                        {
                            coder = Protobuff.CoderFactory.NewDecoder(propType, buffer);

                            if (Type.GetTypeCode(propType).Equals(TypeCode.DateTime))
                                property.SetValue(target, DecodeDate((UInt32)coder.Value));
                            else
                                property.SetValue(target, coder.Value);
                        }

                        i += coder.Data.Count;
                        i--;

                        start = true;
                    }
                }

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }



        /// <summary>
        /// Encode dateTime to unsigned 32bit integer
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        private UInt32 EncodeDate(DateTime dateTime)
        {
            const int baseYear = 2012;

            Int64 result = 0;

            result = dateTime.Year;
            result -= baseYear;                                 // max 63 yrs (2012 + 63 = 2075)
            result &= 0x3f;                                     // 0000 0000 0000 0000 0000 0000 0011 1111

            Int64 measure = dateTime.Month & 0x0f;              // 0000 0000 0000 0000 0000 0000 0000 1111
            result <<= 4;                                       // 0000 0000 0000 0000 0000 0011 1111 0000
            result |= measure;                                  // 0000 0000 0000 0000 0000 0011 1111 1111

            measure = dateTime.Day & 0x1f;                      // 0000 0000 0000 0000 0000 0000 0001 1111
            result <<= 5;                                       // 0000 0000 0000 0000 0111 1111 1110 0000
            result |= measure;                                  // 0000 0000 0000 0000 0111 1111 1111 1111

            measure = dateTime.Hour & 0x1f;                     // 0000 0000 0000 0000 0000 0000 0001 1111
            result <<= 5;                                       // 0000 0000 0000 1111 1111 1111 1110 0000
            result |= measure;                                  // 0000 0000 0000 1111 1111 1111 1111 1111

            measure = dateTime.Minute & 0x3f;                   // 0000 0000 0000 0000 0000 0000 0011 1111
            result <<= 6;                                       // 0000 0011 1111 1111 1111 1111 1100 0000
            result |= measure;                                  // 0000 0011 1111 1111 1111 1111 1111 1111

            measure = dateTime.Second & 0x3f;                   // 0000 0000 0000 0000 0000 0000 0011 1111 
            result <<= 6;                                       // 1111 1111 1111 1111 1111 1111 1100 0000
            result |= measure;                                  // 1111 1111 1111 1111 1111 1111 1111 1111


            return (UInt32)result;
        }

        /// <summary>
        /// Decode unsigned 32bit integer to dateTime
        /// </summary>
        /// <param name="encodedDate"></param>
        /// <returns></returns>
        private DateTime DecodeDate(UInt32 encodedDate)
        {
            const int baseYear = 2012;

            DateTime result = DateTime.MinValue;

            try
            {
                int dateCode = (int)encodedDate;

                int second = dateCode & 0x3F;

                int minute = dateCode >> 6;
                minute &= 0x3F;

                int hour = dateCode >> 12;
                hour &= 0x1F;

                int day = dateCode >> 17;
                day &= 0x1F;

                int month = dateCode >> 22;
                month &= 0x0F;

                int year = dateCode >> 26;
                year &= 0x3F;
                year += baseYear;

                result = new DateTime(year, month, day, hour, minute, second);
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }


        /// <summary>
        /// Packet encode target object
        /// </summary>
        /// <param name="opCode"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public List<byte> Encode(OpCode opCode, object target)
        {
            List<byte> result = new List<byte>();

            _Message = string.Empty;

            try
            {
                result.Add((byte)opCode);

                if (target != null)
                {
                    PropertyInfo[] propInfos = target.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                                     .Where(a => Attribute.IsDefined(a, typeof(Handset.PropIdAttribute), false)).ToArray();

                    foreach (PropertyInfo prop in propInfos)
                    {
                        result.AddRange(EncodeProperty(target, prop));
                    }
                }

                if (!PacketWrapper.Encode(result.ToArray()))
                    throw new ApplicationException(PacketWrapper.Message);

                result.Clear();

                result.AddRange(PacketWrapper.PayLoad);
            }
            catch (Exception e)
            {
                _Message = e.Message;

                result.Clear();
            }

            return result;
        }

        /// <summary>
        /// Decode the packet into the target object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="packet"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public bool Decode<T>(List<byte> packet, T target)
        {
            bool result = false;

            try
            {
                if (!PacketWrapper.Decode(packet))
                    throw new ApplicationException(PacketWrapper.Message);

                result = ParseResponse(PacketWrapper.PayLoad, target);
            }
            catch (Exception e)
            {
                _Message = e.Message;
            }

            return result;
        }


        private Protobuff.ICoder EncodeObject(object obj, PropertyInfo propInfo, byte fieldInfo)
        {
            Protobuff.ICoder coder = new Protobuff.LengthDelimitCoder(propInfo.GetValue(obj));

            if (propInfo.PropertyType.IsGenericType &&
               (propInfo.PropertyType.GetInterface(nameof(System.Collections.ICollection)) != null))
            {
                if (propInfo.PropertyType.GenericTypeArguments[0].Name.Equals("Calibration"))
                {
                    EncodeCollection<Handset.Calibration>(coder, fieldInfo);
                }
                else if (propInfo.PropertyType.GenericTypeArguments[0].Name.Equals("PointReading"))
                {
                    EncodeCollection<Handset.PointReading>(coder, fieldInfo);
                }
                else if (propInfo.PropertyType.GenericTypeArguments[0].Name.Equals("FreqComponent"))
                {
                    EncodeCollection<Handset.FreqComponent>(coder, fieldInfo);
                }
                else
                    throw new ApplicationException($"Unsupported type [{propInfo}]");
            }
            else if (propInfo.PropertyType.IsArray && propInfo.PropertyType.Name.Equals("Byte[]"))
            {
                #region byte array

                byte[] buffer = (byte[])coder.Value;

                if (buffer != null)
                {
                    Protobuff.ICoder coderSize = new Protobuff.VarIntCoder32(buffer.Length);

                    coder.Data.AddRange(coderSize.Data);

                    coder.Data.AddRange(buffer);
                }
                else
                    coder.Data.Add(0);

                #endregion
            }
            else
            {
                if (coder.Value != null)                            // untested (Reading Results)
                {
                    PropertyInfo[] propInfos = coder.Value.GetType().GetProperties(BindingFlags.Public |
                                                                                   BindingFlags.Instance);

                    foreach (PropertyInfo property in propInfos)
                    {
                        coder.Data.AddRange(EncodeProperty(coder.Value, property));
                    }

                    Protobuff.ICoder coderSize = new Protobuff.VarIntCoder32(coder.Data.Count);

                    coder.Data.InsertRange(0, coderSize.Data);
                }
            }

            return coder;
        }

        private void EncodeCollection<T>(Protobuff.ICoder coder, byte fieldInfo)
        {
            List<T> elements = (List<T>)coder.Value;

            foreach (T item in elements)
            {
                int dataStart = coder.Data.Count;

                EncodeElement<T>(item, coder);

                if (dataStart > 0)
                    coder.Data.Insert(dataStart, fieldInfo);        // field number and wireType
            }
        }

        private void EncodeElement<T>(object target, Protobuff.ICoder coder)
        {
            int dataStart = coder.Data.Count;

            PropertyInfo[] propInfos = target.GetType().GetProperties(BindingFlags.Public |
                                                                      BindingFlags.Instance);

            foreach (PropertyInfo property in propInfos)
            {
                coder.Data.AddRange(EncodeProperty(target, property));
            }

            Protobuff.ICoder coderSize = new Protobuff.VarIntCoder32(coder.Data.Count - dataStart);

            coder.Data.InsertRange(dataStart, coderSize.Data);
        }

        private void DecodeElement<T>(object target, PropertyInfo pi, out Protobuff.ICoder coder, List<byte> buffer)
        {
            coder = new Protobuff.LengthDelimitCoder(buffer, (T)Activator.CreateInstance(typeof(T)));

            ParseResponse(coder.Data.ToArray(), coder.Value);       // populate object

            List<T> collection = (List<T>)pi.GetValue(target);      // get reference to target collection

            collection.Add((T)coder.Value);
        }
    }
}
// Copyright (c) 2021 Zilico Limited
