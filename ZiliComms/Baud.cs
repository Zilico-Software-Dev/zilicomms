﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 20220811

namespace ZiliComms
{
    public enum Baud
    {
        Rate_9600 = 9600,
        Rate_19200 = 19200,
        Rate_115200 = 115200,
        Rate_230400 = 230400,
        Rate_460800 = 460800,
        Rate_921600 = 921600,
    }
}
// Copyright (c) 2022 Zilico Limited
