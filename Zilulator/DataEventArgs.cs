﻿using System;

namespace Zilulator
{
	public class DataEventArgs : EventArgs
	{
		private string _Message;

		private byte[] _Data;



		public string Message
		{ get { return _Message; } }

		public byte[] Data
		{ get { return _Data; } }



		public DataEventArgs(byte[] data) : this(string.Empty, data) { }

        public DataEventArgs(string message, byte[] data)
		{
			_Message = message;

			_Data = data;
		}
	}
}
