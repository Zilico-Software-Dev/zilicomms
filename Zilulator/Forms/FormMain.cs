﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zilulator
{
    public partial class FormMain : Form
    {
        #region fields

        private string _ApplicationTitle;

        private ToolStripComboBox _ToolStripComboBoxBaudRate;

        private ZiliConnection.Monitor _USBMonitor;

        private string _Vid;
        private string _Pid;

        private CommsAdaptor _CommsAdaptor;

        private ZiliComms.ZedScan _ZedScan;


        private ZiliComms.Handset.Information _Info;
        private ZiliComms.Handset.Session _Session;

        #endregion

        #region private methods

        private void DisplayText(string msg)
        {
            if (InvokeRequired)
                this.Invoke(new MethodInvoker(() => this.DisplayText(msg)));
            else
            {
                if (textBoxDisplay.Text.Length > 30000)
                    textBoxDisplay.Text = textBoxDisplay.Text.Substring(textBoxDisplay.Text.Length - 30000);

                textBoxDisplay.Text += Environment.NewLine;
                textBoxDisplay.Text += msg;
                textBoxDisplay.Text += Environment.NewLine;

                textBoxDisplay.SelectionStart = textBoxDisplay.TextLength;
                textBoxDisplay.ScrollToCaret();
            }
        }

        private byte GetCommPortNumber()
        {
            System.Management.ManagementScope connectionScope = new System.Management.ManagementScope();
            System.Management.SelectQuery serialQuery = new System.Management.SelectQuery("SELECT * FROM Win32_PnPEntity"); // Win32_SerialPort");
            System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher(connectionScope, serialQuery);

            System.Management.ManagementObjectCollection stuff = searcher.Get();

            int result = 0;

            try
            {
                foreach (System.Management.ManagementObject mo in stuff)
                {
                    //System.Diagnostics.Trace.WriteLine($"Manufacturer: {mo["Manufacturer"]}");

                    if (mo.ToString().Contains(_Vid) && mo.ToString().Contains(_Pid))
                    {
                        foreach (var prop in mo.Properties)
                        {
                            if ((prop.Value != null) && prop.Value.ToString().Contains("(COM"))
                            {
                                string value = prop.Value.ToString();

                                int idx = value.IndexOf("(COM");
                                idx += 4;

                                value = value.Substring(idx);
                                value = value.Substring(0, value.Length - 1);

                                result = int.Parse(value);

                                break;
                            }
                        }

                        if (result > 0)
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                DisplayText(e.Message);
            }

            return (byte)result;
        }

        /// <summary>
        /// Display the baud rate if non shown, or set the Comms adaptor baud rate to selected
        /// </summary>
        private void ShowBaudRate()
        {
            this.Invoke((MethodInvoker)delegate
            {
                if (string.IsNullOrEmpty(_ToolStripComboBoxBaudRate.Text))
                    _ToolStripComboBoxBaudRate.Text = _CommsAdaptor.BaudRate.ToString();
                else
                    _CommsAdaptor.BaudRate = int.Parse(_ToolStripComboBoxBaudRate.Text);
            });
        }

        private List<byte> CommandReply(byte[] data)
        {
            List<byte> result = null;

            try
            {
                ZiliComms.Commands.OpCode messageId = (ZiliComms.Commands.OpCode)data[0];

                switch (messageId)
                {
                    case ZiliComms.Commands.OpCode.REQUEST_DEVICE_INFO:
                        result = _ZedScan.Encode(ZiliComms.Commands.OpCode.DEVICE_INFORMATION, _Info);
                        break;
                    case ZiliComms.Commands.OpCode.DEVICE_INFORMATION:
                        break;
                    case ZiliComms.Commands.OpCode.REQUEST_RESULTS:
                        break;
                    case ZiliComms.Commands.OpCode.READING_RESULT:
                        break;
                    case ZiliComms.Commands.OpCode.REQUEST_SESSION_INFORMATION:
                        result = _ZedScan.Encode(ZiliComms.Commands.OpCode.SESSION_INFORMATION, _Session);
                        break;
                    case ZiliComms.Commands.OpCode.SESSION_INFORMATION:

                        // decode data to session object
                        ZiliComms.Handset.Session session = new ZiliComms.Handset.Session();

                        if (_ZedScan.Decode(_CommsAdaptor.RxBuffer, session))
                            _Session = session;
                        else
                            throw new ApplicationException(_ZedScan.Message);

                        result = _ZedScan.Encode(ZiliComms.Commands.OpCode.ACK, null);

                        break;
                    case ZiliComms.Commands.OpCode.CODE_UPDATE_START_BLOCK:
                        break;
                    case ZiliComms.Commands.OpCode.CODE_UPDATE_BLOCK:
                        break;
                    case ZiliComms.Commands.OpCode.CODE_UPDATE_END_BLOCK:
                        break;
                    case ZiliComms.Commands.OpCode.LANGUAGE_UPDATE_START_BLOCK:
                        break;
                    case ZiliComms.Commands.OpCode.LANGUAGE_UPDATE_BLOCK:
                        break;
                    case ZiliComms.Commands.OpCode.LANGUAGE_UPDATE_END_BLOCK:
                        break;
                    case ZiliComms.Commands.OpCode.DELETE_SESSION_RESULTS:

                        _Session = new ZiliComms.Handset.Session();

                        result = _ZedScan.Encode(ZiliComms.Commands.OpCode.ACK, null);

                        break;
                    case ZiliComms.Commands.OpCode.REQUEST_CONFIGURATION:
                        break;
                    case ZiliComms.Commands.OpCode.CONFIGURATION:
                        break;
                    case ZiliComms.Commands.OpCode.LANGUAGE_SELECTION:
                        break;
                    case ZiliComms.Commands.OpCode.ACK:
                        break;
                    case ZiliComms.Commands.OpCode.SET_UNIQUE_ID:
                        break;
                    case ZiliComms.Commands.OpCode.NAK:
                        break;
                    case ZiliComms.Commands.OpCode.NOP:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                DisplayText(e.Message);
            }

            return result;
        }

        #endregion

        #region ~ctor

        public FormMain()
        {
            InitializeComponent();

            if (System.Diagnostics.Debugger.IsAttached)
            {
                System.Diagnostics.Trace.Listeners.Clear();
                System.Diagnostics.Trace.Listeners.Add(new System.Diagnostics.TextWriterTraceListener(Console.Out));
            }

            #region App name and version

            System.Reflection.Assembly executingAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            Attribute titleAttribute = Attribute.GetCustomAttribute(executingAssembly,
                                                                    typeof(System.Reflection.AssemblyTitleAttribute));
            System.Reflection.AssemblyTitleAttribute title = (System.Reflection.AssemblyTitleAttribute)titleAttribute;

            _ApplicationTitle = title.Title;

            this.Text = _ApplicationTitle;

            this.Text += "    " + Application.ProductVersion;

            this.Icon = System.Drawing.Icon.ExtractAssociatedIcon(Application.ExecutablePath);

            #endregion

            #region baud rate combo box

            _ToolStripComboBoxBaudRate = new System.Windows.Forms.ToolStripComboBox();
            _ToolStripComboBoxBaudRate.AutoSize = false;
            _ToolStripComboBoxBaudRate.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            _ToolStripComboBoxBaudRate.IntegralHeight = false;
            _ToolStripComboBoxBaudRate.Margin = new Padding(0, 1, 0, 3);
            _ToolStripComboBoxBaudRate.Height = 20;
            _ToolStripComboBoxBaudRate.Width = 75;

            _ToolStripComboBoxBaudRate.Items.AddRange(new string[] { "9600", "19200", "115200", "230400", "460800", "921600" });

            _ToolStripComboBoxBaudRate.TextChanged += new EventHandler(_ToolStripComboBoxBaudRate_TextChanged);

            statusStripMain.Items.Insert(1, _ToolStripComboBoxBaudRate);

            #endregion

            _Vid = "0403";
            _Pid = "6001";

            _ZedScan = new ZiliComms.ZedScan();


            //_Info = new ZiliComms.Handset.Information(Application.ProductVersion,
            //                  "ZSEm-SW-001",
            //                  "123456", null);

            //_Info = new ZiliComms.Handset.Information(Application.ProductVersion,
            //                                          "ZSEm-SW-001",
            //                                          "123456",
            //                                          new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
            //                                                       0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
            //                                                       0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
            //                                                       0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f });

            List<byte> buffer = new List<byte>();
            for (int i = 0; i < 1024; i++)
            {
                if (i < 256)
                    buffer.Add((byte)i);
                else
                    buffer.Add((byte)(i >> 8));
            }

            _Info = new ZiliComms.Handset.Information(Application.ProductVersion,
                                          "ZSEm-SW-001",
                                          "123456",
                                          buffer.ToArray());


            _Session = new ZiliComms.Handset.Session("Number 2", "Zilulator",
                                                     ZiliComms.Handset.ReferralType.BSCC_No_Referral,
                                                     DateTime.Now, true);
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            _USBMonitor = new ZiliConnection.Monitor(ZiliConnection.Guids.GUID_DEVINTERFACE_COMPORT, _Vid, _Pid);

            _USBMonitor.USBStateChangeEvent += _USBMonitor_USBStateChangeEvent;

            _USBMonitor.Start();
        }

        #endregion

        #region form event handlers

        private void _ToolStripComboBoxBaudRate_TextChanged(object sender, EventArgs e)
        {
            if (_CommsAdaptor != null)
            {
                if (_CommsAdaptor.PortIsOpen)
                {
                    _CommsAdaptor.Close();
                    _CommsAdaptor.BaudRate = int.Parse(_ToolStripComboBoxBaudRate.Text);
                    _CommsAdaptor.Open();
                }
                else
                    _CommsAdaptor.BaudRate = int.Parse(_ToolStripComboBoxBaudRate.Text);
            }
        }


        private void emulationModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(_CommsAdaptor!=null)
                _CommsAdaptor.EmulationMode = emulationModeToolStripMenuItem.Checked;
        }

        private void clearScreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBoxDisplay.Text = string.Empty;
        }

        #endregion

        #region object event handlers

        private void _USBMonitor_USBStateChangeEvent(object sender, ZiliConnection.USBStateChangeEventArgs e)
        {
            //DisplayMessage($"{e.State}");

            if (e.State.Equals(ZiliConnection.StateChange.Arrival))
            {
                _CommsAdaptor = new CommsAdaptor();

                _CommsAdaptor.TraceEnabled = true;

                _CommsAdaptor.EmulationMode = emulationModeToolStripMenuItem.Checked;
                _CommsAdaptor.PortNumber = GetCommPortNumber();

                _CommsAdaptor.ErrorEvent += new EventHandler<DataEventArgs>(_CommsAdaptor_ErrorEvent);
                _CommsAdaptor.DataEvent += new EventHandler<DataEventArgs>(_CommsAdaptor_DataEvent);
                _CommsAdaptor.CommandEvent += new EventHandler<DataEventArgs>(_CommsAdaptor_CommandEvent);

                toolStripStatusLabelPortNumber.Text = $"COM {_CommsAdaptor.PortNumber}";

                ShowBaudRate();

                _CommsAdaptor.Open();
            }
            else
            {
                if (_CommsAdaptor != null)
                {
                    _CommsAdaptor.ErrorEvent -= new EventHandler<DataEventArgs>(_CommsAdaptor_ErrorEvent);
                    _CommsAdaptor.DataEvent -= new EventHandler<DataEventArgs>(_CommsAdaptor_DataEvent);
                    _CommsAdaptor.CommandEvent -= new EventHandler<DataEventArgs>(_CommsAdaptor_CommandEvent);

                    _CommsAdaptor.Dispose();
                    _CommsAdaptor = null;
                }
            }
        }

        #region comms event handlers

        private void _CommsAdaptor_ErrorEvent(object sender, DataEventArgs e)
        {
            DisplayText("v--------------- ERROR --------------v");
            DisplayText(e.Message);
            DisplayText(BufferFormatter.ToHex(e.Data));
            DisplayText("------------------^^------------------");
        }

        private void _CommsAdaptor_DataEvent(object sender, DataEventArgs e)
        {
            string msg = string.Empty;

            foreach (byte item in e.Data)
            {
                msg += item.ToString("X2");
                msg += " ";
            }

            try
            {
                DisplayText(msg);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
            }
        }

        private void _CommsAdaptor_CommandEvent(object sender, DataEventArgs e)
        {
            List<byte> reply = CommandReply(e.Data);

            if (reply != null)
            {
                byte[] buffer = reply.ToArray();

                if (displayTXToolStripMenuItem.Checked)             // display Tx
                {
                    DisplayText("v---------------- TX ----------------v");
                    DisplayText(BufferFormatter.ToHex(buffer));
                    DisplayText("------------------^^------------------");
                }

                _CommsAdaptor.Transmit(buffer);
            }
        }

        #endregion

        #endregion

        private void informationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InformationScreen information = new InformationScreen(_Info);

            if (information.ShowDialog(this).Equals(DialogResult.OK))
            {
                _Info = information.Information;

                // save to settings ...
            }
        }
    }
}
// Copyright (c) 2022 Zilico Limited
