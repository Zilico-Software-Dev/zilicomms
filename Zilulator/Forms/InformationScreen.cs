﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zilulator
{
    public partial class InformationScreen : Form
    {
        private ZiliComms.Handset.Information _Info;


        public ZiliComms.Handset.Information Information
        { get { return _Info; } }


        public InformationScreen(ZiliComms.Handset.Information info)
        {
            InitializeComponent();

            _Info = info;
        }

        private void InformationScreen_Load(object sender, EventArgs e)
        {
            textBoxSoftwareVersion.Text = _Info.SoftwareVersion;
            textBoxHardwareVersion.Text = _Info.HardwareVersion;
            textBoxSerialNumber.Text = _Info.SerialNumber;

            textBoxErrorLog.Text = BufferFormatter.ToHex(_Info.ErrorLog);
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            byte[] buffer = null;                                   // convert error log to byte array

            _Info = new ZiliComms.Handset.Information(textBoxSoftwareVersion.Text, 
                                                      textBoxHardwareVersion.Text, 
                                                      textBoxSerialNumber.Text,
                                                      buffer);
        }
    }
}
// Copyright (c) 2022 Zilico Limited
