﻿
namespace Zilulator
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStripMain = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelBaudRate = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelMiddleSpace = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelPortNumber = new System.Windows.Forms.ToolStripStatusLabel();
            this.textBoxDisplay = new System.Windows.Forms.TextBox();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emulationModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayTXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemDivider0 = new System.Windows.Forms.ToolStripSeparator();
            this.informationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearScreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStripMain.SuspendLayout();
            this.menuStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStripMain
            // 
            this.statusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelBaudRate,
            this.toolStripStatusLabelMiddleSpace,
            this.toolStripStatusLabelPortNumber});
            this.statusStripMain.Location = new System.Drawing.Point(0, 476);
            this.statusStripMain.Name = "statusStripMain";
            this.statusStripMain.Size = new System.Drawing.Size(536, 22);
            this.statusStripMain.TabIndex = 10;
            this.statusStripMain.Text = "statusStrip1";
            // 
            // toolStripStatusLabelBaudRate
            // 
            this.toolStripStatusLabelBaudRate.Name = "toolStripStatusLabelBaudRate";
            this.toolStripStatusLabelBaudRate.Size = new System.Drawing.Size(66, 17);
            this.toolStripStatusLabelBaudRate.Text = "Baud Rate  ";
            // 
            // toolStripStatusLabelMiddleSpace
            // 
            this.toolStripStatusLabelMiddleSpace.Name = "toolStripStatusLabelMiddleSpace";
            this.toolStripStatusLabelMiddleSpace.Size = new System.Drawing.Size(380, 17);
            this.toolStripStatusLabelMiddleSpace.Spring = true;
            // 
            // toolStripStatusLabelPortNumber
            // 
            this.toolStripStatusLabelPortNumber.AutoSize = false;
            this.toolStripStatusLabelPortNumber.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken;
            this.toolStripStatusLabelPortNumber.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripStatusLabelPortNumber.Name = "toolStripStatusLabelPortNumber";
            this.toolStripStatusLabelPortNumber.Size = new System.Drawing.Size(75, 17);
            this.toolStripStatusLabelPortNumber.Text = "COM #";
            // 
            // textBoxDisplay
            // 
            this.textBoxDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDisplay.BackColor = System.Drawing.Color.Black;
            this.textBoxDisplay.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDisplay.ForeColor = System.Drawing.Color.Lime;
            this.textBoxDisplay.Location = new System.Drawing.Point(12, 27);
            this.textBoxDisplay.Multiline = true;
            this.textBoxDisplay.Name = "textBoxDisplay";
            this.textBoxDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxDisplay.Size = new System.Drawing.Size(512, 446);
            this.textBoxDisplay.TabIndex = 11;
            // 
            // menuStripMain
            // 
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.clearScreenToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(536, 24);
            this.menuStripMain.TabIndex = 12;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.emulationModeToolStripMenuItem,
            this.displayTXToolStripMenuItem,
            this.toolStripMenuItemDivider0,
            this.informationToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // emulationModeToolStripMenuItem
            // 
            this.emulationModeToolStripMenuItem.CheckOnClick = true;
            this.emulationModeToolStripMenuItem.Name = "emulationModeToolStripMenuItem";
            this.emulationModeToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.emulationModeToolStripMenuItem.Text = "Emulation Mode";
            this.emulationModeToolStripMenuItem.Click += new System.EventHandler(this.emulationModeToolStripMenuItem_Click);
            // 
            // displayTXToolStripMenuItem
            // 
            this.displayTXToolStripMenuItem.CheckOnClick = true;
            this.displayTXToolStripMenuItem.Name = "displayTXToolStripMenuItem";
            this.displayTXToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.displayTXToolStripMenuItem.Text = "Display TX";
            // 
            // toolStripMenuItemDivider0
            // 
            this.toolStripMenuItemDivider0.Name = "toolStripMenuItemDivider0";
            this.toolStripMenuItemDivider0.Size = new System.Drawing.Size(159, 6);
            // 
            // informationToolStripMenuItem
            // 
            this.informationToolStripMenuItem.Name = "informationToolStripMenuItem";
            this.informationToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.informationToolStripMenuItem.Text = "Information";
            this.informationToolStripMenuItem.Click += new System.EventHandler(this.informationToolStripMenuItem_Click);
            // 
            // clearScreenToolStripMenuItem
            // 
            this.clearScreenToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.clearScreenToolStripMenuItem.Name = "clearScreenToolStripMenuItem";
            this.clearScreenToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.clearScreenToolStripMenuItem.Text = "Clear Screen";
            this.clearScreenToolStripMenuItem.Click += new System.EventHandler(this.clearScreenToolStripMenuItem_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 498);
            this.Controls.Add(this.textBoxDisplay);
            this.Controls.Add(this.statusStripMain);
            this.Controls.Add(this.menuStripMain);
            this.MainMenuStrip = this.menuStripMain;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.statusStripMain.ResumeLayout(false);
            this.statusStripMain.PerformLayout();
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStripMain;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelBaudRate;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelMiddleSpace;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelPortNumber;
        private System.Windows.Forms.TextBox textBoxDisplay;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem clearScreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emulationModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItemDivider0;
        private System.Windows.Forms.ToolStripMenuItem displayTXToolStripMenuItem;
    }
}

