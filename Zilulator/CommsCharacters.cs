﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zilulator
{
    public enum CommsCharacter : byte
    {
        End = 0xFD,
        Escape = 0xFE,
        EscapeAlt = 0xFF,
    }

    public enum CommsState
    {
        Normal = 0,
        Escaped = 1,
    }

    public enum MessageState
    {
        Extracting = 1,
        End = 2,
        Complete = 3,
    }

}
// Copyright (c) 2021 Zilico Limited
