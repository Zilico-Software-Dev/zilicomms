﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using System.Linq;
//using System.Text;
using System.Threading.Tasks;

namespace Zilulator
{
    internal static class BufferFormatter
    {
        public static string ToHex(byte[] buffer)
        {
            string result = string.Empty;

            if (buffer != null)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                int index = 0;
                while (index < buffer.Length)
                {
                    for (int width = 0; width < 16; width++)
                    {
                        if (index < buffer.Length)
                        {
                            if (width.Equals(8))
                                sb.Append(" ");

                            sb.Append($"{buffer[index]:X2} ");

                            index++;
                        }
                    }

                    sb.AppendLine();
                }

                result = sb.ToString();
            }

            return result.Trim();
        }
    }
}
// Copyright (c) 2022 Zilico Limited
