﻿using System;
using System.Collections.Generic;
using System.IO.Ports;

namespace Zilulator
{
    internal class CommsAdaptor
    {
        #region Fields

        private bool _Disposed;

        private bool _TraceEnabled;

        private string _Message;

        private bool _EmulationMode;

        private bool _PortIsOpen;

        private Int32 _BaudRate;
        private Parity _Parity;
        private StopBits _StopBits;
        private Int32 _DataBits;
        private Byte _PortNumber;

        private SerialPort _CommPort;

        private List<Byte> _RxBuffer;



        private System.Threading.SendOrPostCallback _SendOrPostErrorEvent;
        private System.Threading.SendOrPostCallback _SendOrPostDataEvent;
        private System.Threading.SendOrPostCallback _SendOrPostCommandEvent;
        private System.ComponentModel.AsyncOperation _Async;



        private UInt32[] _CRC32Table;

        private CommsState _CommsState;
        private MessageState _MessageState;



        #endregion

        #region Properties

        internal bool TraceEnabled
        {
            get { return _TraceEnabled; }
            set { _TraceEnabled = value; }
        }

        internal String Message
        { get { return _Message; } }

        internal bool EmulationMode
        {
            get { return _EmulationMode; }
            set { _EmulationMode = value; }
        }

        internal Boolean PortIsOpen
        { get { return _PortIsOpen; } }

        /// <summary>
        /// Comm Port Number
        /// </summary>
        internal Byte PortNumber
        {
            get
            {
                return _PortNumber;
            }
            set
            {
                if (_CommPort != null) 
                    Close();

                _PortNumber = value;
            }
        }

        /// <summary>
        /// Baud Rate of the port
        /// </summary>
        internal int BaudRate
        {
            get
            {
                return _BaudRate;
            }
            set
            {
                if (_CommPort != null) 
                    Close();

                _BaudRate = value;
            }
        }

        /// <summary>
        /// Number of data bits (8)
        /// </summary>
        internal int DataBits
        {
            get { return _DataBits; }
            set { _DataBits = value; }
        }

        /// <summary>
        /// Type of parity bit to use
        /// </summary>
        internal Parity Parity
        {
            get { return _Parity; }
            set { _Parity = value; }
        }

        /// <summary>
        /// number of stop bits (1)
        /// </summary>
        internal StopBits StopBits
        {
            get { return _StopBits; }
            set { _StopBits = value; }
        }


        internal List<byte> RxBuffer
        { get { return _RxBuffer; } }

        #endregion

        #region ~ctors

        internal CommsAdaptor() : this(0) { }

        internal CommsAdaptor(byte portNumber) : this(portNumber, 19200) { }

        internal CommsAdaptor(byte portNumber, Int32 baudRate)
        {
            _Disposed = false;

            _TraceEnabled = false;

            _Message = string.Empty;

            _EmulationMode = false;


            _SendOrPostErrorEvent = new System.Threading.SendOrPostCallback(RaiseErrorEventAsync);
            _SendOrPostDataEvent = new System.Threading.SendOrPostCallback(RaiseDataEventAsync);
            _SendOrPostCommandEvent = new System.Threading.SendOrPostCallback(RaiseCommandEventAsync);
            _Async = System.ComponentModel.AsyncOperationManager.CreateOperation(null);

            _PortNumber = portNumber;
            _BaudRate = baudRate;
            _DataBits = 8;
            _Parity = Parity.None;
            _StopBits = System.IO.Ports.StopBits.One;

            _RxBuffer = new List<byte>();

            Construct();



            _CRC32Table = CRC32();

            _CommsState = CommsState.Normal;
            _MessageState = MessageState.Extracting;
        }

        ~CommsAdaptor()
        {
            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine("Zilulator.CommsAdaptor Destruct");

            Dispose(false);
        }

        #endregion

        #region Private Methods

        private void Construct()
        {
            _Message = string.Empty;


            _CommPort = new SerialPort();

            _CommPort.DataReceived += new SerialDataReceivedEventHandler(commPort_DataReceived);
            _CommPort.ErrorReceived += new SerialErrorReceivedEventHandler(commPort_ErrorReceived);
            _CommPort.PinChanged += new SerialPinChangedEventHandler(commPort_PinChanged);


            _PortIsOpen = _CommPort.IsOpen;
        }

        private void ReLoad()
        {
            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine("Zilulator.CommsAdaptor.ReLoad");

            try // Only System.GC.Collect() could throw an error
            {
                _Message = string.Empty;

                if (_RxBuffer != null)
                    _RxBuffer.Clear();

                Close();

                UnhookDisposeCommPort();
            }
            catch (Exception ex)
            {
                if (_TraceEnabled) 
                    System.Diagnostics.Trace.WriteLine("Zilulator.CommsAdaptor.ReLoad Error : " + ex.Message);
            }

            Construct();
        }

        public void Dispose()
        {
            Dispose(true);

            // Call SupressFinalize in case a subclass implements a finaliser.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_Disposed)
            {
                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine("Zilulator.CommsAdaptor.Dispose");

                if (disposing) // Dispose managed resources.
                {
                    if (_CommPort != null)
                    {
                        Close();

                        UnhookDisposeCommPort();
                    }
                }

                // Release unmanaged resources.

                _CommPort = null;

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine("Zilulator.CommsAdaptor.Dispose Memory used before collection     : " + GC.GetTotalMemory(false));
                System.GC.Collect();
                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine("Zilulator.CommsAdaptor.Dispose Memory used after full collection : " + GC.GetTotalMemory(true));

                _Disposed = true;
            }
        }

        private void UnhookDisposeCommPort()
        {
            if (_CommPort != null)
            {
                _CommPort.DataReceived -= new SerialDataReceivedEventHandler(commPort_DataReceived);
                _CommPort.ErrorReceived -= new SerialErrorReceivedEventHandler(commPort_ErrorReceived);
                _CommPort.PinChanged -= new SerialPinChangedEventHandler(commPort_PinChanged);

                _CommPort.Dispose();

                _CommPort = null;
            }
        }

        #endregion

        #region Serial Port Event Handlers

        private void commPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                if (_CommPort.BytesToRead > 0)
                {
                    byte[] commBuffer = new byte[_CommPort.BytesToRead];

                    _CommPort.Read(commBuffer, 0, commBuffer.Length);

                    if (_TraceEnabled)
                        System.Diagnostics.Trace.WriteLine("RX : " + commBuffer.Length.ToString() + " bytes");

                    if (_EmulationMode)
                    {
                        _RxBuffer.AddRange(commBuffer);

                        DecodeRx();
                    }
                    else
                        _RxBuffer.Clear();

                    RaiseDataEvent(commBuffer);
                }
            }
            catch (Exception ex)
            {
                _Message = ex.Message;

                RaiseErrorEvent();

                _RxBuffer.Clear();
            }
        }

        private void commPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine("Zilulator.CommsAdaptor.SerialErrorReceived : " + e.ToString());
        }

        private void commPort_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine("Zilulator.CommsAdaptor.PinChanged " + e.EventType);
            //PinChanged CtsChanged     The Clear to Send (CTS) signal changed state. This signal is used to indicate whether data can be sent over the serial port.
            //PinChanged DsrChanged     The Data Set Ready (DSR) signal changed state. This signal is used to indicate whether the device on the serial port is ready to operate
            //PinChanged CDChanged      The Carrier Detect (CD) signal changed state. This signal is used to indicate whether a modem is connected to a working phone line and a data carrier signal is detected.
            //PinChanged Ring           A ring indicator was detected.

            if (e.EventType != SerialPinChange.Break)
                ReLoad();
        }

        #endregion

        #region Events

        public event EventHandler<DataEventArgs> DataEvent;

        protected virtual void OnDataEvent(DataEventArgs e)
        {
            if (DataEvent != null) DataEvent(this, e);
        }

        private void RaiseDataEventAsync(object e)
        {
            OnDataEvent(new DataEventArgs((byte[])e));

            //_DataEventRaised = false;
        }

        private void RaiseDataEvent(byte[] data)
        {
            _Async.Post(_SendOrPostDataEvent, data);
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        public event EventHandler<DataEventArgs> CommandEvent;

        protected virtual void OnCommandEvent(DataEventArgs e)
        {
            if (CommandEvent != null) 
                CommandEvent(this, e);

            _RxBuffer.Clear();
        }

        private void RaiseCommandEventAsync(object e)
        {
            OnCommandEvent(new DataEventArgs(((List<byte>)e).ToArray()));
        }

        /// <summary>
        /// Comms command received
        /// </summary>
        private void RaiseCommandEvent(List<byte> payLoad)
        {
            _Async.Post(_SendOrPostCommandEvent, payLoad);
        }

        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        public event EventHandler<DataEventArgs> ErrorEvent;

        protected virtual void OnErrorEvent(DataEventArgs e)
        {
            if (ErrorEvent != null)
                ErrorEvent(this, e);
        }

        private void RaiseErrorEventAsync(object e)
        {
            OnErrorEvent((DataEventArgs)e);
        }

        /// <summary>
        /// Comms command received
        /// </summary>
        private void RaiseErrorEvent()
        {
            _Async.Post(_SendOrPostErrorEvent, new DataEventArgs(_Message, _RxBuffer.ToArray()));
        }

        #endregion



        internal bool Open()
        {
            bool result = false;

            try
            {
                if (!_CommPort.IsOpen)
                {
                    #region Set Comm parameters

                    _CommPort.PortName = "COM" + _PortNumber.ToString();

                    _CommPort.BaudRate = _BaudRate;
                    _CommPort.Parity = _Parity;
                    _CommPort.DataBits = _DataBits;
                    _CommPort.StopBits = _StopBits;

                    #endregion

                    _CommPort.Open();

                    // Don't like this but SerialPinChangedEventHandler not always raised
                    System.GC.SuppressFinalize(_CommPort.BaseStream);

                    if (_TraceEnabled)
                        System.Diagnostics.Trace.WriteLine("Zilulator.CommsAdaptor.OpenPort Comm Port " + _PortNumber.ToString() + " Opened");

                    result = true;
                }
            }
            catch (Exception ex)
            {
                _Message = ex.Message;

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine("Zilulator.CommsAdaptor.OpenPort " + _Message);
            }

            if (_CommPort == null)
                _PortIsOpen = false;
            else
                _PortIsOpen = _CommPort.IsOpen;

            return result;
        }

        internal bool Close()
        {
            bool result = false;

            try
            {
                if (_CommPort.IsOpen)
                {
                    // Don't like this but SerialPinChangedEventHandler not always raised
                    System.GC.ReRegisterForFinalize(_CommPort.BaseStream);

                    _CommPort.Close();

                    if (_TraceEnabled)
                        System.Diagnostics.Trace.WriteLine("Zilulator.CommsAdaptor.ClosePort Comm Port " + _PortNumber + " Closed");
                }

                result = true;
            }
            catch (Exception ex)
            {
                _Message = "Port Close Error : " + ex.Message;

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine("Zilulator.CommsAdaptor.ClosePort " + _Message);

                UnhookDisposeCommPort();
            }

            if (_CommPort == null)
                _PortIsOpen = false;
            else
                _PortIsOpen = _CommPort.IsOpen;

            return result;
        }

        internal void Transmit(byte[] buffer)
        {
            _Message = $"Zilulator.CommsAdaptor.Transmit {buffer.Length} bytes";
            _Message += Environment.NewLine;
            _Message += $"Tx : {ToHex(buffer)}";

            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine(_Message);

            _CommPort.Write(buffer, 0, buffer.Length);
        }




        public void DecodeRx()
        {
            _CommsState = CommsState.Normal;
            _MessageState = MessageState.Extracting;

            List<byte> payLoad = new List<byte>();

            UInt32 checkSum = 0;
            int chkSmCount = 0;

            foreach (byte data in _RxBuffer)
            {
                byte chrBuffer = data;

                if (data.Equals((byte)CommsCharacter.Escape))
                {
                    _CommsState = CommsState.Escaped;
                }
                else
                {
                    if (_CommsState.Equals(CommsState.Escaped))
                    {
                        if (data.Equals((byte)CommsCharacter.EscapeAlt))
                        {
                            chrBuffer = (byte)CommsCharacter.Escape;
                            _CommsState = CommsState.Normal;
                        }
                        else if (data.Equals((byte)CommsCharacter.End) && _MessageState.Equals(MessageState.Extracting))
                        {
                            _MessageState = MessageState.End;
                        }
                        else                                        // Message Id / Start of Payload
                        {
                            //_MessageId = data;
                            payLoad.Add(chrBuffer);

                            _MessageState = MessageState.Extracting;
                        }
                    }

                    if (_CommsState.Equals(CommsState.Normal))
                    {
                        if (_MessageState.Equals(MessageState.Extracting))
                        {
                            payLoad.Add(chrBuffer);
                        }
                        else if (_MessageState.Equals(MessageState.End))	// accumulate checksum
                        {
                            checkSum <<= 8;
                            checkSum += data;

                            chkSmCount++;

                            if (chkSmCount > 3)
                                _MessageState = MessageState.Complete;
                        }
                    }

                    _CommsState = CommsState.Normal;
                }
            }

            if (_MessageState.Equals(MessageState.Complete))
            {
                if (CalculateCRC(payLoad).Equals(checkSum))
                    RaiseCommandEvent(payLoad);
                else
                    throw new ApplicationException("Corrupt packet");
            }
        }



        public string ToHex(byte[] buffer)
        {
            string result = string.Empty;

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (byte item in buffer)
            {
                sb.Append($"{item:X2} ");
            }

            result = sb.ToString();

            return result.Trim();
        }


        // https://stackoverflow.com/questions/53438815/how-to-build-crc32-table-for-ogg#53442047

        /// <summary>
        /// MPEG2 CRC32 Table generation
        /// </summary>
        private UInt32[] CRC32()
        {
            const UInt32 polynomial = 0x04c11db7;

            uint[] checksumTable = new uint[0x100];

            UInt32 crc = 0;
            for (UInt32 c = 0; c < 0x100; c++)
            {
                crc = c << 24;

                for (int i = 0; i < 8; i++)
                {
                    UInt32 b = crc >> 31;
                    crc <<= 1;
                    crc ^= (0 - b) & polynomial;
                }

                checksumTable[c] = crc;
            }

            return checksumTable;
        }

        private UInt32 CalculateCRC(IEnumerable<byte> data)
        {
            UInt32 accumulator = 0xffffffff;

            foreach (byte element in data)
            {
                accumulator = _CRC32Table[(accumulator >> 24) ^ element] ^ (accumulator << 8);
            }

            return accumulator;
        }
    }
}
