﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

namespace ZiliConnection
{
    // Enum values do not matter they are only used as keys in a dictionary.

    /// <summary>
    /// Selection of available GUID
    /// </summary>
    public enum Guids
    {
        GUID_BTHPORT_DEVICE_INTERFACE = 0,
        GUID_DEVICE_APPLICATIONLAUNCH_BUTTON = 1,
        GUID_DEVICE_MEMORY = 2,
        GUID_DEVICE_MESSAGE_INDICATOR = 3,
        GUID_DEVICE_PROCESSOR = 4,
        GUID_DEVINTERFACE_COMPORT = 5,
        GUID_DEVINTERFACE_DISK = 6,
        GUID_DEVINTERFACE_HID = 7,
        GUID_DEVINTERFACE_MODEM = 8,
        GUID_DEVINTERFACE_NET = 9,
        GUID_DEVINTERFACE_PARALLEL = 10,
        GUID_DEVINTERFACE_PARCLASS = 11,
        GUID_DEVINTERFACE_SERENUM_BUS_ENUMERATOR = 12,
        GUID_DEVINTERFACE_STORAGEPORT = 13,
        GUID_DEVINTERFACE_USB_DEVICE = 14,
        GUID_DEVINTERFACE_USB_HOST_CONTROLLER = 15,
        GUID_DEVINTERFACE_USB_HUB = 16,
    }
}
// Copyright (c) 2021 Zilico Limited
