﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

namespace ZiliConnection
{
    /// <summary>
    /// Container for VID and PID strings
    /// </summary>
    internal class VendorProductIDs
    {
        private string _Vid;
        private string _Pid;


        /// <summary>
        /// Vendor ID
        /// </summary>
        public string Vid
        { get { return _Vid; } }

        /// <summary>
        /// Product ID
        /// </summary>
        public string Pid
        { get { return _Pid; } }


        /// <summary>
        /// Instantiate a new instance for VID and PID strings
        /// </summary>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        public VendorProductIDs(string vid,string pid)
        {
            _Vid = vid;
            _Pid = pid;
        }


        public override string ToString()
        {
            return $"VID {_Vid}    PID {_Pid}";
        }
    }
}
// Copyright (c) 2021 Zilico Limited
