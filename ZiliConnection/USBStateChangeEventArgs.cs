﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;

namespace ZiliConnection
{
    /// <summary>
    /// USB Device change event arguments
    /// </summary>
    public class USBStateChangeEventArgs : EventArgs
    {
        #region Fields

        private readonly StateChange _StateChangeEvent;

        #endregion

        #region Properties

        public StateChange State
        { get { return _StateChangeEvent; } }

        #endregion

        #region Constructor

        public USBStateChangeEventArgs(StateChange stateChangeEvent)
        {
            _StateChangeEvent = stateChangeEvent;
        }

        #endregion
    }
}
// Copyright (c) 2021 Zilico Limited
