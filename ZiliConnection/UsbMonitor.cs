﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;

namespace ZiliConnection
{
    /// <summary>
    /// Capture Windows messages
    /// </summary>
    public class Monitor : System.Windows.Forms.NativeWindow, IDisposable
    {
        #region Fields

        private string _Message;

        private bool _TraceEnabled;

        private bool _Disposed;

        private bool _IsRegistered;

        private bool _IsMonitoring;

        private bool _NotifyNodeChanges;

        private VendorProductIDs _VendorProductIDs;

        private string _DevicePath;

        private Guid _DeviceClass;

        private IntPtr _UsbEventHandle;

        private System.Threading.AutoResetEvent _MonitorWaitHandle;
        private System.Threading.AutoResetEvent _StartWaitHandle;

        private System.Threading.Tasks.Task _MonitorThread;
        private System.Threading.Tasks.Task _EventThread;

        private List<StateChange> _EventQueue;
        private bool _IsWaitingEvent;

        #endregion

        #region Properties

        /// <summary>
        /// Status message
        /// </summary>
        public string Message
        { get { return _Message; } }

        /// <summary>
        /// Debug trace output enabled
        /// </summary>
        public bool TraceEnabled
        {
            get { return _TraceEnabled; }
            set { _TraceEnabled = value; }
        }

        /// <summary>
        /// Indicates if events are to be raised for every Node Change
        /// </summary>
        public bool NotifyNodeChanges
        { get { return _NotifyNodeChanges; }
            set { _NotifyNodeChanges = value; }
        }

        /// <summary>
        /// Indicates whether Monitor is registered to receive windows messages
        /// </summary>
        public bool IsRegistered
        { get { return _IsRegistered; } }

        /// <summary>
        /// Indicates if windows messages are intercepted by a Native Window (virtual form)
        /// </summary>
        public bool IsMonitoring
        { get { return _IsMonitoring; } }


        /// <summary>
        /// Path to the registry key for the USB device
        /// </summary>
        public string DevicePath
        { get { return _DevicePath; } }


        //internal Guid DeviceClass
        //{
        //    get { return _DeviceClass; }
        //    set { _DeviceClass = value; }
        //}

        #endregion

        #region DeviceChangedEvent

        public delegate void DeviceNotifierEventHandler(object sender, USBStateChangeEventArgs e);

        public event DeviceNotifierEventHandler USBStateChangeEvent;

        public void OnUSBStateChangeEvent(USBStateChangeEventArgs e)
        {
            if (USBStateChangeEvent != null) USBStateChangeEvent(this, e);
        }

        private void RaiseUSBStateChangeEvent(StateChange state)
        {
            if ((_EventThread != null) && !_EventThread.IsCompleted)
            {
                if (_EventQueue == null)
                    _EventQueue = new List<StateChange>();

                _EventQueue.Add(state);
            }
            else
            {
                USBStateChangeEventArgs args = new USBStateChangeEventArgs(state);

                _EventThread = System.Threading.Tasks.Task.Run(() => OnUSBStateChangeEvent(args));
            }

            if ((_EventQueue != null) && (_EventQueue.Count > 0))
            {
                if (!_IsWaitingEvent)
                {
                    _IsWaitingEvent = true;
                    _EventThread.ContinueWith((StateChange) => ProcessEventQueue(state));
                }
            }
        }

        private void ProcessEventQueue(StateChange state)
        {
            if (_EventQueue.Count > 0)
            {
                StateChange stateChange = _EventQueue[0];
                _EventQueue.RemoveAt(0);

                _IsWaitingEvent = false;

                RaiseUSBStateChangeEvent(stateChange);
            }
        }

        #endregion

        #region ~ctor

        /// <summary>
        /// Instantiate new instance with an empty GUID
        /// </summary>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        public Monitor(string vid, string pid) : this(Guid.Empty, vid, pid, true) { }

        /// <summary>
        /// Instantiates and registers instance for windows messages with optional tracing
        /// </summary>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        /// <param name="enableTrace"></param>
        public Monitor(string vid, string pid, bool enableTrace) : this(Guid.Empty, vid, pid, enableTrace) { }


        /// <summary>
        /// Instantiates and registers instance for windows messages with optional tracing
        /// </summary>
        /// <param name="deviceClass"></param>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        /// <param name="enableTrace"></param>
        public Monitor(Guids deviceClass, string vid, string pid, bool enableTrace)
            : this(GuidLookup.GetValue(deviceClass), vid, pid, enableTrace) { }

        /// <summary>
        /// Instantiates and registers instance for windows messages
        /// </summary>
        /// <param name="deviceClass"></param>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        public Monitor(Guids deviceClass, string vid, string pid)
            : this(GuidLookup.GetValue(deviceClass), vid, pid, false) { }


        /// <summary>
        /// Instantiates and registers instance for windows messages with optional tracing
        /// </summary>
        /// <param name="deviceClass"></param>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        public Monitor(Guid deviceClass, string vid, string pid) : this(deviceClass, vid, pid, false) { }

        /// <summary>
        /// Instantiates and registers instance for windows messages
        /// </summary>
        /// <param name="deviceClass"></param>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        /// <param name="enableTrace"></param>
        public Monitor(Guid deviceClass, string vid, string pid, bool enableTrace)
        {
            _DeviceClass = deviceClass;

            _VendorProductIDs = new VendorProductIDs(vid, pid);

            _TraceEnabled = enableTrace;

            _Disposed = false;
            _IsRegistered = false;
            _IsMonitoring = false;

            _NotifyNodeChanges = false;

            _DevicePath = string.Empty;

            _UsbEventHandle = IntPtr.Zero;

            _MonitorWaitHandle = new System.Threading.AutoResetEvent(false);
            _StartWaitHandle = new System.Threading.AutoResetEvent(false);

            _IsWaitingEvent = false;

            AttachQueueHandle();
        }


        ~Monitor()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of readability and maintainability.
            this.Dispose(false);
        }


        /// <summary>
        /// Releases all resources used by the Monitor
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to take this object off the finalization queue
            // and prevent finalization code for this object from executing a second time.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.Dispose Disposing : " + disposing.ToString());

            if (!_Disposed)
            {
                // If disposing, dispose all managed and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (_IsMonitoring)
                        _MonitorWaitHandle.Set();
                }

                // If not disposing, only the following code is executed.

                // Call the appropriate methods to clean up unmanaged resources such as
                // window handles, or open files and streams

                // Destroy the NativeWindow handle
                if (_IsRegistered) 
                    this.ReleaseHandle();

                this.DestroyHandle();

                if (_IsRegistered)                                  // make sure unregistered after form handle disposed
                    UnRegisterDeviceNotifications();

                _Disposed = true;
            }
        }

        #endregion


        /// <summary>
        /// Retrieve an enumerated devices path
        /// </summary>
        /// <param name="vid"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        public bool GetDevicePath(string vid, string pid)
        {
            bool result = false;

            _Message = string.Empty;

            _DevicePath = string.Empty;

            IntPtr deviceInfoTable = IntPtr.Zero;
            IntPtr propertyValueBuffer = IntPtr.Zero;

            const Int32 INVALID_HANDLE_VALUE = -1;
            const int MAX_ENUMERATIONS = 255;

            try
            {
                if (string.IsNullOrEmpty(vid))
                    throw new ApplicationException("VID not set");

                if (string.IsNullOrEmpty(pid))
                    throw new ApplicationException("PID not set");

                vid = vid.ToUpper();
                pid = pid.ToUpper();

                #region Get pointer to Device Info Table (Device Information Sets)

                if (_DeviceClass.Equals(Guid.Empty))
                {
                    // Pointer to connected USB devices
                    deviceInfoTable = Win32.Adaptor.SetupDiGetClassDevs(IntPtr.Zero, "USB", IntPtr.Zero,
                        (int)(Win32.DeviceInfoGetClassFlag.Present | Win32.DeviceInfoGetClassFlag.AllClasses));
                }
                else
                {
                    // Pointer to connected device infos that match _DeviceClass GUID
                    deviceInfoTable = Win32.Adaptor.SetupDiGetClassDevs(ref _DeviceClass, null, IntPtr.Zero,
                        (uint)(Win32.DeviceInfoGetClassFlag.Present | Win32.DeviceInfoGetClassFlag.DeviceInterface));
                }

                if (deviceInfoTable.ToInt32().Equals(INVALID_HANDLE_VALUE))
                    throw new ApplicationException($"SetupDiGetClassDevs {Win32.Adaptor.GetLastError()}");

                #endregion

                Win32.SP_DEVICE_INTERFACE_DATA interfaceData = Win32.Adaptor.NewDeviceInterfaceData();

                Win32.SP_DEVINFO_DATA deviceInfoData = Win32.Adaptor.NewDeviceInfoData();

                uint interfaceIndex = 0;

                string previousVid = string.Empty;
                string previousPid = string.Empty;
                int previousCount = 0;

                bool gotDetail = false;

                while (!gotDetail)
                {
                    if (!_DeviceClass.Equals(Guid.Empty))           // Currently not working with empty GUID...
                    {
                        // Need interfaceData to find device path
                        if (!IsEnumerated(deviceInfoTable, interfaceIndex, ref interfaceData))
                            throw new ApplicationException("Device not found");
                    }

                    // Retrieve the deviceInfoData from enumerated devices in deviceInfoTable
                    if (!Win32.Adaptor.SetupDiEnumDeviceInfo(deviceInfoTable, interfaceIndex, ref deviceInfoData))
                    {
                        Win32.ReturnCode returnCode = Win32.Adaptor.GetLastError();

                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine($"ZiliConnection.Monitor.GetDevicePath SetupDiEnumDeviceInfo : {returnCode}");

                        if (returnCode.Equals(Win32.ReturnCode.SUCCESS))
                            throw new ApplicationException("Device not found");
                    }

                    UInt32 registryType = (UInt32)Win32.RegistryType.REG_SZ;
                    UInt32 requiredSize = 0;
                    UInt32 propertyBufferSize = Win32.Adaptor.MAX_PATH;

                    propertyValueBuffer = Win32.Adaptor.AllocateMemory((int)propertyBufferSize);

                    // Get the HARDWARE_ID, propertyValueBuffer string containing VID and PID
                    if (Win32.Adaptor.SetupDiGetDeviceRegistryProperty(deviceInfoTable, ref deviceInfoData,
                                                                       (uint)Win32.RegistryProperty.SPDRP_HARDWAREID,
                                                                       ref registryType, propertyValueBuffer,
                                                                       propertyBufferSize, ref requiredSize))
                    {
                        VendorProductIDs found = ParseVidPid(propertyValueBuffer);

                        if (found.Vid.Equals(vid) && found.Pid.Equals(pid))
                        {
                            Win32.SP_DEVICE_INTERFACE_DETAIL_DATA detailedInterfaceDataStructure = Win32.Adaptor.NewDeviceInterfaceDetailData(deviceInfoTable);

                            int structureSize = 0;

                            // First call to get structureSize, returns false
                            result = Win32.Adaptor.SetupDiGetDeviceInterfaceDetail(deviceInfoTable,
                                        ref interfaceData, IntPtr.Zero, 0, ref structureSize, ref deviceInfoData);

                            // Retrieve the Device Path
                            result = Win32.Adaptor.SetupDiGetDeviceInterfaceDetail(deviceInfoTable,
                                        ref interfaceData, ref detailedInterfaceDataStructure,
                                        structureSize, ref structureSize, ref deviceInfoData);

                            if (result)
                            {
                                string path = detailedInterfaceDataStructure.DevicePath.ToString();

                                if (!path.Contains("mi_00"))        // kludge for EG not removing redundant I2c interface
                                {
                                    _DevicePath = detailedInterfaceDataStructure.DevicePath.ToString();

                                    gotDetail = true;

                                    if (_TraceEnabled)
                                        System.Diagnostics.Trace.WriteLine($"ZiliConnection.Monitor.GetDevicePath : {_DevicePath}");
                                }
                            }
                        }
                        else
                        {
                            #region check how many times same Vid and Pid returned indicating end of table

                            if (previousVid.Equals(found.Vid) && previousPid.Equals(found.Pid))
                                previousCount++;
                            else
                            {
                                previousCount = 0;

                                previousVid = found.Vid;
                                previousPid = found.Pid;
                            }

                            if (previousCount > MAX_ENUMERATIONS)   // break out of loop
                                gotDetail = true;

                            #endregion
                        }
                    }

                    Win32.Adaptor.ReleaseMemory(propertyValueBuffer);

                    propertyValueBuffer = IntPtr.Zero;

                    interfaceIndex++;
                }
            }
            catch (Exception e)
            {
                _Message = e.Message;

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"ZiliConnection.Monitor.GetDevicePath : {_Message}");
            }
            finally
            {
                CleanUp(deviceInfoTable, propertyValueBuffer);
            }

            return result;
        }

        /// <summary>
        /// Start the Device Change Notification thread
        /// </summary>
        public bool Start()
        {
            bool result = false;

            const int ThreadTimeOut = 5000;

            _Message = string.Empty;

            try
            {
                _StartWaitHandle.Reset();

                UnRegisterDeviceNotifications();                    // remove existing registration

                if (_IsMonitoring)
                {
                    if (!_StartWaitHandle.WaitOne(ThreadTimeOut))   // wait for current task to stop.
                    {
                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine($"ZiliConnection.Monitor.Start : {_MonitorThread.Status}");

                        throw new ApplicationException("Unable to terminate Device Change Notification Task");
                    }
                }

                _MonitorThread = System.Threading.Tasks.Task.Run(DeviceChangeNotification);

                result = true;
            }
            catch (Exception e)
            {
                _Message = e.Message;

                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"ZiliConnection.Monitor.Start : {_Message}");
            }

            return result;
        }



        /// <summary>
        /// The Monitoring thread
        /// </summary>
        private void DeviceChangeNotification()
        {
            _IsMonitoring = true;

            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine($"ZiliConnection.Monitor.DeviceChangeNotification Thread : {System.Threading.Thread.CurrentThread.Name}");

            if (string.IsNullOrEmpty(_DevicePath))
            {
                StateChange state = StateChange.Nodes_Changed;

                // can't get path if not connected (enumerated)
                if (GetDevicePath(_VendorProductIDs.Vid, _VendorProductIDs.Pid))
                    state = StateChange.Arrival;
                else
                    state = StateChange.Remove_Complete;

                RaiseUSBStateChangeEvent(state);
            }

            ChangeHandle();

            _MonitorWaitHandle.WaitOne();                           // stay here until disposed or restarted

            _IsMonitoring = false;

            _StartWaitHandle.Set();                                 // indicate method complete

            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.DeviceChangeNotification Thread : Complete");
        }

        

        /// <summary>
        /// Are any _DeviceClass connected
        /// </summary>
        /// <param name="deviceInfoTable"></param>
        /// <param name="interfaceIndex"></param>
        /// <param name="interfaceData"></param>
        /// <returns></returns>
        private bool IsEnumerated(IntPtr deviceInfoTable, uint interfaceIndex, ref Win32.SP_DEVICE_INTERFACE_DATA interfaceData)
        {
            return Win32.Adaptor.SetupDiEnumDeviceInterfaces(deviceInfoTable, IntPtr.Zero, ref _DeviceClass,
                                                             interfaceIndex, ref interfaceData);
        }

        /// <summary>
        /// Retrieve VID and PID from propertyValueBuffer
        /// </summary>
        /// <param name="propertyValueBuffer"></param>
        /// <returns></returns>
        private VendorProductIDs ParseVidPid(IntPtr propertyValueBuffer)
        {
            string foundVid = string.Empty;
            string foundPid = string.Empty;

            try
            {
                string description = Win32.Adaptor.ParseString(propertyValueBuffer);

                description = description.ToUpper();

                int offset = 4;                             // length of search string
                int vidIdx = description.IndexOf("VID_");
                int pidIdx = description.IndexOf("PID_");

                if ((vidIdx < 0) || (pidIdx < 0))           // not found
                {
                    offset = 3;
                    vidIdx = description.IndexOf("VID");
                    pidIdx = description.IndexOf("PID");

                    if ((vidIdx < 0) || (pidIdx < 0))           // not found
                        throw new ApplicationException($"DeviceRegistryProperty missing {description}");
                }

                vidIdx += offset;                           // length of "VID_"
                pidIdx += offset;                           // length of "PID_"

                if ((description.Length < (vidIdx + 4)) || (description.Length < (pidIdx + 4)))
                    throw new ApplicationException($"Malformed DeviceRegistryProperty {description}");

                foundVid = description.Substring(vidIdx, 4);
                foundPid = description.Substring(pidIdx, 4);
            }
            catch (Exception e)
            {
                if (_TraceEnabled)
                    System.Diagnostics.Trace.WriteLine($"ZiliConnection.Monitor.ParseVidPid : {e.Message}");
            }

            return new VendorProductIDs(foundVid, foundPid);
        }

        /// <summary>
        /// Free memory used by DeviceInfoList and propertyValueBuffer
        /// </summary>
        /// <param name="deviceInfoTable"></param>
        /// <param name="propertyValueBuffer"></param>
        private void CleanUp(IntPtr deviceInfoTable, IntPtr propertyValueBuffer)
        {
            try
            {
                // If we had a valid DeviceInfoTable, we should dispose of it
                if (!deviceInfoTable.Equals(IntPtr.Zero))
                {
                    if (!Win32.Adaptor.SetupDiDestroyDeviceInfoList(deviceInfoTable))
                    {
                        if (!string.IsNullOrEmpty(_Message))
                            _Message = $"Destroy Device Info List : {Win32.Adaptor.GetLastError()}";
                    }
                }

                if (!propertyValueBuffer.Equals(IntPtr.Zero))
                    Win32.Adaptor.ReleaseMemory(propertyValueBuffer);
            }
            catch (Exception)
            { }
        }

        #region Registration Initialisation

        /// <summary>
        /// Create a handle into the Windows message queue
        /// </summary>
        private void AttachQueueHandle()
        {
            // Create a handle into the Windows message queue, triggers OnHandleChange() --> ChangeHandle()
            System.Windows.Forms.CreateParams cp = new System.Windows.Forms.CreateParams();
            this.CreateHandle(cp);
        }

        /// <summary>
        /// Try to register if unregistered or unregister if registered
        /// </summary>
        private void ChangeHandle()
        {
            if (_IsRegistered)
                UnRegisterDeviceNotifications();
            else
                RegisterDeviceNotifications();
        }

        #endregion

        #region Device Notification Registration

        /// <summary>
        /// <para>Registers a window to receive windows messages when a device is inserted/removed.</para> 
        /// <para>Need to call this from a form when its handle has been created, not in the form constructor.</para>
        /// <para>Use form's OnHandleCreated override.</para>
        /// </summary>
        private void RegisterDeviceNotifications()
        {
            IntPtr handle = IntPtr.Zero;

            if (_DeviceClass != Guid.Empty)
                handle = Win32.Adaptor.NewDeviceBroadcastInterface(this.Handle, _DeviceClass);

            _UsbEventHandle = handle;

            if (_UsbEventHandle != IntPtr.Zero)
                _IsRegistered = true;

            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine($"ZiliConnection.Monitor.ChangeHandle     Registered : {_IsRegistered}");
        }

        /// <summary>
        /// Remove registration to receive notifications
        /// </summary>
        private void UnRegisterDeviceNotifications()
        {
            if (_IsRegistered)
            {
                if (_UsbEventHandle != IntPtr.Zero)
                {
                    if (Win32.Adaptor.UnregisterDeviceNotification(_UsbEventHandle))
                    {
                        _UsbEventHandle = IntPtr.Zero;
                        _IsRegistered = false;
                    }
                }
                else
                    _IsRegistered = false;

                if (_IsMonitoring && (!_IsRegistered))
                    _MonitorWaitHandle.Set();                       // release the monitor thread - don't think needed
            }

            if (_TraceEnabled)
                System.Diagnostics.Trace.WriteLine($"ZiliConnection.Monitor.UnRegisterHandle Registered : {_IsRegistered}");
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Specifies a notification method that is called when the handle for a window is changed
        /// </summary>
        protected override void OnHandleChange()
        {
            base.OnHandleChange();

            ChangeHandle();
        }

        /// <summary>
        /// Windows message processor override
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            const int WM_DEVICECHANGE = 0x0219;                     // 537

            if (m.Msg.Equals(WM_DEVICECHANGE))
            {
                switch ((Win32.WindowsMessageDeviceChange)m.WParam)
                {
                    case Win32.WindowsMessageDeviceChange.DBT_CONFIGCHANGECANCELED:
                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc : DBT_CONFIGCHANGECANCELED");
                        break;
                    case Win32.WindowsMessageDeviceChange.DBT_CONFIGCHANGED:
                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc : DBT_CONFIGCHANGED");
                        break;
                    case Win32.WindowsMessageDeviceChange.DBT_CUSTOMEVENT:
                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc : DBT_CUSTOMEVENT");
                        break;
                    case Win32.WindowsMessageDeviceChange.DBT_DEVICEARRIVAL:

                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc : DBT_DEVICEARRIVAL");

                        try
                        {
                            Win32.DeviceBroadcastInterface device = (Win32.DeviceBroadcastInterface)m.GetLParam(typeof(Win32.DeviceBroadcastInterface));

                            if (device.ClassGuid.Equals(_DeviceClass))
                            {
                                if (string.IsNullOrEmpty(_DevicePath))
                                    GetDevicePath(_VendorProductIDs.Vid, _VendorProductIDs.Pid);

                                if ((!string.IsNullOrEmpty(_DevicePath)) && _DevicePath.Trim().ToUpper().Equals(device.Name.Trim().ToUpper()))
                                    RaiseUSBStateChangeEvent(StateChange.Arrival);
                            }
                        }
                        catch (Exception e)
                        {
                            if (_TraceEnabled)
                                System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc DBT_DEVICEARRIVAL Error : " + e.Message);
                        }

                        break;

                    case Win32.WindowsMessageDeviceChange.DBT_DEVICEQUERYREMOVE:
                        // Device is about to be removed, any application can cancel the removal
                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc : DBT_DEVICEQUERYREMOVE");
                        break;
                    case Win32.WindowsMessageDeviceChange.DBT_DEVICEQUERYREMOVEFAILED:
                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc : DBT_DEVICEQUERYREMOVEFAILED");
                        break;
                    case Win32.WindowsMessageDeviceChange.DBT_DEVICEREMOVECOMPLETE:

                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc : DBT_DEVICEREMOVECOMPLETE");

                        try
                        {
                            Win32.DeviceBroadcastInterface device = (Win32.DeviceBroadcastInterface)m.GetLParam(typeof(Win32.DeviceBroadcastInterface));

                            if (_DevicePath.Trim().ToUpper().Equals(device.Name.Trim().ToUpper()))
                                RaiseUSBStateChangeEvent(StateChange.Remove_Complete);
                        }
                        catch (Exception e)
                        {
                            if (_TraceEnabled)
                                System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc DBT_DEVICEREMOVECOMPLETE Error : " + e.Message);
                        }

                        break;

                    case Win32.WindowsMessageDeviceChange.DBT_DEVICEREMOVEPENDING:
                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc : DBT_DEVICEREMOVEPENDING");
                        break;
                    case Win32.WindowsMessageDeviceChange.DBT_DEVICETYPESPECIFIC:
                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc : DBT_DEVICETYPESPECIFIC");
                        break;
                    case Win32.WindowsMessageDeviceChange.DBT_DEVNODES_CHANGED:

                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc : DBT_DEVNODES_CHANGED");

                        if (_NotifyNodeChanges)
                            RaiseUSBStateChangeEvent(StateChange.Nodes_Changed);

                        break;

                    case Win32.WindowsMessageDeviceChange.DBT_QUERYCHANGECONFIG:
                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc : DBT_QUERYCHANGECONFIG");
                        break;
                    case Win32.WindowsMessageDeviceChange.DBT_USERDEFINED:
                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc : DBT_USERDEFINED");
                        break;
                    default:
                        if (_TraceEnabled)
                            System.Diagnostics.Trace.WriteLine("ZiliConnection.Monitor.WndProc : Unknown " + m.WParam.ToString());
                        break;
                }
            }

            base.WndProc(ref m);
        }

        #endregion
    }
}
// Copyright (c) 2021 Zilico Limited
