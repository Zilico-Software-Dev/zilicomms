﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Runtime.InteropServices;

namespace ZiliConnection.Win32
{
    /// <summary>
    /// <para>DEV_BROADCAST_HDR structure</para>
    /// <para>Used when registering a window to receive messages about devices added or removed from the system.</para>
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode, Pack = 1)]
    internal class DeviceBroadcastInterface
    {
        public int Size;
        public int DeviceType;
        public int Reserved;
        public Guid ClassGuid;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string Name;
    }
}
// Copyright (c) 2021 Zilico Limited
