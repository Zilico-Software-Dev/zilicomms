﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;

namespace ZiliConnection.Win32
{
    /// <summary>
    /// Device Info Data Structure
    /// </summary>
    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 1)]
    internal struct SP_DEVINFO_DATA
    {
        public uint Size;
        public System.Guid ClassGuid;
        public uint DevInst;
        public UIntPtr Reserved;

        public void Initialize()
        {
            this.Size = (uint)System.Runtime.InteropServices.Marshal.SizeOf(typeof(SP_DEVINFO_DATA));
        }
    }
}
// Copyright (c) 2021 Zilico Limited
