﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System.Runtime.InteropServices;

namespace ZiliConnection.Win32
{
    /// <summary>
    /// Device Interface Detail Data
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Auto)]
    internal struct SP_DEVICE_INTERFACE_DETAIL_DATA
    {
        public uint Size;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = (int)Adaptor.MAX_PATH)]
        public string DevicePath;

        public void Initialize()
        {
            this.Size = (uint)Marshal.SizeOf(typeof(SP_DEVICE_INTERFACE_DETAIL_DATA));
        }
    }
}
// Copyright (c) 2021 Zilico Limited
