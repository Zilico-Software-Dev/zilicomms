﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

namespace ZiliConnection.Win32
{
    // https://docs.microsoft.com/en-us/windows/win32/api/dbt/ns-dbt-dev_broadcast_hdr
    internal enum DeviceBroadcastType : int
    {
        /// <summary>
        /// OEM- or IHV-defined device type. This structure is a DEV_BROADCAST_OEM structure.
        /// </summary>
        DBT_DEVTYP_OEM = 0x00000000,
        /// <summary>
        /// Logical volume. This structure is a DEV_BROADCAST_VOLUME structure.
        /// </summary>
        DBT_DEVTYP_VOLUME = 0x00000002,
        /// <summary>
        /// Port device (serial or parallel). This structure is a DEV_BROADCAST_PORT structure.
        /// </summary>
        DBT_DEVTYP_PORT = 0x00000003,
        /// <summary>
        /// Class of devices. This structure is a DEV_BROADCAST_DEVICEINTERFACE structure.
        /// </summary>
        DBT_DEVTYP_DEVICEINTERFACE = 0x00000005,
        /// <summary>
        /// File system handle. This structure is a DEV_BROADCAST_HANDLE structure.
        /// </summary>
        DBT_DEVTYP_HANDLE = 0x00000006,
    }
}
// Copyright (c) 2021 Zilico Limited
