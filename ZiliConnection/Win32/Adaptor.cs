﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Runtime.InteropServices;

namespace ZiliConnection.Win32
{
    /// <summary>
    /// Adaptor for Win32 InteropServices 
    /// </summary>
    internal static class Adaptor
    {
        public const uint MAX_PATH = 260;

        #region Wrapper methods for allocating, releasing memory, parsing strings and getting last error

        /// <summary>
        /// Allocate block of memory and return pointer
        /// </summary>
        /// <param name="bufferSize"></param>
        /// <returns></returns>
        public static IntPtr AllocateMemory(int bufferSize)
        {
            return Marshal.AllocHGlobal(bufferSize);
        }

        /// <summary>
        /// Release memory block
        /// </summary>
        /// <param name="memoryPointer"></param>
        public static void ReleaseMemory(IntPtr memoryPointer)
        {
            Marshal.FreeHGlobal(memoryPointer);
        }

        /// <summary>
        /// Copies all characters up to first null from unmanaged memory
        /// </summary>
        /// <param name="memoryPointer"></param>
        /// <returns></returns>
        public static string ParseString(IntPtr memoryPointer)
        {
            return Marshal.PtrToStringAnsi(memoryPointer);
        }

        /// <summary>
        /// Generate size of memoryPointer (SP_DEVICE_INTERFACE_DETAIL_DATA structure) i.e. 6 
        /// </summary>
        /// <param name="memoryPointer"></param>
        /// <returns></returns>
        public static uint PointerSize(IntPtr memoryPointer)
        {
            // http://www.pinvoke.net/default.aspx/setupapi.setupdigetdeviceinterfacedetail

            uint result = 0;

            if (IntPtr.Size.Equals(8))              // for 64 bit operating systems
                result = (uint)Marshal.SizeOf(memoryPointer);
            else
                result = (uint)Marshal.SystemDefaultCharSize + (uint)Marshal.SizeOf(memoryPointer);

            return result;
        }


        /// <summary>
        /// String representation of the last Win32 API calls return code
        /// </summary>
        /// <returns></returns>
        public static ReturnCode GetLastError()
        {
            return (ReturnCode)Marshal.GetLastWin32Error();
        }

        #endregion

        #region Structure factories

        /// <summary>
        /// Initialise data structure SP_DEVICE_INTERFACE_DATA
        /// </summary>
        /// <returns></returns>
        public static SP_DEVICE_INTERFACE_DATA NewDeviceInterfaceData()
        {
            Win32.SP_DEVICE_INTERFACE_DATA interfaceData = new Win32.SP_DEVICE_INTERFACE_DATA();

            interfaceData.Initialize();

            return interfaceData;
        }

        /// <summary>
        /// Initialise data structure SP_DEVINFO_DATA
        /// </summary>
        /// <returns></returns>
        public static SP_DEVINFO_DATA NewDeviceInfoData()
        {
            Win32.SP_DEVINFO_DATA deviceInfoData = new Win32.SP_DEVINFO_DATA();

            deviceInfoData.Initialize();

            return deviceInfoData;
        }

        /// <summary>
        /// Initialise data structure SP_DEVICE_INTERFACE_DETAIL_DATA
        /// </summary>
        /// <param name="deviceInfoTable"></param>
        /// <returns></returns>
        public static SP_DEVICE_INTERFACE_DETAIL_DATA NewDeviceInterfaceDetailData(IntPtr deviceInfoTable)
        {
            Win32.SP_DEVICE_INTERFACE_DETAIL_DATA detailedInterfaceDataStructure = new Win32.SP_DEVICE_INTERFACE_DETAIL_DATA();

            detailedInterfaceDataStructure.Initialize();

            detailedInterfaceDataStructure.Size = PointerSize(deviceInfoTable);

            return detailedInterfaceDataStructure;
        }

        /// <summary>
        /// Returns a handle to a DEV_BROADCAST_HDR for the reception of notifications
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="deviceClass"></param>
        /// <returns></returns>
        public static IntPtr NewDeviceBroadcastInterface(IntPtr hWnd, Guid deviceClass)
        {
            IntPtr result = IntPtr.Zero;

            Win32.DeviceBroadcastInterface broadcastInterface = new Win32.DeviceBroadcastInterface();

            broadcastInterface.Size = Marshal.SizeOf(broadcastInterface);
            broadcastInterface.ClassGuid = deviceClass;
            broadcastInterface.DeviceType = (int)DeviceBroadcastType.DBT_DEVTYP_DEVICEINTERFACE;
            broadcastInterface.Reserved = 0;

            result = Win32.Adaptor.RegisterDeviceNotification(hWnd, broadcastInterface,
                                                              (uint)Win32.DeviceNotifyHandle.DEVICE_NOTIFY_WINDOW_HANDLE);

            return result;
        }

        #endregion

        #region Get Device Path, SETUPAPI calls

        /// <summary>
        /// Allocates an InfoSet memory block within Windows that contains device details
        /// </summary>
        /// <param name="ClassGuid">class GUID</param>
        /// <param name="Enumerator">The PnP enumerator identifier. Not used</param>
        /// <param name="hwndParent">A handle to the top-level window to be used for a user interface associated
        /// with installing a device instance into the device information set. Not used</param>
        /// <param name="Flags">Flags describing the filtering to use (DiCfg)</param>
        /// <returns>A reference to the InfoSet</returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SetupDiGetClassDevs
        (ref System.Guid ClassGuid, string Enumerator, IntPtr hwndParent, UInt32 Flags);

        /// <summary>
        /// Allocates an InfoSet memory block within Windows that contains device details
        /// </summary>
        /// <param name="ClassGuid">Pointer to a Class GUID</param>
        /// <param name="Enumerator">The PnP enumerator identifier.</param>
        /// <param name="hwndParent">A handle to the top-level window used for a user interface associated 
        /// with installing a device instance into the device information set. Not used</param>
        /// <param name="Flags">Flags describing the filtering to use (DiCfg)</param>
        /// <returns>A reference to the InfoSet</returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SetupDiGetClassDevs
        (IntPtr ClassGuid, string Enumerator, IntPtr hwndParent, int Flags);


        /// <summary>
        /// Free InfoSet memory block
        /// </summary>
        /// <param name="DeviceInfoSet">handle to a device info list to deallocate from RAM.</param>
        /// <returns>True if successful</returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Auto)]
        public static extern bool SetupDiDestroyDeviceInfoList(IntPtr DeviceInfoSet);


        /// <summary>
        /// Gets the DeviceInterfaceData for a device from an InfoSet
        /// </summary>
        /// <param name="DeviceInfoSet">InfoSet to access</param>
        /// <param name="DeviceInfoData">Not used</param>
        /// <param name="InterfaceClassGuid">Device class guid</param>
        /// <param name="MemberIndex">Index into InfoSet for device</param>
        /// <param name="DeviceInterfaceData">Fill in a SP_DEVICE_INTERFACE_DATA struct.</param>
        /// <returns>True if successful, false if not (e.g. when index is passed end of InfoSet)</returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Auto)]
        public static extern bool SetupDiEnumDeviceInterfaces
        (
            IntPtr DeviceInfoSet,
            IntPtr DeviceInfoData,
            ref System.Guid InterfaceClassGuid,
            UInt32 MemberIndex,
            ref SP_DEVICE_INTERFACE_DATA DeviceInterfaceData
        );

        /// <summary>
        /// Gets the SP_DEVINFO_DATA
        /// </summary>
        /// <param name="DeviceInfoSet">InfoSet to access</param>
        /// <param name="MemberIndex">Index into InfoSet for device</param>
        /// <param name="DeviceInfoData">SP_DEVINFO_DATA struct to populate</param>
        /// <returns>True if successful</returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Auto)]
        public static extern bool SetupDiEnumDeviceInfo
        (IntPtr DeviceInfoSet, UInt32 MemberIndex, ref SP_DEVINFO_DATA DeviceInfoData);

        // http://msdn.microsoft.com/en-us/library/windows/hardware/ff551967(v=vs.85).aspx
        /// <summary>
        ///  Retrieve a specified Plug and Play device property
        /// </summary>
        /// <param name="DeviceInfoSet">InfoSet to access</param>
        /// <param name="DeviceInfoData">SP_DEVINFO_DATA struct that specifies the device information element in DeviceInfoSet</param>
        /// <param name="Property">property to be retrieved</param>
        /// <param name="PropertyRegDataType">A pointer to a variable that receives the data type of the property that is being retrieved</param>
        /// <param name="PropertyBuffer">A pointer to a buffer that receives the property that is being retrieved</param>
        /// <param name="PropertyBufferSize">The size, in bytes, of the PropertyBuffer buffer</param>
        /// <param name="RequiredSize">The required size, in bytes, of the PropertyBuffer </param>
        /// <returns></returns>
        [DllImport("setupapi.dll")]
        public static extern bool SetupDiGetDeviceRegistryProperty
        (
            IntPtr DeviceInfoSet, 
            ref SP_DEVINFO_DATA DeviceInfoData, 
            UInt32 Property, 
            ref UInt32 PropertyRegDataType, 
            IntPtr PropertyBuffer, 
            UInt32 PropertyBufferSize, 
            ref UInt32 RequiredSize
        );


        /// <summary>
        /// Used to get the RequiredSize of the struct you need to send
        /// </summary>
        /// <param name="DeviceInfoSet">InfoSet to access</param>
        /// <param name="DeviceInterfaceData">DeviceInterfaceData to use</param>
        /// <param name="DeviceInterfaceDetailData">DeviceInterfaceDetailData to fill with data</param>
        /// <param name="DeviceInterfaceDetailSize">The size of DeviceInterfaceDetailData</param>
        /// <param name="RequiredSize">Returns the required size of DeviceInterfaceDetailData when DeviceInterfaceDetailSize is set to zero</param>
        /// <param name="DeviceInfoData">SP_DEVINFO_DATA</param>
        /// <returns>True if successful</returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Auto)]
        public static extern bool SetupDiGetDeviceInterfaceDetail
        (
            IntPtr DeviceInfoSet, 
            ref SP_DEVICE_INTERFACE_DATA DeviceInterfaceData, 
            IntPtr DeviceInterfaceDetailData, 
            int DeviceInterfaceDetailSize, 
            ref int RequiredSize, 
            ref SP_DEVINFO_DATA DeviceInfoData
        );

        /// <summary>
        /// Gets the interface detail from a DeviceInterfaceData. This is pretty much the device path.
        /// </summary>
        /// <param name="DeviceInfoSet">InfoSet to access</param>
        /// <param name="DeviceInterfaceData">DeviceInterfaceData to use</param>
        /// <param name="DeviceInterfaceDetailData">DeviceInterfaceDetailData to fill with data</param>
        /// <param name="DeviceInterfaceDetailSize">The size of DeviceInterfaceDetailData</param>
        /// <param name="RequiredSize">The required size of DeviceInterfaceDetailSize</param>
        /// <param name="DeviceInfoData">SP_DEVINFO_DATA</param>
        /// <returns>True if successful</returns>
        [DllImport("setupapi.dll", CharSet = CharSet.Auto)]
        public static extern bool SetupDiGetDeviceInterfaceDetail
        (
            IntPtr DeviceInfoSet, 
            ref SP_DEVICE_INTERFACE_DATA DeviceInterfaceData, 
            ref SP_DEVICE_INTERFACE_DETAIL_DATA DeviceInterfaceDetailData, 
            int DeviceInterfaceDetailSize, 
            ref int RequiredSize, 
            ref SP_DEVINFO_DATA DeviceInfoData
        );

        #endregion

        #region Windows Message Registration for (dis)connection, USER32 calls

        //public static extern IntPtr RegisterDeviceNotification(IntPtr hWnd, IntPtr NotificationFilter, UInt32 Flags);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr RegisterDeviceNotification(IntPtr hWnd,
                                                               DeviceBroadcastInterface NotificationFilter,
                                                               UInt32 Flags);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern bool UnregisterDeviceNotification(IntPtr hHandle);

        #endregion
    }
}
// Copyright (c) 2021 Zilico Limited 
