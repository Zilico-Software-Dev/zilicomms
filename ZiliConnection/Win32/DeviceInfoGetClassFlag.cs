﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

namespace ZiliConnection.Win32
{
    //https://docs.microsoft.com/en-us/windows/win32/api/setupapi/nf-setupapi-setupdigetclassdevsw
    //http://www.pinvoke.net/default.aspx/Enums/DIGCF%20.html
    /// <summary>
    /// DIGCF
    /// </summary>
    internal enum DeviceInfoGetClassFlag
    {
        /// <summary>
        /// Return only the device that is associated with the system default device interface, 
        /// if one is set, for the specified device interface classes.
        /// </summary>
        Default = 0x00000001,

        /// <summary>
        /// Return only devices that are currently present
        /// </summary>
        Present = 0x00000002,

        /// <summary>
        /// Gets all classes, ignores the guid
        /// </summary>
        AllClasses = 0x00000004,

        /// <summary>
        /// Gets only classes that are part of the current hardware profile
        /// </summary>
        Profile = 0x00000008,

        /// <summary>
        /// Return devices that expose interfaces of the interface class that are specified by ClassGuid.
        /// </summary>
        DeviceInterface = 0x00000010,
    }
}
// Copyright (c) 2021 Zilico Limited
