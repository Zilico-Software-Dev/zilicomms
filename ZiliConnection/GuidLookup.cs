﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

using System;
using System.Collections.Generic;

namespace ZiliConnection
{
    /// <summary>
    /// <para>Dictionary of GUIDs from Microsoft docs area</para>
    /// <para>https://docs.microsoft.com/en-us/windows-hardware/drivers/install/system-defined-device-setup-classes-available-to-vendors</para>
    /// <para>Also e.g.</para>
    /// <para>https://docs.microsoft.com/en-us/windows-hardware/drivers/install/guid-devinterface-comport</para>
    /// </summary>
    internal static class GuidLookup
    {
        private static Dictionary<Guids, string> _Lookup = new Dictionary<Guids, string>()
        {
            {Guids.GUID_BTHPORT_DEVICE_INTERFACE, "0850302A-B344-4fda-9BE9-90576B8D46F0"} ,
            {Guids.GUID_DEVICE_APPLICATIONLAUNCH_BUTTON, "629758EE-986E-4D9E-8E47-DE27F8AB054D"},
            {Guids.GUID_DEVICE_MEMORY, "3FD0F03D-92E0-45FB-B75C-5ED8FFB01021"},
            {Guids.GUID_DEVICE_MESSAGE_INDICATOR, "CD48A365-FA94-4CE2-A232-A1B764E5D8B4"},
            {Guids.GUID_DEVICE_PROCESSOR, "97FADB10-4E33-40AE-359C-8BEF029DBDD0"},
            {Guids.GUID_DEVINTERFACE_COMPORT, "86E0D1E0-8089-11D0-9CE4-08003E301F73"},
            {Guids.GUID_DEVINTERFACE_DISK, "53F56307-B6BF-11D0-94F2-00A0C91EFB8B"},
            {Guids.GUID_DEVINTERFACE_HID, "4D1E55B2-F16F-11CF-88CB-001111000030"},
            {Guids.GUID_DEVINTERFACE_MODEM, "2C7089AA-2E0E-11D1-B114-00C04FC2AAE4"},
            {Guids.GUID_DEVINTERFACE_NET, "CAC88484-7515-4C03-82E6-71A87ABAC361"},
            {Guids.GUID_DEVINTERFACE_PARALLEL, "97F76EF0-F883-11D0-AF1F-0000F800845C"},
            {Guids.GUID_DEVINTERFACE_PARCLASS, "811FC6A5-F728-11D0-A537-0000F8753ED1"},
            {Guids.GUID_DEVINTERFACE_SERENUM_BUS_ENUMERATOR, "4D36E978-E325-11CE-BFC1-08002BE10318"},
            {Guids.GUID_DEVINTERFACE_STORAGEPORT, "2ACCFE60-C130-11D2-B082-00A0C91EFB8B"},
            {Guids.GUID_DEVINTERFACE_USB_DEVICE, "A5DCBF10-6530-11D2-901F-00C04FB951ED"},
            {Guids.GUID_DEVINTERFACE_USB_HOST_CONTROLLER, "3ABF6F2D-71C4-462A-8A92-1E6861E6AF27"},
            {Guids.GUID_DEVINTERFACE_USB_HUB, "F18A0E88-C30C-11D0-8815-00A0C906BED8"}
        };

        /// <summary>
        /// Return GUID for Device Class, empty if not found
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static Guid GetValue(Guids key)
        {
            Guid result = Guid.Empty;

            if (_Lookup.TryGetValue(key, out string guid))
                result = new Guid(guid);

            return result;
        }
    }
}
// Copyright (c) 2021 Zilico Limited
