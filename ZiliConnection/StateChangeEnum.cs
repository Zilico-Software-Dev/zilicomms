﻿// Company : Zilico Limited
// Copyright (c) 2021 Zilico Limited
// Author : P Kaye
// Date : 2021

namespace ZiliConnection
{
    public enum StateChange
    {
        /// <summary>
        /// Device has been plugged in
        /// </summary>
        Arrival = 0,
        /// <summary>
        /// Device has been removed
        /// </summary>
        Remove_Complete = 1,
        /// <summary>
        /// Indicates a change of enumerated USB devices
        /// </summary>
        Nodes_Changed = 2,
    }
}
// Copyright (c) 2021 Zilico Limited
