﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZiliCator
{
    static class Program
    {
        // Single instance application
        // Mutex can be made static so that GC doesn't recycle
        // same effect with GC.KeepAlive(mutex) at the end of main
        static System.Threading.Mutex mutex = new System.Threading.Mutex(false, "ZiliCator");


        [System.Runtime.InteropServices.DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        private static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            System.Reflection.Assembly executingAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Reflection.AssemblyTitleAttribute title = (System.Reflection.AssemblyTitleAttribute)System.Reflection.AssemblyTitleAttribute.GetCustomAttribute(executingAssembly, typeof(System.Reflection.AssemblyTitleAttribute));

            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                #region Attach trace to console

                if (System.Diagnostics.Debugger.IsAttached)
                {
                    System.Diagnostics.Trace.Listeners.Clear();
                    System.Diagnostics.Trace.Listeners.Add(new System.Diagnostics.TextWriterTraceListener(Console.Out));
                }

                #endregion


                Main main = new Main(title.Title);

                main.ShowScreen();
                //main.Start();

                //Application.Run(new FormMain(title.Title));


                mutex.ReleaseMutex();
            }
            else
            {
                // find instance and activate...
                IntPtr hWndOtherOne = FindWindowByCaption(IntPtr.Zero, title.Title);

                if (!SetForegroundWindow(hWndOtherOne))
                    MessageBox.Show("Application already started!", title.Title,
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
