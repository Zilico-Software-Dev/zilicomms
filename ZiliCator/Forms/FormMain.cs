﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZiliComms.Commands;

namespace ZiliCator
{
    public partial class FormMain : Form
    {
        private string _ApplicationTitle;


        #region Adaptor Selection Event

        internal event EventHandler<AdaptorType> AdaptorSelectionEvent;

        protected virtual void OnAdaptorSelectionEvent(object e)
        {
            if (AdaptorSelectionEvent != null) AdaptorSelectionEvent(this, (AdaptorType)e);
        }

        private void RaiseAdaptorSelectionEvent(AdaptorType adaptorType)
        {
            OnAdaptorSelectionEvent(adaptorType);
        }

        #endregion

        #region Change Baud Rate Event

        internal event EventHandler<UInt32> ChangeBaudRateEvent;

        protected virtual void OnChangeBaudRateEvent(object e)
        {
            if (ChangeBaudRateEvent != null) ChangeBaudRateEvent(this, (UInt32)e);
        }

        private void RaiseChangeBaudRateEvent(UInt32 baudRate)
        {
            OnChangeBaudRateEvent(baudRate);
        }

        #endregion

        #region Single Byte Mode Event

        internal event EventHandler<bool> ChangeSingleByteModeEvent;

        protected virtual void OnChangeSingleByteModeEvent(object e)
        {
            if (ChangeSingleByteModeEvent != null) ChangeSingleByteModeEvent(this, (bool)e);
        }

        private void RaiseChangeSingleByteModeEvent(bool singleByteMode)
        {
            OnChangeSingleByteModeEvent(singleByteMode);
        }

        #endregion 

        #region Execute Command Event

        internal event EventHandler<OpCode> ExecuteCommandEvent;

        protected virtual void OnExecuteCommandEvent(object e)
        {
            if (ExecuteCommandEvent != null) ExecuteCommandEvent(this, (OpCode)e);
        }

        private void RaiseExecuteCommandEvent(OpCode commandCode)
        {
            OnExecuteCommandEvent(commandCode);
        }

        #endregion


        public bool SingleByteMode// => singleByteModeToolStripMenuItem.Checked; // read only
        {
            get { return singleByteModeToolStripMenuItem.Checked; }
            set { singleByteModeToolStripMenuItem.Checked = value; }
        }


        public void DisplayMessage(string message)
        {
            try
            {
                this.Invoke((MethodInvoker)delegate
                {
                    if (textBoxDisplay.Text.Length > 32768)
                        textBoxDisplay.Text = textBoxDisplay.Text.Substring(textBoxDisplay.Text.Length - 32768);

                    textBoxDisplay.Text += Environment.NewLine;
                    textBoxDisplay.Text += message;
                    textBoxDisplay.Text += Environment.NewLine;

                    textBoxDisplay.SelectionStart = textBoxDisplay.TextLength;
                    textBoxDisplay.ScrollToCaret();
                }, message);
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e.Message);

                // race to display message before window handle created... caused by presetting adaptor
            }
        }

        public void AddBaudRates(List<string> rates)
        {
            foreach (string item in rates)
            {
                ToolStripMenuItem toolStripItem = new ToolStripMenuItem(item, null, BaudSelection);

                toolStripItem.CheckOnClick = true;

                baudRateToolStripMenuItem.DropDownItems.Add(toolStripItem);
            }
        }

        internal void SetAdaptor(AdaptorType adaptor)
        {
            ToolStripMenuItem menuItem = null;

            switch (adaptor)
            {
                case AdaptorType.NotSet:
                    break;
                case AdaptorType.ZedScan_1:
                    menuItem = zedScan1ToolStripMenuItem;
                    break;
                case AdaptorType.vComm:
                    menuItem = vCommToolStripMenuItem;
                    break;
                case AdaptorType.HID:
                    menuItem = hIDToolStripMenuItem;
                    break;
                default:
                    break;
            }

            if (menuItem != null)
            {
                menuItem.Checked = true;
                //MenuAdaptorSelection(menuItem, null);
            }
        }


        public void ShowProgress(int percentComplete)
        {
            if (this.InvokeRequired)
                this.Invoke((MethodInvoker)delegate { ShowProgress(percentComplete); });
            else
            {
                if (percentComplete > 100)
                    percentComplete = 100;

                toolStripProgressBar.Value = percentComplete;
            }
        }



        public FormMain(string applicationTitle)
        {
            InitializeComponent();

            #region Title and icon

            _ApplicationTitle = applicationTitle;

            StringBuilder caption = new StringBuilder(_ApplicationTitle);
            caption.Append("    Version ");
            caption.Append(Application.ProductVersion);
            this.Text = caption.ToString();

            this.Icon = System.Drawing.Icon.ExtractAssociatedIcon(System.Windows.Forms.Application.ExecutablePath);

            #endregion

            textBoxDisplay.Text = string.Empty;
        }



        private void clearScreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBoxDisplay.Text = string.Empty;
        }

        private void MenuAdaptorSelection(object sender, EventArgs e)
        {
            ToolStripMenuItem thisAdaptor = (ToolStripMenuItem)sender;

            if (thisAdaptor.Checked)
            {
                AdaptorType adaptorType = AdaptorType.NotSet;

                if (thisAdaptor != zedScan1ToolStripMenuItem)
                    zedScan1ToolStripMenuItem.Checked = false;
                else
                    adaptorType = AdaptorType.ZedScan_1;

                if (thisAdaptor != vCommToolStripMenuItem)
                    vCommToolStripMenuItem.Checked = false;
                else
                    adaptorType = AdaptorType.vComm;

                if (thisAdaptor != hIDToolStripMenuItem)
                    hIDToolStripMenuItem.Checked = false;
                else
                    adaptorType = AdaptorType.HID;

                RaiseAdaptorSelectionEvent(adaptorType);
            }
            else
            {
                thisAdaptor.Checked = true;                         // ignore if already checked
            }
        }

        private void BaudSelection(object sender, EventArgs e)
        {
            ToolStripMenuItem thisBaudRate = (ToolStripMenuItem)sender;

            if (thisBaudRate.Checked)
            {
                foreach (ToolStripMenuItem item in baudRateToolStripMenuItem.DropDownItems)
                {
                    if (item != thisBaudRate)
                        item.Checked = false;
                }

                System.Diagnostics.Trace.WriteLine($"raise baud change event {sender}");

                UInt32 baud = 0;

                if (UInt32.TryParse(sender.ToString(), out baud))
                    RaiseChangeBaudRateEvent(baud);
                else
                    DisplayMessage($"Illegal Baud Rate {sender}");
            }
            else
                RaiseChangeBaudRateEvent(0);                        // set baud rate to none
        }

        private void singleByteModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RaiseChangeSingleByteModeEvent(singleByteModeToolStripMenuItem.Checked);
        }



        private void buttonInfo_Click(object sender, EventArgs e)
        {
            RaiseExecuteCommandEvent(OpCode.REQUEST_DEVICE_INFO);
        }

        private void buttonStartSession_Click(object sender, EventArgs e)
        {
            RaiseExecuteCommandEvent(OpCode.SESSION_INFORMATION);
        }

        private void buttonReadSession_Click(object sender, EventArgs e)
        {
            RaiseExecuteCommandEvent(OpCode.REQUEST_SESSION_INFORMATION);
        }

        private void buttonDeleteSession_Click(object sender, EventArgs e)
        {
            RaiseExecuteCommandEvent(OpCode.DELETE_SESSION_RESULTS);
        }

        private void buttonRequestResults_Click(object sender, EventArgs e)
        {
            RaiseExecuteCommandEvent(OpCode.REQUEST_RESULTS);
        }


        private void buttonLanguagePack_Click(object sender, EventArgs e)
        {
            RaiseExecuteCommandEvent(OpCode.LANGUAGE_UPDATE_START_BLOCK);
        }

        private void buttonFirmware_Click(object sender, EventArgs e)
        {
            RaiseExecuteCommandEvent(OpCode.CODE_UPDATE_START_BLOCK);
        }



        private void button1_Click(object sender, EventArgs e)
        {
            RaiseExecuteCommandEvent(OpCode.CONFIGURATION);
        }
    }
}
// Copyright (c) 2022 Zilico Limited
