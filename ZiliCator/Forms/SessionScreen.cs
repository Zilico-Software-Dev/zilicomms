﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZiliCator.Forms
{
    public partial class SessionScreen : Form
    {
        private Session _Session;



        public ZiliComms.Handset.Session Session
        { get { return _Session; } }



        public SessionScreen(Session session)
        {
            InitializeComponent();

            _Session = session;
        }

        private void Session_Load(object sender, EventArgs e)
        {
            textBoxNHSNumber.Text = _Session.NHSnumber;
            textBoxPatientName.Text = _Session.PatientName;
            textBoxDateOfBirth.Text = _Session.DateOfBirth.ToString("yyyy-MM-dd");
            textBoxPatientId.Text = _Session.PatientId;
            textBoxOperatorId.Text = _Session.OperatorId;

            textBoxCurrentTime.Text = _Session.StartTime.ToString();
            textBoxTrainingMode.Text = _Session.TrainingMode.ToString();


            List<ZiliComms.Handset.ReferralType> referralTypes = new List<ZiliComms.Handset.ReferralType>();
            foreach (ZiliComms.Handset.ReferralType referralType in Enum.GetValues(typeof(ZiliComms.Handset.ReferralType)))
            {
                referralTypes.Add(referralType);

                comboBoxReferralType.Items.Add(referralType);
            }

            comboBoxReferralType.SelectedItem = _Session.ReferralType;
        }



        private void buttonOK_Click(object sender, EventArgs e)
        {
            try
            {
                ZiliComms.Handset.ReferralType referralType = (ZiliComms.Handset.ReferralType)comboBoxReferralType.SelectedItem;

                DateTime dateOfBirth = Convert.ToDateTime(textBoxDateOfBirth.Text);

                //DateTime currentTime = Convert.ToDateTime(textBoxCurrentTime.Text);

                bool trainingMode = Convert.ToBoolean(textBoxTrainingMode.Text);


                _Session = new Session(textBoxPatientName.Text,
                                       textBoxPatientId.Text,
                                       dateOfBirth,
                                       textBoxNHSNumber.Text,
                                       textBoxOperatorId.Text,
                                       referralType,
                                       trainingMode);
            }
            catch (Exception)
            {
                DialogResult = DialogResult.None;
            }
        }
    }
}
// Copyright (c) 2022 Zilico Limited
