﻿
namespace ZiliCator.Forms
{
    partial class SessionScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNhsNumber = new System.Windows.Forms.Label();
            this.textBoxNHSNumber = new System.Windows.Forms.TextBox();
            this.textBoxPatientName = new System.Windows.Forms.TextBox();
            this.labelPatientName = new System.Windows.Forms.Label();
            this.textBoxDateOfBirth = new System.Windows.Forms.TextBox();
            this.labelDateOfBirth = new System.Windows.Forms.Label();
            this.textBoxPatientId = new System.Windows.Forms.TextBox();
            this.labelPatientId = new System.Windows.Forms.Label();
            this.textBoxOperatorId = new System.Windows.Forms.TextBox();
            this.labelOperatorId = new System.Windows.Forms.Label();
            this.labelReferralType = new System.Windows.Forms.Label();
            this.textBoxCurrentTime = new System.Windows.Forms.TextBox();
            this.labelTime = new System.Windows.Forms.Label();
            this.textBoxTrainingMode = new System.Windows.Forms.TextBox();
            this.labelTrainingMode = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.comboBoxReferralType = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // labelNhsNumber
            // 
            this.labelNhsNumber.AutoSize = true;
            this.labelNhsNumber.Location = new System.Drawing.Point(13, 13);
            this.labelNhsNumber.Name = "labelNhsNumber";
            this.labelNhsNumber.Size = new System.Drawing.Size(70, 13);
            this.labelNhsNumber.TabIndex = 0;
            this.labelNhsNumber.Text = "NHS Number";
            // 
            // textBoxNHSNumber
            // 
            this.textBoxNHSNumber.Location = new System.Drawing.Point(94, 10);
            this.textBoxNHSNumber.Name = "textBoxNHSNumber";
            this.textBoxNHSNumber.Size = new System.Drawing.Size(184, 20);
            this.textBoxNHSNumber.TabIndex = 1;
            // 
            // textBoxPatientName
            // 
            this.textBoxPatientName.Location = new System.Drawing.Point(94, 36);
            this.textBoxPatientName.Name = "textBoxPatientName";
            this.textBoxPatientName.Size = new System.Drawing.Size(184, 20);
            this.textBoxPatientName.TabIndex = 3;
            // 
            // labelPatientName
            // 
            this.labelPatientName.AutoSize = true;
            this.labelPatientName.Location = new System.Drawing.Point(13, 39);
            this.labelPatientName.Name = "labelPatientName";
            this.labelPatientName.Size = new System.Drawing.Size(71, 13);
            this.labelPatientName.TabIndex = 2;
            this.labelPatientName.Text = "Patient Name";
            // 
            // textBoxDateOfBirth
            // 
            this.textBoxDateOfBirth.Location = new System.Drawing.Point(94, 62);
            this.textBoxDateOfBirth.Name = "textBoxDateOfBirth";
            this.textBoxDateOfBirth.Size = new System.Drawing.Size(184, 20);
            this.textBoxDateOfBirth.TabIndex = 5;
            // 
            // labelDateOfBirth
            // 
            this.labelDateOfBirth.AutoSize = true;
            this.labelDateOfBirth.Location = new System.Drawing.Point(13, 65);
            this.labelDateOfBirth.Name = "labelDateOfBirth";
            this.labelDateOfBirth.Size = new System.Drawing.Size(66, 13);
            this.labelDateOfBirth.TabIndex = 4;
            this.labelDateOfBirth.Text = "Date of Birth";
            // 
            // textBoxPatientId
            // 
            this.textBoxPatientId.Location = new System.Drawing.Point(94, 88);
            this.textBoxPatientId.Name = "textBoxPatientId";
            this.textBoxPatientId.Size = new System.Drawing.Size(184, 20);
            this.textBoxPatientId.TabIndex = 7;
            // 
            // labelPatientId
            // 
            this.labelPatientId.AutoSize = true;
            this.labelPatientId.Location = new System.Drawing.Point(13, 91);
            this.labelPatientId.Name = "labelPatientId";
            this.labelPatientId.Size = new System.Drawing.Size(52, 13);
            this.labelPatientId.TabIndex = 6;
            this.labelPatientId.Text = "Patient Id";
            // 
            // textBoxOperatorId
            // 
            this.textBoxOperatorId.Location = new System.Drawing.Point(94, 114);
            this.textBoxOperatorId.Name = "textBoxOperatorId";
            this.textBoxOperatorId.Size = new System.Drawing.Size(184, 20);
            this.textBoxOperatorId.TabIndex = 9;
            // 
            // labelOperatorId
            // 
            this.labelOperatorId.AutoSize = true;
            this.labelOperatorId.Location = new System.Drawing.Point(13, 117);
            this.labelOperatorId.Name = "labelOperatorId";
            this.labelOperatorId.Size = new System.Drawing.Size(60, 13);
            this.labelOperatorId.TabIndex = 8;
            this.labelOperatorId.Text = "Operator Id";
            // 
            // labelReferralType
            // 
            this.labelReferralType.AutoSize = true;
            this.labelReferralType.Location = new System.Drawing.Point(13, 143);
            this.labelReferralType.Name = "labelReferralType";
            this.labelReferralType.Size = new System.Drawing.Size(71, 13);
            this.labelReferralType.TabIndex = 10;
            this.labelReferralType.Text = "Referral Type";
            // 
            // textBoxCurrentTime
            // 
            this.textBoxCurrentTime.Enabled = false;
            this.textBoxCurrentTime.Location = new System.Drawing.Point(94, 166);
            this.textBoxCurrentTime.Name = "textBoxCurrentTime";
            this.textBoxCurrentTime.Size = new System.Drawing.Size(184, 20);
            this.textBoxCurrentTime.TabIndex = 13;
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Location = new System.Drawing.Point(13, 169);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(30, 13);
            this.labelTime.TabIndex = 12;
            this.labelTime.Text = "Time";
            // 
            // textBoxTrainingMode
            // 
            this.textBoxTrainingMode.Location = new System.Drawing.Point(94, 192);
            this.textBoxTrainingMode.Name = "textBoxTrainingMode";
            this.textBoxTrainingMode.Size = new System.Drawing.Size(184, 20);
            this.textBoxTrainingMode.TabIndex = 15;
            // 
            // labelTrainingMode
            // 
            this.labelTrainingMode.AutoSize = true;
            this.labelTrainingMode.Location = new System.Drawing.Point(13, 195);
            this.labelTrainingMode.Name = "labelTrainingMode";
            this.labelTrainingMode.Size = new System.Drawing.Size(75, 13);
            this.labelTrainingMode.TabIndex = 14;
            this.labelTrainingMode.Text = "Training Mode";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(205, 226);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 17;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(124, 226);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 16;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // comboBoxReferralType
            // 
            this.comboBoxReferralType.FormattingEnabled = true;
            this.comboBoxReferralType.Location = new System.Drawing.Point(94, 140);
            this.comboBoxReferralType.Name = "comboBoxReferralType";
            this.comboBoxReferralType.Size = new System.Drawing.Size(184, 21);
            this.comboBoxReferralType.TabIndex = 18;
            // 
            // SessionScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 261);
            this.Controls.Add(this.comboBoxReferralType);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.textBoxTrainingMode);
            this.Controls.Add(this.labelTrainingMode);
            this.Controls.Add(this.textBoxCurrentTime);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.labelReferralType);
            this.Controls.Add(this.textBoxOperatorId);
            this.Controls.Add(this.labelOperatorId);
            this.Controls.Add(this.textBoxPatientId);
            this.Controls.Add(this.labelPatientId);
            this.Controls.Add(this.textBoxDateOfBirth);
            this.Controls.Add(this.labelDateOfBirth);
            this.Controls.Add(this.textBoxPatientName);
            this.Controls.Add(this.labelPatientName);
            this.Controls.Add(this.textBoxNHSNumber);
            this.Controls.Add(this.labelNhsNumber);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "SessionScreen";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Session";
            this.Load += new System.EventHandler(this.Session_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNhsNumber;
        private System.Windows.Forms.TextBox textBoxNHSNumber;
        private System.Windows.Forms.TextBox textBoxPatientName;
        private System.Windows.Forms.Label labelPatientName;
        private System.Windows.Forms.TextBox textBoxDateOfBirth;
        private System.Windows.Forms.Label labelDateOfBirth;
        private System.Windows.Forms.TextBox textBoxPatientId;
        private System.Windows.Forms.Label labelPatientId;
        private System.Windows.Forms.TextBox textBoxOperatorId;
        private System.Windows.Forms.Label labelOperatorId;
        private System.Windows.Forms.Label labelReferralType;
        private System.Windows.Forms.TextBox textBoxCurrentTime;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.TextBox textBoxTrainingMode;
        private System.Windows.Forms.Label labelTrainingMode;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.ComboBox comboBoxReferralType;
    }
}