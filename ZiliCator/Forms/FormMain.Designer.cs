﻿
namespace ZiliCator
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxDisplay = new System.Windows.Forms.TextBox();
            this.buttonInfo = new System.Windows.Forms.Button();
            this.buttonStartSession = new System.Windows.Forms.Button();
            this.buttonReadSession = new System.Windows.Forms.Button();
            this.buttonDeleteSession = new System.Windows.Forms.Button();
            this.buttonRequestResults = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonLanguagePack = new System.Windows.Forms.Button();
            this.buttonFirmware = new System.Windows.Forms.Button();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adaptorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zedScan1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vCommToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.baudRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singleByteModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemDivider1 = new System.Windows.Forms.ToolStripSeparator();
            this.clearScreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStripMain = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStripMain.SuspendLayout();
            this.statusStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxDisplay
            // 
            this.textBoxDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDisplay.BackColor = System.Drawing.Color.Black;
            this.textBoxDisplay.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDisplay.ForeColor = System.Drawing.Color.Lime;
            this.textBoxDisplay.Location = new System.Drawing.Point(12, 27);
            this.textBoxDisplay.Multiline = true;
            this.textBoxDisplay.Name = "textBoxDisplay";
            this.textBoxDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxDisplay.Size = new System.Drawing.Size(590, 348);
            this.textBoxDisplay.TabIndex = 0;
            this.textBoxDisplay.Text = "1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n9\r\n0123456789\r\n1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n9\r\n0123456789\r\n";
            // 
            // buttonInfo
            // 
            this.buttonInfo.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonInfo.Location = new System.Drawing.Point(64, 381);
            this.buttonInfo.Name = "buttonInfo";
            this.buttonInfo.Size = new System.Drawing.Size(117, 49);
            this.buttonInfo.TabIndex = 1;
            this.buttonInfo.Text = "Info";
            this.buttonInfo.UseVisualStyleBackColor = true;
            this.buttonInfo.Click += new System.EventHandler(this.buttonInfo_Click);
            // 
            // buttonStartSession
            // 
            this.buttonStartSession.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonStartSession.Location = new System.Drawing.Point(187, 381);
            this.buttonStartSession.Name = "buttonStartSession";
            this.buttonStartSession.Size = new System.Drawing.Size(117, 49);
            this.buttonStartSession.TabIndex = 2;
            this.buttonStartSession.Text = "Start Session";
            this.buttonStartSession.UseVisualStyleBackColor = true;
            this.buttonStartSession.Click += new System.EventHandler(this.buttonStartSession_Click);
            // 
            // buttonReadSession
            // 
            this.buttonReadSession.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonReadSession.Location = new System.Drawing.Point(310, 381);
            this.buttonReadSession.Name = "buttonReadSession";
            this.buttonReadSession.Size = new System.Drawing.Size(117, 49);
            this.buttonReadSession.TabIndex = 3;
            this.buttonReadSession.Text = "Read Session";
            this.buttonReadSession.UseVisualStyleBackColor = true;
            this.buttonReadSession.Click += new System.EventHandler(this.buttonReadSession_Click);
            // 
            // buttonDeleteSession
            // 
            this.buttonDeleteSession.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonDeleteSession.Location = new System.Drawing.Point(433, 381);
            this.buttonDeleteSession.Name = "buttonDeleteSession";
            this.buttonDeleteSession.Size = new System.Drawing.Size(117, 49);
            this.buttonDeleteSession.TabIndex = 4;
            this.buttonDeleteSession.Text = "Delete Session";
            this.buttonDeleteSession.UseVisualStyleBackColor = true;
            this.buttonDeleteSession.Click += new System.EventHandler(this.buttonDeleteSession_Click);
            // 
            // buttonRequestResults
            // 
            this.buttonRequestResults.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonRequestResults.Location = new System.Drawing.Point(64, 436);
            this.buttonRequestResults.Name = "buttonRequestResults";
            this.buttonRequestResults.Size = new System.Drawing.Size(117, 49);
            this.buttonRequestResults.TabIndex = 5;
            this.buttonRequestResults.Text = "Request Results";
            this.buttonRequestResults.UseVisualStyleBackColor = true;
            this.buttonRequestResults.Click += new System.EventHandler(this.buttonRequestResults_Click);
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button1.Location = new System.Drawing.Point(187, 436);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 49);
            this.button1.TabIndex = 6;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonLanguagePack
            // 
            this.buttonLanguagePack.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonLanguagePack.Location = new System.Drawing.Point(433, 436);
            this.buttonLanguagePack.Name = "buttonLanguagePack";
            this.buttonLanguagePack.Size = new System.Drawing.Size(117, 49);
            this.buttonLanguagePack.TabIndex = 7;
            this.buttonLanguagePack.Text = "Language Pack";
            this.buttonLanguagePack.UseVisualStyleBackColor = true;
            this.buttonLanguagePack.Click += new System.EventHandler(this.buttonLanguagePack_Click);
            // 
            // buttonFirmware
            // 
            this.buttonFirmware.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonFirmware.Location = new System.Drawing.Point(310, 436);
            this.buttonFirmware.Name = "buttonFirmware";
            this.buttonFirmware.Size = new System.Drawing.Size(117, 49);
            this.buttonFirmware.TabIndex = 8;
            this.buttonFirmware.Text = "Write Firmware";
            this.buttonFirmware.UseVisualStyleBackColor = true;
            this.buttonFirmware.Click += new System.EventHandler(this.buttonFirmware_Click);
            // 
            // menuStripMain
            // 
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.clearScreenToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(614, 24);
            this.menuStripMain.TabIndex = 9;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adaptorToolStripMenuItem,
            this.baudRateToolStripMenuItem,
            this.singleByteModeToolStripMenuItem,
            this.toolStripMenuItemDivider1});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // adaptorToolStripMenuItem
            // 
            this.adaptorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zedScan1ToolStripMenuItem,
            this.vCommToolStripMenuItem,
            this.hIDToolStripMenuItem});
            this.adaptorToolStripMenuItem.Name = "adaptorToolStripMenuItem";
            this.adaptorToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.adaptorToolStripMenuItem.Text = "Adaptor";
            // 
            // zedScan1ToolStripMenuItem
            // 
            this.zedScan1ToolStripMenuItem.CheckOnClick = true;
            this.zedScan1ToolStripMenuItem.Name = "zedScan1ToolStripMenuItem";
            this.zedScan1ToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.zedScan1ToolStripMenuItem.Text = "ZedScan 1";
            this.zedScan1ToolStripMenuItem.Click += new System.EventHandler(this.MenuAdaptorSelection);
            // 
            // vCommToolStripMenuItem
            // 
            this.vCommToolStripMenuItem.CheckOnClick = true;
            this.vCommToolStripMenuItem.Name = "vCommToolStripMenuItem";
            this.vCommToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.vCommToolStripMenuItem.Text = "vComm";
            this.vCommToolStripMenuItem.Click += new System.EventHandler(this.MenuAdaptorSelection);
            // 
            // hIDToolStripMenuItem
            // 
            this.hIDToolStripMenuItem.CheckOnClick = true;
            this.hIDToolStripMenuItem.Name = "hIDToolStripMenuItem";
            this.hIDToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.hIDToolStripMenuItem.Text = "HID";
            this.hIDToolStripMenuItem.Click += new System.EventHandler(this.MenuAdaptorSelection);
            // 
            // baudRateToolStripMenuItem
            // 
            this.baudRateToolStripMenuItem.Name = "baudRateToolStripMenuItem";
            this.baudRateToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.baudRateToolStripMenuItem.Text = "Baud Rate";
            // 
            // singleByteModeToolStripMenuItem
            // 
            this.singleByteModeToolStripMenuItem.CheckOnClick = true;
            this.singleByteModeToolStripMenuItem.Name = "singleByteModeToolStripMenuItem";
            this.singleByteModeToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.singleByteModeToolStripMenuItem.Text = "Single Byte Mode";
            this.singleByteModeToolStripMenuItem.Click += new System.EventHandler(this.singleByteModeToolStripMenuItem_Click);
            // 
            // toolStripMenuItemDivider1
            // 
            this.toolStripMenuItemDivider1.Name = "toolStripMenuItemDivider1";
            this.toolStripMenuItemDivider1.Size = new System.Drawing.Size(163, 6);
            // 
            // clearScreenToolStripMenuItem
            // 
            this.clearScreenToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.clearScreenToolStripMenuItem.Name = "clearScreenToolStripMenuItem";
            this.clearScreenToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.clearScreenToolStripMenuItem.Text = "Clear Screen";
            this.clearScreenToolStripMenuItem.Click += new System.EventHandler(this.clearScreenToolStripMenuItem_Click);
            // 
            // statusStripMain
            // 
            this.statusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripProgressBar,
            this.toolStripStatusLabel2});
            this.statusStripMain.Location = new System.Drawing.Point(0, 488);
            this.statusStripMain.Name = "statusStripMain";
            this.statusStripMain.Size = new System.Drawing.Size(614, 22);
            this.statusStripMain.TabIndex = 10;
            this.statusStripMain.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(48, 17);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(500, 16);
            this.toolStripProgressBar.Step = 1;
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(48, 17);
            this.toolStripStatusLabel2.Spring = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 510);
            this.Controls.Add(this.statusStripMain);
            this.Controls.Add(this.buttonFirmware);
            this.Controls.Add(this.buttonLanguagePack);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonRequestResults);
            this.Controls.Add(this.buttonDeleteSession);
            this.Controls.Add(this.buttonReadSession);
            this.Controls.Add(this.buttonStartSession);
            this.Controls.Add(this.buttonInfo);
            this.Controls.Add(this.textBoxDisplay);
            this.Controls.Add(this.menuStripMain);
            this.MainMenuStrip = this.menuStripMain;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.statusStripMain.ResumeLayout(false);
            this.statusStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxDisplay;
        private System.Windows.Forms.Button buttonInfo;
        private System.Windows.Forms.Button buttonStartSession;
        private System.Windows.Forms.Button buttonReadSession;
        private System.Windows.Forms.Button buttonDeleteSession;
        private System.Windows.Forms.Button buttonRequestResults;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonLanguagePack;
        private System.Windows.Forms.Button buttonFirmware;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adaptorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zedScan1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vCommToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem baudRateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearScreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem singleByteModeToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStripMain;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItemDivider1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
    }
}

