﻿
											Zilicator
											=========
___________________________________________________________________________________________________________

Communications test application for ZiliComms library
___________________________________________________________________________________________________________

ToDo:
	
	fix: preselected adaptor race in Display Message at start up

___________________________________________________________________________________________________________

Notes:

		BAUD_2400 = 2400,
		BAUD_4800 = 4800,
		BAUD_9600 = 9600,
		BAUD_14400 = 14400,
		BAUD_19200 = 19200,
		BAUD_28800 = 28800,
		BAUD_38400 = 38400,
		//BAUD_56000 = 56000,	// WinBase.h
		BAUD_57600 = 57600,
		BAUD_76800 = 76800,
		BAUD_115200 = 115200,
		//BAUD_128000 = 128000,	// WinBase.h
		BAUD_230400 = 230400,
		//BAUD_256000 = 256000,	// WinBase.h
		BAUD_460800 = 460800,
		BAUD_512000 = 512000,	// SwitchBaudRate
		BAUD_576000 = 576000,
		BAUD_921600 = 921600,
		BAUD_1000000 = 1000000,	// SwitchBaudRate
		BAUD_1036800 = 1036800,
		//BAUD_1500000 = 1500000,	// ?
		BAUD_1843200 = 1843200,
		BAUD_2000000 = 2000000,	// SwitchBaudRate
		BAUD_2073600 = 2073600,
		BAUD_3000000 = 3000000,	// SwitchBaudRate
		BAUD_3110400 = 3110400,
		BAUD_4147200 = 4147200,


