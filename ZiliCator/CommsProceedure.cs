﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;

namespace ZiliCator
{
    /// <summary>
    /// Method pointer object
    /// </summary>
    internal class CommsProceedure
    {
        #region fields

        private Action _Proceedure;

        private Action<object[]> _ParamterisedProceedure;

        private object[] _Parameters;

        #endregion

        #region properties

        public Action Proceedure
        { get { return _Proceedure; } }

        public Action<object[]> ParamterisedProceedure
        { get { return _ParamterisedProceedure; } }

        public object[] Parameters
        { get { return _Parameters; } }

        #endregion

        #region ~ctor

        /// <summary>
        /// Method pointer object
        /// </summary>
        /// <param name="proceedure"></param>
        public CommsProceedure(Action proceedure)
        {
            _Proceedure = proceedure;

            _ParamterisedProceedure = null;

            _Parameters = null;
        }

        /// <summary>
        /// Parametrised method pointer object
        /// </summary>
        /// <param name="proceedure"></param>
        /// <param name="parameters"></param>
        public CommsProceedure(Action<object[]> proceedure, object[] parameters)
        {
            _Proceedure = null;

            _ParamterisedProceedure = proceedure;

            _Parameters = parameters;
        }

        #endregion

        public override string ToString()
        {
            string name = string.Empty;

            if (_Proceedure != null)
                name = _Proceedure.Method.Name;
            else if (_ParamterisedProceedure != null)
                name = _ParamterisedProceedure.Method.Name;

            return name;
        }
    }
}
// Copyright (c) 2022 Zilico Limited
