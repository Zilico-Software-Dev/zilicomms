﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZiliComms.Commands;

namespace ZiliCator
{
    internal class Main
    {
        #region fields

        private string _ApplicationTitle;


        private FormMain _FormMain;

        private static Action<string> DisplayMessage;


        private ZiliConnection.Monitor _USBMonitor;

        ZiliConnection.Guids _DeviceType;
        private string _Vid;
        private string _Pid;


        private ZiliComms.ZedScan _ZedScan;

        private UInt32 _BaudRate;
        private bool _SingleByteMode;


        private List<CommsProceedure> _CommsProceedures;

        private readonly object _ListLock;

        private bool _IsBusyCommsTask;



        #endregion



        public Main(string applicationTitle)
        {
            _ApplicationTitle = applicationTitle;

            _DeviceType = ZiliConnection.Guids.GUID_DEVINTERFACE_HID;

            _ZedScan = new ZiliComms.ZedScan();
            _ZedScan.DataTransferEvent += _ZedScan_DataTransferEvent;

            _BaudRate = Properties.Settings.Default.Baud;           // 0
            _SingleByteMode = Properties.Settings.Default.SingleByteMode;


            _FormMain = new FormMain(_ApplicationTitle);

            DisplayMessage = _FormMain.DisplayMessage;

            _FormMain.AddBaudRates(new List<string>() { "9600", "19200", "115200", "230400", "460800", "921600" });

            _FormMain.SingleByteMode = _SingleByteMode;

            _FormMain.AdaptorSelectionEvent += _FormMain_AdaptorSelectionEvent;
            _FormMain.ChangeBaudRateEvent += _FormMain_ChangeBaudRateEvent;
            _FormMain.ChangeSingleByteModeEvent += _FormMain_ChangeSingleByteModeEvent;
            _FormMain.ExecuteCommandEvent += _FormMain_ExecuteCommandEvent;

            if (!Properties.Settings.Default.Adaptor.Equals((int)AdaptorType.NotSet))
            {
                _FormMain.SetAdaptor((AdaptorType)Properties.Settings.Default.Adaptor);

                _FormMain_AdaptorSelectionEvent(null, (AdaptorType)Properties.Settings.Default.Adaptor);
            }

            _CommsProceedures = new List<CommsProceedure>();
            _ListLock = new object();
            _IsBusyCommsTask = false;
        }



        private void _ZedScan_DataTransferEvent(ZiliComms.DataTransferEventArgs eventArgs)
        {
            _FormMain.ShowProgress(eventArgs.PercentageComplete);
        }



        public void ShowScreen()
        {
            _FormMain.ShowDialog(); // block ...
        }

        //public void Start()
        //{
        //    ShowScreen();
        //}


        #region FormMain events

        private void _USBMonitor_USBStateChangeEvent(object sender, ZiliConnection.USBStateChangeEventArgs e)
        {
            if (_FormMain.IsHandleCreated)
                DisplayMessage($"{e.State}");

            bool traceEnabled = true;

            if (e.State.Equals(ZiliConnection.StateChange.Arrival))
            {
                System.Diagnostics.Trace.WriteLine($"_USBMonitor_USBStateChangeEvent: {_USBMonitor.DevicePath}");

                if ((_ZedScan.Port != null) && (!_ZedScan.Port.DevicePath.Equals(_USBMonitor.DevicePath)))
                    _ZedScan.Port.Dispose();

                if ((_ZedScan.Port == null) || (_ZedScan.Port.IsDisposed))
                {
                    if (_DeviceType.Equals(ZiliConnection.Guids.GUID_DEVINTERFACE_HID))
                        _ZedScan.Port = new ZiliComms.HID(_USBMonitor.DevicePath, traceEnabled);
                    else
                        _ZedScan.Port = new ZiliComms.Serial(_USBMonitor.DevicePath, traceEnabled);

                    _ZedScan.Port.SingleByteMode = _FormMain.SingleByteMode;
                }

                if (_ZedScan.Port.Open())                          // close and reopen
                {
                    DisplayMessage("Comms Open");

                    _FormMain_ChangeBaudRateEvent(sender, _BaudRate);
                }
                else
                    DisplayMessage($"Open Comms Error : {_ZedScan.Port.Message}");
            }
            else
            {
                if (_ZedScan.Port != null)
                {
                    _ZedScan.Port.Dispose();
                    _ZedScan.Port = null;
                }
            }
        }

        private void _FormMain_AdaptorSelectionEvent(object sender, AdaptorType e)
        {
            if (_USBMonitor != null)
            {
                _USBMonitor.USBStateChangeEvent -= _USBMonitor_USBStateChangeEvent;
                _USBMonitor.Dispose();
            }

            #region set _DeviceType, _Vid and _Pid

            switch (e)
            {
                case AdaptorType.NotSet:
                    _DeviceType = ZiliConnection.Guids.GUID_DEVINTERFACE_HID;

                    _Vid = string.Empty;
                    _Pid = string.Empty;

                    break;

                case AdaptorType.ZedScan_1:

                    _ZedScan.ZedScanVersion = 1;

                    _DeviceType = ZiliConnection.Guids.GUID_DEVINTERFACE_COMPORT;

                    _Vid = "0483";
                    _Pid = "5740";

                    break;

                case AdaptorType.vComm:

                    _ZedScan.ZedScanVersion = 2;

                    _DeviceType = ZiliConnection.Guids.GUID_DEVINTERFACE_COMPORT;

                    _Vid = "0403";
                    _Pid = "6001";

                    break;

                case AdaptorType.HID:

                    _ZedScan.ZedScanVersion = 2;

                    _DeviceType = ZiliConnection.Guids.GUID_DEVINTERFACE_HID;

                    _Vid = "0403";
                    _Pid = "6030";

                    break;

                default:
                    _DeviceType = ZiliConnection.Guids.GUID_DEVINTERFACE_HID;

                    _Vid = string.Empty;
                    _Pid = string.Empty;

                    break;
            }

            #endregion

            if (e != AdaptorType.NotSet)
            {
                _USBMonitor = new ZiliConnection.Monitor(_DeviceType, _Vid, _Pid);

                _USBMonitor.USBStateChangeEvent += _USBMonitor_USBStateChangeEvent;

                //_USBMonitor.TraceEnabled = true;

                _USBMonitor.Start();

                if (sender != null)
                    DisplayMessage($"Adaptor {e} Selected");
            }

            Properties.Settings.Default.Adaptor = (int)e;
            Properties.Settings.Default.Save();
        }

        private void _FormMain_ChangeBaudRateEvent(object sender, uint e)
        {
            if (_ZedScan.Port != null)
            {
                if (_ZedScan.Port.SetBaudRate((int)e))
                {
                    _BaudRate = e;

                    Properties.Settings.Default.Baud = _BaudRate;
                    Properties.Settings.Default.Save();

                    DisplayMessage($"Baud Rate set to {_BaudRate}");
                }
                else
                    DisplayMessage($"Error changing Baud Rate to {e}, {_ZedScan.Port.Message}");
            }
        }

        private void _FormMain_ChangeSingleByteModeEvent(object sender, bool e)
        {
            _SingleByteMode = e;

            if (_ZedScan.Port != null)
            {
                _ZedScan.Port.SingleByteMode = _SingleByteMode;

                DisplayMessage($"Single Byte Mode set to {_SingleByteMode}");
            }

            Properties.Settings.Default.SingleByteMode = _SingleByteMode;
            Properties.Settings.Default.Save();
        }

        private void _FormMain_ExecuteCommandEvent(object sender, OpCode commandCode)
        {
            System.Diagnostics.Trace.WriteLine($"Execute Command: {commandCode}");

            switch (commandCode)
            {
                case OpCode.REQUEST_DEVICE_INFO:
                    AddCommsTask(new CommsProceedure(RequestInformation));
                    break;
                case OpCode.DEVICE_INFORMATION:
                    break;
                case OpCode.REQUEST_RESULTS:
                    break;
                case OpCode.READING_RESULT:
                    AddCommsTask(new CommsProceedure(RequestResults));
                    break;
                case OpCode.REQUEST_SESSION_INFORMATION:
                    AddCommsTask(new CommsProceedure(RequestSessionDetails));
                    break;
                case OpCode.SESSION_INFORMATION:
                    EnterSessionDetails();
                    break;
                case OpCode.CODE_UPDATE_START_BLOCK:
                    SelectFirmware();
                    break;
                case OpCode.CODE_UPDATE_BLOCK:
                    break;
                case OpCode.CODE_UPDATE_END_BLOCK:
                    break;
                case OpCode.LANGUAGE_UPDATE_START_BLOCK:
                    SelectLanguagePack();
                    break;
                case OpCode.LANGUAGE_UPDATE_BLOCK:
                    break;
                case OpCode.LANGUAGE_UPDATE_END_BLOCK:
                    break;
                case OpCode.DELETE_SESSION_RESULTS:
                    AddCommsTask(new CommsProceedure(DeleteSessionResults));
                    break;
                case OpCode.REQUEST_CONFIGURATION:
                    break;
                case OpCode.CONFIGURATION:
                    DebugTest();
                    break;
                case OpCode.LANGUAGE_SELECTION:
                    break;
                case OpCode.ACK:
                    break;
                case OpCode.SET_UNIQUE_ID:
                    break;
                case OpCode.NAK:
                    break;
                case OpCode.NOP:
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Async Command Queue

        private void AddCommsTask(CommsProceedure newCommsProceedure)
        {
            lock (_ListLock)
            {
                _CommsProceedures.Add(newCommsProceedure);
            }

            try
            {
                if (!_IsBusyCommsTask)
                {
                    _IsBusyCommsTask = true;


                    Task.Run(CommsTaskProcessor);


                }
            }
            catch (Exception e)
            {
                DisplayMessage($"Add Comms Task {newCommsProceedure} {e.Message}");
            }
        }

        private async Task CommsTaskProcessor()
        {
            CommsProceedure commsAction = null;

            try
            {
                while (_CommsProceedures.Count > 0)
                {
                    lock (_ListLock)
                    {
                        commsAction = _CommsProceedures[0];

                        _CommsProceedures.RemoveAt(0);
                    }

                    if (commsAction != null)
                    {
                        if (commsAction.Proceedure != null)
                        {
                            await Task.Run(commsAction.Proceedure);
                        }
                        else if (commsAction.ParamterisedProceedure != null)
                        {
                            await Task.Run(() => commsAction.ParamterisedProceedure(commsAction.Parameters));
                        }

                        // Abort / Error ?

                    }
                }
            }
            catch (Exception e)
            {
                DisplayMessage($"Comms Task Processor {commsAction} {e.Message}");
            }
            finally
            {
                _IsBusyCommsTask = false;
            }
        }

        #endregion


        private void RequestInformation()
        {
            if (_ZedScan.GetDeviceInformation())
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.AppendLine("Device Information");
                sb.AppendLine($"    Serial Number    : {_ZedScan.DeviceInformation.SerialNumber}");
                sb.AppendLine($"    Hardware Version : {_ZedScan.DeviceInformation.HardwareVersion}");
                sb.AppendLine($"    Software Version : {_ZedScan.DeviceInformation.SoftwareVersion}");

                if (_ZedScan.ZedScanVersion.Equals(2))
                {
                    sb.AppendLine($"    Charge State     : {((ZiliComms.Handset.Information)_ZedScan.DeviceInformation).ChargeState}");
                    sb.AppendLine($"    Battery Charge   : {((ZiliComms.Handset.Information)_ZedScan.DeviceInformation).BatteryCharge}");
                    sb.AppendLine($"    Reading Status   : {((ZiliComms.Handset.Information)_ZedScan.DeviceInformation).ReadingStatus}");
                    sb.AppendLine($"    Language Code    : {((ZiliComms.Handset.Information)_ZedScan.DeviceInformation).LanguageCode}");
                }

                if (_ZedScan.DeviceInformation.ErrorLog != null)
                {
                    // {_ZedScan.Port.ToHex(_ZedScan.DeviceInformation.ErrorLog)}");

                    sb.AppendLine($"    Error Log        :");

                    int index = 0;
                    while (index < _ZedScan.DeviceInformation.ErrorLog.Length)
                    {
                        for (int width = 0; width < 16; width++)
                        {
                            if (index < _ZedScan.DeviceInformation.ErrorLog.Length)
                            {
                                if (width.Equals(8))
                                    sb.Append(" ");

                                sb.Append($"{_ZedScan.DeviceInformation.ErrorLog[index]:X2} ");

                                index++;
                            }
                        }

                        sb.AppendLine();
                    }
                }
                else
                    sb.AppendLine($"    Error Log        : None");

                DisplayMessage(sb.ToString());

                if (_ZedScan.ZedScanVersion.Equals(2))
                {
                    ZiliComms.Handset.LogRecord logRecord = ((ZiliComms.Handset.Information)_ZedScan.DeviceInformation).LogRecord;

                    sb.Clear();
                    sb.AppendLine("Log record");
                    sb.AppendLine($"    Version            : {logRecord.Version}");
                    sb.AppendLine($"    Firmware Version   : {logRecord.FirmwareVersion}");
                    sb.AppendLine($"    Device ID          : {logRecord.DeviceID}");
                    sb.AppendLine($"    Unique Id          : {logRecord.UniqueId}");
                    sb.AppendLine($"    Alert Code         : {logRecord.AlertCode}");
                    sb.AppendLine($"    Training Status    : {logRecord.TrainingModeStatus}");

                    //sb.AppendLine($"    Seconds            : {logRecord.ClockSeconds}");
                    //sb.AppendLine($"    Minutes            : {logRecord.ClockMinutes}");
                    //sb.AppendLine($"    Hours              : {logRecord.ClockHours}");
                    //sb.AppendLine($"    Days               : {logRecord.ClockDays}");
                    //sb.AppendLine($"    Months             : {logRecord.ClockMonths}");
                    //sb.AppendLine($"    Years              : {logRecord.ClockYears}");

                    sb.AppendLine($"    Date time          : {logRecord.ClockYears}/{logRecord.ClockMonths:00}/{logRecord.ClockDays:00} {logRecord.ClockHours:00}:{logRecord.ClockMinutes:00}:{logRecord.ClockSeconds:00}");

                    sb.AppendLine($"    Critical Flag      : {logRecord.CriticalErrorFlag}");
                    sb.AppendLine($"    File Name          : {logRecord.CriticalErrorFileName}");
                    sb.AppendLine($"    Line Number        : {logRecord.LineNumber}");
                    sb.AppendLine($"    Error Code         : {logRecord.ErrorCode}");
                    sb.AppendLine($"    Battery Percentage : {logRecord.BatteryPercentage}");
                    sb.AppendLine($"    Dock State         : {logRecord.DockState}");
                    sb.AppendLine($"    Points Scanned     : {logRecord.PointsScanned}");
                    sb.AppendLine($"    Points Read        : {logRecord.PointsRead}");
                    sb.AppendLine($"    Quality Checks     : {logRecord.QualityChecks}");
                    sb.AppendLine($"    Measuring          : {logRecord.Measuring}");
                    sb.AppendLine();
                    if (logRecord.EventRecords != null)
                    {
                        sb.AppendLine($"    Event Records      : ");
                        foreach (ZiliComms.Handset.LogEventRecord item in logRecord.EventRecords)
                        {
                            sb.AppendLine($"                       : {item}");
                        }
                        sb.AppendLine();
                    }
                    sb.AppendLine($"    Fail Counts        : ");
                    sb.AppendLine($"{logRecord.Measurements}");

                    DisplayMessage(sb.ToString());
                }
            }
            else
            {
                DisplayMessage(_ZedScan.Message);
            }
        }


        private void EnterSessionDetails()
        {
            Session session = null;

            if (_ZedScan.Session != null)
            {
                session = new Session("Subject", _ZedScan.Session.PatientId, DateTime.UtcNow,
                                      "NHS 123", _ZedScan.Session.OperatorId,
                                      _ZedScan.Session.ReferralType,
                                       _ZedScan.Session.TrainingMode);

            }
            else
            {
                session = new Session("Subject", "Subject Number", DateTime.UtcNow,
                                      "NHS 123", "Dr Death",
                                      ZiliComms.Handset.ReferralType.BSCC_Borderline,
                                      true);
            }

            Forms.SessionScreen sessionScreen = new Forms.SessionScreen(session);

            if (sessionScreen.ShowDialog(_FormMain).Equals(System.Windows.Forms.DialogResult.OK))
            {
                // add to command queue
                AddCommsTask(new CommsProceedure(SendSession, new object[] { sessionScreen.Session }));
            }
        }

        private void SendSession(object[] parameters)
        {
            ZiliComms.Handset.Session session = (ZiliComms.Handset.Session)parameters[0];

            if (_ZedScan.StartSession(session))
            {
                if (_ZedScan.Session.Equals(session))
                {
                    DisplayMessage($"Session {session} sent,{Environment.NewLine}        {_ZedScan.Session} received");

                    // save _ZedScan.Session
                }
                else
                    DisplayMessage("Session corrupt");
            }
            else
                DisplayMessage(_ZedScan.Message);
        }


        private void RequestSessionDetails()
        {
            if (_ZedScan.GetSession())
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.AppendLine("Session Details (* not part of communications)");
                //sb.AppendLine($"   *NHS Number       : {_ZedScan.Session.NHSnumber}");
                //sb.AppendLine($"   *Patient Name     : {_ZedScan.Session.PatientName}");
                //sb.AppendLine($"   *Date of Birth    : {_ZedScan.Session.DateOfBirth}");
                sb.AppendLine($"   *NHS Number       : Not held by handset");
                sb.AppendLine($"   *Patient Name     : Not held by handset");
                sb.AppendLine($"   *Date of Birth    : Not held by handset");

                sb.AppendLine($"    Patient Id       : {_ZedScan.Session.PatientId}");
                sb.AppendLine($"    Operator Id      : {_ZedScan.Session.OperatorId}");
                sb.AppendLine($"    Referral Type    : {_ZedScan.Session.ReferralType}");
                sb.AppendLine($"    Current Time     : {_ZedScan.Session.StartTime}");
                sb.AppendLine($"    Training Mode    : {_ZedScan.Session.TrainingMode}");

                DisplayMessage(sb.ToString());
            }
            else
            {
                DisplayMessage(_ZedScan.Message);
            }
        }

        private void DeleteSessionResults()
        {
            if (_ZedScan.DeleteSessionResults())
            {
                DisplayMessage("Session results deleted");
            }
            else
                DisplayMessage(_ZedScan.Message);
        }

        private void RequestResults()
        {
            if (_ZedScan.GetResults())
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.AppendLine("Session Results");
                sb.AppendLine($"    Scan Threshold     : {_ZedScan.Results.ScanThreshold}");
                sb.AppendLine($"    Single Threshold   : {_ZedScan.Results.SingleThreshold}");
                sb.AppendLine($"    High Grade Present : {_ZedScan.Results.HighGradePresent}");
                sb.AppendLine($"    ZedScan Opinion    : Screen {_ZedScan.Results.ZedScanOpinion}");

                DisplayMessage(sb.ToString());
            }
            else
            {
                DisplayMessage(_ZedScan.Message);
            }
        }




        private void SelectFirmware()
        {
            System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog();

            openFileDialog.Filter = "Firmware (*.bin)|*.bin";
            openFileDialog.Multiselect = false;
            openFileDialog.RestoreDirectory = false;
            openFileDialog.AutoUpgradeEnabled = true;
            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;
            openFileDialog.DefaultExt = "BIN";
            openFileDialog.ShowReadOnly = true;

            if (openFileDialog.ShowDialog().Equals(System.Windows.Forms.DialogResult.OK))
            {
                AddCommsTask(new CommsProceedure(SendFirmware, new object[] { openFileDialog.FileName }));
            }
        }

        private void SendFirmware(object[] parameters)
        {
            if (_ZedScan.SendFirmware((string)parameters[0]))
            {
                DisplayMessage("Firmware sent");

                _FormMain.ShowProgress(0);
            }
            else
                DisplayMessage(_ZedScan.Message);
        }



        private void SelectLanguagePack()
        {
            System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog();

            openFileDialog.Filter = "Language Packs (*.bin)|*.bin";
            openFileDialog.Multiselect = false;
            openFileDialog.RestoreDirectory = false;
            openFileDialog.AutoUpgradeEnabled = true;
            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;
            openFileDialog.DefaultExt = "BIN";
            openFileDialog.ShowReadOnly = true;

            if (openFileDialog.ShowDialog().Equals(System.Windows.Forms.DialogResult.OK))
            {
                AddCommsTask(new CommsProceedure(SendLanguagePack, new object[] { openFileDialog.FileName }));
            }
        }

        private void SendLanguagePack(object[] parameters)
        {
            if (_ZedScan.SendLanguagePack((string)parameters[0]))
            {
                DisplayMessage("Language pack sent");

                _FormMain.ShowProgress(0);
            }
            else
                DisplayMessage(_ZedScan.Message);
        }





        private void DecodeLog()
        {
            byte[] data =
            {
                0x42, 0x23, 0x04, 0x02, 0x05, 0x30, 0x2E, 0x30, 0x2E, 0x31, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                0xFF, 0xFF, 0x0C, 0x5A, 0x00, 0x00, 0x00, 0x53, 0x00, 0x00, 0x00, 0x32, 0x00, 0x00, 0x00, 0x2D,
                0x00, 0x00, 0x00, 0x48, 0x00, 0x00, 0x00, 0x53, 0x00, 0x00, 0x00, 0x2D, 0x00, 0x00, 0x00, 0x30,
                0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x31,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x11, 0x11, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00,
                0x00, 0xD0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x64, 0x00, 0xC8, 0x22, 0x22, 0x33, 0x33, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x44, 0x44, 0x04, 0x03, 0x00, 0x01, 0x00, 0x00, 0x00, 0x03, 0x00,
                0x0F, 0x00, 0x01, 0x00, 0x03, 0x00, 0x0B, 0x00, 0x01, 0x00, 0x23, 0x03, 0x0B, 0x01, 0x02, 0x00,
                0x55, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x66, 0x66
            };

            ZiliComms.Handset.LogRecord logRecord = new ZiliComms.Handset.LogRecord(data);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendLine("Log record");
            sb.AppendLine($"    Version            : {logRecord.Version}");
            sb.AppendLine($"    Firmware Version   : {logRecord.FirmwareVersion}");
            sb.AppendLine($"    Device ID          : {logRecord.DeviceID}");
            sb.AppendLine($"    Unique Id          : {logRecord.UniqueId}");
            sb.AppendLine($"    Alert Code         : {logRecord.AlertCode}");
            sb.AppendLine($"    Training Status    : {logRecord.TrainingModeStatus}");
            sb.AppendLine($"    Seconds            : {logRecord.ClockSeconds}");
            sb.AppendLine($"    Minutes            : {logRecord.ClockMinutes}");
            sb.AppendLine($"    Hours              : {logRecord.ClockHours}");
            sb.AppendLine($"    Days               : {logRecord.ClockDays}");
            sb.AppendLine($"    Months             : {logRecord.ClockMonths}");
            sb.AppendLine($"    Years              : {logRecord.ClockYears}");
            sb.AppendLine($"    Critical Flag      : {logRecord.CriticalErrorFlag}");
            sb.AppendLine($"    File Name          : {logRecord.CriticalErrorFileName}");
            sb.AppendLine($"    Line Number        : {logRecord.LineNumber}");
            sb.AppendLine($"    Error Code         : {logRecord.ErrorCode}");
            sb.AppendLine($"    Battery Percentage : {logRecord.BatteryPercentage}");
            sb.AppendLine($"    Dock State         : {logRecord.DockState}");
            sb.AppendLine($"    Points Scanned     : {logRecord.PointsScanned}");
            sb.AppendLine($"    Points Read        : {logRecord.PointsRead}");
            sb.AppendLine($"    Quality Checks     : {logRecord.QualityChecks}");
            sb.AppendLine($"    Measuring          : {logRecord.Measuring}");
            sb.AppendLine();
            sb.AppendLine($"    Event Records      : ");
            foreach (ZiliComms.Handset.LogEventRecord item in logRecord.EventRecords)
            {
                sb.AppendLine($"                       : {item}");
            }
            sb.AppendLine();
            sb.AppendLine($"    Fail Counts        : ");
            sb.AppendLine($"{logRecord.Measurements}");

            DisplayMessage(sb.ToString());
        }




        private void DebugTest()
        {
            #region Parse file to byte array

            System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog();

            openFileDialog.Filter = "Comms Trace (*.txt)|*.txt";
            openFileDialog.Multiselect = false;
            openFileDialog.RestoreDirectory = false;
            openFileDialog.AutoUpgradeEnabled = true;
            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;
            openFileDialog.DefaultExt = "TXT";
            openFileDialog.ShowReadOnly = true;

            List<byte> payLoad = new List<byte>();

            if (openFileDialog.ShowDialog().Equals(System.Windows.Forms.DialogResult.OK))
            {
                var fileData = System.IO.File.ReadLines(openFileDialog.FileName,System.Text.Encoding.ASCII);

                foreach (var line in fileData)
                {
                    if (line.ToUpper().Contains("RX :"))
                    {
                        int start = line.ToUpper().IndexOf(":");
                        start++;

                        while (start < line.Length)
                        {
                            start++;

                            string sample = line.Substring(start, 2);

                            payLoad.Add(Convert.ToByte(sample, 16));

                            start += 2;
                        }

                        break;
                    }
                }
            }

            #endregion


            bool decoded = ZiliComms.PacketWrapper.Decode(payLoad);


            // from ..\_Docs\Results trace.txt

            // field (1) Address  2 - 17 0a 08 4a ...
            // field (2) Address 26 - 12 07 49 6e 76
            // field (3) Address 35 - 1d ee 7c 3f 3f = 0.748
            // field (4) Address 40 - 25 00 00 00 3f
            // field (5) Address 45 - 

            //      field # & wire type
            // 2a =   5      LengthDelimit
            // 0f = length = 15
            // 0d =   1      bit32

            // Address  45, 2a 0f  0d  00 00 00 00  15  5c 1c ff 43  1d  42 44 4e bc
            //          62, 2a 0f  0d  00 00 80 3f  15  3e d4 fd 43  1d  38 36 a5 bb
            //          79  2a 0f  0d  00 00 00 40  15  ea 1d ff 43  1d  7b b7 06 bc
            //          96  2a 0f  0d  00 00 40 40  15  9d bf fe 43  1d  e3 23 94 bc
            //         113  2a 0f  0d  00 00 80 40  15  a4 8d fe 43  1d  0c a1 19 bd
            //         130  2a 0f  0d  00 00 a0 40  15  a5 07 fe 43  1d  58 8d 9e bd
            //         147  2a 0f  0d  00 00 c0 40  15  75 dd fb 43  1d  9d 5c 1d be *
            //         164  2a 0f  0d  00 00 e0 40  15  1f 53 f3 43  1d  b1 3b 9a be *
            //         181  2a 0f  0d  00 00 00 41  15  00 06 d8 43  1d  00 7b 0e bf
            //         198  2a 0f  0d  00 00 10 41  15  38 c2 9e 43  1d  36 48 64 bf
            //         215  2a 0f  0d  00 00 20 41  15  98 56 3c 43  1d  87 14 97 bf
            //         232  2a 0f  0d  00 00 30 41  15  a1 94 c6 42  1d  b0 6a ad bf
            //         249  2a 0f  0d  00 00 40 41  15  85 73 48 42  1d  73 98 b8 bf
            //         266  2a 0f  0d  00 00 50 41  15  bc 06 c7 41  1d  e8 a7 be bf

            // 0x32 =   #6      LengthDelimit
            // 0x8f 0x03 = length = 399
            // 0x08 

            // Address 284, 0x8f 0x03 0x08


            if (!_ZedScan.TestGetResults(ZiliComms.PacketWrapper.PayLoad)) //payLoad.ToArray()))
                DisplayMessage($"Get Results Error : {_ZedScan.Message}");
            else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                sb.AppendLine($"HighGrade Present  : {_ZedScan.Results.HighGradePresent}");
                sb.AppendLine($"Serial Number      : {_ZedScan.Results.SerialNumber}");
                sb.AppendLine($"Session Info       : {_ZedScan.Results.SessionInfo}");

                foreach (var cal in _ZedScan.Results.CalibrationData)
                {
                    sb.AppendLine($"Calibration        : {cal}");
                }

                foreach (var reading in _ZedScan.Results.Readings)
                {
                    sb.AppendLine($"Reading            : {reading}");
                }

                DisplayMessage(sb.ToString());

                if (!_ZedScan.TestSetResult(_ZedScan.Results))
                    DisplayMessage($"Set Results Error : {_ZedScan.Message}");
                else
                {
                    DisplayMessage("Results decoded and encoded ok");
                }
            }

        }
    }
}
// Copyright (c) 2022 Zilico Limited
