﻿
namespace ZiliCator.Forms
{
    partial class ParameterEntryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cytologyListBox = new System.Windows.Forms.ComboBox();
            this.numberLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.cytologyLabel = new System.Windows.Forms.Label();
            this.parameterTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cytologyListBox
            // 
            this.cytologyListBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cytologyListBox.FormattingEnabled = true;
            this.cytologyListBox.Location = new System.Drawing.Point(99, 16);
            this.cytologyListBox.Name = "cytologyListBox";
            this.cytologyListBox.Size = new System.Drawing.Size(205, 21);
            this.cytologyListBox.TabIndex = 12;
            // 
            // numberLabel
            // 
            this.numberLabel.AutoSize = true;
            this.numberLabel.Location = new System.Drawing.Point(11, 19);
            this.numberLabel.Name = "numberLabel";
            this.numberLabel.Size = new System.Drawing.Size(44, 13);
            this.numberLabel.TabIndex = 10;
            this.numberLabel.Text = "Number";
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(229, 46);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 9;
            this.okButton.Text = "Ok";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(148, 46);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 8;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // cytologyLabel
            // 
            this.cytologyLabel.AutoSize = true;
            this.cytologyLabel.Location = new System.Drawing.Point(11, 19);
            this.cytologyLabel.Name = "cytologyLabel";
            this.cytologyLabel.Size = new System.Drawing.Size(47, 13);
            this.cytologyLabel.TabIndex = 11;
            this.cytologyLabel.Text = "Cytology";
            this.cytologyLabel.Visible = false;
            // 
            // parameterTextBox
            // 
            this.parameterTextBox.Location = new System.Drawing.Point(99, 18);
            this.parameterTextBox.Name = "parameterTextBox";
            this.parameterTextBox.Size = new System.Drawing.Size(205, 20);
            this.parameterTextBox.TabIndex = 7;
            // 
            // ParameterEntryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 84);
            this.Controls.Add(this.cytologyListBox);
            this.Controls.Add(this.numberLabel);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.cytologyLabel);
            this.Controls.Add(this.parameterTextBox);
            this.Name = "ParameterEntryForm";
            this.Text = "ParameterEntryForm";
            this.Load += new System.EventHandler(this.ParameterEntryForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cytologyListBox;
        private System.Windows.Forms.Label numberLabel;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label cytologyLabel;
        private System.Windows.Forms.TextBox parameterTextBox;
    }
}