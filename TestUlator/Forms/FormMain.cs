﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using ZiliComms.Commands;
using System.Timers;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ZiliCator
{
    public partial class FormMain : Form
    {
        private string _ApplicationTitle;
        static int Retry_Counter = 0;
        static bool Result_Pending = false;
        Microsoft.Office.Interop.Word.Application winword;
        AutoTest AutoTestUnit;
        bool InCommand = false;

        #region Adaptor Selection Event

        internal event EventHandler<AdaptorType> AdaptorSelectionEvent;

        protected virtual void OnAdaptorSelectionEvent(object e)
        {
            if (AdaptorSelectionEvent != null) AdaptorSelectionEvent(this, (AdaptorType)e);
        }

        private void RaiseAdaptorSelectionEvent(AdaptorType adaptorType)
        {
            OnAdaptorSelectionEvent(adaptorType);
        }

        #endregion

        #region Change Baud Rate Event

        internal event EventHandler<UInt32> ChangeBaudRateEvent;

        protected virtual void OnChangeBaudRateEvent(object e)
        {
            if (ChangeBaudRateEvent != null) ChangeBaudRateEvent(this, (UInt32)e);
        }

        private void RaiseChangeBaudRateEvent(UInt32 baudRate)
        {
            OnChangeBaudRateEvent(baudRate);
        }

        #endregion

        #region Single Byte Mode Event

        internal event EventHandler<bool> ChangeSingleByteModeEvent;

        protected virtual void OnChangeSingleByteModeEvent(object e)
        {
            if (ChangeSingleByteModeEvent != null) ChangeSingleByteModeEvent(this, (bool)e);
        }

        private void RaiseChangeSingleByteModeEvent(bool singleByteMode)
        {
            OnChangeSingleByteModeEvent(singleByteMode);
        }

        #endregion 

        #region Execute Command Event

        internal event EventHandler<OpCode> ExecuteCommandEvent;

        protected virtual void OnExecuteCommandEvent(object e)
        {
            if (ExecuteCommandEvent != null) ExecuteCommandEvent(this, (OpCode)e);
        }

        private void RaiseExecuteCommandEvent(OpCode commandCode)
        {
            OnExecuteCommandEvent(commandCode);
        }

        #endregion

        uint TestCurrentTime;
        DateTime TestDate = new DateTime(2021, 09, 02, 13, 00, 00);
        String TestoperatorID = "Bill Bob";
        String TestpatientID = "4321";
        String TestpatientName = "Daisy Duke";
        String TestpatientDOB = @"26/7/97";


        public void DisplayMessage(string message)
        {
            try
            {
                this.Invoke((MethodInvoker)delegate
                {
                    if (textBoxDisplay.Text.Length > 32768)
                        textBoxDisplay.Text = textBoxDisplay.Text.Substring(textBoxDisplay.Text.Length - 32768);

                    textBoxDisplay.Text += Environment.NewLine;
                    textBoxDisplay.Text += message;
                    textBoxDisplay.Text += Environment.NewLine;

                    textBoxDisplay.SelectionStart = textBoxDisplay.TextLength;
                    textBoxDisplay.ScrollToCaret();
                }, message);
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e.Message);

                // race to display message before window handle created... caused by presetting adaptor
            }
        }

        public void AddBaudRates(List<string> rates)
        {
            foreach (string item in rates)
            {
                ToolStripMenuItem toolStripItem = new ToolStripMenuItem(item, null, BaudSelection);

                toolStripItem.CheckOnClick = true;

                baudRateToolStripMenuItem.DropDownItems.Add(toolStripItem);
            }
        }

        internal void SetAdaptor(AdaptorType adaptor)
        {
            ToolStripMenuItem menuItem = null;

            switch (adaptor)
            {
                case AdaptorType.NotSet:
                    break;
                case AdaptorType.ZedScan_1:
                    menuItem = zedScan1ToolStripMenuItem;
                    break;
                case AdaptorType.vComm:
                    menuItem = vCommToolStripMenuItem;
                    break;
                case AdaptorType.HID:
                    menuItem = hIDToolStripMenuItem;
                    break;
                default:
                    break;
            }

            if (menuItem != null)
            {
                menuItem.Checked = true;
                //MenuAdaptorSelection(menuItem, null);
            }
        }

        public bool SingleByteMode// => singleByteModeToolStripMenuItem.Checked; // read only
        {
            get { return singleByteModeToolStripMenuItem.Checked; }
            set { singleByteModeToolStripMenuItem.Checked = value; }
        }



        public FormMain(string applicationTitle)
        {
            InitializeComponent();

            #region Title and icon

            _ApplicationTitle = applicationTitle;

            StringBuilder caption = new StringBuilder(_ApplicationTitle);
            caption.Append("    Version ");
            caption.Append(Application.ProductVersion);
            this.Text = caption.ToString();

     //       this.Icon = System.Drawing.Icon.ExtractAssociatedIcon(System.Windows.Forms.Application.ExecutablePath);

            #endregion

            textBoxDisplay.Text = string.Empty;
            AutoTestUnit = new AutoTest();
            screenPictureBox.Image = new Bitmap(160, 160);
            screenCopyPictureBox.Image = new Bitmap(160, 160);
            /*
             * Check databse connection
             */
            commandDataGridView.ColumnCount = 3;
            commandDataGridView.Columns[0].Name = "Command";
            commandDataGridView.Columns[1].Name = "Parameter";
            commandDataGridView.Columns[2].Name = "Expected Reply";
            commandDataGridView.Columns[0].MinimumWidth = 200;
            commandDataGridView.Columns[2].MinimumWidth = 400;

            TestCurrentTime = IntegerDateTime.ConvertToInteger(TestDate);
            string[] AvalibleComPorts = SerialPort.GetPortNames();
            ComPortList.Items.Clear();
            if (AvalibleComPorts.Count() > 0)
            {
                foreach (String ComPort in AvalibleComPorts)  //add each avalible com port to the combo box lists
                {
                    ComPortList.Items.Add(ComPort);
                }
                ComPortList.SelectedIndex = 0;
                ComPortList.Invalidate();
            }
            else
            {
                MessageBox.Show("No available COM Ports found.");
            }
        }



        private void clearScreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBoxDisplay.Text = string.Empty;
            TechnicalDisplayBox.Clear();
        }

        private void MenuAdaptorSelection(object sender, EventArgs e)
        {
            ToolStripMenuItem thisAdaptor = (ToolStripMenuItem)sender;

            if (thisAdaptor.Checked)
            {
                AdaptorType adaptorType = AdaptorType.NotSet;

                if (thisAdaptor != zedScan1ToolStripMenuItem)
                    zedScan1ToolStripMenuItem.Checked = false;
                else
                    adaptorType = AdaptorType.ZedScan_1;

                if (thisAdaptor != vCommToolStripMenuItem)
                    vCommToolStripMenuItem.Checked = false;
                else
                    adaptorType = AdaptorType.vComm;

                if (thisAdaptor != hIDToolStripMenuItem)
                    hIDToolStripMenuItem.Checked = false;
                else
                    adaptorType = AdaptorType.HID;

                RaiseAdaptorSelectionEvent(adaptorType);
            }
            else
            {
                thisAdaptor.Checked = true;                         // ignore if already checked
            }
        }

        private void BaudSelection(object sender, EventArgs e)
        {
            ToolStripMenuItem thisBaudRate = (ToolStripMenuItem)sender;

            if (thisBaudRate.Checked)
            {
                foreach (ToolStripMenuItem item in baudRateToolStripMenuItem.DropDownItems)
                {
                    if (item != thisBaudRate)
                        item.Checked = false;
                }

                System.Diagnostics.Trace.WriteLine($"raise baud change event {sender}");

                UInt32 baud = 0;

                if (UInt32.TryParse(sender.ToString(), out baud))
                    RaiseChangeBaudRateEvent(baud);
                else
                    DisplayMessage($"Illegal Baud Rate {sender}");
            }
            else
                RaiseChangeBaudRateEvent(0);                        // set baud rate to none
        }

        private void singleByteModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RaiseChangeSingleByteModeEvent(singleByteModeToolStripMenuItem.Checked);
        }



        private void buttonInfo_Click(object sender, EventArgs e)
        {
            RaiseExecuteCommandEvent(OpCode.REQUEST_DEVICE_INFO);
        }

        private void buttonStartSession_Click(object sender, EventArgs e)
        {
            RaiseExecuteCommandEvent(OpCode.SESSION_INFORMATION);
        }

        private void buttonReadSession_Click(object sender, EventArgs e)
        {
            RaiseExecuteCommandEvent(OpCode.REQUEST_SESSION_INFORMATION);
        }

        private void buttonDeleteSession_Click(object sender, EventArgs e)
        {
            RaiseExecuteCommandEvent(OpCode.DELETE_SESSION_RESULTS);
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button60_Click(object sender, EventArgs e)
        {
            if (AutoTestUnit.IsConnected() == false)
            {
                if (ComPortList.SelectedItem != null)
                {
                    AutoTestUnit.Connect(230400, ComPortList.SelectedItem.ToString());
                    button60.Text = "Disconnect AutoTest";
                }
            }
            else
            {
                AutoTestUnit.Disconnect();
                button60.Text = "Connect AutoTest";
            }
        }
        const byte KEY_ALL_OFF = 0;
        const byte KEY_RIGHT = 1;
        const byte KEY_LEFT = 2;
        const byte KEY_ENTER = 4;
        const byte KEY_DOWN = 8;
        const byte KEY_SCAN = 16;
        private void button12_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[10];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GetKeyboardState();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on get keybard state: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                AutoTestUnit.GetReply(ref Answer);
                if (Answer[0] == 0)
                    UpdateTechnicalConsole("No Keys Pressed\n");
                if ((Answer[0] & KEY_RIGHT) > 0)
                    UpdateTechnicalConsole("Key 3 On \n");
                if ((Answer[0] & KEY_LEFT) > 0)
                    UpdateTechnicalConsole("Key 1 On \n");
                if ((Answer[0] & KEY_ENTER) > 0)
                    UpdateTechnicalConsole("Key 2 On \n");
                if ((Answer[0] & KEY_SCAN) > 0)
                    UpdateTechnicalConsole("Scan Key On \n");
            }
            Cursor.Current = Cursors.Default;

        }

        private void button15_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Enable output", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetRedLed(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set Red LED: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Red LED set = " + status.ToString() + " \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button23_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Enable output", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetGreenLed(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set Green LED: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Green LED set = " + status.ToString() + " \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button24_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[10];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GetSnoutState();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on get snout state: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                AutoTestUnit.GetReply(ref Answer);
                if (Answer[0] == 0)
                    UpdateTechnicalConsole("Sensor not fitted\n");
                else
                    UpdateTechnicalConsole("Sensor fitted\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button25_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Enable LCD", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetLCD_State(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set LCD state: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("LCD set = " + status.ToString() + " \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button28_Click(object sender, EventArgs e)
        {
            byte status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetLCD_TestScreen();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set LCD Test Screen: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("LCD Test Screen Displayed " + status.ToString() + " \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button26_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Enable output", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetLCD_Backlight(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set LCD Backlight Drive: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Backlight set = " + status.ToString() + " \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button27_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Enable output", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetLCD_Dim(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set LCD dim control: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Dim set = " + status.ToString() + " \n");
            }
            Cursor.Current = Cursors.Default;

        }

        private void button29_Click(object sender, EventArgs e)
        {
            byte status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetSpeakerFrequency(UInt16.Parse(textBox2.Text));
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set speaker frequency: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Speaker frequency set " + status.ToString() + " \n");
            }
            Cursor.Current = Cursors.Default;

        }

        private void button63_Click(object sender, EventArgs e)
        {
            byte status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.ResetHandset();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on reset handset " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Handset reset requested \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button79_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Enable Pwr", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetUARTBufferPwr(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set UART Buffer Power: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("UART Buffer Pwr = " + status.ToString() + " \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button80_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Enable Output", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetUARTBufferOE(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set UART Buffer OE: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("UART Buffer OE = " + status.ToString() + " \n");
            }
            Cursor.Current = Cursors.Default;
        }
        void UpdateTechnicalConsole(string MyLine)
        {
            TechnicalDisplayBox.AppendText(MyLine);
            TechnicalDisplayBox.SelectionStart = TechnicalDisplayBox.TextLength;
            TechnicalDisplayBox.ScrollToCaret();
        }
        private void button81_Click(object sender, EventArgs e)
        {
            byte MyIndex = (byte)backlightComboBox.SelectedIndex;
            if (backlightComboBox.SelectedItem == null)
                return;
            if ((MyIndex >= 0) && (MyIndex <= 10))
            {
                MyIndex = (byte)(MyIndex * 10);
                if (InCommand == true)
                {
                    MessageBox.Show("Busy");
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                AutoTestUnit.ClearBuffer();
                int Result = AutoTestUnit.SetBacklightPWM_Pwr(MyIndex);
                if (Result != 0)
                {
                    UpdateTechnicalConsole("Error on set backlight PWM: " + AutoTestUnit.ErrorToText(Result) + "\n");
                }
                else
                {
                    UpdateTechnicalConsole("Backlight set to " + backlightComboBox.SelectedItem.ToString() + "\n");
                }
                Cursor.Current = Cursors.Default;
            }
            else
                MessageBox.Show("Select power first");
        }

        string SetFileName = "";
        private void loadSetButton_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofdlg = new OpenFileDialog();
                ofdlg.DefaultExt = ".ztestset";
                ofdlg.Filter = "ZedScan Test Script files (*.ztestset)|*.ztestset";
                // Do something  
                if ((ofdlg.ShowDialog() == DialogResult.OK) && (System.IO.File.Exists(ofdlg.FileName)))
                {
                    scriptSetGridView.Rows.Clear();
                    SetFileName = ofdlg.FileName;
                    ScriptLoadSaveObject LoadIt = new ScriptLoadSaveObject();
                    LoadScript(scriptSetGridView, ofdlg.FileName, LoadIt);
                    resultSetGridView.Rows.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem loading test set " + ex.Message.ToString());
            }
        }

        private void runScriptSetButton_Click(object sender, EventArgs e)
        {
            if (InCommand == false)
            {
                StartScriptSet();
            }
        }
        private bool Playing = false;
        private int PlayLineIndex = 0;
        private int ResetProtection = 0;
        private bool ScriptFailedFlag = false;
        private int ScriptCount = 0;
        private int ScriptFailCount = 0;
        int ScriptSetIndex = 0;
        bool ExecScriptSet = false;
        private void StartScriptSet()
        {
            ScriptFailedFlag = false;
            ScriptCount = 0;
            ScriptFailCount = 0;
            InCommand = true;
            ExecScriptSet = true;
            Cursor.Current = Cursors.WaitCursor;
            // now fire up executer
            resultSetGridView.Rows.Clear();
            ScriptSetIndex = 0;
            scriptSetTimer.Enabled = true;
        }
        private bool ScriptFailed()
        {
            return (ScriptFailedFlag);
        }

        private void SetScriptFailed()
        {
            ScriptFailedFlag = true;
            ScriptFailCount++; ;
        }
        int hardResetProtection = 0;
        int ResetAttempts = 0;
        private void StartScript()
        {
            ScriptFailedFlag = false;
            PlayLineIndex = 0;
            ResetAttempts = 0;
            Playing = true;
            Cursor.Current = Cursors.WaitCursor;
            InCommand = true;
            runScriptButton.Text = "Stop Script";
            runScriptButton.BackColor = Color.Red;
            /* Always reset target to known point befor testing */

            tmrDevicePoll.Enabled = true;
            playbackTimer.Enabled = true;
            /* always start from the start to prevent manipulation of device */

        }
        private void StopScriptSet()
        {
            DateTime now = DateTime.Now;
            scriptSetTimer.Enabled = false;
            InCommand = false;
            ExecScriptSet = false;
            Cursor.Current = Cursors.Default;
            AddExecResult("Set complete at " + now.ToShortDateString() + " " + now.ToShortTimeString());
            SaveGeneratedReport();
        }
        enum User_Interactions
        {
            USER_DOCK,
            USER_UNDOCK,
            USER_SHEATH_ON,
            USER_SHEATH_OFF,
            USER_PRESS_KEY1,
            USER_PRESS_KEY_ENTER,
            USER_PRESS_KEY3,
            USER_PRESS_KEY_SCAN,
            USER_COMPARE_SCREEN,
            USER_CREATE_READING,
            USER_SET_BOUNDARY,
            USER_SET_BATTERY,
            USER_CREATE_SESSION,
            USER_CANCEL_SESSION,
            USER_COMPARE_SYSTEM_STATE,
            USER_COMPARE_LED_STATE,
            USER_SET_CYTOLOGY,
            USER_SET_SESSION_TIME,
            USER_DELAY_NMS

        };
        readonly string[] CommandTable ={"USER_DOCK" ,
            "USER_UNDOCK" ,
            "USER_SHEATH_ON" ,
            "USER_SHEATH_OFF" ,
            "USER_PRESS_KEY1",
            "USER_PRESS_ENTER",
            "USER_PRESS_KEY3",
            "USER_PRESS_KEY_SCAN",
            "USER_COMPARE_SCREEN" ,
            "USER_CREATE_READING" ,
            "USER_SET_BOUNDARY" ,
            "USER_SET_BATTERY" ,
            "USER_CREATE_SESSION" ,
            "USER_CANCEL_SESSION",
            "USER_COMPARE_SYSTEM_STATE",
            "USER_COMPARE_LED_STATE",
            "USER_SET_CYTOLOGY",
            "USER_SET_SESSION_TIME",
            "USER_DELAY_NMS"
        };
        private void SaveGeneratedReport()
        {
            try
            {
                // we completed the batch script so auto save the report file
                if (resultSetGridView.Rows.Count == 0)
                    return;
                DateTime now = DateTime.Now;
                string ResultFileName = SetFileName.Split('.')[0];
                ResultFileName = ResultFileName + " Result " + now.ToShortDateString() + " " + now.ToShortTimeString() + ".txt";
                ResultFileName = ResultFileName.Replace(':', '-');
                ResultFileName = ResultFileName.Replace('/', '-');
                /* THeres an issue with the path in the stream writer. It appends existing path with one on the fiale name */
                String MyPath = Path.GetDirectoryName(ResultFileName);
                //                Directory.SetCurrentDirectory(MyPath);
                ResultFileName = Path.GetFileName(ResultFileName);

                using (StreamWriter writer = new StreamWriter(ResultFileName))
                {

                    foreach (DataGridViewRow Row in resultSetGridView.Rows)
                    {
                        if ((Row.Cells.Count >= 1) && (Row.Cells[0].Value != null))
                        {
                            writer.WriteLine(Row.Cells[0].Value.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem saving test report " + ex.Message.ToString());
            }
        }
        public void PaintScreenMap(PictureBox screen, byte[] Buffer)
        {
            int currentX = 0;
            int currentY = 0;
            int r, g, b;
            for (UInt32 Index = 0; Index < (128 * 160 * 2); Index = Index + 2)
            {
                uint Data = ((uint)Buffer[Index] << 8) | (uint)Buffer[Index + 1];
                // colours are 5 bit each in 16 bits
                // * Input: 5 bits red, 6 bits green, 5 bits blue color. */
                r = (int)(Data & 0x001f);
                g = (int)((Data >> 5) & 0x003f);
                b = (int)((Data >> 11) & 0x001f);
                // colours are 5 bit so shift left by 3
                r = r << 3;
                g = g << 2;
                b = b << 3;
                // draw screen buffer onto windows bitmap
                ((Bitmap)screen.Image).SetPixel(currentX, currentY, Color.FromArgb(r, g, b));
                currentX++;
                if (currentX >= 128)
                {
                    currentX = 0;
                    currentY++;
                }
            }
            screen.Refresh();
        }
        private void StartSession()
        {
        }
        private bool ExecuteAndVerifyCommand(int PlayLineIndex)
        {
            if (commandDataGridView.Rows[PlayLineIndex].Cells[0].Value == null)
                return true;
            string Command = commandDataGridView.Rows[PlayLineIndex].Cells[0].Value.ToString();
            string Parameter = commandDataGridView.Rows[PlayLineIndex].Cells[1].Value.ToString();
            string ExpectedReply = commandDataGridView.Rows[PlayLineIndex].Cells[2].Value.ToString();

            AutoTestUnit.ClearBuffer();
            AddExecResult("Exe " + Command + " " + Parameter);


            if (Command.Equals(CommandTable[(int)User_Interactions.USER_CANCEL_SESSION]) == true)
            {//
                return true;
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_COMPARE_SCREEN]) == true)
            {// 
                Cursor.Current = Cursors.WaitCursor;
                byte[] ScreenArray = new byte[0];
                byte[] CompareArray = new byte[0];
                AutoTestUnit.ClearBuffer();
                int Result = AutoTestUnit.GrabScreen();
                if (Result != 0)
                {
                    return false;
                    //MessageBox.Show("Error on compare screen: " + AutoTestUnit.ErrorToText(Result));
                }
                else
                {
                    AutoTestUnit.GetReply(ref ScreenArray);
                    PaintScreenMap(screenPictureBox, ScreenArray);
                    try
                    {
                        if (System.IO.File.Exists(Parameter))
                        {

                            CompareArray = System.IO.File.ReadAllBytes(Parameter);
                            PaintScreenMap(screenCopyPictureBox, CompareArray);
                            if (CompareArray.Count() != ScreenArray.Count())
                            {
                                //  MessageBox.Show("Screen file wrong size = " + CompareArray.Count().ToString());
                                return false;
                            }
                            bool Match = true;
                            int Index = 0;
                            foreach (byte Data in ScreenArray)
                            {
                                if (Data != CompareArray[Index])
                                {
                                    AddExecResult("Screen doesn't match!");
                                    if (ExecScriptSet == false)
                                        MessageBox.Show("Screen doesn't match!");
                                    return false; ;
                                }
                                Index++;
                            }
                        }
                        else
                        {
                            AddExecResult("Screen file not found");
                            return false;
                            //MessageBox.Show("Screen file not found");
                        }
                    }
                    catch (Exception ex)
                    {
                        AddExecResult("Failed to compare file with screen" + ex.Message);
                        return false;
                    }
                }
                return true;
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_CREATE_READING]) == true)
            {// 
                return true;
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_CREATE_SESSION]) == true)
            {// 
                StartSession();
                return true;
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_DELAY_NMS]) == true)
            {// 
                Thread.Sleep(int.Parse(Parameter));
                return true;
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_DOCK]) == true)
            {// 
                if (AutoTestUnit.DockHandset() != 0)
                    return false;
                else
                    return true;
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_PRESS_KEY_ENTER]) == true)
            {// 
                if (AutoTestUnit.EnterKey() != 0)
                    return false;
                else
                    return true;
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_PRESS_KEY_SCAN]) == true)
            {// 
                if (AutoTestUnit.ScanKey() != 0)
                    return false;
                else
                    return true;
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_PRESS_KEY1]) == true)
            {// 
                if (AutoTestUnit.LeftKey() != 0)
                    return false;
                else
                    return true;
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_PRESS_KEY3]) == true)
            {// 
                if (AutoTestUnit.RightKey() != 0)
                    return false;
                else
                    return true;
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_SET_BATTERY]) == true)
            {// 
                try
                {
                    if (AutoTestUnit.SetBattery(int.Parse(Parameter)) != 0)
                        return false;
                    else
                        return true;
                }
                catch
                {
                    return false;
                }
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_SET_BOUNDARY]) == true)
            {// 
                try
                {
                    if (AutoTestUnit.SetBoundary(int.Parse(Parameter)) != 0)
                        return false;
                    else
                        return true;
                }
                catch
                {
                    return false;
                }
            }

            if (Command.Equals(CommandTable[(int)User_Interactions.USER_SHEATH_OFF]) == true)
            {// 
                if (AutoTestUnit.RemoveSheath() != 0)
                    return false;
                else
                    return true;
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_SHEATH_ON]) == true)
            {// 
                if (AutoTestUnit.FitSheath() != 0)
                    return false;
                else
                    return true;
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_UNDOCK]) == true)
            {// 
                if (AutoTestUnit.UnDockHandset() != 0)
                    return false;
                else
                    return true;
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_COMPARE_SYSTEM_STATE]) == true)
            {// 
                byte[] Answer = new byte[10];
                if (AutoTestUnit.GetSMState() != 0)
                {
                    return false;
                }
                else
                {
                    AutoTestUnit.GetReply(ref Answer);
                    int State = Answer[0] << 8;
                    State = State | Answer[1];
                    if (ExpectedReply.Equals(ConvertSystemStateIndexToString(State)) == true)
                    {
                        return true;
                    }
                    else
                    {
                        AddExecResult("Current state " + ConvertSystemStateIndexToString(State) + " Doesn't match " + ExpectedReply);
                        return false;
                    }
                }
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_COMPARE_LED_STATE]) == true)
            {// 
                byte[] Answer = new byte[10];
                if (AutoTestUnit.GetLEDState() != 0)
                {
                    return false;
                }
                else
                {
                    AutoTestUnit.GetReply(ref Answer);
                    int State = Answer[0] << 8;
                    State = State | Answer[1];
                    if (ExpectedReply.Equals(ConvertLEDStateIndexToString(State)) == true)
                    {
                        return true;
                    }
                    else
                    {
                        AddExecResult("Current LED state " + ConvertLEDStateIndexToString(State) + " Doesn't match " + ExpectedReply);
                        return false;
                    }
                }
            }
            if (Command.Equals(CommandTable[(int)User_Interactions.USER_SET_CYTOLOGY]) == true)
            {
                //     TestreferralType = (ReferralType)(int)Enum.Parse(typeof(ParameterEntryForm.Referral_Type_et), Parameter);
                return true;
            }

            return false;
        }

        private void hardResetTimer_Tick(object sender, EventArgs e)
        {
            hardResetTimer.Enabled = false;
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void playbackTimer_Tick(object sender, EventArgs e)
        {
            playbackTimer.Enabled = false;
            if (PlayLineIndex < commandDataGridView.Rows.Count)
            {

                //if((PlayLineIndex==0) && (lblConnectionStatus.Text.Equals("Connected")==false))
                //{// we're waiting for reset unit to come back online
                //    if (ResetProtection > 150)
                //    {/* reset wasn't clean so go again*/
                //        tmrDevicePoll.Enabled = true;
                //        _deviceConnectionStatus = DeviceConnectionStatus.DISCONNECTED;
                //        pollForDeviceSent = PollingForDevice.INACTIVE;
                //        AutoTestUnit.TargetReset();
                //        ResetProtection = 0;
                //        ResetAttempts ++;
                //        if (ResetAttempts > 3)
                //        {
                //            SetScriptFailed();
                //            StopScript();
                //            if (ExecScriptSet == false) 
                //                MessageBox.Show("Error, device failed to reset correctly");
                //            AddExecResult("Error, device failed to reset correctly");
                //            return;
                //        }
                //    }
                //    ResetProtection++;
                //    playbackTimer.Enabled = true;
                //}
                //else
                {
                    ResetProtection = 0;
                    if (ExecuteAndVerifyCommand(PlayLineIndex) == false)
                    {
                        StopScript();
                        SetScriptFailed();
                        if (ExecScriptSet == false)
                            MessageBox.Show("Error at line : " + PlayLineIndex.ToString());
                        AddExecResult("Error at line : " + PlayLineIndex.ToString());
                    }
                    else
                    {
                        playbackTimer.Enabled = true;
                    }
                    PlayLineIndex++;
                    if ((PlayLineIndex < commandDataGridView.Rows.Count) &&
                        (commandDataGridView.Rows[PlayLineIndex].Cells[0] != null))
                        commandDataGridView.CurrentCell = commandDataGridView.Rows[PlayLineIndex].Cells[0];
                }
            }
            else
            {/* reached final line so success, generate report? */
                StopScript();
                ScriptCount++;
                if (ExecScriptSet == false)
                    MessageBox.Show("Completed script at ");
                AddExecResult("Completed script at ");
            }

        }

        private void StopScript()
        {
            Playing = false;
            tmrDevicePoll.Enabled = false;
            runScriptButton.Text = "Run Script";
            runScriptButton.BackColor = Color.Lime;
            playbackTimer.Enabled = false;
            InCommand = false;
            Cursor.Current = Cursors.Default;
        }
        public bool SaveScript(DataGridView Grid, string FileName, ScriptLoadSaveObject MyObject)
        {

            try
            {
                ScriptLoadSaveObject SaveIt = new ScriptLoadSaveObject();
                foreach (DataGridViewRow Row in Grid.Rows)
                {
                    if (Row.Cells[0].Value != null)
                    {
                        if (Row.Cells.Count < 1)
                            return (false);
                        string[] TextRow = new string[Row.Cells.Count];
                        TextRow[0] = Row.Cells[0].Value.ToString();
                        if (Row.Cells.Count >= 3)
                        {
                            if (Row.Cells[1].Value != null) TextRow[1] = Row.Cells[1].Value.ToString();
                            else TextRow[1] = "";
                            if (Row.Cells[2].Value != null) TextRow[2] = Row.Cells[2].Value.ToString();
                            else TextRow[2] = "";
                        }
                        MyObject.Script.Add(TextRow);
                    }
                }
                if (MyObject.Script.Count > 0)
                {
                    System.IO.FileStream xStream = new System.IO.FileStream(
                    FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write,
                       System.IO.FileShare.None);
                    System.Xml.Serialization.XmlSerializer serialiser =
                        new System.Xml.Serialization.XmlSerializer(MyObject.GetType());
                    serialiser.Serialize(xStream, MyObject);
                    xStream.Close();
                }
            }
            catch
            {
                return false;
            }
            return (true);
        }

        public bool LoadScript(DataGridView Grid, string FileName, ScriptLoadSaveObject MyObject)
        {
            string[] TextRow = new string[3];

            try
            {
                System.IO.FileStream xStream = new System.IO.FileStream(
                    FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read,
                    System.IO.FileShare.Read);
                MyObject = new ScriptLoadSaveObject();
                System.Xml.Serialization.XmlSerializer serialiser =
                    new System.Xml.Serialization.XmlSerializer(MyObject.GetType());
                MyObject = (ScriptLoadSaveObject)serialiser.Deserialize(xStream);
                xStream.Close();
                foreach (string[] Line in MyObject.Script)
                {
                    Grid.Rows.Add(Line);
                }
            }
            catch
            {
                return false;
            }
            return (true);
        }

        private void tmrDevicePoll_Tick(object sender, EventArgs e)
        {

        }

        private void scriptSetTimer_Tick(object sender, EventArgs e)
        {
            string FileName = "";
            DateTime now = DateTime.Now;

            scriptSetTimer.Enabled = false;
            if (Playing == false && ExecScriptSet == true)
            {// no script running so load next script
                // check if we're at the end of the list
                if ((ScriptSetIndex >= scriptSetGridView.Rows.Count) ||
                    (scriptSetGridView.Rows[ScriptSetIndex].Cells.Count != 1) ||
                    (scriptSetGridView.Rows[ScriptSetIndex].Cells[0].Value == null))
                {// we're at the end
                    AddExecResult("Script set " + FileName + " completed with " + ScriptFailCount.ToString() +
                        " Fails out of " + ScriptCount.ToString() + " Scripts.");
                    StopScriptSet();
                }
                else
                {
                    if (ScriptFailed() == false)
                    {
                        FileName = scriptSetGridView.Rows[ScriptSetIndex].Cells[0].Value.ToString();
                        if ((FileName.Length > 0) && (System.IO.File.Exists(FileName) == true))
                        {// files valid so load up script
                            commandDataGridView.Rows.Clear();
                            ScriptLoadSaveObject LoadIt = new ScriptLoadSaveObject();

                            AddExecResult(now.ToShortDateString() + " " + now.ToShortTimeString());
                            if (LoadScript(commandDataGridView, FileName, LoadIt) == true)
                            {// scripts loaded so now run it
                                AddExecResult("Loaded Script " + FileName);
                                StartScript();
                                scriptSetTimer.Enabled = true;
                                ScriptSetIndex++;
                            }
                            else
                            {// script load failed
                                AddExecResult("Failed to load Script " + FileName);
                                StopScriptSet();
                            }
                        }
                        else
                        {// script doesn't exist
                            AddExecResult("Failed to find Script " + FileName);
                            StopScriptSet();
                        }
                    }
                    else
                    {// script doesn't exist
                        AddExecResult("Script Failed to complete " + FileName);
                        StopScriptSet();
                    }
                }
            }
            else
                scriptSetTimer.Enabled = true;

        }
        private void AddExecResult(string ResultString)
        {
            resultSetGridView.Rows.Add(PlayLineIndex.ToString() + ":" + ResultString);
            resultSetGridView.Invalidate();
        }
        Int32 MSKeyCount = 0;
        private void keyPressTimer_Tick(object sender, EventArgs e)
        {
            if (MSKeyCount < 30 * 1000)
            {// no delay greater than a thirty allowed for practical reasons
                MSKeyCount += keyPressTimer.Interval;
            }
        }
        #region system_state_conversion
        enum System_State_et
        {
            SYSTEM_STATE_INIT,
            SYSTEM_STATE_WARMUP,
            SYSTEM_STATE_ST,         /* Self-Test */
            SYSTEM_STATE_PRID,       /* Product ID */
            SYSTEM_STATE_OT,         /* Operator Test */
            SYSTEM_STATE_PID,        /* Show Patient ID */
            SYSTEM_STATE_RT,         /* Referral Type */
            SYSTEM_STATE_WRT,        /* Warning - Wrong Referral Type */
            SYSTEM_STATE_FS,         /* Fit Sheath */
            SYSTEM_STATE_CAL,        /* Calibration check */
            SYSTEM_STATE_CAL2,
            SYSTEM_STATE_SCAN,       /* Scanning Mode */
            SYSTEM_STATE_SCAN2,
            SYSTEM_STATE_READ,       /* 10 Taking a reading */
            SYSTEM_STATE_RESULT,     /* Showing result in scanning mode */
            SYSTEM_STATE_WRF,        /* Warning: Reading Failed */
            SYSTEM_STATE_FMQ,        /* Finished measurements? */
            SYSTEM_STATE_HGQ,        /* High-Grade Question */
            SYSTEM_STATE_HGQ2,        /* High-Grade Question */
            SYSTEM_STATE_RA,         /* Results A */
            SYSTEM_STATE_RBT,        /* Results B (text) */
            SYSTEM_STATE_RBM,        /* Results B (map) */
            SYSTEM_STATE_RCT,        /* Results C (text) */
            SYSTEM_STATE_RCM,        /* Results C (map) */
            SYSTEM_STATE_RDT,        /* 20 Results D (text) */
            SYSTEM_STATE_RDM,        /* Results D (map) */
            SYSTEM_STATE_RET,        /* Results E (text) */
            SYSTEM_STATE_REM,        /* Results E (map) */
            SYSTEM_STATE_STQ,        /* See & Treat? */
            SYSTEM_STATE_STN,        /* No See & Treat */
            SYSTEM_STATE_SPQ,        /* Use Single Point? */
            SYSTEM_STATE_SP,         /* Single Point Mode */
            SYSTEM_STATE_SP2,
            SYSTEM_STATE_SP_READ,    /* Taking a reading */
            SYSTEM_STATE_SP_RESULT,  /* Showing result in single-point mode */
            SYSTEM_STATE_RDS,        /* 30 Return to Docking Station */
            SYSTEM_STATE_RDS2,        /* 30 Return to Docking Station */
            SYSTEM_STATE_RS,         /* Remove Sheath */
            SYSTEM_STATE_CTH,        /* 33 Connected to Host */
            SYSTEM_STATE_PD,         /* Power Down */
            SYSTEM_STATE_WPD,        /* Warning re. Power Down */
            SYSTEM_STATE_OFF,        /* Power Down */
            NUM_SYSTEM_STATES
        };

        string ConvertSystemStateIndexToString(int state)
        {
            if (state > (int)System_State_et.NUM_SYSTEM_STATES)
                state = (int)System_State_et.NUM_SYSTEM_STATES;
            return (Enum.GetName(typeof(System_State_et), state));
        }
        #endregion
        enum LED_Pattern_et
        {
            LED_OFF,
            LED_GREEN,
            LED_RED,
            LED_BOTH,
            LED_FLASH_GREEN,
            LED_FLASH_SLOW_GREEN,
            LED_FLASH_RED,
            LED_ALTERNATE,
            NUM_LED_PATTERNS
        };

        string ConvertLEDStateIndexToString(int state)
        {
            if (state > (int)LED_Pattern_et.NUM_LED_PATTERNS)
                state = (int)LED_Pattern_et.NUM_LED_PATTERNS;
            return (Enum.GetName(typeof(LED_Pattern_et), state));
        }

        private void button32_Click(object sender, EventArgs e)
        {
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.EnableFPGAInterface();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on enable fpga interface: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("FPGA Interface Enabled\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button33_Click(object sender, EventArgs e)
        {
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.DisableFPGAInterface();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on disable fpga interface: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("FPGA Interface Disabled\n");
            }
            Cursor.Current = Cursors.Default;
        }
        enum Read_States_e
        {
            READ_STATE_INIT = 0,
            READ_STATE_OFF = 1,
            READ_STATE_ENABLE = 2,
            READ_STATE_IDLE = 3,
            READ_STATE_CONFIGURE_MATRIX = 4,
            READ_STATE_CONFIGURE_FREQUENCY = 5,
            READ_STATE_READ = 6,
            READ_STATE_DATA = 7,
            READ_STATE_DISABLE = 8,
            READ_STATE_ERROR = 9,
            NUM_READ_STATES
        };
        private void button58_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[10];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.ReadMeasurementDrvState();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on get measurement driver state: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                AutoTestUnit.GetReply(ref Answer);
                Read_States_e State = (Read_States_e)Answer[1];
                switch (State)
                {
                    case Read_States_e.READ_STATE_INIT:
                        UpdateTechnicalConsole("Driver Initialising\n");
                        break;
                    case Read_States_e.READ_STATE_OFF:
                        UpdateTechnicalConsole("Driver Interface Off \n");
                        break;
                    case Read_States_e.READ_STATE_ENABLE:
                        UpdateTechnicalConsole("Driver Interface Enabling \n");
                        break;
                    case Read_States_e.READ_STATE_IDLE:
                        UpdateTechnicalConsole("Driver Interface Idle \n");
                        break;
                    case Read_States_e.READ_STATE_CONFIGURE_MATRIX:
                        UpdateTechnicalConsole("Driver Configuring Matrix \n");
                        break;
                    case Read_States_e.READ_STATE_CONFIGURE_FREQUENCY:
                        UpdateTechnicalConsole("Driver Configuring Frequency \n");
                        break;
                    case Read_States_e.READ_STATE_READ:
                        UpdateTechnicalConsole("Driver Executing Measurement \n");
                        break;
                    case Read_States_e.READ_STATE_DATA:
                        UpdateTechnicalConsole("Driver Reading Measurement Data \n");
                        break;
                    case Read_States_e.READ_STATE_DISABLE:
                        UpdateTechnicalConsole("Driver Disabling Interface \n");
                        break;
                    case Read_States_e.READ_STATE_ERROR:
                        UpdateTechnicalConsole("Driver In Error  \n");
                        break;
                }
            }
            Cursor.Current = Cursors.Default;

        }

        private void button34_Click(object sender, EventArgs e)
        {
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.RunFPGAMeasurement();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on run fpga measurement: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Measurement Cycle Executed " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button57_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[16];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.RequestFPGAResults();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on read FPGA Results: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 8)
                {
                    UInt32 Temp;
                    float Magnitude = 0;
                    float Phase = 0;


                    Magnitude = BitConverter.ToSingle(Answer, 0);
                    Phase = BitConverter.ToSingle(Answer, 4);

                    UpdateTechnicalConsole("Device calculated Magnitude = " + Magnitude.ToString() + " \n");
                    UpdateTechnicalConsole("Device calculated Phase = " + Phase.ToString() + " \n");
                }
                else
                    UpdateTechnicalConsole("Error on read FPGA Results: not enough bytes\n");
            }
            Cursor.Current = Cursors.Default;

        }

        private void button62_Click(object sender, EventArgs e)
        {
            byte status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.ResetFPGADriver();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on reset fpga driver: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Driver reset requested \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button35_Click(object sender, EventArgs e)
        {
            byte MyIndex = (byte)comboBoxManagedSw.SelectedIndex;
            if (comboBoxManagedSw.SelectedItem == null)
                return;
            if (MyIndex >= 0)
            {
                if (InCommand == true)
                {
                    MessageBox.Show("Busy");
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                AutoTestUnit.ClearBuffer();
                int Result = AutoTestUnit.ConfigureMatrix(MyIndex);
                if (Result != 0)
                {
                    UpdateTechnicalConsole("Error on configure matrix: " + AutoTestUnit.ErrorToText(Result) + "\n");
                }
                else
                {
                    UpdateTechnicalConsole("Matrix configured to path " + comboBoxManagedSw.SelectedItem.ToString() + "\n");
                }
                Cursor.Current = Cursors.Default;
            }
            else
                MessageBox.Show("Select route first");
        }

        private void button36_Click(object sender, EventArgs e)
        {
            byte MyIndex = (byte)comboBoxManagedFreq.SelectedIndex;
            if (comboBoxManagedFreq.SelectedItem == null)
                return;
            if (MyIndex >= 0)
            {
                if (InCommand == true)
                {
                    MessageBox.Show("Busy");
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                AutoTestUnit.ClearBuffer();
                int Result = AutoTestUnit.ConfigureFrequency(MyIndex);
                if (Result != 0)
                {
                    UpdateTechnicalConsole("Error on configure frequency: " + AutoTestUnit.ErrorToText(Result) + "\n");
                }
                else
                {
                    UpdateTechnicalConsole("FPGA frequency set to " + comboBoxManagedFreq.SelectedItem.ToString() + "\n");
                }
                Cursor.Current = Cursors.Default;
            }

        }

        private void button37_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[150];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GetFPGA_Version();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on get fpga version: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {

                    UpdateTechnicalConsole("FPGA Software Version: ");
                    UpdateTechnicalConsole(Answer[0].ToString() + ".");
                    UpdateTechnicalConsole(Answer[1].ToString() + ".");
                    UpdateTechnicalConsole(Answer[2].ToString() + ".");
                    UpdateTechnicalConsole(Answer[3].ToString() + "\n");
                }
                else
                    UpdateTechnicalConsole("Error on read cosine table: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button51_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Enable output", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetFPGAReset(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set fpga reset state: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (status == 1)
                    UpdateTechnicalConsole("FPGA in reset \n");
                else
                    UpdateTechnicalConsole("FPGA released reset \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button42_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[10];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.ReadMatrix();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on read matrix switch: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 8)
                {

                    UpdateTechnicalConsole("Matrix Line 1 = " + Answer[0].ToString() + " \n");
                    UpdateTechnicalConsole("Matrix Line 2 = " + Answer[1].ToString() + " \n");
                    UpdateTechnicalConsole("Matrix Line 3 = " + Answer[2].ToString() + " \n");
                    UpdateTechnicalConsole("Matrix Line 4 = " + Answer[3].ToString() + " \n");
                    UpdateTechnicalConsole("Matrix Line 5 = " + Answer[4].ToString() + " \n");
                    UpdateTechnicalConsole("Matrix Line 6 = " + Answer[5].ToString() + " \n");
                    UpdateTechnicalConsole("Matrix Line 7 = " + Answer[6].ToString() + " \n");
                    UpdateTechnicalConsole("Matrix Line 8 = " + Answer[7].ToString() + " \n");
                }
                else
                    UpdateTechnicalConsole("Error on read matrix switch: not enough bytes\n");
            }
            Cursor.Current = Cursors.Default;

        }

        private void button38_Click(object sender, EventArgs e)
        {
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.WriteSineWave();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on write sine table: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Sine LUT written \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button52_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Enable output", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetFPGAGo(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set fpga go: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (status == 1)
                    UpdateTechnicalConsole("FPGA GO : measurment run \n");
                else
                    UpdateTechnicalConsole("FPGA GO Cleared \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button43_Click(object sender, EventArgs e)
        {
            byte MyIndex = (byte)comboBoxUnmanagedSw.SelectedIndex;

            if (comboBoxUnmanagedSw.SelectedItem == null)
                return;
            if (MyIndex >= 0)
            {
                if (InCommand == true)
                {
                    MessageBox.Show("Busy");
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                AutoTestUnit.ClearBuffer();
                int Result = AutoTestUnit.SetMatrix(MyIndex);
                if (Result != 0)
                {
                    UpdateTechnicalConsole("Error on set matrix switch: " + AutoTestUnit.ErrorToText(Result) + "\n");
                }
                else
                {
                    UpdateTechnicalConsole("Matrix set = " + comboBoxUnmanagedSw.SelectedItem.ToString() + " \n");
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void button39_Click(object sender, EventArgs e)
        {
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.WriteCoSineWave();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on write cosine table: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Cosine LUT written \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button53_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[10];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.ReadFPGA_ReadyState();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on get fpga ready state: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                AutoTestUnit.GetReply(ref Answer);
                if (Answer[1] == 0)
                    UpdateTechnicalConsole("FPGA Reading Not Ready \n");
                else
                    UpdateTechnicalConsole("FPGA Reading Ready \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button44_Click(object sender, EventArgs e)
        {
            byte MyIndex = (byte)comboBoxUnmanagedFreq.SelectedIndex;

            if (comboBoxUnmanagedFreq.SelectedItem == null)
                return;
            if (MyIndex >= 0)
            {
                if (InCommand == true)
                {
                    MessageBox.Show("Busy");
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                AutoTestUnit.ClearBuffer();
                int Result = AutoTestUnit.SetFrequency(MyIndex);
                if (Result != 0)
                {
                    UpdateTechnicalConsole("Error on set frequency: " + AutoTestUnit.ErrorToText(Result) + "\n");
                }
                else
                {
                    UpdateTechnicalConsole("Frequency set = " + comboBoxUnmanagedFreq.SelectedItem.ToString() + " \n");
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void button41_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[150];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.ReadSineWave();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on read sine table: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 128)
                {

                    UpdateTechnicalConsole("Sine table: ");
                    // there are 128 bytes forming a 64 entry table of words
                    for (int Index = 0; Index < 64; Index++)
                    {
                        UInt16 Value = Answer[Index * 2];
                        Int16 Total = 0;

                        Value = (UInt16)(Value + ((UInt16)Answer[(Index * 2) + 1] << 8));
                        Total = (Int16)Value;
                        UpdateTechnicalConsole(Index.ToString() + " = " + Total.ToString() + "\n");
                    }
                }
                else
                    UpdateTechnicalConsole("Error on read cosine table: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button54_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Enable output", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetAnalogueState(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set ADC Active: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (status == 1)
                    UpdateTechnicalConsole("ADC set active \n");
                else
                    UpdateTechnicalConsole("ADC set inactive \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button45_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[10];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.ReadFrequency();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on read frequency: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                AutoTestUnit.GetReply(ref Answer);
                UpdateTechnicalConsole("Frequency Setting = " + comboBoxUnmanagedFreq.Items[Answer[0]].ToString() + " \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button40_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[150];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.ReadCoSineWave();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on read cosine table: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 128)
                {
                    UpdateTechnicalConsole("Cosine table: " + AutoTestUnit.ErrorToText(Result) + "\n");
                    // there are 128 bytes forming a 64 entry table of words
                    for (int Index = 0; Index < 64; Index++)
                    {
                        UInt16 Value = Answer[Index * 2];
                        Int16 Total = 0;

                        Value = (UInt16)(Value + ((UInt16)Answer[(Index * 2) + 1] << 8));
                        Total = (Int16)Value;
                        UpdateTechnicalConsole(Index.ToString() + " = " + Total.ToString() + "\n");
                    }
                }
                else
                    UpdateTechnicalConsole("Error on read cosine table: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            Cursor.Current = Cursors.Default;

        }

        private void button59_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Enable output", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetMatrixReset(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set matrix reset state: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (status == 1)
                    UpdateTechnicalConsole("Matrix in reset\n");
                else
                    UpdateTechnicalConsole("Matrix released from reset\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button46_Click(object sender, EventArgs e)
        {
            byte MyIndex = (byte)comboBoxUnmanagedSamples.SelectedIndex;

            if (comboBoxUnmanagedSamples.SelectedItem == null)
                return;
            if (MyIndex >= 0)
            {
                if (InCommand == true)
                {
                    MessageBox.Show("Busy");
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                AutoTestUnit.ClearBuffer();
                int Result = AutoTestUnit.SetSamples(MyIndex);
                if (Result != 0)
                {
                    UpdateTechnicalConsole("Error on set samples: " + AutoTestUnit.ErrorToText(Result) + "\n");
                }
                else
                {
                    UpdateTechnicalConsole("FPGA samples set to: " + comboBoxUnmanagedSamples.SelectedItem.ToString() + "\n");
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void button48_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Enable output", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetAnaloguePower(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set analogue power state: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (status == 1)
                    UpdateTechnicalConsole("Analogue power on\n");
                else
                    UpdateTechnicalConsole("Analogue power off\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button61_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[16];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GetFPGAResults();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on read FPGA Results: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 30)
                {
                    UInt32 Temp;
                    Int32 VoltsI = 0, VoltsQ = 0, AmpsI = 0, AmpsQ = 0;
                    UInt16 VoltsORC = 0, AmpsORC = 0, Checksum = 0;
                    float Magnitude = 0;
                    float Phase = 0;

                    Temp = Answer[0];
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[1]);
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[2]);
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[3]);
                    VoltsI = (Int32)Temp;
                    Temp = Answer[4];
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[5]);
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[6]);
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[7]);
                    VoltsQ = (Int32)Temp;
                    Temp = Answer[8];
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[9]);
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[10]);
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[11]);
                    AmpsI = (Int32)Temp;
                    Temp = Answer[12];
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[13]);
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[14]);
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[15]);
                    AmpsQ = (Int32)Temp;
                    Temp = Answer[16];
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[17]);
                    VoltsORC = (UInt16)Temp;
                    Temp = Answer[18];
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[19]);
                    AmpsORC = (UInt16)Temp;
                    Temp = Answer[20];
                    Temp = (UInt32)((UInt32)Temp << 8);
                    Temp = (UInt32)(Temp + Answer[21]);
                    Checksum = (UInt16)Temp;

                    //Temp = Answer[22];
                    //Temp = (UInt32)((UInt32)Temp << 8);
                    //Temp = (UInt32)(Temp + Answer[23]);
                    //Temp = (UInt32)((UInt32)Temp << 8);
                    //Temp = (UInt32)(Temp + Answer[24]);
                    //Temp = (UInt32)((UInt32)Temp << 8);
                    //Temp = (UInt32)(Temp + Answer[25]);
                    Magnitude = BitConverter.ToSingle(Answer, 22);
                    Phase = BitConverter.ToSingle(Answer, 26);
                    //Magnitude = Magnitude ;
                    UpdateTechnicalConsole("Volts I = " + VoltsI.ToString() + " \n");
                    UpdateTechnicalConsole("Volts Q = " + VoltsQ.ToString() + " \n");
                    UpdateTechnicalConsole("Amps I = " + AmpsI.ToString() + " \n");
                    UpdateTechnicalConsole("Amps Q = " + AmpsQ.ToString() + " \n");
                    UpdateTechnicalConsole("Volts ORC = " + VoltsORC.ToString() + " \n");
                    UpdateTechnicalConsole("Amps ORC = " + AmpsORC.ToString() + " \n");
                    UpdateTechnicalConsole("Checksum = " + Checksum.ToString() + " \n");
                    UpdateTechnicalConsole("Device calculated Magnitude = " + Magnitude.ToString() + " \n");
                    UpdateTechnicalConsole("Device calculated Phase = " + Phase.ToString() + " \n");
                }
                else
                    UpdateTechnicalConsole("Error on read FPGA Results: not enough bytes\n");
            }
            Cursor.Current = Cursors.Default;

        }

        private void button47_Click(object sender, EventArgs e)
        {
            UInt32 Samples = 0;
            byte[] Answer = new byte[10];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.ReadSamples();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on read samples: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                Result = (int)AutoTestUnit.GetReply(ref Answer);
                if (Result >= 4)
                {
                    Samples = Samples + ((UInt32)Answer[3] << 24);
                    Samples = Samples + ((UInt32)Answer[2] << 16);
                    Samples = Samples + ((UInt32)Answer[1] << 8);
                    Samples = Samples + ((UInt32)Answer[0] << 0);
                    UpdateTechnicalConsole("Samples Setting = " + Samples.ToString() + " \n");
                }
                else
                {
                    UpdateTechnicalConsole("Error on read samples: not enough returned data\n");
                }
            }
            Cursor.Current = Cursors.Default;

        }

        private void button49_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Enable output", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetFPGAPower(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set fpga power state: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (status == 1)
                    UpdateTechnicalConsole("FPGA power on\n");
                else
                    UpdateTechnicalConsole("FPGA power off\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button50_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Enable output", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetFPGAClock(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set fpga clock state: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (status == 1)
                    UpdateTechnicalConsole("FPGA Clock On \n");
                else
                    UpdateTechnicalConsole("FPGA Clock Off \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button65_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[10];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GetADC_Voltages();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on get ADC Voltages: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 6)
                {
                    UInt16 SupplyVolts = 0;
                    UInt16 BatteryVolts = 0;
                    UInt16 RefVoltage = 0;
                    SupplyVolts = Answer[0];
                    SupplyVolts = (UInt16)((int)SupplyVolts << 8);
                    SupplyVolts = (UInt16)(SupplyVolts + Answer[1]);
                    RefVoltage = Answer[2];
                    RefVoltage = (UInt16)((int)RefVoltage << 8);
                    RefVoltage = (UInt16)(RefVoltage + Answer[3]);
                    BatteryVolts = Answer[4];
                    BatteryVolts = (UInt16)((int)BatteryVolts << 8);
                    BatteryVolts = (UInt16)(BatteryVolts + Answer[5]);
                    UpdateTechnicalConsole("Supply Voltage = " + SupplyVolts.ToString() + " mv\n");
                    UpdateTechnicalConsole("Ref Voltage = " + RefVoltage.ToString() + "mv\n");
                    UpdateTechnicalConsole("Battery Voltage = " + BatteryVolts.ToString() + " mv\n");
                }
                else
                    UpdateTechnicalConsole("Error on get ADC Voltages: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button31_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[10];

            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GetChargeChipStatus();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on get charge chip status: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 1)
                {
                    if ((Answer[0] & 0x01) == 0)
                    {
                        UpdateTechnicalConsole("On Battery - Power Good Line Not Good (low)\n");
                    }
                    else
                    //     if ((Answer[0] & 0x01) == 0)
                    {
                        UpdateTechnicalConsole("Running On Dock\n");
                        if (((Answer[0] & 0x02) == 0) && ((Answer[0] & 0x03) == 0))
                            UpdateTechnicalConsole("Charging Suspended\n");
                        if (((Answer[0] & 0x02) > 0) && ((Answer[0] & 0x03) > 0))
                            UpdateTechnicalConsole("Precharge\n");
                        if (((Answer[0] & 0x02) > 0) && ((Answer[0] & 0x03) == 0))
                            UpdateTechnicalConsole("Fast Charge\n");
                        if (((Answer[0] & 0x02) == 0) && ((Answer[0] & 0x03) > 0))
                            UpdateTechnicalConsole("Charge Complete\n");
                    }
                }
            }
            Cursor.Current = Cursors.Default;

        }

        private void button55_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Set Charge Chip power 100/500maf", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetChargeChipPower(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set charge chip power: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Charge chip power set = " + status.ToString() + " \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button56_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Set Charge Chip Status on (yes)/off (no)", "State Picker", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                status = 1;
            else
                status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetChargeChipStatus(status);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on set charge chip status: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Charge chip set = " + status.ToString() + " \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button68_Click(object sender, EventArgs e)
        {
            byte MyIndex = (byte)referralSessionComboBox.SelectedIndex;
            byte Referral = 0;
            if (referralSessionComboBox.SelectedItem == null)
                return;
            if (referralSessionComboBox.SelectedItem.ToString() == "MENU_SEL_BSCC_NONE")
                Referral = 35;
            if (referralSessionComboBox.SelectedItem.ToString() == "MENU_SEL_BSCC_BORDER")
                Referral = 39;
            if (referralSessionComboBox.SelectedItem.ToString() == "MENU_SEL_BSCC_BGLAN")
                Referral = 40;
            if (referralSessionComboBox.SelectedItem.ToString() == "MENU_SEL_BSCC_LGRAD")
                Referral = 41;
            if (referralSessionComboBox.SelectedItem.ToString() == "MENU_SEL_BSCC_Q_HGRAD")
                Referral = 42;
            if (referralSessionComboBox.SelectedItem.ToString() == "MENU_SEL_BSCC_HGRAD")
                Referral = 5;
            if (referralSessionComboBox.SelectedItem.ToString() == "MENU_SEL_BSCC_Q_INV")
                Referral = 6;
            if (referralSessionComboBox.SelectedItem.ToString() == "MENU_SEL_BSCC_GNEO")
                Referral = 7;

            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.StartSession(Referral);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on start session: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Session Started with referral  " + referralSessionComboBox.SelectedItem.ToString() + "\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button67_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[10];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GetSMState();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error getting SM state: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                AutoTestUnit.GetReply(ref Answer);
                int State = Answer[0] << 8;
                State = State | Answer[1];
                UpdateTechnicalConsole(ConvertSystemStateIndexToString(State) + "\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button114_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[10];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GetLEDState();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error getting LED state: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                AutoTestUnit.GetReply(ref Answer);
                int State = Answer[0] << 8;
                State = State | Answer[1];
                UpdateTechnicalConsole(ConvertLEDStateIndexToString(State) + "\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button78_Click(object sender, EventArgs e)
        {
            byte status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.TestWatchdogReset();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Test watchdog reset failed\n");
            }
            else
            {
                UpdateTechnicalConsole("Handset watchdog reset correctly \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button72_Click(object sender, EventArgs e)
        {
            byte status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.CauseHardFault();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on cause hard fault\n");
            }
            else
            {
                UpdateTechnicalConsole("Handset hardfault requested \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button73_Click(object sender, EventArgs e)
        {
            byte status = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.CauseCriticalError();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on cause critical error\n");
            }
            else
            {
                UpdateTechnicalConsole("Handset critical error requested \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button75_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[10];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GetNV_RAM_Status();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on get External Flash Status: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 3)
                {
                    UpdateTechnicalConsole("Manufacturer = " + Answer[0].ToString("x") + " \n");
                    UpdateTechnicalConsole("Type = " + Answer[1].ToString("x") + Answer[2].ToString("x") + "\n");
                }
                else
                    UpdateTechnicalConsole("Error on get External Flash Status: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button74_Click(object sender, EventArgs e)
        {
            byte status = 0;

            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.EraseProgramImage();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Failed to erase program image from external flash: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Program image erased correctly \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button69_Click(object sender, EventArgs e)
        {
            byte status = 0;

            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.EraseErrorRecord();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Failed to erase error record from external flash: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Error record erased correctly \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button70_Click(object sender, EventArgs e)
        {
            byte status = 0;

            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.EraseUID();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Failed to erase Unique Id from external flash: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Unique Id erased correctly \n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button71_Click(object sender, EventArgs e)
        {
            byte status = 0;

            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.CorruptProgramImage();
            if (Result != 0)
            {
                UpdateTechnicalConsole("External Flash Test Failed: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("External Flash Test Completed Successfully\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button76_Click(object sender, EventArgs e)
        {

        }

        private void button75_Click_1(object sender, EventArgs e)
        {

        }

        private void button74_Click_1(object sender, EventArgs e)
        {

        }

        private void button116_Click(object sender, EventArgs e)
        {
            byte status = 0;
            DialogResult dialogResult = MessageBox.Show("Serial number will  be overwritten, are you sure?", "Set Unique Id", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {

                if (InCommand == true)
                {
                    MessageBox.Show("Busy");
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                AutoTestUnit.ClearBuffer();
                int Result = AutoTestUnit.SetUID(UIDtextBox.Text);
                if (Result != 0)
                {
                    UpdateTechnicalConsole("Error on set device UID\n");
                }
                else
                {
                    UpdateTechnicalConsole("Device UID set to " + UIDtextBox.Text + "\n");
                }
                Cursor.Current = Cursors.Default;
            }
        }

        private void button115_Click(object sender, EventArgs e)
        {
            byte status = 0;
            byte[] Answer = new byte[150];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GetUID();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request device UID\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 50)
                {

                    UpdateTechnicalConsole("Device ID: ");
                    // there are 100 bytes forming a string with 50 char
                    string s = System.Text.Encoding.UTF8.GetString(Answer, 0, Answer.Length);
                    UpdateTechnicalConsole(s);
                    UpdateTechnicalConsole("\n");
                }
                else
                    UpdateTechnicalConsole("Error on read UID: " + AutoTestUnit.ErrorToText(Result) + "\n");

            }
            Cursor.Current = Cursors.Default;
        }

        private void button76_Click_1(object sender, EventArgs e)
        {
            byte status = 0;
            int Result = 0;
            byte[] Answer = new byte[150];
            UInt32 Value = 0;
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            UpdateTechnicalConsole("------------------------------------- \n");
            //------------------------------------
            AutoTestUnit.ClearBuffer();
            Result = AutoTestUnit.GetLifeCounter(0);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request life counters\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {
                    UpdateTechnicalConsole("Counter Resets: ");
                    Value = ((UInt32)Answer[0]) << 24;
                    Value = Value | ((UInt32)Answer[1]) << 16;
                    Value = Value | ((UInt32)Answer[2]) << 8;
                    Value = Value | ((UInt32)Answer[3]);
                    UpdateTechnicalConsole(Value.ToString() + "\n");
                }
            }
            //-------------------------------------
            AutoTestUnit.ClearBuffer();
            Result = AutoTestUnit.GetLifeCounter(1);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request life counters\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {
                    UpdateTechnicalConsole("Counter Corruptions: ");
                    Value = ((UInt32)Answer[0]) << 24;
                    Value = Value | ((UInt32)Answer[1]) << 16;
                    Value = Value | ((UInt32)Answer[2]) << 8;
                    Value = Value | ((UInt32)Answer[3]);
                    UpdateTechnicalConsole(Value.ToString() + "\n");
                }
            }
            //-------------------------------------            
            AutoTestUnit.ClearBuffer();
            Result = AutoTestUnit.GetLifeCounter(2);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request life counters\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {
                    UpdateTechnicalConsole("Seconds On Dock: ");
                    Value = ((UInt32)Answer[0]) << 24;
                    Value = Value | ((UInt32)Answer[1]) << 16;
                    Value = Value | ((UInt32)Answer[2]) << 8;
                    Value = Value | ((UInt32)Answer[3]);
                    UpdateTechnicalConsole(Value.ToString() + "\n");
                }
            }
            //-------------------------------------   
            AutoTestUnit.ClearBuffer();
            Result = AutoTestUnit.GetLifeCounter(3);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request life counters\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {
                    UpdateTechnicalConsole("Seconds On Charge: ");
                    Value = ((UInt32)Answer[0]) << 24;
                    Value = Value | ((UInt32)Answer[1]) << 16;
                    Value = Value | ((UInt32)Answer[2]) << 8;
                    Value = Value | ((UInt32)Answer[3]);
                    UpdateTechnicalConsole(Value.ToString() + "\n");
                }
            }
            //-------------------------------------     
            AutoTestUnit.ClearBuffer();
            Result = AutoTestUnit.GetLifeCounter(4);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request life counters\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {
                    UpdateTechnicalConsole("Seconds On Battery: ");
                    Value = ((UInt32)Answer[0]) << 24;
                    Value = Value | ((UInt32)Answer[1]) << 16;
                    Value = Value | ((UInt32)Answer[2]) << 8;
                    Value = Value | ((UInt32)Answer[3]);
                    UpdateTechnicalConsole(Value.ToString() + "\n");
                }
            }
            //-------------------------------------      
            AutoTestUnit.ClearBuffer();
            Result = AutoTestUnit.GetLifeCounter(5);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request life counters\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {
                    UpdateTechnicalConsole("Number Of Charge Cycles: ");
                    Value = ((UInt32)Answer[0]) << 24;
                    Value = Value | ((UInt32)Answer[1]) << 16;
                    Value = Value | ((UInt32)Answer[2]) << 8;
                    Value = Value | ((UInt32)Answer[3]);
                    UpdateTechnicalConsole(Value.ToString() + "\n");
                }
            }
            //-------------------------------------     
            AutoTestUnit.ClearBuffer();
            Result = AutoTestUnit.GetLifeCounter(6);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request life counters\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {
                    UpdateTechnicalConsole("Charge Complete Count: ");
                    Value = ((UInt32)Answer[0]) << 24;
                    Value = Value | ((UInt32)Answer[1]) << 16;
                    Value = Value | ((UInt32)Answer[2]) << 8;
                    Value = Value | ((UInt32)Answer[3]);
                    UpdateTechnicalConsole(Value.ToString() + "\n");
                }
            }
            //-------------------------------------          
            AutoTestUnit.ClearBuffer();
            Result = AutoTestUnit.GetLifeCounter(7);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request life counters\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {
                    UpdateTechnicalConsole("Readings Taken Count: ");
                    Value = ((UInt32)Answer[0]) << 24;
                    Value = Value | ((UInt32)Answer[1]) << 16;
                    Value = Value | ((UInt32)Answer[2]) << 8;
                    Value = Value | ((UInt32)Answer[3]);
                    UpdateTechnicalConsole(Value.ToString() + "\n");
                }
            }
            //-------------------------------------   
            AutoTestUnit.ClearBuffer();
            Result = AutoTestUnit.GetLifeCounter(8);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request life counters\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {
                    UpdateTechnicalConsole("Readings Failed Count(inc fails): ");
                    Value = ((UInt32)Answer[0]) << 24;
                    Value = Value | ((UInt32)Answer[1]) << 16;
                    Value = Value | ((UInt32)Answer[2]) << 8;
                    Value = Value | ((UInt32)Answer[3]);
                    UpdateTechnicalConsole(Value.ToString() + "\n");
                }
            }
            //-------------------------------------   
            AutoTestUnit.ClearBuffer();
            Result = AutoTestUnit.GetLifeCounter(9);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request life counters\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {
                    UpdateTechnicalConsole("Sessions Started Count(inc fails): ");
                    Value = ((UInt32)Answer[0]) << 24;
                    Value = Value | ((UInt32)Answer[1]) << 16;
                    Value = Value | ((UInt32)Answer[2]) << 8;
                    Value = Value | ((UInt32)Answer[3]);
                    UpdateTechnicalConsole(Value.ToString() + "\n");
                }
            }
            //-------------------------------------   
            AutoTestUnit.ClearBuffer();
            Result = AutoTestUnit.GetLifeCounter(10);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request life counters\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {
                    UpdateTechnicalConsole("Incorrect Session Info (WRT) Count: ");
                    Value = ((UInt32)Answer[0]) << 24;
                    Value = Value | ((UInt32)Answer[1]) << 16;
                    Value = Value | ((UInt32)Answer[2]) << 8;
                    Value = Value | ((UInt32)Answer[3]);
                    UpdateTechnicalConsole(Value.ToString() + "\n");
                }
            }
            //-------------------------------------   
            AutoTestUnit.ClearBuffer();
            Result = AutoTestUnit.GetLifeCounter(11);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request life counters\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {
                    UpdateTechnicalConsole("CAL Failed Count: ");
                    Value = ((UInt32)Answer[0]) << 24;
                    Value = Value | ((UInt32)Answer[1]) << 16;
                    Value = Value | ((UInt32)Answer[2]) << 8;
                    Value = Value | ((UInt32)Answer[3]);
                    UpdateTechnicalConsole(Value.ToString() + "\n");
                }
            }
            //-------------------------------------   
            AutoTestUnit.ClearBuffer();
            Result = AutoTestUnit.GetLifeCounter(12);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request life counters\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {
                    UpdateTechnicalConsole("User Requested Powerdown Count: ");
                    Value = ((UInt32)Answer[0]) << 24;
                    Value = Value | ((UInt32)Answer[1]) << 16;
                    Value = Value | ((UInt32)Answer[2]) << 8;
                    Value = Value | ((UInt32)Answer[3]);
                    UpdateTechnicalConsole(Value.ToString() + "\n");
                }
            }
            //-------------------------------------   
            AutoTestUnit.ClearBuffer();
            Result = AutoTestUnit.GetLifeCounter(13);
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on request life counters\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 4)
                {
                    UpdateTechnicalConsole("Auto Powerdown Count: ");
                    Value = ((UInt32)Answer[0]) << 24;
                    Value = Value | ((UInt32)Answer[1]) << 16;
                    Value = Value | ((UInt32)Answer[2]) << 8;
                    Value = Value | ((UInt32)Answer[3]);
                    UpdateTechnicalConsole(Value.ToString() + "\n");
                }
            }
            //-------------------------------------   
            UpdateTechnicalConsole("------------------------------------- \n");
            Cursor.Current = Cursors.Default;

        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            InCommand = true;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.LeftKey();
            if (Result != 0)
            {
                MessageBox.Show("Error on left key : " + AutoTestUnit.ErrorToText(Result));
            }
            else
            {
                AddCommand(CommandTable[(int)User_Interactions.USER_PRESS_KEY1], "NULL", "ACK");
                AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
            }
            InCommand = false;
            Cursor.Current = Cursors.Default;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            byte[] Reply = new byte[0];

            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            InCommand = true;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.EnterKey();
            if (Result != 0)
            {
                MessageBox.Show("Error on Enter key : " + AutoTestUnit.ErrorToText(Result));
            }
            else
            {
                AddCommand(CommandTable[(int)User_Interactions.USER_PRESS_KEY_ENTER], "NULL", "ACK");
                AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
            }
            InCommand = false;
            Cursor.Current = Cursors.Default;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.RightKey();
            if (Result != 0)
            {
                MessageBox.Show("Error on right key : " + AutoTestUnit.ErrorToText(Result));
            }
            else
            {
                AddCommand(CommandTable[(int)User_Interactions.USER_PRESS_KEY3], "NULL", "ACK");
                AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button66_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            byte[] ScreenArray = new byte[0];
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GrabScreen();
            if (Result != 0)
            {
                MessageBox.Show("Error on grab screen: " + AutoTestUnit.ErrorToText(Result));
            }
            else
            {
                AutoTestUnit.GetReply(ref ScreenArray);
                PaintScreenMap(screenPictureBox, ScreenArray);
            }
            Cursor.Current = Cursors.Default;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            byte[] Reply = new byte[0];

            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            InCommand = true;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.ScanKey();
            if (Result != 0)
            {
                MessageBox.Show("Error on scan key : " + AutoTestUnit.ErrorToText(Result));
            }
            else
            {
                AddCommand(CommandTable[(int)User_Interactions.USER_PRESS_KEY_SCAN], "NULL", "ACK");
                AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
            }
            InCommand = false;
            Cursor.Current = Cursors.Default;
        }

        private void dockButton_Click(object sender, EventArgs e)
        {
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.DockHandset();
            if (Result != 0)
            {
                MessageBox.Show("Error on dock handset: " + AutoTestUnit.ErrorToText(Result));
            }
            else
            {
                AddCommand(CommandTable[(int)User_Interactions.USER_DOCK], "NULL", "ACK");
                AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button17_Click(object sender, EventArgs e)
        {
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.FitSheath();
            if (Result != 0)
            {
                MessageBox.Show("Error on fit sheath: " + AutoTestUnit.ErrorToText(Result));
            }
            else
            {
                AddCommand(CommandTable[(int)User_Interactions.USER_SHEATH_ON], "NULL", "ACK");
                AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
            }
            Cursor.Current = Cursors.Default;
        }

        private void failBoundaryButton_Click(object sender, EventArgs e)
        {
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetBoundary(0);
            if (Result != 0)
            {
                MessageBox.Show("Error on set boundary fail: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                AddCommand(CommandTable[(int)User_Interactions.USER_SET_BOUNDARY], "0", "ACK");
                AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
            }
            Cursor.Current = Cursors.Default;
        }

        private void sessionButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            StartSession();
            AddCommand(CommandTable[(int)User_Interactions.USER_CREATE_SESSION], "NULL", "NULL");
            AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
            Cursor.Current = Cursors.Default;
        }

        private void readingButton_Click(object sender, EventArgs e)
        {

        }

        private void undockButton_Click(object sender, EventArgs e)
        {
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.UnDockHandset();
            if (Result != 0)
            {
                MessageBox.Show("Error on undock handset: " + AutoTestUnit.ErrorToText(Result));
            }
            else
            {
                AddCommand(CommandTable[(int)User_Interactions.USER_UNDOCK], "NULL", "ACK");
                AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.RemoveSheath();
            if (Result != 0)
            {
                MessageBox.Show("Error on remove sheath: " + AutoTestUnit.ErrorToText(Result));
            }
            else
            {
                AddCommand(CommandTable[(int)User_Interactions.USER_SHEATH_OFF], "NULL", "ACK");
                AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
            }
            Cursor.Current = Cursors.Default;
        }

        private void passBoundaryButton_Click(object sender, EventArgs e)
        {
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.SetBoundary(1);
            if (Result != 0)
            {
                MessageBox.Show("Error on set boundary fail: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                AddCommand(CommandTable[(int)User_Interactions.USER_SET_BOUNDARY], "1", "ACK");
                AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
            }
            Cursor.Current = Cursors.Default;
        }

        private void cytologyButton_Click(object sender, EventArgs e)
        {
            //if (InCommand == true)
            //{
            //    MessageBox.Show("Busy");
            //    return;
            //}
            //Cursor.Current = Cursors.WaitCursor;
            //ZiliCator.Forms.ParameterEntryForm GetValueForm = new ZiliCator.Forms.ParameterEntryForm(false, true);
            //GetValueForm.ShowDialog();
            //if (GetValueForm.ResultValue == true)
            //{
            //    TestreferralType = (ReferralType)GetValueForm.IntValue;
            //    string referralName = Enum.GetName(typeof(ZiliCator.Forms.ParameterEntryForm.Referral_Type_et), (ReferralType)GetValueForm.IntValue);
            //    AddCommand(CommandTable[(int)User_Interactions.USER_SET_CYTOLOGY], referralName, "NULL");
            //    AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
            //}
            //Cursor.Current = Cursors.Default;
        }

        private void userDelayButton_Click(object sender, EventArgs e)
        {
            ZiliCator.Forms.ParameterEntryForm GetValueForm = new ZiliCator.Forms.ParameterEntryForm(true, false);
            GetValueForm.ShowDialog();
            if (GetValueForm.ResultValue == true)
            {
                AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], GetValueForm.IntValue.ToString(), "NON");
                Thread.Sleep(GetValueForm.IntValue);
            }
            GetValueForm.Dispose();
        }

        private void batteryButton_Click(object sender, EventArgs e)
        {
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            ZiliCator.Forms.ParameterEntryForm GetValueForm = new ZiliCator.Forms.ParameterEntryForm(true, false);
            GetValueForm.ShowDialog();
            if (GetValueForm.ResultValue == true)
            {
                if ((GetValueForm.IntValue <= 100) && (GetValueForm.IntValue >= 0))
                {
                    AutoTestUnit.ClearBuffer();
                    int Result = AutoTestUnit.SetBattery(GetValueForm.IntValue);
                    if (Result != 0)
                    {
                        MessageBox.Show("Error on set battery level: " + AutoTestUnit.ErrorToText(Result) + "\n");
                    }
                    else
                    {
                        AddCommand(CommandTable[(int)User_Interactions.USER_SET_BATTERY], GetValueForm.IntValue.ToString(), "ACK");
                        AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
                    }
                }
                else
                    MessageBox.Show("Battery must be 0 - 100%");
            }
            Cursor.Current = Cursors.Default;
        }

        private void fullStatusCheckButton_Click(object sender, EventArgs e)
        {
            checkLEDStateButton_Click(sender, e);
            statemachineButton_Click(sender, e);
            Cursor.Current = Cursors.WaitCursor;
            byte[] ScreenArray = new byte[0];
            byte[] CompareArray = new byte[0];
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GrabScreen();
            if (Result != 0)
            {
                MessageBox.Show("Error on compare screen: " + AutoTestUnit.ErrorToText(Result));
            }
            else
            {
                AutoTestUnit.GetReply(ref ScreenArray);
                PaintScreenMap(screenPictureBox, ScreenArray);
                DateTime now = DateTime.Now;
                string ResultFileName = "Screen " + now.ToShortDateString() + " " + now.ToLongTimeString() + ".zscreen";
                ResultFileName = ResultFileName.Replace(':', '-');
                ResultFileName = ResultFileName.Replace('/', '-');
                try
                {// save a copy of bitmap

                    try
                    {// save a copy of bitmap
                        if (System.IO.File.Exists(ResultFileName))
                            System.IO.File.Delete(ResultFileName);
                        System.IO.File.WriteAllBytes(ResultFileName, ScreenArray);
                    }
                    catch
                    {
                        MessageBox.Show("Failed to save screen shot");
                    }

                    AddCommand(CommandTable[(int)User_Interactions.USER_COMPARE_SCREEN], ResultFileName, "ACK");
                    AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to compare file with screen" + ex.Message);
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void compareScreenButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            byte[] ScreenArray = new byte[0];
            byte[] CompareArray = new byte[0];
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GrabScreen();
            if (Result != 0)
            {
                MessageBox.Show("Error on compare screen: " + AutoTestUnit.ErrorToText(Result));
            }
            else
            {
                AutoTestUnit.GetReply(ref ScreenArray);
                PaintScreenMap(screenPictureBox, ScreenArray);
                OpenFileDialog sfdlg = new OpenFileDialog();
                sfdlg.DefaultExt = ".zscreen";
                sfdlg.Filter = "ZedScan screen files (*.zscreen)|*.zscreen";
                // Do something  
                if (sfdlg.ShowDialog() == DialogResult.OK)
                {

                    try
                    {// save a copy of bitmap
                        if (System.IO.File.Exists(sfdlg.FileName))
                        {

                            CompareArray = System.IO.File.ReadAllBytes(sfdlg.FileName);
                            PaintScreenMap(screenCopyPictureBox, CompareArray);
                            if (CompareArray.Count() != ScreenArray.Count())
                            {
                                MessageBox.Show("Screen file wrong size = " + CompareArray.Count().ToString());
                                return;
                            }
                            bool Match = true;
                            int Index = 0;
                            foreach (byte Data in ScreenArray)
                            {
                                if (Data != CompareArray[Index])
                                {
                                    MessageBox.Show("Screen doesn't match!");
                                    return;
                                }
                                Index++;
                            }
                            AddCommand(CommandTable[(int)User_Interactions.USER_COMPARE_SCREEN], sfdlg.FileName, "ACK");
                            AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
                        }
                        else
                        {
                            MessageBox.Show("Screen file not found");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to compare file with screen" + ex.Message);
                    }
                }
            }
            Cursor.Current = Cursors.Default;

        }
        private void AddCommand(string Command, string Parameter, string ExpectedReply)
        {
            string[] Row = new string[3];

            Row[0] = Command;
            Row[1] = Parameter;
            Row[2] = ExpectedReply;
            commandDataGridView.Rows.Add(Row);
        }
        private void statemachineButton_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[10];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GetSMState();
            if (Result != 0)
            {
                MessageBox.Show("Error getting SM state: " + AutoTestUnit.ErrorToText(Result));
            }
            else
            {
                AutoTestUnit.GetReply(ref Answer);
                int State = Answer[0] << 8;
                State = State | Answer[1];
                AddCommand(CommandTable[(int)User_Interactions.USER_COMPARE_SYSTEM_STATE], "NULL", ConvertSystemStateIndexToString(State));
                AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
            }
            Cursor.Current = Cursors.Default;

        }


        private int KeyPressed()
        {
            keyPressTimer.Enabled = false;
            // record the time since last key press
            Int32 Time = MSKeyCount + (keyPressTimer.Interval * 3);
            // reset timer
            MSKeyCount = 0;
            keyPressTimer.Enabled = true;
            return Time;
        }
        private void checkLEDStateButton_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[10];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GetLEDState();
            if (Result != 0)
            {
                MessageBox.Show("Error getting LED state: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                AutoTestUnit.GetReply(ref Answer);
                int State = Answer[0] << 8;
                State = State | Answer[1];
                AddCommand(CommandTable[(int)User_Interactions.USER_COMPARE_LED_STATE], "NULL", ConvertLEDStateIndexToString(State));
                AddCommand(CommandTable[(int)User_Interactions.USER_DELAY_NMS], KeyPressed().ToString(), "NON");
            }
            Cursor.Current = Cursors.Default; 
        }

        private void convertButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            byte[] ScreenArray = new byte[0];
            byte[] CompareArray = new byte[0];

            OpenFileDialog sfdlg = new OpenFileDialog();
            sfdlg.DefaultExt = ".zscreen";
            sfdlg.Filter = "ZedScan screen files (*.zscreen)|*.zscreen";
            // Do something  
            if (sfdlg.ShowDialog() == DialogResult.OK)
            {

                try
                {// save a copy of bitmap
                    if (System.IO.File.Exists(sfdlg.FileName))
                    {

                        CompareArray = System.IO.File.ReadAllBytes(sfdlg.FileName);
                        PaintScreenMap(screenCopyPictureBox, CompareArray);
                        SaveFileDialog storefdlg = new SaveFileDialog();
                        storefdlg.DefaultExt = ".bmp";
                        storefdlg.Filter = "ZedScan screen files (*.bmp)|*.bmp";
                        // Do something  
                        if (storefdlg.ShowDialog() == DialogResult.OK)
                        {
                            screenCopyPictureBox.Image.Save(storefdlg.FileName, ImageFormat.Bmp);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Screen file not found");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to convert screen file " + ex.Message);
                }
            }
            Cursor.Current = Cursors.Default;

        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void runScriptButton_Click(object sender, EventArgs e)
        {
            if (Playing == false)
            {
                StartScript();
            }
            else
            {
                StopScript();
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfdlg = new SaveFileDialog();
            sfdlg.DefaultExt = ".zscr";
            sfdlg.Filter = "ZedScan Test Script files (*.zscr)|*.zscr";
            // Do something  
            if (sfdlg.ShowDialog() == DialogResult.OK)
            {
                ScriptLoadSaveObject SaveIt = new ScriptLoadSaveObject();
                SaveScript(commandDataGridView, sfdlg.FileName, SaveIt);
            }
        }

        private void grabScreenButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            byte[] ScreenArray = new byte[0];
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GrabScreen();
            if (Result != 0)
            {
                MessageBox.Show("Error on grab screen: " + AutoTestUnit.ErrorToText(Result));
            }
            else
            {
                AutoTestUnit.GetReply(ref ScreenArray);
                PaintScreenMap(screenPictureBox, ScreenArray);
                SaveFileDialog sfdlg = new SaveFileDialog();
                sfdlg.DefaultExt = ".zscreen";
                sfdlg.Filter = "ZedScan screen files (*.zscreen)|*.zscreen";
                // Do something  
                if (sfdlg.ShowDialog() == DialogResult.OK)
                {

                    try
                    {// save a copy of bitmap
                        if (System.IO.File.Exists(sfdlg.FileName))
                            System.IO.File.Delete(sfdlg.FileName);
                        System.IO.File.WriteAllBytes(sfdlg.FileName, ScreenArray);
                    }
                    catch
                    {
                        MessageBox.Show("Failed to save screen shot");
                    }
                }
            }
            Cursor.Current = Cursors.Default;

        }

        private Image CropImage(Image img, Rectangle rect)
        {
            return ((Bitmap)img).Clone(rect, img.PixelFormat);
        }

        private void button64_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            byte[] ScreenArray = new byte[0];
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GrabScreen();
            if (Result != 0)
            {
                MessageBox.Show("Error on grab screen: " + AutoTestUnit.ErrorToText(Result));
            }
            else
            {
                AutoTestUnit.GetReply(ref ScreenArray);
                PaintScreenMap(screenPictureBox, ScreenArray);
                SaveFileDialog sfdlg = new SaveFileDialog();
                sfdlg.DefaultExt = ".bmp";
                sfdlg.Filter = "ZedScan screen bitmaps (*.bmp)|*.bmp";
                // Do something  
                if (sfdlg.ShowDialog() == DialogResult.OK)
                {

                    try
                    {// save a copy of bitmap
                        if (System.IO.File.Exists(sfdlg.FileName))
                            System.IO.File.Delete(sfdlg.FileName);

                        Rectangle CropRectangle = new Rectangle(0, 0, 127, 159);
                        PictureBox NewPictureBox = new PictureBox();
                        NewPictureBox.Image = CropImage(screenPictureBox.Image, CropRectangle);
                        NewPictureBox.Image.Save(sfdlg.FileName, ImageFormat.Bmp);
                    }
                    catch
                    {
                        MessageBox.Show("Failed to save screen shot");
                    }
                }
            }
            Cursor.Current = Cursors.Default;

        }

        private void newScriptButton_Click(object sender, EventArgs e)
        {
            commandDataGridView.Rows.Clear();
            keyPressTimer.Enabled = false;
            MSKeyCount = 0;
        }

        private void loadScriptButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdlg = new OpenFileDialog();
            ofdlg.DefaultExt = ".zscr";
            ofdlg.Filter = "ZedScan Test Script files (*.zscr)|*.zscr";
            // Do something  
            if ((ofdlg.ShowDialog() == DialogResult.OK) && (System.IO.File.Exists(ofdlg.FileName)))
            {
                commandDataGridView.Rows.Clear();
                ScriptLoadSaveObject LoadIt = new ScriptLoadSaveObject();
                LoadScript(commandDataGridView, ofdlg.FileName, LoadIt);
            }
        }

        private void baudRateToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void button118_Click(object sender, EventArgs e)
        {

        }

        private void button117_Click(object sender, EventArgs e)
        {
            AdaptorType adaptorType = AdaptorType.NotSet;
            adaptorType = AdaptorType.HID;

            RaiseAdaptorSelectionEvent(adaptorType);
        }

        private void button119_Click(object sender, EventArgs e)
        {
            RaiseChangeSingleByteModeEvent(singleByteModeToolStripMenuItem.Checked);
        }

        private void button60_Click_1(object sender, EventArgs e)
        {
            if (AutoTestUnit.IsConnected() == false)
            {
                if (ComPortList.SelectedItem != null)
                {
                    AutoTestUnit.Connect(230400, ComPortList.SelectedItem.ToString());
                    button60.Text = "Disconnect AutoTest";
                }
            }
            else
            {
                AutoTestUnit.Disconnect();
                button60.Text = "Connect AutoTest";
            }
        }

        private void button117_Click_1(object sender, EventArgs e)
        {

        }

        private void iOControlTabPage_Click(object sender, EventArgs e)
        {

        }

        private void button118_Click_1(object sender, EventArgs e)
        {
            byte MyIndex = (byte)TonePatternComboBox.SelectedIndex;
            if (TonePatternComboBox.SelectedItem == null)
                return;
            if (MyIndex >= 0)
            {
                if (InCommand == true)
                {
                    MessageBox.Show("Busy");
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                AutoTestUnit.ClearBuffer();
                int Result = AutoTestUnit.SetTone_Pattern(MyIndex);
                if (Result != 0)
                {
                    UpdateTechnicalConsole("Error on set Tone pattern: " + AutoTestUnit.ErrorToText(Result) + "\n");
                }
                else
                {
                    UpdateTechnicalConsole("Tone pattern set to " + TonePatternComboBox.SelectedItem.ToString() + "\n");
                }
                Cursor.Current = Cursors.Default;
            }
            else
                MessageBox.Show("Select pattern first");
        }

        private void button119_Click_1(object sender, EventArgs e)
        {
            byte MyIndex = (byte)LEDPatternComboBox.SelectedIndex;
            if (LEDPatternComboBox.SelectedItem == null)
                return;
            if (MyIndex >= 0)
            {
                if (InCommand == true)
                {
                    MessageBox.Show("Busy");
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                AutoTestUnit.ClearBuffer();
                int Result = AutoTestUnit.SetLED_Pattern(MyIndex);
                if (Result != 0)
                {
                    UpdateTechnicalConsole("Error on set LED pattern: " + AutoTestUnit.ErrorToText(Result) + "\n");
                }
                else
                {
                    UpdateTechnicalConsole("LED pattern set to " + LEDPatternComboBox.SelectedItem.ToString() + "\n");
                }
                Cursor.Current = Cursors.Default;
            }
            else
                MessageBox.Show("Select pattern first");
        }
        private void DisplayFrequency(int Frequency)
        {
            switch (Frequency)
            {
                case 0:
                    UpdateTechnicalConsole("76 Hz \n");
                    break;
                case 1:
                    UpdateTechnicalConsole("153 Hz \n");
                    break;
                case 2:
                    UpdateTechnicalConsole("305 Hz \n");
                    break;
                case 3:
                    UpdateTechnicalConsole("610 Hz \n");
                    break;
                case 4:
                    UpdateTechnicalConsole("1200 Hz \n");
                    break;
                case 5:
                    UpdateTechnicalConsole("2400 Hz \n");
                    break;
                case 6:
                    UpdateTechnicalConsole("4900 Hz \n");
                    break;
                case 7:
                    UpdateTechnicalConsole("9800 Hz \n");
                    break;
                case 8:
                    UpdateTechnicalConsole("20 KHz \n");
                    break;
                case 9:
                    UpdateTechnicalConsole("39 KHz \n");
                    break;
                case 10:
                    UpdateTechnicalConsole("78 KHz \n");
                    break;
                case 11:
                    UpdateTechnicalConsole("156 KHz \n");
                    break;
                case 12:
                    UpdateTechnicalConsole("312 KHz \n");
                    break;
                case 13:
                    UpdateTechnicalConsole("625 KHz \n");
                    break;
            }

        }
        private void button120_Click(object sender, EventArgs e)
        {

            byte[] Answer = new byte[1000];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GetPointData();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on read point data: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= 896)
                {
                    Int32 Frequency = 0;
                    Int32 Sweep = 0;
                    UInt32 Temp;
                    float Magnitude = 0;
                    float Phase = 0;
                    float Real = 0;
                    float Imaginary = 0;
                    Int32 AnswerOffset=0;
                    const Int32 ReadingsPerSweep = 8;
                    const Int32 bytesPerFreq = (((ReadingsPerSweep * 4) * 2) + 8);
                    float[] MeanReal = new float[14];
                    float[] MeanImag = new float[14];

                    for (Frequency = 0; Frequency < 14; Frequency++)
                    {
                        MeanReal[Frequency] = 0;
                        MeanImag[Frequency] = 0;
                        DisplayFrequency(Frequency);
                        for (Sweep = 0; Sweep < 8; Sweep++)
                        {
                            AnswerOffset = (Frequency * bytesPerFreq) + ((Sweep * 4) * 2);
                            Magnitude = BitConverter.ToSingle(Answer, AnswerOffset);
                            UpdateTechnicalConsole("Sweep " + Sweep.ToString() + " Mag = " + Magnitude.ToString());
                            AnswerOffset += 4;
                            Phase = BitConverter.ToSingle(Answer, AnswerOffset);
                            UpdateTechnicalConsole(" Phase = " + Phase.ToString() + " \n");
                            MeanReal[Frequency] = MeanReal[Frequency] + (float) 
                                (Magnitude * Math.Cos((double)Phase));
                            MeanImag[Frequency] = MeanImag[Frequency] + (float)
                                (Magnitude * Math.Sin((double)Phase));
                        }
                        AnswerOffset += 4;
                        Real = BitConverter.ToSingle(Answer, AnswerOffset);
                        AnswerOffset += 4;
                        Imaginary = BitConverter.ToSingle(Answer, AnswerOffset);
                        MeanReal[Frequency] = MeanReal[Frequency] / 8;
                        MeanImag[Frequency] = MeanImag[Frequency] / 8;
                        UpdateTechnicalConsole("Calc Real " + MeanReal[Frequency].ToString() + " Reported Real = " + Real.ToString() + "\n");
                        UpdateTechnicalConsole("Calc Imag " + MeanImag[Frequency].ToString() + " Reported Imag = " + Imaginary.ToString() + "\n");

                    }
                }
                else
                    UpdateTechnicalConsole("Error on read point data: not enough bytes\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void button121_Click(object sender, EventArgs e)
        {
            byte MyIndex = (byte)selectScreenComboBox.SelectedIndex;
            if (selectScreenComboBox.SelectedItem == null)
                return;
            if (MyIndex >= 0)
            {
                if (InCommand == true)
                {
                    MessageBox.Show("Busy");
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                AutoTestUnit.ClearBuffer();
                int Result = AutoTestUnit.DisplaySystemScreen(MyIndex);
                if (Result != 0)
                {
                    UpdateTechnicalConsole("Error on set system screen: " + AutoTestUnit.ErrorToText(Result) + "\n");
                }
                else
                {
                    UpdateTechnicalConsole("System screen set to " + selectScreenComboBox.SelectedItem.ToString() + "\n");
                }
                Cursor.Current = Cursors.Default;
            }
            else
                MessageBox.Show("Select pattern first");
        }

        private void button122_Click(object sender, EventArgs e)
        {

            byte[] Answer = new byte[1000];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.GetCalData();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on read cal data: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                if (AutoTestUnit.GetReply(ref Answer) >= (14 * 4 * 2))
                {
                    Int32 Frequency = 0;
                    float Magnitude = 0;
                    float Phase = 0;
                    Int32 AnswerOffset = 0;
                    const Int32 bytesPerFreq = 4 * 2;


                    for (Frequency = 0; Frequency < 14; Frequency++)
                    {
                        Magnitude = 0;
                        Phase = 0;
                        DisplayFrequency(Frequency);

                        AnswerOffset = (Frequency * bytesPerFreq) ;
                        Magnitude = BitConverter.ToSingle(Answer, AnswerOffset);
                        AnswerOffset += 4;
                        Phase = BitConverter.ToSingle(Answer, AnswerOffset);

                        UpdateTechnicalConsole("Cal Mag " + Magnitude.ToString());
                        UpdateTechnicalConsole(" Cal Phase " + Phase.ToString() + "\n");

                    }
                }
                else
                    UpdateTechnicalConsole("Error on read cal data: not enough bytes\n");
            }
            Cursor.Current = Cursors.Default;
        }

        private void buttonRequestResults_Click(object sender, EventArgs e)
        {
            RaiseExecuteCommandEvent(OpCode.READING_RESULT);           
        }

        private void setSystemStateButton_Click(object sender, EventArgs e)
        {
            byte MyIndex = (byte)systemStateComboBox.SelectedIndex;
            if (systemStateComboBox.SelectedItem == null)
                return;
            if (MyIndex >= 0)
            {
                if (InCommand == true)
                {
                    MessageBox.Show("Busy");
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                AutoTestUnit.ClearBuffer();
                int Result = AutoTestUnit.SetSystemState(MyIndex);
                if (Result != 0)
                {
                    UpdateTechnicalConsole("Error on set system state: " + AutoTestUnit.ErrorToText(Result) + "\n");
                }
                else
                {
                    UpdateTechnicalConsole("System state set to " + systemStateComboBox.SelectedItem.ToString() + "\n");
                }
                Cursor.Current = Cursors.Default;
            }
            else
                MessageBox.Show("Select state first");

        }

        private void corruptChecksumButton_Click(object sender, EventArgs e)
        {
            byte MyIndex = (byte)protectedDataComboBox.SelectedIndex;
            if (protectedDataComboBox.SelectedItem == null)
                return;
            if (MyIndex >= 0)
            {
                if (InCommand == true)
                {
                    MessageBox.Show("Busy");
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                AutoTestUnit.ClearBuffer();
                int Result = AutoTestUnit.CorruptDataChecksum(MyIndex);
                if (Result != 0)
                {
                    UpdateTechnicalConsole("Error on corrupt data checksum: " + AutoTestUnit.ErrorToText(Result) + "\n");
                }
                else
                {
                    UpdateTechnicalConsole("Checksum corrupted for " + protectedDataComboBox.SelectedItem.ToString() + "\n");
                }
                Cursor.Current = Cursors.Default;
            }
            else
                MessageBox.Show("Select Data first");

        }

        private void protectedDataComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            byte[] Answer = new byte[10];
            if (InCommand == true)
            {
                MessageBox.Show("Busy");
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            AutoTestUnit.ClearBuffer();
            int Result = AutoTestUnit.EraseLanguagePack();
            if (Result != 0)
            {
                UpdateTechnicalConsole("Error on erase language pack: " + AutoTestUnit.ErrorToText(Result) + "\n");
            }
            else
            {
                UpdateTechnicalConsole("Language pack erased\n");
            }
            Cursor.Current = Cursors.Default;
        }
    }
}
// Copyright (c) 2022 Zilico Limited
