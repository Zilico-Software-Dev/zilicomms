﻿
namespace ZiliCator
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.textBoxDisplay = new System.Windows.Forms.TextBox();
            this.buttonInfo = new System.Windows.Forms.Button();
            this.buttonStartSession = new System.Windows.Forms.Button();
            this.buttonReadSession = new System.Windows.Forms.Button();
            this.buttonDeleteSession = new System.Windows.Forms.Button();
            this.buttonRequestResults = new System.Windows.Forms.Button();
            this.languagePackButton = new System.Windows.Forms.Button();
            this.loadFirmwareButton = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adaptorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zedScan1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vCommToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.baudRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.singleByteModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemDivider1 = new System.Windows.Forms.ToolStripSeparator();
            this.clearScreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStripMain = new System.Windows.Forms.StatusStrip();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.comsSettingsTabPage = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ComPortList = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button60 = new System.Windows.Forms.Button();
            this.deviceComsTabPage = new System.Windows.Forms.TabPage();
            this.remoteControlTabPage = new System.Windows.Forms.TabPage();
            this.TechnicalDisplayBox = new System.Windows.Forms.RichTextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.FPGATabPage = new System.Windows.Forms.TabPage();
            this.button62 = new System.Windows.Forms.Button();
            this.button61 = new System.Windows.Forms.Button();
            this.button59 = new System.Windows.Forms.Button();
            this.button58 = new System.Windows.Forms.Button();
            this.button57 = new System.Windows.Forms.Button();
            this.button54 = new System.Windows.Forms.Button();
            this.comboBoxUnmanagedSamples = new System.Windows.Forms.ComboBox();
            this.comboBoxUnmanagedSw = new System.Windows.Forms.ComboBox();
            this.comboBoxUnmanagedFreq = new System.Windows.Forms.ComboBox();
            this.button53 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.button50 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxManagedSw = new System.Windows.Forms.ComboBox();
            this.comboBoxManagedFreq = new System.Windows.Forms.ComboBox();
            this.button36 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.batteryAndChargingTabPage = new System.Windows.Forms.TabPage();
            this.button56 = new System.Windows.Forms.Button();
            this.button55 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button65 = new System.Windows.Forms.Button();
            this.systemControlTabPage = new System.Windows.Forms.TabPage();
            this.protectedDataComboBox = new System.Windows.Forms.ComboBox();
            this.corruptChecksumButton = new System.Windows.Forms.Button();
            this.systemStateComboBox = new System.Windows.Forms.ComboBox();
            this.setSystemStateButton = new System.Windows.Forms.Button();
            this.button122 = new System.Windows.Forms.Button();
            this.selectScreenComboBox = new System.Windows.Forms.ComboBox();
            this.button121 = new System.Windows.Forms.Button();
            this.button120 = new System.Windows.Forms.Button();
            this.button119 = new System.Windows.Forms.Button();
            this.button118 = new System.Windows.Forms.Button();
            this.TonePatternComboBox = new System.Windows.Forms.ComboBox();
            this.button114 = new System.Windows.Forms.Button();
            this.button67 = new System.Windows.Forms.Button();
            this.button78 = new System.Windows.Forms.Button();
            this.button73 = new System.Windows.Forms.Button();
            this.LEDPatternComboBox = new System.Windows.Forms.ComboBox();
            this.button72 = new System.Windows.Forms.Button();
            this.referralSessionComboBox = new System.Windows.Forms.ComboBox();
            this.button68 = new System.Windows.Forms.Button();
            this.externalFlashTabPage = new System.Windows.Forms.TabPage();
            this.button71 = new System.Windows.Forms.Button();
            this.button70 = new System.Windows.Forms.Button();
            this.button69 = new System.Windows.Forms.Button();
            this.button74 = new System.Windows.Forms.Button();
            this.button75 = new System.Windows.Forms.Button();
            this.auditInfoTabPage = new System.Windows.Forms.TabPage();
            this.button77 = new System.Windows.Forms.Button();
            this.button76 = new System.Windows.Forms.Button();
            this.button115 = new System.Windows.Forms.Button();
            this.UIDtextBox = new System.Windows.Forms.TextBox();
            this.button116 = new System.Windows.Forms.Button();
            this.iOControlTabPage = new System.Windows.Forms.TabPage();
            this.backlightComboBox = new System.Windows.Forms.ComboBox();
            this.button81 = new System.Windows.Forms.Button();
            this.button80 = new System.Windows.Forms.Button();
            this.button79 = new System.Windows.Forms.Button();
            this.button63 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button29 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.scriptRunnerTabPage = new System.Windows.Forms.TabPage();
            this.runScriptSetButton = new System.Windows.Forms.Button();
            this.loadSetButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.resultSetGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scriptSetGridView = new System.Windows.Forms.DataGridView();
            this.ScriptFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scriptCreatorTabPage = new System.Windows.Forms.TabPage();
            this.button113 = new System.Windows.Forms.Button();
            this.commandDataGridView = new System.Windows.Forms.DataGridView();
            this.button112 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button111 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button110 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button109 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button108 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button107 = new System.Windows.Forms.Button();
            this.undockButton = new System.Windows.Forms.Button();
            this.button106 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button105 = new System.Windows.Forms.Button();
            this.grabScreenButton = new System.Windows.Forms.Button();
            this.button104 = new System.Windows.Forms.Button();
            this.screenPictureBox = new System.Windows.Forms.PictureBox();
            this.button103 = new System.Windows.Forms.Button();
            this.screenCopyPictureBox = new System.Windows.Forms.PictureBox();
            this.button102 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.button101 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.button100 = new System.Windows.Forms.Button();
            this.sessionButton = new System.Windows.Forms.Button();
            this.button99 = new System.Windows.Forms.Button();
            this.runScriptButton = new System.Windows.Forms.Button();
            this.button98 = new System.Windows.Forms.Button();
            this.dockButton = new System.Windows.Forms.Button();
            this.button97 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button96 = new System.Windows.Forms.Button();
            this.loadScriptButton = new System.Windows.Forms.Button();
            this.button95 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button94 = new System.Windows.Forms.Button();
            this.compareScreenButton = new System.Windows.Forms.Button();
            this.button93 = new System.Windows.Forms.Button();
            this.failBoundaryButton = new System.Windows.Forms.Button();
            this.button92 = new System.Windows.Forms.Button();
            this.readingButton = new System.Windows.Forms.Button();
            this.button91 = new System.Windows.Forms.Button();
            this.statemachineButton = new System.Windows.Forms.Button();
            this.button90 = new System.Windows.Forms.Button();
            this.passBoundaryButton = new System.Windows.Forms.Button();
            this.button89 = new System.Windows.Forms.Button();
            this.userDelayButton = new System.Windows.Forms.Button();
            this.button88 = new System.Windows.Forms.Button();
            this.batteryButton = new System.Windows.Forms.Button();
            this.button87 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button86 = new System.Windows.Forms.Button();
            this.checkLEDStateButton = new System.Windows.Forms.Button();
            this.button85 = new System.Windows.Forms.Button();
            this.getLEDStateButton = new System.Windows.Forms.Button();
            this.button84 = new System.Windows.Forms.Button();
            this.fullStatusCheckButton = new System.Windows.Forms.Button();
            this.button83 = new System.Windows.Forms.Button();
            this.cytologyButton = new System.Windows.Forms.Button();
            this.button82 = new System.Windows.Forms.Button();
            this.setSessionTimeButton = new System.Windows.Forms.Button();
            this.button66 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button64 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.convertButton = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.newScriptButton = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.batchScriptTabPage = new System.Windows.Forms.TabPage();
            this.saveSetButton = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.setCreationDataGridView = new System.Windows.Forms.DataGridView();
            this.File = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button117 = new System.Windows.Forms.Button();
            this.tmrDevicePoll = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.playbackTimer = new System.Windows.Forms.Timer(this.components);
            this.hardResetTimer = new System.Windows.Forms.Timer(this.components);
            this.scriptSetTimer = new System.Windows.Forms.Timer(this.components);
            this.keyPressTimer = new System.Windows.Forms.Timer(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.menuStripMain.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.comsSettingsTabPage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.deviceComsTabPage.SuspendLayout();
            this.remoteControlTabPage.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.FPGATabPage.SuspendLayout();
            this.batteryAndChargingTabPage.SuspendLayout();
            this.systemControlTabPage.SuspendLayout();
            this.externalFlashTabPage.SuspendLayout();
            this.auditInfoTabPage.SuspendLayout();
            this.iOControlTabPage.SuspendLayout();
            this.scriptRunnerTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultSetGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scriptSetGridView)).BeginInit();
            this.scriptCreatorTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.commandDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.screenPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.screenCopyPictureBox)).BeginInit();
            this.batchScriptTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.setCreationDataGridView)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxDisplay
            // 
            this.textBoxDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDisplay.BackColor = System.Drawing.Color.Black;
            this.textBoxDisplay.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDisplay.ForeColor = System.Drawing.Color.Lime;
            this.textBoxDisplay.Location = new System.Drawing.Point(44, 32);
            this.textBoxDisplay.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDisplay.Multiline = true;
            this.textBoxDisplay.Name = "textBoxDisplay";
            this.textBoxDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxDisplay.Size = new System.Drawing.Size(1571, 581);
            this.textBoxDisplay.TabIndex = 0;
            this.textBoxDisplay.Text = "1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n9\r\n0123456789\r\n1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n7\r\n8\r\n9\r\n0123456789\r\n";
            // 
            // buttonInfo
            // 
            this.buttonInfo.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonInfo.Location = new System.Drawing.Point(127, 622);
            this.buttonInfo.Margin = new System.Windows.Forms.Padding(4);
            this.buttonInfo.Name = "buttonInfo";
            this.buttonInfo.Size = new System.Drawing.Size(156, 60);
            this.buttonInfo.TabIndex = 1;
            this.buttonInfo.Text = "Info";
            this.buttonInfo.UseVisualStyleBackColor = true;
            this.buttonInfo.Click += new System.EventHandler(this.buttonInfo_Click);
            // 
            // buttonStartSession
            // 
            this.buttonStartSession.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonStartSession.Location = new System.Drawing.Point(291, 622);
            this.buttonStartSession.Margin = new System.Windows.Forms.Padding(4);
            this.buttonStartSession.Name = "buttonStartSession";
            this.buttonStartSession.Size = new System.Drawing.Size(156, 60);
            this.buttonStartSession.TabIndex = 2;
            this.buttonStartSession.Text = "Start Session";
            this.buttonStartSession.UseVisualStyleBackColor = true;
            this.buttonStartSession.Click += new System.EventHandler(this.buttonStartSession_Click);
            // 
            // buttonReadSession
            // 
            this.buttonReadSession.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonReadSession.Location = new System.Drawing.Point(455, 622);
            this.buttonReadSession.Margin = new System.Windows.Forms.Padding(4);
            this.buttonReadSession.Name = "buttonReadSession";
            this.buttonReadSession.Size = new System.Drawing.Size(156, 60);
            this.buttonReadSession.TabIndex = 3;
            this.buttonReadSession.Text = "Read Session";
            this.buttonReadSession.UseVisualStyleBackColor = true;
            this.buttonReadSession.Click += new System.EventHandler(this.buttonReadSession_Click);
            // 
            // buttonDeleteSession
            // 
            this.buttonDeleteSession.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonDeleteSession.Location = new System.Drawing.Point(619, 622);
            this.buttonDeleteSession.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDeleteSession.Name = "buttonDeleteSession";
            this.buttonDeleteSession.Size = new System.Drawing.Size(156, 60);
            this.buttonDeleteSession.TabIndex = 4;
            this.buttonDeleteSession.Text = "Delete Session";
            this.buttonDeleteSession.UseVisualStyleBackColor = true;
            this.buttonDeleteSession.Click += new System.EventHandler(this.buttonDeleteSession_Click);
            // 
            // buttonRequestResults
            // 
            this.buttonRequestResults.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonRequestResults.Location = new System.Drawing.Point(127, 689);
            this.buttonRequestResults.Margin = new System.Windows.Forms.Padding(4);
            this.buttonRequestResults.Name = "buttonRequestResults";
            this.buttonRequestResults.Size = new System.Drawing.Size(156, 60);
            this.buttonRequestResults.TabIndex = 5;
            this.buttonRequestResults.Text = "Request Results";
            this.buttonRequestResults.UseVisualStyleBackColor = true;
            this.buttonRequestResults.Click += new System.EventHandler(this.buttonRequestResults_Click);
            // 
            // languagePackButton
            // 
            this.languagePackButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.languagePackButton.Location = new System.Drawing.Point(291, 689);
            this.languagePackButton.Margin = new System.Windows.Forms.Padding(4);
            this.languagePackButton.Name = "languagePackButton";
            this.languagePackButton.Size = new System.Drawing.Size(156, 60);
            this.languagePackButton.TabIndex = 6;
            this.languagePackButton.Text = "Load Language Pack";
            this.languagePackButton.UseVisualStyleBackColor = true;
            // 
            // loadFirmwareButton
            // 
            this.loadFirmwareButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.loadFirmwareButton.Location = new System.Drawing.Point(455, 689);
            this.loadFirmwareButton.Margin = new System.Windows.Forms.Padding(4);
            this.loadFirmwareButton.Name = "loadFirmwareButton";
            this.loadFirmwareButton.Size = new System.Drawing.Size(156, 60);
            this.loadFirmwareButton.TabIndex = 7;
            this.loadFirmwareButton.Text = "Load Firmware Image";
            this.loadFirmwareButton.UseVisualStyleBackColor = true;
            this.loadFirmwareButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button3.Location = new System.Drawing.Point(619, 689);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(156, 60);
            this.button3.TabIndex = 8;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // menuStripMain
            // 
            this.menuStripMain.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.clearScreenToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStripMain.Size = new System.Drawing.Size(1693, 28);
            this.menuStripMain.TabIndex = 9;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adaptorToolStripMenuItem,
            this.baudRateToolStripMenuItem,
            this.singleByteModeToolStripMenuItem,
            this.toolStripMenuItemDivider1});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(179, 24);
            this.settingsToolStripMenuItem.Text = "Device Comms Settings";
            // 
            // adaptorToolStripMenuItem
            // 
            this.adaptorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zedScan1ToolStripMenuItem,
            this.vCommToolStripMenuItem,
            this.hIDToolStripMenuItem});
            this.adaptorToolStripMenuItem.Name = "adaptorToolStripMenuItem";
            this.adaptorToolStripMenuItem.Size = new System.Drawing.Size(209, 26);
            this.adaptorToolStripMenuItem.Text = "Adaptor";
            // 
            // zedScan1ToolStripMenuItem
            // 
            this.zedScan1ToolStripMenuItem.CheckOnClick = true;
            this.zedScan1ToolStripMenuItem.Name = "zedScan1ToolStripMenuItem";
            this.zedScan1ToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.zedScan1ToolStripMenuItem.Text = "ZedScan 1";
            this.zedScan1ToolStripMenuItem.Click += new System.EventHandler(this.MenuAdaptorSelection);
            // 
            // vCommToolStripMenuItem
            // 
            this.vCommToolStripMenuItem.CheckOnClick = true;
            this.vCommToolStripMenuItem.Name = "vCommToolStripMenuItem";
            this.vCommToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.vCommToolStripMenuItem.Text = "vComm";
            this.vCommToolStripMenuItem.Click += new System.EventHandler(this.MenuAdaptorSelection);
            // 
            // hIDToolStripMenuItem
            // 
            this.hIDToolStripMenuItem.CheckOnClick = true;
            this.hIDToolStripMenuItem.Name = "hIDToolStripMenuItem";
            this.hIDToolStripMenuItem.Size = new System.Drawing.Size(161, 26);
            this.hIDToolStripMenuItem.Text = "HID";
            this.hIDToolStripMenuItem.Click += new System.EventHandler(this.MenuAdaptorSelection);
            // 
            // baudRateToolStripMenuItem
            // 
            this.baudRateToolStripMenuItem.Name = "baudRateToolStripMenuItem";
            this.baudRateToolStripMenuItem.Size = new System.Drawing.Size(209, 26);
            this.baudRateToolStripMenuItem.Text = "Baud Rate";
            this.baudRateToolStripMenuItem.Click += new System.EventHandler(this.baudRateToolStripMenuItem_Click);
            // 
            // singleByteModeToolStripMenuItem
            // 
            this.singleByteModeToolStripMenuItem.CheckOnClick = true;
            this.singleByteModeToolStripMenuItem.Name = "singleByteModeToolStripMenuItem";
            this.singleByteModeToolStripMenuItem.Size = new System.Drawing.Size(209, 26);
            this.singleByteModeToolStripMenuItem.Text = "Single Byte Mode";
            this.singleByteModeToolStripMenuItem.Click += new System.EventHandler(this.singleByteModeToolStripMenuItem_Click);
            // 
            // toolStripMenuItemDivider1
            // 
            this.toolStripMenuItemDivider1.Name = "toolStripMenuItemDivider1";
            this.toolStripMenuItemDivider1.Size = new System.Drawing.Size(206, 6);
            // 
            // clearScreenToolStripMenuItem
            // 
            this.clearScreenToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.clearScreenToolStripMenuItem.Name = "clearScreenToolStripMenuItem";
            this.clearScreenToolStripMenuItem.Size = new System.Drawing.Size(105, 24);
            this.clearScreenToolStripMenuItem.Text = "Clear Screen";
            this.clearScreenToolStripMenuItem.Click += new System.EventHandler(this.clearScreenToolStripMenuItem_Click);
            // 
            // statusStripMain
            // 
            this.statusStripMain.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStripMain.Location = new System.Drawing.Point(0, 848);
            this.statusStripMain.Name = "statusStripMain";
            this.statusStripMain.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStripMain.Size = new System.Drawing.Size(1693, 22);
            this.statusStripMain.TabIndex = 10;
            this.statusStripMain.Text = "statusStrip1";
            // 
            // button4
            // 
            this.button4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button4.Location = new System.Drawing.Point(127, 622);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(156, 60);
            this.button4.TabIndex = 1;
            this.button4.Text = "Info";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.buttonInfo_Click);
            // 
            // button5
            // 
            this.button5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button5.Location = new System.Drawing.Point(291, 622);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(156, 60);
            this.button5.TabIndex = 2;
            this.button5.Text = "Start Session";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.buttonStartSession_Click);
            // 
            // button6
            // 
            this.button6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button6.Location = new System.Drawing.Point(455, 622);
            this.button6.Margin = new System.Windows.Forms.Padding(4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(156, 60);
            this.button6.TabIndex = 3;
            this.button6.Text = "Read Session";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.buttonReadSession_Click);
            // 
            // button7
            // 
            this.button7.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button7.Location = new System.Drawing.Point(619, 622);
            this.button7.Margin = new System.Windows.Forms.Padding(4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(156, 60);
            this.button7.TabIndex = 4;
            this.button7.Text = "Delete Session";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.buttonDeleteSession_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.comsSettingsTabPage);
            this.tabControl1.Controls.Add(this.deviceComsTabPage);
            this.tabControl1.Controls.Add(this.remoteControlTabPage);
            this.tabControl1.Controls.Add(this.scriptRunnerTabPage);
            this.tabControl1.Controls.Add(this.scriptCreatorTabPage);
            this.tabControl1.Controls.Add(this.batchScriptTabPage);
            this.tabControl1.Location = new System.Drawing.Point(16, 33);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1661, 806);
            this.tabControl1.TabIndex = 11;
            // 
            // comsSettingsTabPage
            // 
            this.comsSettingsTabPage.Controls.Add(this.groupBox2);
            this.comsSettingsTabPage.Controls.Add(this.groupBox1);
            this.comsSettingsTabPage.Location = new System.Drawing.Point(4, 25);
            this.comsSettingsTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.comsSettingsTabPage.Name = "comsSettingsTabPage";
            this.comsSettingsTabPage.Size = new System.Drawing.Size(1653, 777);
            this.comsSettingsTabPage.TabIndex = 5;
            this.comsSettingsTabPage.Text = "Comms Interfaces";
            this.comsSettingsTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(29, 238);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(1591, 510);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Device Interface";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ComPortList);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.button60);
            this.groupBox1.Location = new System.Drawing.Point(29, 30);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1591, 182);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Auto Test Interface";
            // 
            // ComPortList
            // 
            this.ComPortList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComPortList.FormattingEnabled = true;
            this.ComPortList.Location = new System.Drawing.Point(101, 36);
            this.ComPortList.Margin = new System.Windows.Forms.Padding(4);
            this.ComPortList.Name = "ComPortList";
            this.ComPortList.Size = new System.Drawing.Size(160, 24);
            this.ComPortList.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(27, 39);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Com Port";
            // 
            // button60
            // 
            this.button60.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button60.Location = new System.Drawing.Point(31, 82);
            this.button60.Margin = new System.Windows.Forms.Padding(4);
            this.button60.Name = "button60";
            this.button60.Size = new System.Drawing.Size(232, 47);
            this.button60.TabIndex = 7;
            this.button60.Text = "Connect Autotest";
            this.button60.UseVisualStyleBackColor = true;
            this.button60.Click += new System.EventHandler(this.button60_Click_1);
            // 
            // deviceComsTabPage
            // 
            this.deviceComsTabPage.Controls.Add(this.textBoxDisplay);
            this.deviceComsTabPage.Controls.Add(this.button4);
            this.deviceComsTabPage.Controls.Add(this.button3);
            this.deviceComsTabPage.Controls.Add(this.button7);
            this.deviceComsTabPage.Controls.Add(this.loadFirmwareButton);
            this.deviceComsTabPage.Controls.Add(this.buttonInfo);
            this.deviceComsTabPage.Controls.Add(this.languagePackButton);
            this.deviceComsTabPage.Controls.Add(this.buttonRequestResults);
            this.deviceComsTabPage.Controls.Add(this.buttonDeleteSession);
            this.deviceComsTabPage.Controls.Add(this.button5);
            this.deviceComsTabPage.Controls.Add(this.buttonReadSession);
            this.deviceComsTabPage.Controls.Add(this.button6);
            this.deviceComsTabPage.Controls.Add(this.buttonStartSession);
            this.deviceComsTabPage.Location = new System.Drawing.Point(4, 25);
            this.deviceComsTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.deviceComsTabPage.Name = "deviceComsTabPage";
            this.deviceComsTabPage.Padding = new System.Windows.Forms.Padding(4);
            this.deviceComsTabPage.Size = new System.Drawing.Size(1653, 777);
            this.deviceComsTabPage.TabIndex = 0;
            this.deviceComsTabPage.Text = "Device";
            this.deviceComsTabPage.UseVisualStyleBackColor = true;
            // 
            // remoteControlTabPage
            // 
            this.remoteControlTabPage.Controls.Add(this.TechnicalDisplayBox);
            this.remoteControlTabPage.Controls.Add(this.tabControl2);
            this.remoteControlTabPage.Location = new System.Drawing.Point(4, 25);
            this.remoteControlTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.remoteControlTabPage.Name = "remoteControlTabPage";
            this.remoteControlTabPage.Padding = new System.Windows.Forms.Padding(4);
            this.remoteControlTabPage.Size = new System.Drawing.Size(1653, 777);
            this.remoteControlTabPage.TabIndex = 1;
            this.remoteControlTabPage.Text = "Remote Test";
            this.remoteControlTabPage.UseVisualStyleBackColor = true;
            // 
            // TechnicalDisplayBox
            // 
            this.TechnicalDisplayBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TechnicalDisplayBox.Location = new System.Drawing.Point(1109, 31);
            this.TechnicalDisplayBox.Margin = new System.Windows.Forms.Padding(4);
            this.TechnicalDisplayBox.Name = "TechnicalDisplayBox";
            this.TechnicalDisplayBox.Size = new System.Drawing.Size(532, 735);
            this.TechnicalDisplayBox.TabIndex = 2;
            this.TechnicalDisplayBox.Text = "";
            // 
            // tabControl2
            // 
            this.tabControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl2.Controls.Add(this.FPGATabPage);
            this.tabControl2.Controls.Add(this.batteryAndChargingTabPage);
            this.tabControl2.Controls.Add(this.systemControlTabPage);
            this.tabControl2.Controls.Add(this.externalFlashTabPage);
            this.tabControl2.Controls.Add(this.auditInfoTabPage);
            this.tabControl2.Controls.Add(this.iOControlTabPage);
            this.tabControl2.Location = new System.Drawing.Point(4, 4);
            this.tabControl2.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1097, 767);
            this.tabControl2.TabIndex = 0;
            // 
            // FPGATabPage
            // 
            this.FPGATabPage.Controls.Add(this.button62);
            this.FPGATabPage.Controls.Add(this.button61);
            this.FPGATabPage.Controls.Add(this.button59);
            this.FPGATabPage.Controls.Add(this.button58);
            this.FPGATabPage.Controls.Add(this.button57);
            this.FPGATabPage.Controls.Add(this.button54);
            this.FPGATabPage.Controls.Add(this.comboBoxUnmanagedSamples);
            this.FPGATabPage.Controls.Add(this.comboBoxUnmanagedSw);
            this.FPGATabPage.Controls.Add(this.comboBoxUnmanagedFreq);
            this.FPGATabPage.Controls.Add(this.button53);
            this.FPGATabPage.Controls.Add(this.button52);
            this.FPGATabPage.Controls.Add(this.button51);
            this.FPGATabPage.Controls.Add(this.label3);
            this.FPGATabPage.Controls.Add(this.button50);
            this.FPGATabPage.Controls.Add(this.button49);
            this.FPGATabPage.Controls.Add(this.button48);
            this.FPGATabPage.Controls.Add(this.button47);
            this.FPGATabPage.Controls.Add(this.button46);
            this.FPGATabPage.Controls.Add(this.button45);
            this.FPGATabPage.Controls.Add(this.button44);
            this.FPGATabPage.Controls.Add(this.button43);
            this.FPGATabPage.Controls.Add(this.button42);
            this.FPGATabPage.Controls.Add(this.button40);
            this.FPGATabPage.Controls.Add(this.button41);
            this.FPGATabPage.Controls.Add(this.button39);
            this.FPGATabPage.Controls.Add(this.button38);
            this.FPGATabPage.Controls.Add(this.button37);
            this.FPGATabPage.Controls.Add(this.label2);
            this.FPGATabPage.Controls.Add(this.comboBoxManagedSw);
            this.FPGATabPage.Controls.Add(this.comboBoxManagedFreq);
            this.FPGATabPage.Controls.Add(this.button36);
            this.FPGATabPage.Controls.Add(this.button35);
            this.FPGATabPage.Controls.Add(this.button34);
            this.FPGATabPage.Controls.Add(this.button33);
            this.FPGATabPage.Controls.Add(this.button32);
            this.FPGATabPage.Location = new System.Drawing.Point(4, 25);
            this.FPGATabPage.Margin = new System.Windows.Forms.Padding(4);
            this.FPGATabPage.Name = "FPGATabPage";
            this.FPGATabPage.Padding = new System.Windows.Forms.Padding(4);
            this.FPGATabPage.Size = new System.Drawing.Size(1089, 738);
            this.FPGATabPage.TabIndex = 0;
            this.FPGATabPage.Text = "FPGA Control";
            this.FPGATabPage.UseVisualStyleBackColor = true;
            // 
            // button62
            // 
            this.button62.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button62.Location = new System.Drawing.Point(485, 96);
            this.button62.Margin = new System.Windows.Forms.Padding(4);
            this.button62.Name = "button62";
            this.button62.Size = new System.Drawing.Size(225, 46);
            this.button62.TabIndex = 76;
            this.button62.Text = "Reset Driver";
            this.toolTip1.SetToolTip(this.button62, "Reset the measurement driver");
            this.button62.UseVisualStyleBackColor = true;
            this.button62.Click += new System.EventHandler(this.button62_Click);
            // 
            // button61
            // 
            this.button61.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button61.Location = new System.Drawing.Point(252, 556);
            this.button61.Margin = new System.Windows.Forms.Padding(4);
            this.button61.Name = "button61";
            this.button61.Size = new System.Drawing.Size(225, 46);
            this.button61.TabIndex = 75;
            this.button61.Text = "Get FPGA Results";
            this.toolTip1.SetToolTip(this.button61, "Readback measurement results from the fpga and process into magnitude and phase");
            this.button61.UseVisualStyleBackColor = true;
            this.button61.Click += new System.EventHandler(this.button61_Click);
            // 
            // button59
            // 
            this.button59.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button59.Location = new System.Drawing.Point(252, 503);
            this.button59.Margin = new System.Windows.Forms.Padding(4);
            this.button59.Name = "button59";
            this.button59.Size = new System.Drawing.Size(225, 46);
            this.button59.TabIndex = 74;
            this.button59.Text = "Set Matrix Reset";
            this.toolTip1.SetToolTip(this.button59, "Assert/de-assert the matrix chips reset line");
            this.button59.UseVisualStyleBackColor = true;
            this.button59.Click += new System.EventHandler(this.button59_Click);
            // 
            // button58
            // 
            this.button58.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button58.Location = new System.Drawing.Point(485, 43);
            this.button58.Margin = new System.Windows.Forms.Padding(4);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(225, 46);
            this.button58.TabIndex = 73;
            this.button58.Text = "Get Driver Stat";
            this.toolTip1.SetToolTip(this.button58, "Get status of driver to see what its state machine is up to");
            this.button58.UseVisualStyleBackColor = true;
            this.button58.Click += new System.EventHandler(this.button58_Click);
            // 
            // button57
            // 
            this.button57.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button57.Location = new System.Drawing.Point(252, 96);
            this.button57.Margin = new System.Windows.Forms.Padding(4);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(225, 46);
            this.button57.TabIndex = 72;
            this.button57.Text = "Get Results";
            this.toolTip1.SetToolTip(this.button57, "Get the results for the measurement cycle");
            this.button57.UseVisualStyleBackColor = true;
            this.button57.Click += new System.EventHandler(this.button57_Click);
            // 
            // button54
            // 
            this.button54.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button54.Location = new System.Drawing.Point(252, 450);
            this.button54.Margin = new System.Windows.Forms.Padding(4);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(225, 46);
            this.button54.TabIndex = 71;
            this.button54.Text = "Set ADC State Active";
            this.toolTip1.SetToolTip(this.button54, "Set ADC\'s active or inactive");
            this.button54.UseVisualStyleBackColor = true;
            this.button54.Click += new System.EventHandler(this.button54_Click);
            // 
            // comboBoxUnmanagedSamples
            // 
            this.comboBoxUnmanagedSamples.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUnmanagedSamples.FormattingEnabled = true;
            this.comboBoxUnmanagedSamples.Items.AddRange(new object[] {
            "262144",
            "131072",
            "65536",
            "32768",
            "16384",
            "16384",
            "16384",
            "16384",
            "16384",
            "16384",
            "16384",
            "16384",
            "16384",
            "16384"});
            this.comboBoxUnmanagedSamples.Location = new System.Drawing.Point(719, 512);
            this.comboBoxUnmanagedSamples.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxUnmanagedSamples.Name = "comboBoxUnmanagedSamples";
            this.comboBoxUnmanagedSamples.Size = new System.Drawing.Size(355, 24);
            this.comboBoxUnmanagedSamples.TabIndex = 70;
            // 
            // comboBoxUnmanagedSw
            // 
            this.comboBoxUnmanagedSw.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUnmanagedSw.FormattingEnabled = true;
            this.comboBoxUnmanagedSw.Items.AddRange(new object[] {
            "AD",
            "BC",
            "AC",
            "BD",
            "Reading(AC)",
            "CAL 5K1",
            "TEST RC"});
            this.comboBoxUnmanagedSw.Location = new System.Drawing.Point(719, 352);
            this.comboBoxUnmanagedSw.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxUnmanagedSw.Name = "comboBoxUnmanagedSw";
            this.comboBoxUnmanagedSw.Size = new System.Drawing.Size(355, 24);
            this.comboBoxUnmanagedSw.TabIndex = 69;
            // 
            // comboBoxUnmanagedFreq
            // 
            this.comboBoxUnmanagedFreq.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUnmanagedFreq.FormattingEnabled = true;
            this.comboBoxUnmanagedFreq.Items.AddRange(new object[] {
            "76 Hz",
            "153 Hz",
            "305 Hz",
            "610 Hz",
            "1200 Hz",
            "2400 Hz",
            "4900 Hz",
            "9800 Hz",
            "20 KHz",
            "39 KHz",
            "78 KHz",
            "156 KHz",
            "312 KHz",
            "625 KHz"});
            this.comboBoxUnmanagedFreq.Location = new System.Drawing.Point(719, 406);
            this.comboBoxUnmanagedFreq.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxUnmanagedFreq.Name = "comboBoxUnmanagedFreq";
            this.comboBoxUnmanagedFreq.Size = new System.Drawing.Size(355, 24);
            this.comboBoxUnmanagedFreq.TabIndex = 68;
            // 
            // button53
            // 
            this.button53.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button53.Location = new System.Drawing.Point(252, 398);
            this.button53.Margin = new System.Windows.Forms.Padding(4);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(225, 46);
            this.button53.TabIndex = 67;
            this.button53.Text = "Read FPGA Ready";
            this.toolTip1.SetToolTip(this.button53, "Ready the ready line state asserted by the fpga");
            this.button53.UseVisualStyleBackColor = true;
            this.button53.Click += new System.EventHandler(this.button53_Click);
            // 
            // button52
            // 
            this.button52.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button52.Location = new System.Drawing.Point(252, 345);
            this.button52.Margin = new System.Windows.Forms.Padding(4);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(225, 46);
            this.button52.TabIndex = 66;
            this.button52.Text = "Set FPGA Go";
            this.toolTip1.SetToolTip(this.button52, "Assert the go line on the fpga to start a measurement cycle");
            this.button52.UseVisualStyleBackColor = true;
            this.button52.Click += new System.EventHandler(this.button52_Click);
            // 
            // button51
            // 
            this.button51.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button51.Location = new System.Drawing.Point(252, 292);
            this.button51.Margin = new System.Windows.Forms.Padding(4);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(225, 46);
            this.button51.TabIndex = 65;
            this.button51.Text = "Set FPGA Reset";
            this.toolTip1.SetToolTip(this.button51, "Assert or release reset line to the fpga");
            this.button51.UseVisualStyleBackColor = true;
            this.button51.Click += new System.EventHandler(this.button51_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(19, 14);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(265, 17);
            this.label3.TabIndex = 64;
            this.label3.Text = "Managed Measurement Interface Control";
            // 
            // button50
            // 
            this.button50.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button50.Location = new System.Drawing.Point(19, 662);
            this.button50.Margin = new System.Windows.Forms.Padding(4);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(225, 46);
            this.button50.TabIndex = 63;
            this.button50.Text = "Set FPGA Clock";
            this.toolTip1.SetToolTip(this.button50, "turn the fpga clock on/off as required");
            this.button50.UseVisualStyleBackColor = true;
            this.button50.Click += new System.EventHandler(this.button50_Click);
            // 
            // button49
            // 
            this.button49.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button49.Location = new System.Drawing.Point(19, 609);
            this.button49.Margin = new System.Windows.Forms.Padding(4);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(225, 46);
            this.button49.TabIndex = 62;
            this.button49.Text = "Set FPGA Power";
            this.toolTip1.SetToolTip(this.button49, "turn on/off the power supply for the fpga chip");
            this.button49.UseVisualStyleBackColor = true;
            this.button49.Click += new System.EventHandler(this.button49_Click);
            // 
            // button48
            // 
            this.button48.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button48.Location = new System.Drawing.Point(19, 556);
            this.button48.Margin = new System.Windows.Forms.Padding(4);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(225, 46);
            this.button48.TabIndex = 61;
            this.button48.Text = "Set Analogue Power";
            this.toolTip1.SetToolTip(this.button48, "Turn on/off the power supply for the analogue subsystem");
            this.button48.UseVisualStyleBackColor = true;
            this.button48.Click += new System.EventHandler(this.button48_Click);
            // 
            // button47
            // 
            this.button47.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button47.Location = new System.Drawing.Point(485, 556);
            this.button47.Margin = new System.Windows.Forms.Padding(4);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(225, 46);
            this.button47.TabIndex = 60;
            this.button47.Text = "Read Samples";
            this.toolTip1.SetToolTip(this.button47, "Readback the number of measurement samples currently configured for the FPGA");
            this.button47.UseVisualStyleBackColor = true;
            this.button47.Click += new System.EventHandler(this.button47_Click);
            // 
            // button46
            // 
            this.button46.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button46.Location = new System.Drawing.Point(485, 503);
            this.button46.Margin = new System.Windows.Forms.Padding(4);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(225, 46);
            this.button46.TabIndex = 59;
            this.button46.Text = "Set Samples";
            this.toolTip1.SetToolTip(this.button46, "Configure the number of required samples. This value is specific for each frequen" +
        "cy.");
            this.button46.UseVisualStyleBackColor = true;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // button45
            // 
            this.button45.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button45.Location = new System.Drawing.Point(485, 450);
            this.button45.Margin = new System.Windows.Forms.Padding(4);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(225, 46);
            this.button45.TabIndex = 58;
            this.button45.Text = "Read Frequency";
            this.toolTip1.SetToolTip(this.button45, "Read back the currently configured frequency from the fpga");
            this.button45.UseVisualStyleBackColor = true;
            this.button45.Click += new System.EventHandler(this.button45_Click);
            // 
            // button44
            // 
            this.button44.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button44.Location = new System.Drawing.Point(485, 399);
            this.button44.Margin = new System.Windows.Forms.Padding(4);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(225, 46);
            this.button44.TabIndex = 57;
            this.button44.Text = "Set Frequency";
            this.toolTip1.SetToolTip(this.button44, "Set the frequency to one of the 14 available frequencies");
            this.button44.UseVisualStyleBackColor = true;
            this.button44.Click += new System.EventHandler(this.button44_Click);
            // 
            // button43
            // 
            this.button43.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button43.Location = new System.Drawing.Point(485, 345);
            this.button43.Margin = new System.Windows.Forms.Padding(4);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(225, 46);
            this.button43.TabIndex = 56;
            this.button43.Text = "Set Matrix SW";
            this.toolTip1.SetToolTip(this.button43, "Set the matrix to one of the 6 paths");
            this.button43.UseVisualStyleBackColor = true;
            this.button43.Click += new System.EventHandler(this.button43_Click);
            // 
            // button42
            // 
            this.button42.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button42.Location = new System.Drawing.Point(485, 292);
            this.button42.Margin = new System.Windows.Forms.Padding(4);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(225, 46);
            this.button42.TabIndex = 55;
            this.button42.Text = "Read Matrix SW";
            this.toolTip1.SetToolTip(this.button42, "Request the line configuration for the matrix. 8 bits by 8 lines");
            this.button42.UseVisualStyleBackColor = true;
            this.button42.Click += new System.EventHandler(this.button42_Click);
            // 
            // button40
            // 
            this.button40.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button40.Location = new System.Drawing.Point(19, 503);
            this.button40.Margin = new System.Windows.Forms.Padding(4);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(225, 46);
            this.button40.TabIndex = 54;
            this.button40.Text = "Read FPGA CoSine";
            this.toolTip1.SetToolTip(this.button40, "Read back the cosine table from the fpga");
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // button41
            // 
            this.button41.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button41.Location = new System.Drawing.Point(19, 450);
            this.button41.Margin = new System.Windows.Forms.Padding(4);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(225, 46);
            this.button41.TabIndex = 53;
            this.button41.Text = "Read FPGA Sine";
            this.toolTip1.SetToolTip(this.button41, "Read back the sine table from the fpga");
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // button39
            // 
            this.button39.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button39.Location = new System.Drawing.Point(19, 398);
            this.button39.Margin = new System.Windows.Forms.Padding(4);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(225, 46);
            this.button39.TabIndex = 52;
            this.button39.Text = "Write FPGA CoSine";
            this.toolTip1.SetToolTip(this.button39, "write cosine table to fpga");
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // button38
            // 
            this.button38.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button38.Location = new System.Drawing.Point(19, 345);
            this.button38.Margin = new System.Windows.Forms.Padding(4);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(225, 46);
            this.button38.TabIndex = 51;
            this.button38.Text = "Write FPGA Sine";
            this.toolTip1.SetToolTip(this.button38, "Write Sine table to fpga");
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // button37
            // 
            this.button37.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button37.Location = new System.Drawing.Point(19, 292);
            this.button37.Margin = new System.Windows.Forms.Padding(4);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(225, 46);
            this.button37.TabIndex = 50;
            this.button37.Text = "Get FPGA Version";
            this.toolTip1.SetToolTip(this.button37, "Get the firmware revision. Due to the way the SPI driver works you will end up wi" +
        "th 1,0,0,0 as the version if the FPGA is powered down");
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(19, 256);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(283, 17);
            this.label2.TabIndex = 49;
            this.label2.Text = "Unmanaged Measurement Interface Control";
            // 
            // comboBoxManagedSw
            // 
            this.comboBoxManagedSw.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxManagedSw.FormattingEnabled = true;
            this.comboBoxManagedSw.Items.AddRange(new object[] {
            "AD",
            "BC",
            "AC",
            "BD",
            "Reading(AC)",
            "CAL 5K1",
            "TEST RC"});
            this.comboBoxManagedSw.Location = new System.Drawing.Point(252, 158);
            this.comboBoxManagedSw.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxManagedSw.Name = "comboBoxManagedSw";
            this.comboBoxManagedSw.Size = new System.Drawing.Size(263, 24);
            this.comboBoxManagedSw.TabIndex = 48;
            // 
            // comboBoxManagedFreq
            // 
            this.comboBoxManagedFreq.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxManagedFreq.FormattingEnabled = true;
            this.comboBoxManagedFreq.Items.AddRange(new object[] {
            "76 Hz",
            "153 Hz",
            "305 Hz",
            "610 Hz",
            "1200 Hz",
            "2400 Hz",
            "4900 Hz",
            "9800 Hz",
            "20 KHz",
            "39 KHz",
            "78 KHz",
            "156 KHz",
            "312 KHz",
            "625 KHz"});
            this.comboBoxManagedFreq.Location = new System.Drawing.Point(252, 210);
            this.comboBoxManagedFreq.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxManagedFreq.Name = "comboBoxManagedFreq";
            this.comboBoxManagedFreq.Size = new System.Drawing.Size(263, 24);
            this.comboBoxManagedFreq.TabIndex = 47;
            // 
            // button36
            // 
            this.button36.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button36.Location = new System.Drawing.Point(19, 202);
            this.button36.Margin = new System.Windows.Forms.Padding(4);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(225, 46);
            this.button36.TabIndex = 46;
            this.button36.Text = "Config Frequency";
            this.toolTip1.SetToolTip(this.button36, "Request the driver write and veryify the frequency and number of samples");
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // button35
            // 
            this.button35.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button35.Location = new System.Drawing.Point(19, 149);
            this.button35.Margin = new System.Windows.Forms.Padding(4);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(225, 46);
            this.button35.TabIndex = 45;
            this.button35.Text = "Config Matrix Switch";
            this.toolTip1.SetToolTip(this.button35, "Request the driver wwrite and verify the desired matrix path");
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // button34
            // 
            this.button34.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button34.Location = new System.Drawing.Point(19, 96);
            this.button34.Margin = new System.Windows.Forms.Padding(4);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(225, 46);
            this.button34.TabIndex = 44;
            this.button34.Text = "Take Measurement";
            this.toolTip1.SetToolTip(this.button34, "Request the driver run the measurement cycle and pull back the fpga data");
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // button33
            // 
            this.button33.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button33.Location = new System.Drawing.Point(252, 43);
            this.button33.Margin = new System.Windows.Forms.Padding(4);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(225, 46);
            this.button33.TabIndex = 43;
            this.button33.Text = "Disable Interface";
            this.toolTip1.SetToolTip(this.button33, "Request the driver disable and power down analogue systems");
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // button32
            // 
            this.button32.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button32.Location = new System.Drawing.Point(19, 43);
            this.button32.Margin = new System.Windows.Forms.Padding(4);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(225, 46);
            this.button32.TabIndex = 42;
            this.button32.Text = "Enable Interface";
            this.toolTip1.SetToolTip(this.button32, "Request the driver enable the FPGA and Analogue systems");
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // batteryAndChargingTabPage
            // 
            this.batteryAndChargingTabPage.Controls.Add(this.button56);
            this.batteryAndChargingTabPage.Controls.Add(this.button55);
            this.batteryAndChargingTabPage.Controls.Add(this.button31);
            this.batteryAndChargingTabPage.Controls.Add(this.button65);
            this.batteryAndChargingTabPage.Location = new System.Drawing.Point(4, 25);
            this.batteryAndChargingTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.batteryAndChargingTabPage.Name = "batteryAndChargingTabPage";
            this.batteryAndChargingTabPage.Size = new System.Drawing.Size(1089, 738);
            this.batteryAndChargingTabPage.TabIndex = 2;
            this.batteryAndChargingTabPage.Text = "Battery and Charging";
            this.batteryAndChargingTabPage.UseVisualStyleBackColor = true;
            // 
            // button56
            // 
            this.button56.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button56.Location = new System.Drawing.Point(16, 174);
            this.button56.Margin = new System.Windows.Forms.Padding(4);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(225, 46);
            this.button56.TabIndex = 18;
            this.button56.Text = "Enable Charger";
            this.toolTip1.SetToolTip(this.button56, "Don\'t disable the charger as it takes out the rail. It is enabled by default");
            this.button56.UseVisualStyleBackColor = true;
            this.button56.Click += new System.EventHandler(this.button56_Click);
            // 
            // button55
            // 
            this.button55.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button55.Location = new System.Drawing.Point(16, 121);
            this.button55.Margin = new System.Windows.Forms.Padding(4);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(225, 46);
            this.button55.TabIndex = 17;
            this.button55.Text = "Set Charge Level";
            this.toolTip1.SetToolTip(this.button55, "Should be at 500ma by default");
            this.button55.UseVisualStyleBackColor = true;
            this.button55.Click += new System.EventHandler(this.button55_Click);
            // 
            // button31
            // 
            this.button31.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button31.Location = new System.Drawing.Point(16, 68);
            this.button31.Margin = new System.Windows.Forms.Padding(4);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(225, 46);
            this.button31.TabIndex = 16;
            this.button31.Text = "Get Charger Status";
            this.toolTip1.SetToolTip(this.button31, "read and interpret the charger status pins");
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button65
            // 
            this.button65.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button65.Location = new System.Drawing.Point(16, 15);
            this.button65.Margin = new System.Windows.Forms.Padding(4);
            this.button65.Name = "button65";
            this.button65.Size = new System.Drawing.Size(225, 46);
            this.button65.TabIndex = 15;
            this.button65.Text = "Get ADC Voltages";
            this.toolTip1.SetToolTip(this.button65, resources.GetString("button65.ToolTip"));
            this.button65.UseVisualStyleBackColor = true;
            this.button65.Click += new System.EventHandler(this.button65_Click);
            // 
            // systemControlTabPage
            // 
            this.systemControlTabPage.Controls.Add(this.protectedDataComboBox);
            this.systemControlTabPage.Controls.Add(this.corruptChecksumButton);
            this.systemControlTabPage.Controls.Add(this.systemStateComboBox);
            this.systemControlTabPage.Controls.Add(this.setSystemStateButton);
            this.systemControlTabPage.Controls.Add(this.button122);
            this.systemControlTabPage.Controls.Add(this.selectScreenComboBox);
            this.systemControlTabPage.Controls.Add(this.button121);
            this.systemControlTabPage.Controls.Add(this.button120);
            this.systemControlTabPage.Controls.Add(this.button119);
            this.systemControlTabPage.Controls.Add(this.button118);
            this.systemControlTabPage.Controls.Add(this.TonePatternComboBox);
            this.systemControlTabPage.Controls.Add(this.button114);
            this.systemControlTabPage.Controls.Add(this.button67);
            this.systemControlTabPage.Controls.Add(this.button78);
            this.systemControlTabPage.Controls.Add(this.button73);
            this.systemControlTabPage.Controls.Add(this.LEDPatternComboBox);
            this.systemControlTabPage.Controls.Add(this.button72);
            this.systemControlTabPage.Controls.Add(this.referralSessionComboBox);
            this.systemControlTabPage.Controls.Add(this.button68);
            this.systemControlTabPage.Location = new System.Drawing.Point(4, 25);
            this.systemControlTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.systemControlTabPage.Name = "systemControlTabPage";
            this.systemControlTabPage.Size = new System.Drawing.Size(1089, 738);
            this.systemControlTabPage.TabIndex = 3;
            this.systemControlTabPage.Text = "System Control";
            this.systemControlTabPage.UseVisualStyleBackColor = true;
            // 
            // protectedDataComboBox
            // 
            this.protectedDataComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.protectedDataComboBox.FormattingEnabled = true;
            this.protectedDataComboBox.Items.AddRange(new object[] {
            "CURRENT POINT",
            "SCAN THRESHOLD",
            "SP THRESHOLD",
            "HG SEEN",
            "ZEDSCAN OPINION",
            "RESULT CALIB",
            "RESULT POINT_1",
            "RESULT POINT_2",
            "RESULT POINT_3",
            "RESULT POINT_4",
            "RESULT POINT_5     ",
            "RESULT POINT_6",
            "RESULT POINT_7",
            "RESULT POINT_8",
            "RESULT POINT_9",
            "RESULT POINT_10",
            "RESULT POINT_11",
            "RESULT POINT_12",
            "RESULT POINT_13",
            "RESULT POINT_14",
            "RESULT POINT_15",
            "RESULT POINT_16",
            "SESSION Operator ID",
            "SESSION PID",
            "SESSION RT ",
            "SESSION TRANING ",
            "PREVIOUS_STATE",
            "CURRENT_STATE"});
            this.protectedDataComboBox.Location = new System.Drawing.Point(874, 683);
            this.protectedDataComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.protectedDataComboBox.Name = "protectedDataComboBox";
            this.protectedDataComboBox.Size = new System.Drawing.Size(193, 24);
            this.protectedDataComboBox.TabIndex = 80;
            this.protectedDataComboBox.SelectedIndexChanged += new System.EventHandler(this.protectedDataComboBox_SelectedIndexChanged);
            // 
            // corruptChecksumButton
            // 
            this.corruptChecksumButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.corruptChecksumButton.Location = new System.Drawing.Point(641, 671);
            this.corruptChecksumButton.Margin = new System.Windows.Forms.Padding(4);
            this.corruptChecksumButton.Name = "corruptChecksumButton";
            this.corruptChecksumButton.Size = new System.Drawing.Size(225, 46);
            this.corruptChecksumButton.TabIndex = 79;
            this.corruptChecksumButton.Text = "Corrupt Protected data Checksum";
            this.corruptChecksumButton.UseVisualStyleBackColor = true;
            this.corruptChecksumButton.Click += new System.EventHandler(this.corruptChecksumButton_Click);
            // 
            // systemStateComboBox
            // 
            this.systemStateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.systemStateComboBox.FormattingEnabled = true;
            this.systemStateComboBox.Items.AddRange(new object[] {
            "INIT",
            "WARMUP",
            "ST",
            "PID",
            "RT",
            "WRT",
            "FS",
            "CAL",
            "CAL2",
            "SCAN",
            "SCAN2",
            "READ",
            "RESULT",
            "WRF",
            "FMQ",
            "HGQ",
            "HGQ2",
            "RA",
            "RBM",
            "RCM",
            "RDM",
            "REM",
            "STQ",
            "STN",
            "SPQ",
            "SP",
            "SP2",
            "SP_READ",
            "SP_RESULT",
            "RDS",
            "RDS2",
            "RS",
            "CTH",
            "PD",
            "WPD",
            "OFF"});
            this.systemStateComboBox.Location = new System.Drawing.Point(253, 142);
            this.systemStateComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.systemStateComboBox.Name = "systemStateComboBox";
            this.systemStateComboBox.Size = new System.Drawing.Size(193, 24);
            this.systemStateComboBox.TabIndex = 78;
            // 
            // setSystemStateButton
            // 
            this.setSystemStateButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.setSystemStateButton.Location = new System.Drawing.Point(20, 130);
            this.setSystemStateButton.Margin = new System.Windows.Forms.Padding(4);
            this.setSystemStateButton.Name = "setSystemStateButton";
            this.setSystemStateButton.Size = new System.Drawing.Size(225, 46);
            this.setSystemStateButton.TabIndex = 77;
            this.setSystemStateButton.Text = "Set System SM State";
            this.setSystemStateButton.UseVisualStyleBackColor = true;
            this.setSystemStateButton.Click += new System.EventHandler(this.setSystemStateButton_Click);
            // 
            // button122
            // 
            this.button122.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button122.Location = new System.Drawing.Point(20, 454);
            this.button122.Margin = new System.Windows.Forms.Padding(4);
            this.button122.Name = "button122";
            this.button122.Size = new System.Drawing.Size(225, 46);
            this.button122.TabIndex = 76;
            this.button122.Text = "Get Calibration Data";
            this.button122.UseVisualStyleBackColor = true;
            this.button122.Click += new System.EventHandler(this.button122_Click);
            // 
            // selectScreenComboBox
            // 
            this.selectScreenComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selectScreenComboBox.FormattingEnabled = true;
            this.selectScreenComboBox.Items.AddRange(new object[] {
            "ST",
            "PID",
            "RT",
            "WRT",
            "FS",
            "WFS",
            "CAL",
            "SCAN",
            "WRF",
            "FMQ",
            "HGQ",
            "RA",
            "RBM",
            "RCM",
            "RDM",
            "REM",
            "STQ",
            "STN",
            "SPQ",
            "SP",
            "RDS",
            "RS",
            "WRS",
            "CTH",
            "PD",
            "WPD",
            "WLB",
            "CLB",
            "ALM"});
            this.selectScreenComboBox.Location = new System.Drawing.Point(253, 520);
            this.selectScreenComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.selectScreenComboBox.Name = "selectScreenComboBox";
            this.selectScreenComboBox.Size = new System.Drawing.Size(193, 24);
            this.selectScreenComboBox.TabIndex = 75;
            // 
            // button121
            // 
            this.button121.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button121.Location = new System.Drawing.Point(20, 508);
            this.button121.Margin = new System.Windows.Forms.Padding(4);
            this.button121.Name = "button121";
            this.button121.Size = new System.Drawing.Size(225, 46);
            this.button121.TabIndex = 74;
            this.button121.Text = "Display System Screen";
            this.button121.UseVisualStyleBackColor = true;
            this.button121.Click += new System.EventHandler(this.button121_Click);
            // 
            // button120
            // 
            this.button120.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button120.Location = new System.Drawing.Point(20, 400);
            this.button120.Margin = new System.Windows.Forms.Padding(4);
            this.button120.Name = "button120";
            this.button120.Size = new System.Drawing.Size(225, 46);
            this.button120.TabIndex = 73;
            this.button120.Text = "Get Point Data";
            this.button120.UseVisualStyleBackColor = true;
            this.button120.Click += new System.EventHandler(this.button120_Click);
            // 
            // button119
            // 
            this.button119.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button119.Location = new System.Drawing.Point(20, 288);
            this.button119.Margin = new System.Windows.Forms.Padding(4);
            this.button119.Name = "button119";
            this.button119.Size = new System.Drawing.Size(225, 46);
            this.button119.TabIndex = 72;
            this.button119.Text = "Set LED Pattern";
            this.button119.UseVisualStyleBackColor = true;
            this.button119.Click += new System.EventHandler(this.button119_Click_1);
            // 
            // button118
            // 
            this.button118.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button118.Location = new System.Drawing.Point(20, 346);
            this.button118.Margin = new System.Windows.Forms.Padding(4);
            this.button118.Name = "button118";
            this.button118.Size = new System.Drawing.Size(225, 46);
            this.button118.TabIndex = 70;
            this.button118.Text = "Set Tone Pattern";
            this.button118.UseVisualStyleBackColor = true;
            this.button118.Click += new System.EventHandler(this.button118_Click_1);
            // 
            // TonePatternComboBox
            // 
            this.TonePatternComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TonePatternComboBox.FormattingEnabled = true;
            this.TonePatternComboBox.Items.AddRange(new object[] {
            "Stop",
            "Tone Good",
            "Tone Good 2",
            "Tone Warn",
            "Tone Warn 2",
            "Tone Alert",
            "Tone Key Click"});
            this.TonePatternComboBox.Location = new System.Drawing.Point(253, 357);
            this.TonePatternComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.TonePatternComboBox.Name = "TonePatternComboBox";
            this.TonePatternComboBox.Size = new System.Drawing.Size(193, 24);
            this.TonePatternComboBox.TabIndex = 71;
            // 
            // button114
            // 
            this.button114.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button114.Location = new System.Drawing.Point(20, 226);
            this.button114.Margin = new System.Windows.Forms.Padding(4);
            this.button114.Name = "button114";
            this.button114.Size = new System.Drawing.Size(225, 46);
            this.button114.TabIndex = 47;
            this.button114.Text = "Fetch LED State";
            this.button114.UseVisualStyleBackColor = true;
            this.button114.Click += new System.EventHandler(this.button114_Click);
            // 
            // button67
            // 
            this.button67.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button67.Location = new System.Drawing.Point(20, 76);
            this.button67.Margin = new System.Windows.Forms.Padding(4);
            this.button67.Name = "button67";
            this.button67.Size = new System.Drawing.Size(225, 46);
            this.button67.TabIndex = 46;
            this.button67.Text = "Fetch System SM State";
            this.button67.UseVisualStyleBackColor = true;
            this.button67.Click += new System.EventHandler(this.button67_Click);
            // 
            // button78
            // 
            this.button78.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button78.Location = new System.Drawing.Point(20, 563);
            this.button78.Margin = new System.Windows.Forms.Padding(4);
            this.button78.Name = "button78";
            this.button78.Size = new System.Drawing.Size(225, 46);
            this.button78.TabIndex = 45;
            this.button78.Text = "Test Watchdog Reset";
            this.toolTip1.SetToolTip(this.button78, resources.GetString("button78.ToolTip"));
            this.button78.UseVisualStyleBackColor = true;
            this.button78.Click += new System.EventHandler(this.button78_Click);
            // 
            // button73
            // 
            this.button73.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button73.Location = new System.Drawing.Point(20, 671);
            this.button73.Margin = new System.Windows.Forms.Padding(4);
            this.button73.Name = "button73";
            this.button73.Size = new System.Drawing.Size(225, 46);
            this.button73.TabIndex = 44;
            this.button73.Text = "Cause Critical Fail";
            this.toolTip1.SetToolTip(this.button73, resources.GetString("button73.ToolTip"));
            this.button73.UseVisualStyleBackColor = true;
            this.button73.Click += new System.EventHandler(this.button73_Click);
            // 
            // LEDPatternComboBox
            // 
            this.LEDPatternComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.LEDPatternComboBox.FormattingEnabled = true;
            this.LEDPatternComboBox.Items.AddRange(new object[] {
            "Off",
            "Solid Green",
            "Solid Orange",
            "Flashing Green",
            "Flashing Slow Green",
            "Flashing Orange",
            "Alternate Green Orange"});
            this.LEDPatternComboBox.Location = new System.Drawing.Point(253, 299);
            this.LEDPatternComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.LEDPatternComboBox.Name = "LEDPatternComboBox";
            this.LEDPatternComboBox.Size = new System.Drawing.Size(193, 24);
            this.LEDPatternComboBox.TabIndex = 69;
            // 
            // button72
            // 
            this.button72.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button72.Location = new System.Drawing.Point(20, 617);
            this.button72.Margin = new System.Windows.Forms.Padding(4);
            this.button72.Name = "button72";
            this.button72.Size = new System.Drawing.Size(225, 46);
            this.button72.TabIndex = 43;
            this.button72.Text = "Cause HardFault";
            this.toolTip1.SetToolTip(this.button72, resources.GetString("button72.ToolTip"));
            this.button72.UseVisualStyleBackColor = true;
            this.button72.Click += new System.EventHandler(this.button72_Click);
            // 
            // referralSessionComboBox
            // 
            this.referralSessionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.referralSessionComboBox.FormattingEnabled = true;
            this.referralSessionComboBox.Items.AddRange(new object[] {
            "MENU_SEL_BSCC_NONE",
            "MENU_SEL_BSCC_BORDER",
            "MENU_SEL_BSCC_BGLAN",
            "MENU_SEL_BSCC_LGRAD",
            "MENU_SEL_BSCC_Q_HGRAD",
            "MENU_SEL_BSCC_HGRAD",
            "MENU_SEL_BSCC_Q_INV",
            "MENU_SEL_BSCC_GNEO"});
            this.referralSessionComboBox.Location = new System.Drawing.Point(253, 32);
            this.referralSessionComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.referralSessionComboBox.Name = "referralSessionComboBox";
            this.referralSessionComboBox.Size = new System.Drawing.Size(355, 24);
            this.referralSessionComboBox.TabIndex = 42;
            // 
            // button68
            // 
            this.button68.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button68.Location = new System.Drawing.Point(20, 23);
            this.button68.Margin = new System.Windows.Forms.Padding(4);
            this.button68.Name = "button68";
            this.button68.Size = new System.Drawing.Size(225, 46);
            this.button68.TabIndex = 41;
            this.button68.Text = "Start Session";
            this.toolTip1.SetToolTip(this.button68, resources.GetString("button68.ToolTip"));
            this.button68.UseVisualStyleBackColor = true;
            this.button68.Click += new System.EventHandler(this.button68_Click);
            // 
            // externalFlashTabPage
            // 
            this.externalFlashTabPage.Controls.Add(this.groupBox4);
            this.externalFlashTabPage.Controls.Add(this.groupBox3);
            this.externalFlashTabPage.Location = new System.Drawing.Point(4, 25);
            this.externalFlashTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.externalFlashTabPage.Name = "externalFlashTabPage";
            this.externalFlashTabPage.Size = new System.Drawing.Size(1089, 738);
            this.externalFlashTabPage.TabIndex = 4;
            this.externalFlashTabPage.Text = "Flash Memory";
            this.externalFlashTabPage.UseVisualStyleBackColor = true;
            // 
            // button71
            // 
            this.button71.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button71.Location = new System.Drawing.Point(29, 250);
            this.button71.Margin = new System.Windows.Forms.Padding(4);
            this.button71.Name = "button71";
            this.button71.Size = new System.Drawing.Size(225, 46);
            this.button71.TabIndex = 21;
            this.button71.Text = "Destructive NV Test";
            this.toolTip1.SetToolTip(this.button71, resources.GetString("button71.ToolTip"));
            this.button71.UseVisualStyleBackColor = true;
            this.button71.Click += new System.EventHandler(this.button71_Click);
            // 
            // button70
            // 
            this.button70.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button70.Location = new System.Drawing.Point(29, 196);
            this.button70.Margin = new System.Windows.Forms.Padding(4);
            this.button70.Name = "button70";
            this.button70.Size = new System.Drawing.Size(225, 46);
            this.button70.TabIndex = 20;
            this.button70.Text = "Erase UID";
            this.toolTip1.SetToolTip(this.button70, resources.GetString("button70.ToolTip"));
            this.button70.UseVisualStyleBackColor = true;
            this.button70.Click += new System.EventHandler(this.button70_Click);
            // 
            // button69
            // 
            this.button69.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button69.Location = new System.Drawing.Point(29, 142);
            this.button69.Margin = new System.Windows.Forms.Padding(4);
            this.button69.Name = "button69";
            this.button69.Size = new System.Drawing.Size(225, 46);
            this.button69.TabIndex = 19;
            this.button69.Text = "Erase Flash Record";
            this.toolTip1.SetToolTip(this.button69, resources.GetString("button69.ToolTip"));
            this.button69.UseVisualStyleBackColor = true;
            this.button69.Click += new System.EventHandler(this.button69_Click);
            // 
            // button74
            // 
            this.button74.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button74.Location = new System.Drawing.Point(29, 88);
            this.button74.Margin = new System.Windows.Forms.Padding(4);
            this.button74.Name = "button74";
            this.button74.Size = new System.Drawing.Size(225, 46);
            this.button74.TabIndex = 18;
            this.button74.Text = "Erase Program Image";
            this.toolTip1.SetToolTip(this.button74, resources.GetString("button74.ToolTip"));
            this.button74.UseVisualStyleBackColor = true;
            this.button74.Click += new System.EventHandler(this.button74_Click);
            // 
            // button75
            // 
            this.button75.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button75.Location = new System.Drawing.Point(29, 34);
            this.button75.Margin = new System.Windows.Forms.Padding(4);
            this.button75.Name = "button75";
            this.button75.Size = new System.Drawing.Size(225, 46);
            this.button75.TabIndex = 17;
            this.button75.Text = "Check Extern Flash";
            this.toolTip1.SetToolTip(this.button75, resources.GetString("button75.ToolTip"));
            this.button75.UseVisualStyleBackColor = true;
            this.button75.Click += new System.EventHandler(this.button75_Click);
            // 
            // auditInfoTabPage
            // 
            this.auditInfoTabPage.Controls.Add(this.button77);
            this.auditInfoTabPage.Controls.Add(this.button76);
            this.auditInfoTabPage.Controls.Add(this.button115);
            this.auditInfoTabPage.Controls.Add(this.UIDtextBox);
            this.auditInfoTabPage.Controls.Add(this.button116);
            this.auditInfoTabPage.Location = new System.Drawing.Point(4, 25);
            this.auditInfoTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.auditInfoTabPage.Name = "auditInfoTabPage";
            this.auditInfoTabPage.Size = new System.Drawing.Size(1089, 738);
            this.auditInfoTabPage.TabIndex = 5;
            this.auditInfoTabPage.Text = "Audit Info";
            this.auditInfoTabPage.UseVisualStyleBackColor = true;
            // 
            // button77
            // 
            this.button77.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button77.Location = new System.Drawing.Point(21, 206);
            this.button77.Margin = new System.Windows.Forms.Padding(4);
            this.button77.Name = "button77";
            this.button77.Size = new System.Drawing.Size(225, 46);
            this.button77.TabIndex = 29;
            this.button77.Text = "Clear Life Counters";
            this.toolTip1.SetToolTip(this.button77, resources.GetString("button77.ToolTip"));
            this.button77.UseVisualStyleBackColor = true;
            // 
            // button76
            // 
            this.button76.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button76.Location = new System.Drawing.Point(21, 142);
            this.button76.Margin = new System.Windows.Forms.Padding(4);
            this.button76.Name = "button76";
            this.button76.Size = new System.Drawing.Size(225, 46);
            this.button76.TabIndex = 28;
            this.button76.Text = "Get Life Counters";
            this.toolTip1.SetToolTip(this.button76, resources.GetString("button76.ToolTip"));
            this.button76.UseVisualStyleBackColor = true;
            this.button76.Click += new System.EventHandler(this.button76_Click_1);
            // 
            // button115
            // 
            this.button115.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button115.Location = new System.Drawing.Point(21, 78);
            this.button115.Margin = new System.Windows.Forms.Padding(4);
            this.button115.Name = "button115";
            this.button115.Size = new System.Drawing.Size(225, 46);
            this.button115.TabIndex = 27;
            this.button115.Text = "Get Device ID";
            this.toolTip1.SetToolTip(this.button115, resources.GetString("button115.ToolTip"));
            this.button115.UseVisualStyleBackColor = true;
            this.button115.Click += new System.EventHandler(this.button115_Click);
            // 
            // UIDtextBox
            // 
            this.UIDtextBox.Location = new System.Drawing.Point(263, 28);
            this.UIDtextBox.Margin = new System.Windows.Forms.Padding(4);
            this.UIDtextBox.Name = "UIDtextBox";
            this.UIDtextBox.Size = new System.Drawing.Size(348, 22);
            this.UIDtextBox.TabIndex = 26;
            this.UIDtextBox.Text = "ZS2-HS-";
            // 
            // button116
            // 
            this.button116.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button116.Location = new System.Drawing.Point(21, 20);
            this.button116.Margin = new System.Windows.Forms.Padding(4);
            this.button116.Name = "button116";
            this.button116.Size = new System.Drawing.Size(225, 46);
            this.button116.TabIndex = 25;
            this.button116.Text = "Set Device ID";
            this.toolTip1.SetToolTip(this.button116, resources.GetString("button116.ToolTip"));
            this.button116.UseVisualStyleBackColor = true;
            this.button116.Click += new System.EventHandler(this.button116_Click);
            // 
            // iOControlTabPage
            // 
            this.iOControlTabPage.Controls.Add(this.backlightComboBox);
            this.iOControlTabPage.Controls.Add(this.button81);
            this.iOControlTabPage.Controls.Add(this.button80);
            this.iOControlTabPage.Controls.Add(this.button79);
            this.iOControlTabPage.Controls.Add(this.button63);
            this.iOControlTabPage.Controls.Add(this.label16);
            this.iOControlTabPage.Controls.Add(this.textBox2);
            this.iOControlTabPage.Controls.Add(this.button29);
            this.iOControlTabPage.Controls.Add(this.button28);
            this.iOControlTabPage.Controls.Add(this.button27);
            this.iOControlTabPage.Controls.Add(this.button26);
            this.iOControlTabPage.Controls.Add(this.button25);
            this.iOControlTabPage.Controls.Add(this.button24);
            this.iOControlTabPage.Controls.Add(this.button23);
            this.iOControlTabPage.Controls.Add(this.button15);
            this.iOControlTabPage.Controls.Add(this.button12);
            this.iOControlTabPage.Location = new System.Drawing.Point(4, 25);
            this.iOControlTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.iOControlTabPage.Name = "iOControlTabPage";
            this.iOControlTabPage.Padding = new System.Windows.Forms.Padding(4);
            this.iOControlTabPage.Size = new System.Drawing.Size(1089, 738);
            this.iOControlTabPage.TabIndex = 1;
            this.iOControlTabPage.Text = "IO Control";
            this.iOControlTabPage.UseVisualStyleBackColor = true;
            this.iOControlTabPage.Click += new System.EventHandler(this.iOControlTabPage_Click);
            // 
            // backlightComboBox
            // 
            this.backlightComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.backlightComboBox.FormattingEnabled = true;
            this.backlightComboBox.Items.AddRange(new object[] {
            "0%",
            "10%",
            "20%",
            "30%",
            "40%",
            "50%",
            "60%",
            "70%",
            "80%",
            "90%",
            "100%"});
            this.backlightComboBox.Location = new System.Drawing.Point(243, 651);
            this.backlightComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.backlightComboBox.Name = "backlightComboBox";
            this.backlightComboBox.Size = new System.Drawing.Size(127, 24);
            this.backlightComboBox.TabIndex = 67;
            // 
            // button81
            // 
            this.button81.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button81.Location = new System.Drawing.Point(4, 642);
            this.button81.Margin = new System.Windows.Forms.Padding(4);
            this.button81.Name = "button81";
            this.button81.Size = new System.Drawing.Size(225, 46);
            this.button81.TabIndex = 66;
            this.button81.Text = "Set Backlight Power";
            this.button81.UseVisualStyleBackColor = true;
            this.button81.Click += new System.EventHandler(this.button81_Click);
            // 
            // button80
            // 
            this.button80.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button80.Location = new System.Drawing.Point(4, 590);
            this.button80.Margin = new System.Windows.Forms.Padding(4);
            this.button80.Name = "button80";
            this.button80.Size = new System.Drawing.Size(225, 46);
            this.button80.TabIndex = 65;
            this.button80.Text = "Set UART Buffer OE";
            this.button80.UseVisualStyleBackColor = true;
            this.button80.Click += new System.EventHandler(this.button80_Click);
            // 
            // button79
            // 
            this.button79.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button79.Location = new System.Drawing.Point(4, 537);
            this.button79.Margin = new System.Windows.Forms.Padding(4);
            this.button79.Name = "button79";
            this.button79.Size = new System.Drawing.Size(225, 46);
            this.button79.TabIndex = 64;
            this.button79.Text = "Set UART Buffer Pwr";
            this.button79.UseVisualStyleBackColor = true;
            this.button79.Click += new System.EventHandler(this.button79_Click);
            // 
            // button63
            // 
            this.button63.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button63.Location = new System.Drawing.Point(4, 484);
            this.button63.Margin = new System.Windows.Forms.Padding(4);
            this.button63.Name = "button63";
            this.button63.Size = new System.Drawing.Size(225, 46);
            this.button63.TabIndex = 63;
            this.button63.Text = "Reset Device";
            this.button63.UseVisualStyleBackColor = true;
            this.button63.Click += new System.EventHandler(this.button63_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label16.Location = new System.Drawing.Point(379, 442);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(165, 17);
            this.label16.TabIndex = 62;
            this.label16.Text = "0 = off, 20 - 20000Hz On";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(237, 439);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(132, 22);
            this.textBox2.TabIndex = 61;
            this.textBox2.Text = "0";
            // 
            // button29
            // 
            this.button29.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button29.Location = new System.Drawing.Point(4, 431);
            this.button29.Margin = new System.Windows.Forms.Padding(4);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(225, 46);
            this.button29.TabIndex = 60;
            this.button29.Text = "Set Speaker Frequency";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // button28
            // 
            this.button28.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button28.Location = new System.Drawing.Point(4, 272);
            this.button28.Margin = new System.Windows.Forms.Padding(4);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(225, 46);
            this.button28.TabIndex = 59;
            this.button28.Text = "Draw LCD Test Screen";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // button27
            // 
            this.button27.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button27.Location = new System.Drawing.Point(4, 378);
            this.button27.Margin = new System.Windows.Forms.Padding(4);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(225, 46);
            this.button27.TabIndex = 58;
            this.button27.Text = "Set LCD Dim State";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // button26
            // 
            this.button26.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button26.Location = new System.Drawing.Point(4, 325);
            this.button26.Margin = new System.Windows.Forms.Padding(4);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(225, 46);
            this.button26.TabIndex = 57;
            this.button26.Text = "Set LCD Backlight State";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // button25
            // 
            this.button25.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button25.Location = new System.Drawing.Point(4, 219);
            this.button25.Margin = new System.Windows.Forms.Padding(4);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(225, 46);
            this.button25.TabIndex = 56;
            this.button25.Text = "Set LCD On/Off";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button24
            // 
            this.button24.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button24.Location = new System.Drawing.Point(4, 166);
            this.button24.Margin = new System.Windows.Forms.Padding(4);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(225, 46);
            this.button24.TabIndex = 55;
            this.button24.Text = "Read Snout Switch";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button23
            // 
            this.button23.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button23.Location = new System.Drawing.Point(4, 113);
            this.button23.Margin = new System.Windows.Forms.Padding(4);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(225, 46);
            this.button23.TabIndex = 54;
            this.button23.Text = "Set Green LED";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button15
            // 
            this.button15.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button15.Location = new System.Drawing.Point(4, 60);
            this.button15.Margin = new System.Windows.Forms.Padding(4);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(225, 46);
            this.button15.TabIndex = 53;
            this.button15.Text = "Set Red LED";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button12
            // 
            this.button12.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button12.Location = new System.Drawing.Point(4, 7);
            this.button12.Margin = new System.Windows.Forms.Padding(4);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(225, 46);
            this.button12.TabIndex = 52;
            this.button12.Text = "Read Keyboard";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // scriptRunnerTabPage
            // 
            this.scriptRunnerTabPage.Controls.Add(this.runScriptSetButton);
            this.scriptRunnerTabPage.Controls.Add(this.loadSetButton);
            this.scriptRunnerTabPage.Controls.Add(this.label11);
            this.scriptRunnerTabPage.Controls.Add(this.label1);
            this.scriptRunnerTabPage.Controls.Add(this.resultSetGridView);
            this.scriptRunnerTabPage.Controls.Add(this.scriptSetGridView);
            this.scriptRunnerTabPage.Location = new System.Drawing.Point(4, 25);
            this.scriptRunnerTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.scriptRunnerTabPage.Name = "scriptRunnerTabPage";
            this.scriptRunnerTabPage.Size = new System.Drawing.Size(1653, 777);
            this.scriptRunnerTabPage.TabIndex = 2;
            this.scriptRunnerTabPage.Text = "Script Runner";
            this.scriptRunnerTabPage.UseVisualStyleBackColor = true;
            // 
            // runScriptSetButton
            // 
            this.runScriptSetButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.runScriptSetButton.Location = new System.Drawing.Point(199, 703);
            this.runScriptSetButton.Margin = new System.Windows.Forms.Padding(4);
            this.runScriptSetButton.Name = "runScriptSetButton";
            this.runScriptSetButton.Size = new System.Drawing.Size(165, 57);
            this.runScriptSetButton.TabIndex = 12;
            this.runScriptSetButton.Text = "Run Test";
            this.runScriptSetButton.UseVisualStyleBackColor = true;
            this.runScriptSetButton.Click += new System.EventHandler(this.runScriptSetButton_Click);
            // 
            // loadSetButton
            // 
            this.loadSetButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.loadSetButton.Location = new System.Drawing.Point(21, 703);
            this.loadSetButton.Margin = new System.Windows.Forms.Padding(4);
            this.loadSetButton.Name = "loadSetButton";
            this.loadSetButton.Size = new System.Drawing.Size(169, 57);
            this.loadSetButton.TabIndex = 11;
            this.loadSetButton.Text = "Load Test Script Set";
            this.loadSetButton.UseVisualStyleBackColor = true;
            this.loadSetButton.Click += new System.EventHandler(this.loadSetButton_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(625, 9);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 17);
            this.label11.TabIndex = 10;
            this.label11.Text = "Result";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(17, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Script Set";
            // 
            // resultSetGridView
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.resultSetGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.resultSetGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultSetGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.resultSetGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.resultSetGridView.Location = new System.Drawing.Point(629, 33);
            this.resultSetGridView.Margin = new System.Windows.Forms.Padding(4);
            this.resultSetGridView.Name = "resultSetGridView";
            this.resultSetGridView.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.resultSetGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.resultSetGridView.RowHeadersWidth = 51;
            this.resultSetGridView.RowTemplate.Height = 24;
            this.resultSetGridView.Size = new System.Drawing.Size(996, 662);
            this.resultSetGridView.TabIndex = 8;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Result";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 1000;
            // 
            // scriptSetGridView
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.scriptSetGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.scriptSetGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.scriptSetGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ScriptFile});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.scriptSetGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.scriptSetGridView.Location = new System.Drawing.Point(21, 33);
            this.scriptSetGridView.Margin = new System.Windows.Forms.Padding(4);
            this.scriptSetGridView.Name = "scriptSetGridView";
            this.scriptSetGridView.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.scriptSetGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.scriptSetGridView.RowHeadersWidth = 51;
            this.scriptSetGridView.RowTemplate.Height = 24;
            this.scriptSetGridView.Size = new System.Drawing.Size(583, 662);
            this.scriptSetGridView.TabIndex = 7;
            // 
            // ScriptFile
            // 
            this.ScriptFile.HeaderText = "Script File";
            this.ScriptFile.MinimumWidth = 6;
            this.ScriptFile.Name = "ScriptFile";
            this.ScriptFile.ReadOnly = true;
            this.ScriptFile.Width = 450;
            // 
            // scriptCreatorTabPage
            // 
            this.scriptCreatorTabPage.Controls.Add(this.button113);
            this.scriptCreatorTabPage.Controls.Add(this.commandDataGridView);
            this.scriptCreatorTabPage.Controls.Add(this.button112);
            this.scriptCreatorTabPage.Controls.Add(this.button18);
            this.scriptCreatorTabPage.Controls.Add(this.button111);
            this.scriptCreatorTabPage.Controls.Add(this.button10);
            this.scriptCreatorTabPage.Controls.Add(this.button110);
            this.scriptCreatorTabPage.Controls.Add(this.button9);
            this.scriptCreatorTabPage.Controls.Add(this.button109);
            this.scriptCreatorTabPage.Controls.Add(this.button11);
            this.scriptCreatorTabPage.Controls.Add(this.button108);
            this.scriptCreatorTabPage.Controls.Add(this.button13);
            this.scriptCreatorTabPage.Controls.Add(this.button107);
            this.scriptCreatorTabPage.Controls.Add(this.undockButton);
            this.scriptCreatorTabPage.Controls.Add(this.button106);
            this.scriptCreatorTabPage.Controls.Add(this.button17);
            this.scriptCreatorTabPage.Controls.Add(this.button105);
            this.scriptCreatorTabPage.Controls.Add(this.grabScreenButton);
            this.scriptCreatorTabPage.Controls.Add(this.button104);
            this.scriptCreatorTabPage.Controls.Add(this.screenPictureBox);
            this.scriptCreatorTabPage.Controls.Add(this.button103);
            this.scriptCreatorTabPage.Controls.Add(this.screenCopyPictureBox);
            this.scriptCreatorTabPage.Controls.Add(this.button102);
            this.scriptCreatorTabPage.Controls.Add(this.label5);
            this.scriptCreatorTabPage.Controls.Add(this.button101);
            this.scriptCreatorTabPage.Controls.Add(this.label6);
            this.scriptCreatorTabPage.Controls.Add(this.button100);
            this.scriptCreatorTabPage.Controls.Add(this.sessionButton);
            this.scriptCreatorTabPage.Controls.Add(this.button99);
            this.scriptCreatorTabPage.Controls.Add(this.runScriptButton);
            this.scriptCreatorTabPage.Controls.Add(this.button98);
            this.scriptCreatorTabPage.Controls.Add(this.dockButton);
            this.scriptCreatorTabPage.Controls.Add(this.button97);
            this.scriptCreatorTabPage.Controls.Add(this.button14);
            this.scriptCreatorTabPage.Controls.Add(this.button96);
            this.scriptCreatorTabPage.Controls.Add(this.loadScriptButton);
            this.scriptCreatorTabPage.Controls.Add(this.button95);
            this.scriptCreatorTabPage.Controls.Add(this.button16);
            this.scriptCreatorTabPage.Controls.Add(this.button94);
            this.scriptCreatorTabPage.Controls.Add(this.compareScreenButton);
            this.scriptCreatorTabPage.Controls.Add(this.button93);
            this.scriptCreatorTabPage.Controls.Add(this.failBoundaryButton);
            this.scriptCreatorTabPage.Controls.Add(this.button92);
            this.scriptCreatorTabPage.Controls.Add(this.readingButton);
            this.scriptCreatorTabPage.Controls.Add(this.button91);
            this.scriptCreatorTabPage.Controls.Add(this.statemachineButton);
            this.scriptCreatorTabPage.Controls.Add(this.button90);
            this.scriptCreatorTabPage.Controls.Add(this.passBoundaryButton);
            this.scriptCreatorTabPage.Controls.Add(this.button89);
            this.scriptCreatorTabPage.Controls.Add(this.userDelayButton);
            this.scriptCreatorTabPage.Controls.Add(this.button88);
            this.scriptCreatorTabPage.Controls.Add(this.batteryButton);
            this.scriptCreatorTabPage.Controls.Add(this.button87);
            this.scriptCreatorTabPage.Controls.Add(this.button8);
            this.scriptCreatorTabPage.Controls.Add(this.button86);
            this.scriptCreatorTabPage.Controls.Add(this.checkLEDStateButton);
            this.scriptCreatorTabPage.Controls.Add(this.button85);
            this.scriptCreatorTabPage.Controls.Add(this.getLEDStateButton);
            this.scriptCreatorTabPage.Controls.Add(this.button84);
            this.scriptCreatorTabPage.Controls.Add(this.fullStatusCheckButton);
            this.scriptCreatorTabPage.Controls.Add(this.button83);
            this.scriptCreatorTabPage.Controls.Add(this.cytologyButton);
            this.scriptCreatorTabPage.Controls.Add(this.button82);
            this.scriptCreatorTabPage.Controls.Add(this.setSessionTimeButton);
            this.scriptCreatorTabPage.Controls.Add(this.button66);
            this.scriptCreatorTabPage.Controls.Add(this.button19);
            this.scriptCreatorTabPage.Controls.Add(this.button64);
            this.scriptCreatorTabPage.Controls.Add(this.button20);
            this.scriptCreatorTabPage.Controls.Add(this.convertButton);
            this.scriptCreatorTabPage.Controls.Add(this.button21);
            this.scriptCreatorTabPage.Controls.Add(this.newScriptButton);
            this.scriptCreatorTabPage.Controls.Add(this.button22);
            this.scriptCreatorTabPage.Location = new System.Drawing.Point(4, 25);
            this.scriptCreatorTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.scriptCreatorTabPage.Name = "scriptCreatorTabPage";
            this.scriptCreatorTabPage.Size = new System.Drawing.Size(1653, 777);
            this.scriptCreatorTabPage.TabIndex = 3;
            this.scriptCreatorTabPage.Text = "Script Creator";
            this.scriptCreatorTabPage.UseVisualStyleBackColor = true;
            // 
            // button113
            // 
            this.button113.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button113.Location = new System.Drawing.Point(1512, 689);
            this.button113.Margin = new System.Windows.Forms.Padding(4);
            this.button113.Name = "button113";
            this.button113.Size = new System.Drawing.Size(116, 63);
            this.button113.TabIndex = 159;
            this.button113.UseVisualStyleBackColor = true;
            // 
            // commandDataGridView
            // 
            this.commandDataGridView.AllowUserToAddRows = false;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.commandDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.commandDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.commandDataGridView.DefaultCellStyle = dataGridViewCellStyle8;
            this.commandDataGridView.Location = new System.Drawing.Point(672, 37);
            this.commandDataGridView.Margin = new System.Windows.Forms.Padding(4);
            this.commandDataGridView.Name = "commandDataGridView";
            this.commandDataGridView.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.commandDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.commandDataGridView.RowHeadersWidth = 51;
            this.commandDataGridView.RowTemplate.Height = 24;
            this.commandDataGridView.Size = new System.Drawing.Size(951, 247);
            this.commandDataGridView.TabIndex = 106;
            // 
            // button112
            // 
            this.button112.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button112.Location = new System.Drawing.Point(1512, 619);
            this.button112.Margin = new System.Windows.Forms.Padding(4);
            this.button112.Name = "button112";
            this.button112.Size = new System.Drawing.Size(116, 63);
            this.button112.TabIndex = 158;
            this.button112.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button18.Location = new System.Drawing.Point(188, 244);
            this.button18.Margin = new System.Windows.Forms.Padding(4);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(73, 63);
            this.button18.TabIndex = 88;
            this.button18.Text = "3";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button111
            // 
            this.button111.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button111.Location = new System.Drawing.Point(1512, 549);
            this.button111.Margin = new System.Windows.Forms.Padding(4);
            this.button111.Name = "button111";
            this.button111.Size = new System.Drawing.Size(116, 63);
            this.button111.TabIndex = 157;
            this.button111.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button10.Location = new System.Drawing.Point(95, 314);
            this.button10.Margin = new System.Windows.Forms.Padding(4);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(85, 63);
            this.button10.TabIndex = 89;
            this.button10.Text = "Scan";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button110
            // 
            this.button110.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button110.Location = new System.Drawing.Point(1512, 479);
            this.button110.Margin = new System.Windows.Forms.Padding(4);
            this.button110.Name = "button110";
            this.button110.Size = new System.Drawing.Size(116, 63);
            this.button110.TabIndex = 156;
            this.button110.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button9.Location = new System.Drawing.Point(829, 309);
            this.button9.Margin = new System.Windows.Forms.Padding(4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(149, 33);
            this.button9.TabIndex = 90;
            this.button9.Text = "Reset Target";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button109
            // 
            this.button109.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button109.Location = new System.Drawing.Point(1388, 689);
            this.button109.Margin = new System.Windows.Forms.Padding(4);
            this.button109.Name = "button109";
            this.button109.Size = new System.Drawing.Size(116, 63);
            this.button109.TabIndex = 155;
            this.button109.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button11.Location = new System.Drawing.Point(21, 244);
            this.button11.Margin = new System.Windows.Forms.Padding(4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(65, 63);
            this.button11.TabIndex = 92;
            this.button11.Text = "1";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button108
            // 
            this.button108.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button108.Location = new System.Drawing.Point(1264, 689);
            this.button108.Margin = new System.Windows.Forms.Padding(4);
            this.button108.Name = "button108";
            this.button108.Size = new System.Drawing.Size(116, 63);
            this.button108.TabIndex = 154;
            this.button108.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button13.Location = new System.Drawing.Point(95, 244);
            this.button13.Margin = new System.Windows.Forms.Padding(4);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(85, 63);
            this.button13.TabIndex = 93;
            this.button13.Text = "2";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button107
            // 
            this.button107.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button107.Location = new System.Drawing.Point(1140, 688);
            this.button107.Margin = new System.Windows.Forms.Padding(4);
            this.button107.Name = "button107";
            this.button107.Size = new System.Drawing.Size(116, 63);
            this.button107.TabIndex = 153;
            this.button107.UseVisualStyleBackColor = true;
            // 
            // undockButton
            // 
            this.undockButton.BackColor = System.Drawing.Color.PaleGreen;
            this.undockButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.undockButton.Location = new System.Drawing.Point(24, 549);
            this.undockButton.Margin = new System.Windows.Forms.Padding(4);
            this.undockButton.Name = "undockButton";
            this.undockButton.Size = new System.Drawing.Size(116, 63);
            this.undockButton.TabIndex = 94;
            this.undockButton.Text = "Undock";
            this.undockButton.UseVisualStyleBackColor = false;
            this.undockButton.Click += new System.EventHandler(this.undockButton_Click);
            // 
            // button106
            // 
            this.button106.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button106.Location = new System.Drawing.Point(1016, 689);
            this.button106.Margin = new System.Windows.Forms.Padding(4);
            this.button106.Name = "button106";
            this.button106.Size = new System.Drawing.Size(116, 63);
            this.button106.TabIndex = 152;
            this.button106.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.PaleGreen;
            this.button17.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button17.Location = new System.Drawing.Point(148, 479);
            this.button17.Margin = new System.Windows.Forms.Padding(4);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(116, 63);
            this.button17.TabIndex = 95;
            this.button17.Text = "Sensor On";
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button105
            // 
            this.button105.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button105.Location = new System.Drawing.Point(892, 689);
            this.button105.Margin = new System.Windows.Forms.Padding(4);
            this.button105.Name = "button105";
            this.button105.Size = new System.Drawing.Size(116, 63);
            this.button105.TabIndex = 151;
            this.button105.UseVisualStyleBackColor = true;
            // 
            // grabScreenButton
            // 
            this.grabScreenButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.grabScreenButton.Location = new System.Drawing.Point(1296, 311);
            this.grabScreenButton.Margin = new System.Windows.Forms.Padding(4);
            this.grabScreenButton.Name = "grabScreenButton";
            this.grabScreenButton.Size = new System.Drawing.Size(149, 71);
            this.grabScreenButton.TabIndex = 96;
            this.grabScreenButton.Text = "Grab Screen For Compare";
            this.grabScreenButton.UseVisualStyleBackColor = true;
            this.grabScreenButton.Click += new System.EventHandler(this.grabScreenButton_Click);
            // 
            // button104
            // 
            this.button104.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button104.Location = new System.Drawing.Point(768, 689);
            this.button104.Margin = new System.Windows.Forms.Padding(4);
            this.button104.Name = "button104";
            this.button104.Size = new System.Drawing.Size(116, 63);
            this.button104.TabIndex = 150;
            this.button104.UseVisualStyleBackColor = true;
            // 
            // screenPictureBox
            // 
            this.screenPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.screenPictureBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.screenPictureBox.Location = new System.Drawing.Point(56, 39);
            this.screenPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.screenPictureBox.MaximumSize = new System.Drawing.Size(169, 196);
            this.screenPictureBox.MinimumSize = new System.Drawing.Size(169, 196);
            this.screenPictureBox.Name = "screenPictureBox";
            this.screenPictureBox.Size = new System.Drawing.Size(169, 196);
            this.screenPictureBox.TabIndex = 97;
            this.screenPictureBox.TabStop = false;
            // 
            // button103
            // 
            this.button103.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button103.Location = new System.Drawing.Point(644, 689);
            this.button103.Margin = new System.Windows.Forms.Padding(4);
            this.button103.Name = "button103";
            this.button103.Size = new System.Drawing.Size(116, 63);
            this.button103.TabIndex = 149;
            this.button103.UseVisualStyleBackColor = true;
            // 
            // screenCopyPictureBox
            // 
            this.screenCopyPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.screenCopyPictureBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.screenCopyPictureBox.Location = new System.Drawing.Point(305, 39);
            this.screenCopyPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.screenCopyPictureBox.MaximumSize = new System.Drawing.Size(169, 196);
            this.screenCopyPictureBox.MinimumSize = new System.Drawing.Size(169, 196);
            this.screenCopyPictureBox.Name = "screenCopyPictureBox";
            this.screenCopyPictureBox.Size = new System.Drawing.Size(169, 196);
            this.screenCopyPictureBox.TabIndex = 98;
            this.screenCopyPictureBox.TabStop = false;
            // 
            // button102
            // 
            this.button102.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button102.Location = new System.Drawing.Point(1388, 619);
            this.button102.Margin = new System.Windows.Forms.Padding(4);
            this.button102.Name = "button102";
            this.button102.Size = new System.Drawing.Size(116, 63);
            this.button102.TabIndex = 148;
            this.button102.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(91, 12);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 17);
            this.label5.TabIndex = 99;
            this.label5.Text = "Current Screen";
            // 
            // button101
            // 
            this.button101.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button101.Location = new System.Drawing.Point(1264, 619);
            this.button101.Margin = new System.Windows.Forms.Padding(4);
            this.button101.Name = "button101";
            this.button101.Size = new System.Drawing.Size(116, 63);
            this.button101.TabIndex = 147;
            this.button101.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(331, 12);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 17);
            this.label6.TabIndex = 100;
            this.label6.Text = "Comparison Copy";
            // 
            // button100
            // 
            this.button100.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button100.Location = new System.Drawing.Point(1140, 619);
            this.button100.Margin = new System.Windows.Forms.Padding(4);
            this.button100.Name = "button100";
            this.button100.Size = new System.Drawing.Size(116, 63);
            this.button100.TabIndex = 146;
            this.button100.UseVisualStyleBackColor = true;
            // 
            // sessionButton
            // 
            this.sessionButton.BackColor = System.Drawing.Color.PaleGreen;
            this.sessionButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.sessionButton.Location = new System.Drawing.Point(396, 479);
            this.sessionButton.Margin = new System.Windows.Forms.Padding(4);
            this.sessionButton.Name = "sessionButton";
            this.sessionButton.Size = new System.Drawing.Size(116, 63);
            this.sessionButton.TabIndex = 101;
            this.sessionButton.Text = "Start Session";
            this.sessionButton.UseVisualStyleBackColor = false;
            this.sessionButton.Click += new System.EventHandler(this.sessionButton_Click);
            // 
            // button99
            // 
            this.button99.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button99.Location = new System.Drawing.Point(1016, 619);
            this.button99.Margin = new System.Windows.Forms.Padding(4);
            this.button99.Name = "button99";
            this.button99.Size = new System.Drawing.Size(116, 63);
            this.button99.TabIndex = 145;
            this.button99.UseVisualStyleBackColor = true;
            // 
            // runScriptButton
            // 
            this.runScriptButton.BackColor = System.Drawing.Color.Lime;
            this.runScriptButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.runScriptButton.Location = new System.Drawing.Point(987, 309);
            this.runScriptButton.Margin = new System.Windows.Forms.Padding(4);
            this.runScriptButton.Name = "runScriptButton";
            this.runScriptButton.Size = new System.Drawing.Size(148, 33);
            this.runScriptButton.TabIndex = 91;
            this.runScriptButton.Text = "Run Script";
            this.runScriptButton.UseVisualStyleBackColor = false;
            this.runScriptButton.Click += new System.EventHandler(this.runScriptButton_Click);
            // 
            // button98
            // 
            this.button98.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button98.Location = new System.Drawing.Point(892, 619);
            this.button98.Margin = new System.Windows.Forms.Padding(4);
            this.button98.Name = "button98";
            this.button98.Size = new System.Drawing.Size(116, 63);
            this.button98.TabIndex = 144;
            this.button98.UseVisualStyleBackColor = true;
            // 
            // dockButton
            // 
            this.dockButton.BackColor = System.Drawing.Color.LightGreen;
            this.dockButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dockButton.Location = new System.Drawing.Point(24, 479);
            this.dockButton.Margin = new System.Windows.Forms.Padding(4);
            this.dockButton.Name = "dockButton";
            this.dockButton.Size = new System.Drawing.Size(116, 63);
            this.dockButton.TabIndex = 102;
            this.dockButton.Text = "Dock";
            this.dockButton.UseVisualStyleBackColor = false;
            this.dockButton.Click += new System.EventHandler(this.dockButton_Click);
            // 
            // button97
            // 
            this.button97.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button97.Location = new System.Drawing.Point(768, 619);
            this.button97.Margin = new System.Windows.Forms.Padding(4);
            this.button97.Name = "button97";
            this.button97.Size = new System.Drawing.Size(116, 63);
            this.button97.TabIndex = 143;
            this.button97.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.PaleGreen;
            this.button14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button14.Location = new System.Drawing.Point(148, 549);
            this.button14.Margin = new System.Windows.Forms.Padding(4);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(116, 63);
            this.button14.TabIndex = 103;
            this.button14.Text = "Sensor Off";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button96
            // 
            this.button96.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button96.Location = new System.Drawing.Point(644, 619);
            this.button96.Margin = new System.Windows.Forms.Padding(4);
            this.button96.Name = "button96";
            this.button96.Size = new System.Drawing.Size(116, 63);
            this.button96.TabIndex = 142;
            this.button96.UseVisualStyleBackColor = true;
            // 
            // loadScriptButton
            // 
            this.loadScriptButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.loadScriptButton.Location = new System.Drawing.Point(1139, 348);
            this.loadScriptButton.Margin = new System.Windows.Forms.Padding(4);
            this.loadScriptButton.Name = "loadScriptButton";
            this.loadScriptButton.Size = new System.Drawing.Size(149, 34);
            this.loadScriptButton.TabIndex = 104;
            this.loadScriptButton.Text = "Load Script";
            this.loadScriptButton.UseVisualStyleBackColor = true;
            this.loadScriptButton.Click += new System.EventHandler(this.loadScriptButton_Click);
            // 
            // button95
            // 
            this.button95.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button95.Location = new System.Drawing.Point(1388, 549);
            this.button95.Margin = new System.Windows.Forms.Padding(4);
            this.button95.Name = "button95";
            this.button95.Size = new System.Drawing.Size(116, 63);
            this.button95.TabIndex = 141;
            this.button95.UseVisualStyleBackColor = true;
            // 
            // button16
            // 
            this.button16.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button16.Location = new System.Drawing.Point(1139, 309);
            this.button16.Margin = new System.Windows.Forms.Padding(4);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(149, 34);
            this.button16.TabIndex = 105;
            this.button16.Text = "Save Script";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button94
            // 
            this.button94.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button94.Location = new System.Drawing.Point(1264, 549);
            this.button94.Margin = new System.Windows.Forms.Padding(4);
            this.button94.Name = "button94";
            this.button94.Size = new System.Drawing.Size(116, 63);
            this.button94.TabIndex = 140;
            this.button94.UseVisualStyleBackColor = true;
            // 
            // compareScreenButton
            // 
            this.compareScreenButton.BackColor = System.Drawing.Color.PaleGreen;
            this.compareScreenButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.compareScreenButton.Location = new System.Drawing.Point(24, 689);
            this.compareScreenButton.Margin = new System.Windows.Forms.Padding(4);
            this.compareScreenButton.Name = "compareScreenButton";
            this.compareScreenButton.Size = new System.Drawing.Size(116, 63);
            this.compareScreenButton.TabIndex = 107;
            this.compareScreenButton.Text = "Compare Screen";
            this.compareScreenButton.UseVisualStyleBackColor = false;
            this.compareScreenButton.Click += new System.EventHandler(this.compareScreenButton_Click);
            // 
            // button93
            // 
            this.button93.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button93.Location = new System.Drawing.Point(1140, 549);
            this.button93.Margin = new System.Windows.Forms.Padding(4);
            this.button93.Name = "button93";
            this.button93.Size = new System.Drawing.Size(116, 63);
            this.button93.TabIndex = 139;
            this.button93.UseVisualStyleBackColor = true;
            // 
            // failBoundaryButton
            // 
            this.failBoundaryButton.BackColor = System.Drawing.Color.PaleGreen;
            this.failBoundaryButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.failBoundaryButton.Location = new System.Drawing.Point(272, 479);
            this.failBoundaryButton.Margin = new System.Windows.Forms.Padding(4);
            this.failBoundaryButton.Name = "failBoundaryButton";
            this.failBoundaryButton.Size = new System.Drawing.Size(116, 63);
            this.failBoundaryButton.TabIndex = 108;
            this.failBoundaryButton.Text = "Fail Boundary";
            this.failBoundaryButton.UseVisualStyleBackColor = false;
            this.failBoundaryButton.Click += new System.EventHandler(this.failBoundaryButton_Click);
            // 
            // button92
            // 
            this.button92.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button92.Location = new System.Drawing.Point(1016, 549);
            this.button92.Margin = new System.Windows.Forms.Padding(4);
            this.button92.Name = "button92";
            this.button92.Size = new System.Drawing.Size(116, 63);
            this.button92.TabIndex = 138;
            this.button92.UseVisualStyleBackColor = true;
            // 
            // readingButton
            // 
            this.readingButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.readingButton.Location = new System.Drawing.Point(520, 479);
            this.readingButton.Margin = new System.Windows.Forms.Padding(4);
            this.readingButton.Name = "readingButton";
            this.readingButton.Size = new System.Drawing.Size(116, 63);
            this.readingButton.TabIndex = 109;
            this.readingButton.Text = "Add Reading";
            this.readingButton.UseVisualStyleBackColor = true;
            this.readingButton.Click += new System.EventHandler(this.readingButton_Click);
            // 
            // button91
            // 
            this.button91.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button91.Location = new System.Drawing.Point(892, 549);
            this.button91.Margin = new System.Windows.Forms.Padding(4);
            this.button91.Name = "button91";
            this.button91.Size = new System.Drawing.Size(116, 63);
            this.button91.TabIndex = 137;
            this.button91.UseVisualStyleBackColor = true;
            // 
            // statemachineButton
            // 
            this.statemachineButton.BackColor = System.Drawing.Color.PaleGreen;
            this.statemachineButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.statemachineButton.Location = new System.Drawing.Point(148, 689);
            this.statemachineButton.Margin = new System.Windows.Forms.Padding(4);
            this.statemachineButton.Name = "statemachineButton";
            this.statemachineButton.Size = new System.Drawing.Size(116, 63);
            this.statemachineButton.TabIndex = 110;
            this.statemachineButton.Text = "Check SM State";
            this.statemachineButton.UseVisualStyleBackColor = false;
            this.statemachineButton.Click += new System.EventHandler(this.statemachineButton_Click);
            // 
            // button90
            // 
            this.button90.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button90.Location = new System.Drawing.Point(768, 549);
            this.button90.Margin = new System.Windows.Forms.Padding(4);
            this.button90.Name = "button90";
            this.button90.Size = new System.Drawing.Size(116, 63);
            this.button90.TabIndex = 136;
            this.button90.UseVisualStyleBackColor = true;
            // 
            // passBoundaryButton
            // 
            this.passBoundaryButton.BackColor = System.Drawing.Color.PaleGreen;
            this.passBoundaryButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.passBoundaryButton.Location = new System.Drawing.Point(272, 549);
            this.passBoundaryButton.Margin = new System.Windows.Forms.Padding(4);
            this.passBoundaryButton.Name = "passBoundaryButton";
            this.passBoundaryButton.Size = new System.Drawing.Size(116, 63);
            this.passBoundaryButton.TabIndex = 111;
            this.passBoundaryButton.Text = "Pass Boundary";
            this.passBoundaryButton.UseVisualStyleBackColor = false;
            this.passBoundaryButton.Click += new System.EventHandler(this.passBoundaryButton_Click);
            // 
            // button89
            // 
            this.button89.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button89.Location = new System.Drawing.Point(1388, 479);
            this.button89.Margin = new System.Windows.Forms.Padding(4);
            this.button89.Name = "button89";
            this.button89.Size = new System.Drawing.Size(116, 63);
            this.button89.TabIndex = 135;
            this.button89.UseVisualStyleBackColor = true;
            // 
            // userDelayButton
            // 
            this.userDelayButton.BackColor = System.Drawing.Color.PaleGreen;
            this.userDelayButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.userDelayButton.Location = new System.Drawing.Point(24, 619);
            this.userDelayButton.Margin = new System.Windows.Forms.Padding(4);
            this.userDelayButton.Name = "userDelayButton";
            this.userDelayButton.Size = new System.Drawing.Size(116, 63);
            this.userDelayButton.TabIndex = 112;
            this.userDelayButton.Text = "Set Delay";
            this.userDelayButton.UseVisualStyleBackColor = false;
            this.userDelayButton.Click += new System.EventHandler(this.userDelayButton_Click);
            // 
            // button88
            // 
            this.button88.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button88.Location = new System.Drawing.Point(1264, 479);
            this.button88.Margin = new System.Windows.Forms.Padding(4);
            this.button88.Name = "button88";
            this.button88.Size = new System.Drawing.Size(116, 63);
            this.button88.TabIndex = 134;
            this.button88.UseVisualStyleBackColor = true;
            // 
            // batteryButton
            // 
            this.batteryButton.BackColor = System.Drawing.Color.PaleGreen;
            this.batteryButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.batteryButton.Location = new System.Drawing.Point(148, 619);
            this.batteryButton.Margin = new System.Windows.Forms.Padding(4);
            this.batteryButton.Name = "batteryButton";
            this.batteryButton.Size = new System.Drawing.Size(116, 63);
            this.batteryButton.TabIndex = 113;
            this.batteryButton.Text = "Battery %";
            this.batteryButton.UseVisualStyleBackColor = false;
            this.batteryButton.Click += new System.EventHandler(this.batteryButton_Click);
            // 
            // button87
            // 
            this.button87.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button87.Location = new System.Drawing.Point(644, 549);
            this.button87.Margin = new System.Windows.Forms.Padding(4);
            this.button87.Name = "button87";
            this.button87.Size = new System.Drawing.Size(116, 63);
            this.button87.TabIndex = 133;
            this.button87.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button8.Location = new System.Drawing.Point(829, 348);
            this.button8.Margin = new System.Windows.Forms.Padding(4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(149, 34);
            this.button8.TabIndex = 114;
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button86
            // 
            this.button86.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button86.Location = new System.Drawing.Point(1140, 479);
            this.button86.Margin = new System.Windows.Forms.Padding(4);
            this.button86.Name = "button86";
            this.button86.Size = new System.Drawing.Size(116, 63);
            this.button86.TabIndex = 132;
            this.button86.UseVisualStyleBackColor = true;
            // 
            // checkLEDStateButton
            // 
            this.checkLEDStateButton.BackColor = System.Drawing.Color.PaleGreen;
            this.checkLEDStateButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.checkLEDStateButton.Location = new System.Drawing.Point(272, 689);
            this.checkLEDStateButton.Margin = new System.Windows.Forms.Padding(4);
            this.checkLEDStateButton.Name = "checkLEDStateButton";
            this.checkLEDStateButton.Size = new System.Drawing.Size(116, 63);
            this.checkLEDStateButton.TabIndex = 115;
            this.checkLEDStateButton.Text = "Check LED State";
            this.checkLEDStateButton.UseVisualStyleBackColor = false;
            this.checkLEDStateButton.Click += new System.EventHandler(this.checkLEDStateButton_Click);
            // 
            // button85
            // 
            this.button85.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button85.Location = new System.Drawing.Point(1016, 479);
            this.button85.Margin = new System.Windows.Forms.Padding(4);
            this.button85.Name = "button85";
            this.button85.Size = new System.Drawing.Size(116, 63);
            this.button85.TabIndex = 131;
            this.button85.UseVisualStyleBackColor = true;
            // 
            // getLEDStateButton
            // 
            this.getLEDStateButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.getLEDStateButton.Location = new System.Drawing.Point(672, 348);
            this.getLEDStateButton.Margin = new System.Windows.Forms.Padding(4);
            this.getLEDStateButton.Name = "getLEDStateButton";
            this.getLEDStateButton.Size = new System.Drawing.Size(149, 34);
            this.getLEDStateButton.TabIndex = 116;
            this.getLEDStateButton.UseVisualStyleBackColor = true;
            // 
            // button84
            // 
            this.button84.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button84.Location = new System.Drawing.Point(892, 479);
            this.button84.Margin = new System.Windows.Forms.Padding(4);
            this.button84.Name = "button84";
            this.button84.Size = new System.Drawing.Size(116, 63);
            this.button84.TabIndex = 130;
            this.button84.UseVisualStyleBackColor = true;
            // 
            // fullStatusCheckButton
            // 
            this.fullStatusCheckButton.BackColor = System.Drawing.Color.PaleGreen;
            this.fullStatusCheckButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.fullStatusCheckButton.Location = new System.Drawing.Point(272, 619);
            this.fullStatusCheckButton.Margin = new System.Windows.Forms.Padding(4);
            this.fullStatusCheckButton.Name = "fullStatusCheckButton";
            this.fullStatusCheckButton.Size = new System.Drawing.Size(116, 63);
            this.fullStatusCheckButton.TabIndex = 117;
            this.fullStatusCheckButton.Text = "Full Status Check";
            this.fullStatusCheckButton.UseVisualStyleBackColor = false;
            this.fullStatusCheckButton.Click += new System.EventHandler(this.fullStatusCheckButton_Click);
            // 
            // button83
            // 
            this.button83.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button83.Location = new System.Drawing.Point(768, 479);
            this.button83.Margin = new System.Windows.Forms.Padding(4);
            this.button83.Name = "button83";
            this.button83.Size = new System.Drawing.Size(116, 63);
            this.button83.TabIndex = 129;
            this.button83.UseVisualStyleBackColor = true;
            // 
            // cytologyButton
            // 
            this.cytologyButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cytologyButton.Location = new System.Drawing.Point(396, 549);
            this.cytologyButton.Margin = new System.Windows.Forms.Padding(4);
            this.cytologyButton.Name = "cytologyButton";
            this.cytologyButton.Size = new System.Drawing.Size(116, 63);
            this.cytologyButton.TabIndex = 118;
            this.cytologyButton.Text = "Select Cytology";
            this.cytologyButton.UseVisualStyleBackColor = true;
            this.cytologyButton.Click += new System.EventHandler(this.cytologyButton_Click);
            // 
            // button82
            // 
            this.button82.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button82.Location = new System.Drawing.Point(644, 479);
            this.button82.Margin = new System.Windows.Forms.Padding(4);
            this.button82.Name = "button82";
            this.button82.Size = new System.Drawing.Size(116, 63);
            this.button82.TabIndex = 128;
            this.button82.UseVisualStyleBackColor = true;
            // 
            // setSessionTimeButton
            // 
            this.setSessionTimeButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.setSessionTimeButton.Location = new System.Drawing.Point(396, 619);
            this.setSessionTimeButton.Margin = new System.Windows.Forms.Padding(4);
            this.setSessionTimeButton.Name = "setSessionTimeButton";
            this.setSessionTimeButton.Size = new System.Drawing.Size(116, 63);
            this.setSessionTimeButton.TabIndex = 119;
            this.setSessionTimeButton.UseVisualStyleBackColor = true;
            // 
            // button66
            // 
            this.button66.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button66.Location = new System.Drawing.Point(56, 384);
            this.button66.Margin = new System.Windows.Forms.Padding(4);
            this.button66.Name = "button66";
            this.button66.Size = new System.Drawing.Size(171, 63);
            this.button66.TabIndex = 127;
            this.button66.Text = "View Device Screen";
            this.button66.UseVisualStyleBackColor = true;
            this.button66.Click += new System.EventHandler(this.button66_Click);
            // 
            // button19
            // 
            this.button19.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button19.Location = new System.Drawing.Point(396, 689);
            this.button19.Margin = new System.Windows.Forms.Padding(4);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(116, 63);
            this.button19.TabIndex = 120;
            this.button19.UseVisualStyleBackColor = true;
            // 
            // button64
            // 
            this.button64.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button64.Location = new System.Drawing.Point(1453, 311);
            this.button64.Margin = new System.Windows.Forms.Padding(4);
            this.button64.Name = "button64";
            this.button64.Size = new System.Drawing.Size(169, 71);
            this.button64.TabIndex = 126;
            this.button64.Text = "Save Current Screen";
            this.button64.UseVisualStyleBackColor = true;
            this.button64.Click += new System.EventHandler(this.button64_Click);
            // 
            // button20
            // 
            this.button20.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button20.Location = new System.Drawing.Point(520, 550);
            this.button20.Margin = new System.Windows.Forms.Padding(4);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(116, 63);
            this.button20.TabIndex = 121;
            this.button20.UseVisualStyleBackColor = true;
            // 
            // convertButton
            // 
            this.convertButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.convertButton.Location = new System.Drawing.Point(672, 309);
            this.convertButton.Margin = new System.Windows.Forms.Padding(4);
            this.convertButton.Name = "convertButton";
            this.convertButton.Size = new System.Drawing.Size(149, 34);
            this.convertButton.TabIndex = 125;
            this.convertButton.Text = "Convert Screenshot";
            this.convertButton.UseVisualStyleBackColor = true;
            this.convertButton.Click += new System.EventHandler(this.convertButton_Click);
            // 
            // button21
            // 
            this.button21.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button21.Location = new System.Drawing.Point(520, 620);
            this.button21.Margin = new System.Windows.Forms.Padding(4);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(116, 63);
            this.button21.TabIndex = 122;
            this.button21.UseVisualStyleBackColor = true;
            // 
            // newScriptButton
            // 
            this.newScriptButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.newScriptButton.Location = new System.Drawing.Point(987, 350);
            this.newScriptButton.Margin = new System.Windows.Forms.Padding(4);
            this.newScriptButton.Name = "newScriptButton";
            this.newScriptButton.Size = new System.Drawing.Size(148, 33);
            this.newScriptButton.TabIndex = 124;
            this.newScriptButton.Text = "New Script";
            this.newScriptButton.UseVisualStyleBackColor = true;
            this.newScriptButton.Click += new System.EventHandler(this.newScriptButton_Click);
            // 
            // button22
            // 
            this.button22.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button22.Location = new System.Drawing.Point(520, 689);
            this.button22.Margin = new System.Windows.Forms.Padding(4);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(116, 63);
            this.button22.TabIndex = 123;
            this.button22.UseVisualStyleBackColor = true;
            // 
            // batchScriptTabPage
            // 
            this.batchScriptTabPage.Controls.Add(this.saveSetButton);
            this.batchScriptTabPage.Controls.Add(this.button30);
            this.batchScriptTabPage.Controls.Add(this.setCreationDataGridView);
            this.batchScriptTabPage.Location = new System.Drawing.Point(4, 25);
            this.batchScriptTabPage.Margin = new System.Windows.Forms.Padding(4);
            this.batchScriptTabPage.Name = "batchScriptTabPage";
            this.batchScriptTabPage.Size = new System.Drawing.Size(1653, 777);
            this.batchScriptTabPage.TabIndex = 4;
            this.batchScriptTabPage.Text = "Batch Scripts";
            this.batchScriptTabPage.UseVisualStyleBackColor = true;
            // 
            // saveSetButton
            // 
            this.saveSetButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.saveSetButton.Location = new System.Drawing.Point(205, 27);
            this.saveSetButton.Margin = new System.Windows.Forms.Padding(4);
            this.saveSetButton.Name = "saveSetButton";
            this.saveSetButton.Size = new System.Drawing.Size(175, 57);
            this.saveSetButton.TabIndex = 13;
            this.saveSetButton.Text = "Save Set";
            this.saveSetButton.UseVisualStyleBackColor = true;
            // 
            // button30
            // 
            this.button30.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button30.Location = new System.Drawing.Point(32, 27);
            this.button30.Margin = new System.Windows.Forms.Padding(4);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(165, 57);
            this.button30.TabIndex = 12;
            this.button30.Text = "Load Set";
            this.button30.UseVisualStyleBackColor = true;
            // 
            // setCreationDataGridView
            // 
            this.setCreationDataGridView.AllowUserToAddRows = false;
            this.setCreationDataGridView.AllowUserToResizeColumns = false;
            this.setCreationDataGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.setCreationDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.setCreationDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.setCreationDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.File});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.setCreationDataGridView.DefaultCellStyle = dataGridViewCellStyle11;
            this.setCreationDataGridView.Location = new System.Drawing.Point(32, 105);
            this.setCreationDataGridView.Margin = new System.Windows.Forms.Padding(4);
            this.setCreationDataGridView.Name = "setCreationDataGridView";
            this.setCreationDataGridView.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.setCreationDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.setCreationDataGridView.RowHeadersWidth = 51;
            this.setCreationDataGridView.RowTemplate.Height = 24;
            this.setCreationDataGridView.Size = new System.Drawing.Size(1588, 642);
            this.setCreationDataGridView.TabIndex = 11;
            // 
            // File
            // 
            this.File.HeaderText = "Script File";
            this.File.MinimumWidth = 6;
            this.File.Name = "File";
            this.File.ReadOnly = true;
            this.File.Width = 1200;
            // 
            // button117
            // 
            this.button117.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button117.Location = new System.Drawing.Point(801, -16);
            this.button117.Margin = new System.Windows.Forms.Padding(4);
            this.button117.Name = "button117";
            this.button117.Size = new System.Drawing.Size(225, 46);
            this.button117.TabIndex = 68;
            this.button117.Text = "Set LED Pattern";
            this.button117.UseVisualStyleBackColor = true;
            this.button117.Click += new System.EventHandler(this.button117_Click_1);
            // 
            // tmrDevicePoll
            // 
            this.tmrDevicePoll.Interval = 3000;
            this.tmrDevicePoll.Tick += new System.EventHandler(this.tmrDevicePoll_Tick);
            // 
            // toolTip1
            // 
            this.toolTip1.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTip1_Popup);
            // 
            // playbackTimer
            // 
            this.playbackTimer.Tick += new System.EventHandler(this.playbackTimer_Tick);
            // 
            // hardResetTimer
            // 
            this.hardResetTimer.Interval = 500;
            this.hardResetTimer.Tick += new System.EventHandler(this.hardResetTimer_Tick);
            // 
            // scriptSetTimer
            // 
            this.scriptSetTimer.Interval = 200;
            this.scriptSetTimer.Tick += new System.EventHandler(this.scriptSetTimer_Tick);
            // 
            // keyPressTimer
            // 
            this.keyPressTimer.Interval = 50;
            this.keyPressTimer.Tick += new System.EventHandler(this.keyPressTimer_Tick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Location = new System.Drawing.Point(28, 373);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1035, 340);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Internal";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button75);
            this.groupBox4.Controls.Add(this.button74);
            this.groupBox4.Controls.Add(this.button71);
            this.groupBox4.Controls.Add(this.button69);
            this.groupBox4.Controls.Add(this.button70);
            this.groupBox4.Location = new System.Drawing.Point(28, 24);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1035, 324);
            this.groupBox4.TabIndex = 23;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "External";
            // 
            // button1
            // 
            this.button1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button1.Location = new System.Drawing.Point(29, 34);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(225, 46);
            this.button1.TabIndex = 22;
            this.button1.Text = "Erase Language Pack";
            this.toolTip1.SetToolTip(this.button1, resources.GetString("button1.ToolTip"));
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1693, 870);
            this.Controls.Add(this.statusStripMain);
            this.Controls.Add(this.menuStripMain);
            this.Controls.Add(this.button117);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripMain;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ZedScan MKII Test App";
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.comsSettingsTabPage.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.deviceComsTabPage.ResumeLayout(false);
            this.deviceComsTabPage.PerformLayout();
            this.remoteControlTabPage.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.FPGATabPage.ResumeLayout(false);
            this.FPGATabPage.PerformLayout();
            this.batteryAndChargingTabPage.ResumeLayout(false);
            this.systemControlTabPage.ResumeLayout(false);
            this.externalFlashTabPage.ResumeLayout(false);
            this.auditInfoTabPage.ResumeLayout(false);
            this.auditInfoTabPage.PerformLayout();
            this.iOControlTabPage.ResumeLayout(false);
            this.iOControlTabPage.PerformLayout();
            this.scriptRunnerTabPage.ResumeLayout(false);
            this.scriptRunnerTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultSetGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scriptSetGridView)).EndInit();
            this.scriptCreatorTabPage.ResumeLayout(false);
            this.scriptCreatorTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.commandDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.screenPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.screenCopyPictureBox)).EndInit();
            this.batchScriptTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.setCreationDataGridView)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxDisplay;
        private System.Windows.Forms.Button buttonInfo;
        private System.Windows.Forms.Button buttonStartSession;
        private System.Windows.Forms.Button buttonReadSession;
        private System.Windows.Forms.Button buttonDeleteSession;
        private System.Windows.Forms.Button buttonRequestResults;
        private System.Windows.Forms.Button languagePackButton;
        private System.Windows.Forms.Button loadFirmwareButton;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adaptorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zedScan1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vCommToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hIDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem baudRateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearScreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem singleByteModeToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStripMain;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItemDivider1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage deviceComsTabPage;
        private System.Windows.Forms.TabPage remoteControlTabPage;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage FPGATabPage;
        private System.Windows.Forms.TabPage iOControlTabPage;
        private System.Windows.Forms.TabPage batteryAndChargingTabPage;
        private System.Windows.Forms.TabPage systemControlTabPage;
        private System.Windows.Forms.TabPage externalFlashTabPage;
        private System.Windows.Forms.TabPage auditInfoTabPage;
        private System.Windows.Forms.TabPage scriptRunnerTabPage;
        private System.Windows.Forms.TabPage scriptCreatorTabPage;
        private System.Windows.Forms.TabPage batchScriptTabPage;
        private System.Windows.Forms.RichTextBox TechnicalDisplayBox;
        private System.Windows.Forms.ComboBox backlightComboBox;
        private System.Windows.Forms.Button button81;
        private System.Windows.Forms.Button button80;
        private System.Windows.Forms.Button button79;
        private System.Windows.Forms.Button button63;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button runScriptSetButton;
        private System.Windows.Forms.Button loadSetButton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView resultSetGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridView scriptSetGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn ScriptFile;
        private System.Windows.Forms.Button button113;
        private System.Windows.Forms.DataGridView commandDataGridView;
        private System.Windows.Forms.Button button112;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button111;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button110;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button109;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button108;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button107;
        private System.Windows.Forms.Button undockButton;
        private System.Windows.Forms.Button button106;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button105;
        private System.Windows.Forms.Button grabScreenButton;
        private System.Windows.Forms.Button button104;
        private System.Windows.Forms.PictureBox screenPictureBox;
        private System.Windows.Forms.Button button103;
        private System.Windows.Forms.PictureBox screenCopyPictureBox;
        private System.Windows.Forms.Button button102;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button101;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button100;
        private System.Windows.Forms.Button sessionButton;
        private System.Windows.Forms.Button button99;
        private System.Windows.Forms.Button runScriptButton;
        private System.Windows.Forms.Button button98;
        private System.Windows.Forms.Button dockButton;
        private System.Windows.Forms.Button button97;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button96;
        private System.Windows.Forms.Button loadScriptButton;
        private System.Windows.Forms.Button button95;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button94;
        private System.Windows.Forms.Button compareScreenButton;
        private System.Windows.Forms.Button button93;
        private System.Windows.Forms.Button failBoundaryButton;
        private System.Windows.Forms.Button button92;
        private System.Windows.Forms.Button readingButton;
        private System.Windows.Forms.Button button91;
        private System.Windows.Forms.Button statemachineButton;
        private System.Windows.Forms.Button button90;
        private System.Windows.Forms.Button passBoundaryButton;
        private System.Windows.Forms.Button button89;
        private System.Windows.Forms.Button userDelayButton;
        private System.Windows.Forms.Button button88;
        private System.Windows.Forms.Button batteryButton;
        private System.Windows.Forms.Button button87;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button86;
        private System.Windows.Forms.Button checkLEDStateButton;
        private System.Windows.Forms.Button button85;
        private System.Windows.Forms.Button getLEDStateButton;
        private System.Windows.Forms.Button button84;
        private System.Windows.Forms.Button fullStatusCheckButton;
        private System.Windows.Forms.Button button83;
        private System.Windows.Forms.Button cytologyButton;
        private System.Windows.Forms.Button button82;
        private System.Windows.Forms.Button setSessionTimeButton;
        private System.Windows.Forms.Button button66;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button64;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button convertButton;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button newScriptButton;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Timer tmrDevicePoll;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Timer playbackTimer;
        private System.Windows.Forms.Timer hardResetTimer;
        private System.Windows.Forms.Timer scriptSetTimer;
        private System.Windows.Forms.Timer keyPressTimer;
        private System.Windows.Forms.Button saveSetButton;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.DataGridView setCreationDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn File;
        private System.Windows.Forms.Button button62;
        private System.Windows.Forms.Button button61;
        private System.Windows.Forms.Button button59;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.ComboBox comboBoxUnmanagedSamples;
        private System.Windows.Forms.ComboBox comboBoxUnmanagedSw;
        private System.Windows.Forms.ComboBox comboBoxUnmanagedFreq;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxManagedSw;
        private System.Windows.Forms.ComboBox comboBoxManagedFreq;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button65;
        private System.Windows.Forms.Button button114;
        private System.Windows.Forms.Button button67;
        private System.Windows.Forms.Button button78;
        private System.Windows.Forms.Button button73;
        private System.Windows.Forms.Button button72;
        private System.Windows.Forms.ComboBox referralSessionComboBox;
        private System.Windows.Forms.Button button68;
        private System.Windows.Forms.Button button71;
        private System.Windows.Forms.Button button70;
        private System.Windows.Forms.Button button69;
        private System.Windows.Forms.Button button74;
        private System.Windows.Forms.Button button75;
        private System.Windows.Forms.Button button77;
        private System.Windows.Forms.Button button76;
        private System.Windows.Forms.Button button115;
        private System.Windows.Forms.TextBox UIDtextBox;
        private System.Windows.Forms.Button button116;
        private System.Windows.Forms.TabPage comsSettingsTabPage;
        private System.Windows.Forms.ComboBox ComPortList;
        private System.Windows.Forms.Button button60;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox LEDPatternComboBox;
        private System.Windows.Forms.Button button117;
        private System.Windows.Forms.ComboBox TonePatternComboBox;
        private System.Windows.Forms.Button button118;
        private System.Windows.Forms.Button button119;
        private System.Windows.Forms.Button button120;
        private System.Windows.Forms.ComboBox selectScreenComboBox;
        private System.Windows.Forms.Button button121;
        private System.Windows.Forms.Button button122;
        private System.Windows.Forms.ComboBox systemStateComboBox;
        private System.Windows.Forms.Button setSystemStateButton;
        private System.Windows.Forms.ComboBox protectedDataComboBox;
        private System.Windows.Forms.Button corruptChecksumButton;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button1;
    }
}

