﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZiliCator.Forms
{
    public partial class ParameterEntryForm : Form
    {
        private bool Integer = false;
        private bool Cytology = false;
        private string stringValue = "";
        public string StringValue
        {
            get
            {
                return stringValue;
            }
            set
            {
                stringValue = value;
            }
        }

        private int intValue = 0;
        public int IntValue
        {
            get
            {
                return intValue;
            }
            set
            {
                intValue = value;
            }
        }
        private bool resultValue = false;
        public bool ResultValue
        {
            get
            {
                return resultValue;
            }
            set
            {
                resultValue = value;
            }
        }
        public ParameterEntryForm(bool integer, bool cytology)
        {
            Integer = integer;
            Cytology = cytology;
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (Integer == true)
            {
                if (int.TryParse(parameterTextBox.Text, out intValue) == true)
                {
                    ResultValue = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("This value is not an integer");
                }
            }
            else
            {
                if (Cytology == true)
                {
                    intValue = cytologyListBox.SelectedIndex;
                    ResultValue = true;
                    this.Close();
                }
                else
                {

                    StringValue = parameterTextBox.Text;
                    this.Close();
                }
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            ResultValue = false;
            this.Close();
        }
        public enum Referral_Type_et
        {
            /* Referral system: Modified BSCC 2008 */
            NO_BSCC_REFERRAL = 0,
            BSCC_REFERRAL_BORDERLINE = 1,
            BSCC_REFERRAL_B_GLANDULAR = 2,
            BSCC_REFERRAL_LOW_GRADE = 3,
            BSCC_REFERRAL_Q_HIGH_GRADE = 4,
            BSCC_REFERRAL_HIGH_GRADE = 5,
            BSCC_REFERRAL_Q_INVASIVE = 6,
            BSCC_REFERRAL_G_NEOPLASIA = 7,
            /* Referral system: TBS 2001 */
            NO_TBS_REFERRAL = 8, /* 8 */
            TBS_REFERRAL_ASC_US = 9,
            TBS_REFERRAL_LSIL = 10,
            TBS_REFERRAL_ASC_H = 11,
            TBS_REFERRAL_HSIL = 12,
            TBS_REFERRAL_SQ_CAR = 13,
            TBS_REFERRAL_AGCUS = 14,
            TBS_REFERRAL_AIS = 15,
            /* Referral system: ECTP */
            NO_ECTP_REFERRAL = 16, /* 16 */
            ECTP_REFERRAL_KOILO = 17,
            ECTP_REFERRAL_CIN1 = 18,
            ECTP_REFERRAL_CIN2 = 19,
            ECTP_REFERRAL_CIN3 = 20,
            ECTP_REFERRAL_Q_INV = 21,
            ECTP_REFERRAL_A_GLAN = 22,
            ECTP_REFERRAL_AIS = 23,
            /* Referral system: AMBS 2004 */
            NO_AMBS_REFERRAL = 24, /* 24 */
            AMBS_REFERRAL_Q_LSIL = 25,
            AMBS_REFERRAL_LSIL = 26,
            AMBS_REFERRAL_Q_HSIL = 27,
            AMBS_REFERRAL_HSIL = 28,
            AMBS_REFERRAL_SQ_CAR = 29,
            AMBS_REFERRAL_AGCUS = 30,
            AMBS_REFERRAL_AIS = 31,

            TBS_REFERRAL_HR_HPV = 32, /* 32 */
            ECTP_REFERRAL_HR_HPV = 33, /* 33 */
            AMBS_REFERRAL_HR_HPV = 34, /* 34 */

            BSCC_NO_CYTOLOGY = 35,
            TBS_NO_CYTOLOGY = 36,
            ECTP_NO_CYTOLOGY = 37,
            AMBS_NO_CYTOLOGY = 38,

            BSCC_HR_HPV_CYTOLOGY_NEGATIVE = 39,
            BSCC_BORDERLINE_ENDO = 40,
            BSCC_BORDERLONE_SQUAMO = 41,
            BSCC_NEW_LOW_GRADE = 42,

            NUM_REFERRAL_TYPES
        };

        private void ParameterEntryForm_Load(object sender, EventArgs e)
        {
            ResultValue = false;
            if (Integer == true)
            {
                numberLabel.Text = "Enter Number";
                parameterTextBox.Text = "0";
                parameterTextBox.Show();
                IntValue = 0;
            }
            else
            {
                numberLabel.Text = "Enter Text";
                parameterTextBox.Text = "";
                parameterTextBox.Hide();
                StringValue = "";
            }
            for (int Index = 0; Index < (int)Referral_Type_et.NUM_REFERRAL_TYPES; Index++)
            {
                cytologyListBox.Items.Add(Enum.GetName(typeof(Referral_Type_et), Index));
            }
            if (Cytology == true)
            {
                cytologyListBox.Show();
                parameterTextBox.Visible = false;
                cytologyListBox.Visible = true;
                numberLabel.Visible = false;
                cytologyLabel.Visible = true;
            }
            else
            {
                cytologyListBox.Hide();
            }

        }
    }
}
