﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Drawing.Imaging;
using System.Drawing;
using ZiliComms.Commands;

namespace ZiliCator
{
    internal class Main
    {
        #region fields

        private string _ApplicationTitle;


        private FormMain _FormMain;

        private static Action<string> DisplayMessage;


        private ZiliConnection.Monitor _USBMonitor;

        ZiliConnection.Guids _DeviceType;
        private string _Vid;
        private string _Pid;


        private ZiliComms.ZedScan _ZedScan;

        private UInt32 _BaudRate;
        private bool _SingleByteMode;


        private List<CommsProceedure> _CommsProceedures;

        private readonly object _ListLock;

        private bool _IsBusyCommsTask;



        #endregion



        public Main(string applicationTitle)
        {
            _ApplicationTitle = applicationTitle;

            _DeviceType = ZiliConnection.Guids.GUID_DEVINTERFACE_HID;

            _ZedScan = new ZiliComms.ZedScan();

            _BaudRate = Properties.Settings.Default.Baud;           // 0
            _SingleByteMode = Properties.Settings.Default.SingleByteMode;


            _FormMain = new FormMain(_ApplicationTitle);

            DisplayMessage = _FormMain.DisplayMessage;

            _FormMain.AddBaudRates(new List<string>() { "9600", "19200", "115200", "230400", "460800", "921600" });

            _FormMain.SingleByteMode = _SingleByteMode;

            _FormMain.AdaptorSelectionEvent += _FormMain_AdaptorSelectionEvent;
            _FormMain.ChangeBaudRateEvent += _FormMain_ChangeBaudRateEvent;
            _FormMain.ChangeSingleByteModeEvent += _FormMain_ChangeSingleByteModeEvent;
            _FormMain.ExecuteCommandEvent += _FormMain_ExecuteCommandEvent;

            if (!Properties.Settings.Default.Adaptor.Equals((int)AdaptorType.NotSet))
            {
                _FormMain.SetAdaptor((AdaptorType)Properties.Settings.Default.Adaptor);

                _FormMain_AdaptorSelectionEvent(null, (AdaptorType)Properties.Settings.Default.Adaptor);
            }

            _CommsProceedures = new List<CommsProceedure>();
            _ListLock = new object();
            _IsBusyCommsTask = false;

        }

        public void ShowScreen()
        {
            _FormMain.ShowDialog(); // block ...
        }

        //public void Start()
        //{
        //    ShowScreen();
        //}


        #region FormMain events

        private void _USBMonitor_USBStateChangeEvent(object sender, ZiliConnection.USBStateChangeEventArgs e)
        {
            if (_FormMain.IsHandleCreated)
                DisplayMessage($"{e.State}");

            bool traceEnabled = true;

            if (e.State.Equals(ZiliConnection.StateChange.Arrival))
            {
                System.Diagnostics.Trace.WriteLine($"_USBMonitor_USBStateChangeEvent: {_USBMonitor.DevicePath}");

                if ((_ZedScan.Port != null) && (!_ZedScan.Port.DevicePath.Equals(_USBMonitor.DevicePath)))
                    _ZedScan.Port.Dispose();

                if ((_ZedScan.Port == null) || (_ZedScan.Port.IsDisposed))
                {
                    if (_DeviceType.Equals(ZiliConnection.Guids.GUID_DEVINTERFACE_HID))
                        _ZedScan.Port = new ZiliComms.HID(_USBMonitor.DevicePath, traceEnabled);
                    else
                        _ZedScan.Port = new ZiliComms.Serial(_USBMonitor.DevicePath, traceEnabled);

                    _ZedScan.Port.SingleByteMode = _FormMain.SingleByteMode;
                }

                if (_ZedScan.Port.Open())                          // close and reopen
                {
                    DisplayMessage("Comms Open");

                    _FormMain_ChangeBaudRateEvent(sender, _BaudRate);
                }
                else
                    DisplayMessage($"Open Comms Error : {_ZedScan.Port.Message}");
            }
            else
            {
                if (_ZedScan.Port != null)
                {
                    _ZedScan.Port.Dispose();
                    _ZedScan.Port = null;
                }
            }
        }

        private void _FormMain_AdaptorSelectionEvent(object sender, AdaptorType e)
        {
            if (_USBMonitor != null)
            {
                _USBMonitor.USBStateChangeEvent -= _USBMonitor_USBStateChangeEvent;
                _USBMonitor.Dispose();
            }

            #region set _DeviceType, _Vid and _Pid

            switch (e)
            {
                case AdaptorType.NotSet:
                    _DeviceType = ZiliConnection.Guids.GUID_DEVINTERFACE_HID;

                    _Vid = string.Empty;
                    _Pid = string.Empty;

                    break;

                case AdaptorType.ZedScan_1:

                    _ZedScan.ZedScanVersion = 1;

                    _DeviceType = ZiliConnection.Guids.GUID_DEVINTERFACE_COMPORT;

                    _Vid = "0483";
                    _Pid = "5740";

                    break;

                case AdaptorType.vComm:

                    _ZedScan.ZedScanVersion = 2;

                    _DeviceType = ZiliConnection.Guids.GUID_DEVINTERFACE_COMPORT;

                    _Vid = "0403";
                    _Pid = "6001";

                    break;

                case AdaptorType.HID:

                    _ZedScan.ZedScanVersion = 2;

                    _DeviceType = ZiliConnection.Guids.GUID_DEVINTERFACE_HID;

                    _Vid = "0403";
                    _Pid = "6030";

                    break;

                default:
                    _DeviceType = ZiliConnection.Guids.GUID_DEVINTERFACE_HID;

                    _Vid = string.Empty;
                    _Pid = string.Empty;

                    break;
            }

            #endregion

            if (e != AdaptorType.NotSet)
            {
                _USBMonitor = new ZiliConnection.Monitor(_DeviceType, _Vid, _Pid);

                _USBMonitor.USBStateChangeEvent += _USBMonitor_USBStateChangeEvent;

                //_USBMonitor.TraceEnabled = true;

                _USBMonitor.Start();

                if (sender != null)
                    DisplayMessage($"Adaptor {e} Selected");
            }

            Properties.Settings.Default.Adaptor = (int)e;
            Properties.Settings.Default.Save();
        }

        private void _FormMain_ChangeBaudRateEvent(object sender, uint e)
        {
            if (_ZedScan.Port != null)
            {
                if (_ZedScan.Port.SetBaudRate((int)e))
                {
                    _BaudRate = e;

                    Properties.Settings.Default.Baud = _BaudRate;
                    Properties.Settings.Default.Save();

                    DisplayMessage($"Baud Rate set to {_BaudRate}");
                }
                else
                    DisplayMessage($"Error changing Baud Rate to {e}, {_ZedScan.Port.Message}");
            }
        }

        private void _FormMain_ChangeSingleByteModeEvent(object sender, bool e)
        {
            _SingleByteMode = e;

            if (_ZedScan.Port != null)
            {
                _ZedScan.Port.SingleByteMode = _SingleByteMode;

                DisplayMessage($"Single Byte Mode set to {_SingleByteMode}");
            }

            Properties.Settings.Default.SingleByteMode = _SingleByteMode;
            Properties.Settings.Default.Save();
        }

        private void _FormMain_ExecuteCommandEvent(object sender, OpCode commandCode)
        {
            System.Diagnostics.Trace.WriteLine($"Execute Command: {commandCode}");

            switch (commandCode)
            {
                case OpCode.REQUEST_DEVICE_INFO:
                    AddCommsTask(new CommsProceedure(RequestInformation));
                    break;
                case OpCode.DEVICE_INFORMATION:
                    break;
                case OpCode.REQUEST_RESULTS:
                    AddCommsTask(new CommsProceedure(RequestResults));
                    break;
                case OpCode.READING_RESULT:
                    AddCommsTask(new CommsProceedure(RequestResults));
                    break;
                case OpCode.REQUEST_SESSION_INFORMATION:
                    AddCommsTask(new CommsProceedure(RequestSessionDetails));
                    break;
                case OpCode.SESSION_INFORMATION:
                    EnterSessionDetails();
                    break;
                case OpCode.CODE_UPDATE_START_BLOCK:
                    break;
                case OpCode.CODE_UPDATE_BLOCK:
                    break;
                case OpCode.CODE_UPDATE_END_BLOCK:
                    break;
                case OpCode.LANGUAGE_UPDATE_START_BLOCK:
                    break;
                case OpCode.LANGUAGE_UPDATE_BLOCK:
                    break;
                case OpCode.LANGUAGE_UPDATE_END_BLOCK:
                    break;
                case OpCode.DELETE_SESSION_RESULTS:
                    AddCommsTask(new CommsProceedure(DeleteSessionResults));
                    break;
                case OpCode.REQUEST_CONFIGURATION:
                    break;
                case OpCode.CONFIGURATION:
                    break;
                case OpCode.LANGUAGE_SELECTION:
                    break;
                case OpCode.ACK:
                    break;
                case OpCode.SET_UNIQUE_ID:
                    break;
                case OpCode.NAK:
                    break;
                case OpCode.NOP:
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Async Command Queue

        private void AddCommsTask(CommsProceedure newCommsProceedure)
        {
            lock (_ListLock)
            {
                _CommsProceedures.Add(newCommsProceedure);
            }

            try
            {
                if (!_IsBusyCommsTask)
                {
                    _IsBusyCommsTask = true;


                    Task.Run(CommsTaskProcessor);


                }
            }
            catch (Exception e)
            {
                DisplayMessage($"Add Comms Task {newCommsProceedure} {e.Message}");
            }
        }

        private async Task CommsTaskProcessor()
        {
            CommsProceedure commsAction = null;

            try
            {
                while (_CommsProceedures.Count > 0)
                {
                    lock (_ListLock)
                    {
                        commsAction = _CommsProceedures[0];

                        _CommsProceedures.RemoveAt(0);
                    }

                    if (commsAction != null)
                    {
                        if (commsAction.Proceedure != null)
                        {
                            await Task.Run(commsAction.Proceedure);
                        }
                        else if (commsAction.ParamterisedProceedure != null)
                        {
                            await Task.Run(() => commsAction.ParamterisedProceedure(commsAction.Parameters));
                        }

                        // Abort / Error ?

                    }
                }
            }
            catch (Exception e)
            {
                DisplayMessage($"Comms Task Processor {commsAction} {e.Message}");
            }
            finally
            {
                _IsBusyCommsTask = false;
            }
        }

        #endregion


        private void RequestInformation()
        {
            if (_ZedScan.GetDeviceInformation())
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.AppendLine("Device Information");
                sb.AppendLine($"    Serial Number    : {_ZedScan.DeviceInformation.SerialNumber}");
                sb.AppendLine($"    Hardware Version : {_ZedScan.DeviceInformation.HardwareVersion}");
                sb.AppendLine($"    Software Version : {_ZedScan.DeviceInformation.SoftwareVersion}");

                if(_ZedScan.ZedScanVersion.Equals(2))
                {
                    sb.AppendLine($"    Charge State     : {((ZiliComms.Handset.Information)_ZedScan.DeviceInformation).ChargeState}");
                    sb.AppendLine($"    Battery Charge   : {((ZiliComms.Handset.Information)_ZedScan.DeviceInformation).BatteryCharge}");
                    sb.AppendLine($"    Language Code    : {((ZiliComms.Handset.Information)_ZedScan.DeviceInformation).LanguageCode}");
                }

                if (_ZedScan.DeviceInformation.ErrorLog != null)
                {
                    // {_ZedScan.Port.ToHex(_ZedScan.DeviceInformation.ErrorLog)}");

                    sb.AppendLine($"    Error Log        :");

                    int index = 0;
                    while (index < _ZedScan.DeviceInformation.ErrorLog.Length)
                    {
                        for (int width = 0; width < 16; width++)
                        {
                            if (index < _ZedScan.DeviceInformation.ErrorLog.Length)
                            {
                                if (width.Equals(8))
                                    sb.Append(" ");

                                sb.Append($"{_ZedScan.DeviceInformation.ErrorLog[index]:X2} ");

                                index++;
                            }
                        }

                        sb.AppendLine();
                    }
                }
                else
                    sb.AppendLine($"    Error Log        : None");

                DisplayMessage(sb.ToString());
            }
            else
            {
                DisplayMessage(_ZedScan.Message);
            }
        }


        private void EnterSessionDetails()
        {
            Session session = null;

            if (_ZedScan.Session != null)
            {
                session = new Session("Subject", _ZedScan.Session.PatientId, DateTime.UtcNow,
                                      "NHS 123", _ZedScan.Session.OperatorId,
                                      _ZedScan.Session.ReferralType,
                                       _ZedScan.Session.TrainingMode);

            }
            else
            {
                session = new Session("Subject", "Subject Number", DateTime.UtcNow,
                                      "NHS 123", "Dr Death",
                                      ZiliComms.Handset.ReferralType.BSCC_Borderline,
                                      true);
            }

            Forms.SessionScreen sessionScreen = new Forms.SessionScreen(session);

            if (sessionScreen.ShowDialog(_FormMain).Equals(System.Windows.Forms.DialogResult.OK))
            {
                // add to command queue
                AddCommsTask(new CommsProceedure(SendSession, new object[] { sessionScreen.Session }));
            }
        }

        private void SendSession(object[] parameters)
        {
            ZiliComms.Handset.Session session = (ZiliComms.Handset.Session)parameters[0];

            if (_ZedScan.StartSession(session))
            {
                if (_ZedScan.Session.Equals(session))
                {
                    DisplayMessage($"Session {session} sent,{Environment.NewLine}        {_ZedScan.Session} received");

                    // save _ZedScan.Session
                }
                else
                    DisplayMessage("Session corrupt");
            }
            else
                DisplayMessage(_ZedScan.Message);
        }


        private void RequestSessionDetails()
        {
            if (_ZedScan.GetSession())
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.AppendLine("Session Details (* not part of communications)");
                //sb.AppendLine($"   *NHS Number       : {_ZedScan.Session.NHSnumber}");
                //sb.AppendLine($"   *Patient Name     : {_ZedScan.Session.PatientName}");
                //sb.AppendLine($"   *Date of Birth    : {_ZedScan.Session.DateOfBirth}");
                sb.AppendLine($"   *NHS Number       : Not held by handset");
                sb.AppendLine($"   *Patient Name     : Not held by handset");
                sb.AppendLine($"   *Date of Birth    : Not held by handset");

                sb.AppendLine($"    Patient Id       : {_ZedScan.Session.PatientId}");
                sb.AppendLine($"    Operator Id      : {_ZedScan.Session.OperatorId}");
                sb.AppendLine($"    Referral Type    : {_ZedScan.Session.ReferralType}");
                sb.AppendLine($"    Current Time     : {_ZedScan.Session.StartTime}");
                sb.AppendLine($"    Training Mode    : {_ZedScan.Session.TrainingMode}");

                DisplayMessage(sb.ToString());
            }
            else
            {
                DisplayMessage(_ZedScan.Message);
            }
        }

        private void DeleteSessionResults()
        {
            if (_ZedScan.DeleteSessionResults())
            {
                DisplayMessage("Session results deleted");
            }
            else
                DisplayMessage(_ZedScan.Message);
        }

        private void RequestResults()
        {
            if (_ZedScan.GetResults())
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                // device ID
                sb.AppendLine($"    Serial Number    : {_ZedScan.Results.SerialNumber}");
                // Scan Threshold
                sb.AppendLine($"    Scan Threshold    : {_ZedScan.Results.ScanThreshold}");

                // sp threshold
                sb.AppendLine($"    SP Threshold    : {_ZedScan.Results.SingleThreshold}");
                // Frequency count
                sb.AppendLine($"    SP Threshold    : {_ZedScan.Results.CalibrationData.Count}");
                // Cal Data 14 frequencies
                for (int CalIndex = 0; CalIndex < _ZedScan.Results.CalibrationData.Count; CalIndex++)
                {
                    sb.AppendLine($"    Freq:{_ZedScan.Results.CalibrationData[CalIndex].Frequency} Magnitude: {_ZedScan.Results.CalibrationData[CalIndex].Real}  Phase: {_ZedScan.Results.CalibrationData[CalIndex].Imag} ");
                }
                // Session data
                sb.AppendLine($"    Patient Id       : { _ZedScan.Results.SessionInfo.PatientId}");
                sb.AppendLine($"    Operator Id      : { _ZedScan.Results.SessionInfo.OperatorId}");
                sb.AppendLine($"    Referral Type    : { _ZedScan.Results.SessionInfo.ReferralType}");
                sb.AppendLine($"    Current Time     : { _ZedScan.Results.SessionInfo.StartTime}");
                sb.AppendLine($"    Training Mode    : { _ZedScan.Results.SessionInfo.TrainingMode}");
                // Frequency count
                sb.AppendLine($"    Points Used    : {_ZedScan.Results.Readings.Count}");
                // 16 points worth of 
                for (int PointIndex = 0; PointIndex < _ZedScan.Results.Readings.Count; PointIndex++)
                {
                    sb.AppendLine($"    Point : { _ZedScan.Results.Readings[PointIndex].PointNumber}");
                    sb.AppendLine($"    T : { _ZedScan.Results.Readings[PointIndex].MeasuredT}");
                    sb.AppendLine($"    Tr : { _ZedScan.Results.Readings[PointIndex].MeasuredTr}");
                    sb.AppendLine($"    Analysis Result : { _ZedScan.Results.Readings[PointIndex].AnalysisResult}");
                    sb.AppendLine($"    Analysis Value : { _ZedScan.Results.Readings[PointIndex].AnalysisValue}");
                    for (int FreqIndex = 0; FreqIndex < _ZedScan.Results.Readings[PointIndex].Frequencies.Count; FreqIndex++)
                    {
                        sb.AppendLine($"    Freq : {_ZedScan.Results.Readings[PointIndex].Frequencies[FreqIndex].Frequency}");
                        sb.AppendLine($"    Real : {_ZedScan.Results.Readings[PointIndex].Frequencies[FreqIndex].Real}");
                        sb.AppendLine($"    Imag : {_ZedScan.Results.Readings[PointIndex].Frequencies[FreqIndex].Imag}");
                        sb.AppendLine($"    Real RD : {_ZedScan.Results.Readings[PointIndex].Frequencies[FreqIndex].SdReal}");
                        sb.AppendLine($"    Image SD : {_ZedScan.Results.Readings[PointIndex].Frequencies[FreqIndex].SdImag}");
                    }
                    // boundary Results
                 //   for (int BoundaryIndex = 0; BoundaryIndex < _ZedScan.Results.Readings[Index].; BoundaryIndex++)
                  //  {
                        // Sweep results
                        // analysis results



                }
                DisplayMessage(sb.ToString());
            }
            else
            {
                DisplayMessage(_ZedScan.Message);
            }
        }
        
    }
}
// Copyright (c) 2022 Zilico Limited
