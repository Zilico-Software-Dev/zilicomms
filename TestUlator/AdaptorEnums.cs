﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using System.Linq;
//using System.Text;
using System.Threading.Tasks;

namespace ZiliCator
{
    internal enum AdaptorType
    {
        NotSet = 0,
        ZedScan_1 = 1,
        vComm = 2,
        HID = 3,
    }
}
// Copyright (c) 2022 Zilico Limited
