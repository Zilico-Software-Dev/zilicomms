﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Timers;

namespace ZiliCator
{
    public class KeyState //: IEquatable<KeyState>
    {
        public bool Used;
        public UInt32 Stage;
        public byte[] ScreenCompareBuffer;
        public byte Key;
        public UInt32 DelayToScreenGrab;
        public UInt32 DelayToNextCommand;
    }

    class AutoTest
    {
        /* Interface message definitions */
        private const byte AI_GET_INTERFACE_VERSION = 0x01;
        private const byte AI_GET_DEVICE_UNIQUE_ID_STRING = 0x02;
        private const byte AI_EXECUTE_FUNCTION = 0x91;
        /* Interface packet offsets */
        private const uint AI_DEST_ADDR_OFFSET = 0x00;
        private const uint AI_LENGTH_OFFSET = 0x01;
        private const uint AI_SOURCE_ADDR_OFFSET = 0x05;
        private const uint AI_COMMAND_OFFSET = 0x06;
        private const uint AI_HEADER_LENGTH = 0x07;
        private const uint AI_PARAMETER_OFFSET = 0x07;
        private const uint AI_REPLY_ACK_OFFSET = 0x07;
        private const uint AI_REPLY_RESULT_OFFSET = 0x08;
        private const uint AI_CHECKSUM_LENGTH = 0x02;

        private const uint AI_SOURCE_ADDR = 0x01;
        private const uint AI_DEST_ADDR = 0x05;

        static bool Connected = false;
        private byte[] DownBuffer;
        private byte[] ScreenBuffer;
        List<KeyState> PlayBuffer;

        const UInt32 BUFFER_SIZE = 1024;
        private UInt32 BufferPointer = 0;
        private static System.Timers.Timer CommandTimer;
        private bool CommandTimedOut = false;

        private static System.Timers.Timer SleepTimer;
        private bool Sleeping = false;


        private byte[] CommandRecordArray = new byte[1024];

        private string DataFileName = "";
        private bool HoldForScreenGrab = false;
        private bool Activity = false;
        private byte[] LastCommand;
        private System.IO.Ports.SerialPort HostConnection;

        private void SleepHandler(Object source, ElapsedEventArgs e)
        {
            Sleeping = false;
            SleepTimer.Enabled = false;
        }

        public void SleepForN_MS(UInt32 Interval)
        {
            Sleeping = true;
            SleepTimer.Interval = Interval;
            SleepTimer.Enabled = true;
            while (Sleeping == true)
            {// lock out this thread
            }
        }

        private void CommandTimedOutHandler(Object source, ElapsedEventArgs e)
        {
            CommandTimedOut = true;
            CommandTimer.Enabled = false;
        }

        public AutoTest()
        {
            DownBuffer = new byte[10000];
            ScreenBuffer = new byte[128 * 160 * 2];
            BufferPointer = 0;
            CommandTimer = new System.Timers.Timer(1000);
            CommandTimer.Elapsed += CommandTimedOutHandler;
            CommandTimer.Enabled = false;
            SleepTimer = new System.Timers.Timer(1);
            SleepTimer.Elapsed += SleepHandler;
            SleepTimer.Enabled = false;
            Connected = false;
            CommandTimedOut = false;
            Sleeping = false;
            System.Timers.Timer RecordingTimer;
            HoldForScreenGrab = false;
            Activity = false;
            RecordingTimer = new System.Timers.Timer(1000);
            RecordingTimer.Elapsed += RecordingHandler;
            RecordingTimer.Enabled = false;
            PlayBuffer = new List<KeyState>();
            HostConnection = new SerialPort();
        }

        public bool Connect(int Baud, string Port)
        {
            HostConnection.PortName = Port;
            HostConnection.BaudRate = Baud;
            HostConnection.Parity = Parity.None;
            HostConnection.StopBits = StopBits.One;
            HostConnection.DataBits = 8;
            HostConnection.Handshake = Handshake.None;
            try
            {
                HostConnection.Open();
                Connected = true;
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show("Failed to open: " + Port + " Error: " + exception.ToString());
            }
            return false;
        }
        public bool Disconnect()
        {
            Connected = false;
            try
            {
                HostConnection.Close();
                return true;
            }
            catch (Exception exception)
            {
                MessageBox.Show("Failed to close port Error: " + exception.ToString());
            }
            return false;
        }

        private void StartCommandTimer()
        {
            CommandTimedOut = false;
            CommandTimer.Enabled = true;
        }

        public UInt32 GetReply(ref byte[] Data)
        {
            int Length = DownBuffer.Count();
            int LoadIndex = 0;
            Data = new byte[Length - (int)AI_REPLY_RESULT_OFFSET - AI_CHECKSUM_LENGTH];
            if (Length > AI_REPLY_ACK_OFFSET)
            {/* theres an answer in the buffer */
                for (int Index = (int)AI_REPLY_RESULT_OFFSET; Index < (Length - (int)AI_CHECKSUM_LENGTH); Index++)
                {
                    Data[LoadIndex] = DownBuffer[Index];
                    LoadIndex++;
                }
            }
            return (UInt32)Data.Length;
        }

        private void StopCommandTimer()
        {
            CommandTimer.Enabled = false;
        }

        public bool IsConnected()
        {
            return Connected;
        }



        public int ClearBuffer()
        {
            double timeout = CommandTimer.Interval;

            if (Connected == true)
            {
                BufferPointer = 0;
                while (HostConnection.BytesToRead > 0)
                {
                    HostConnection.ReadByte(); /* empty the buffer */
                }
            }
            return 0;
        }



        public int SendExtCommand(byte Command, UInt32 Length, byte[] Param)
        {// send command and set up the reciever for the correct number of char reply
            int Status = 0;
            ushort Checksum = 0;
            List<byte> Queue = new List<byte>();
            bool PacketFound = false;
            byte DataByte = 0x00;
            UInt32 DataLength = 0x00;
            /* create new packet array of appropriate size */
            byte[] TxPacket = new byte[AI_HEADER_LENGTH + Param.Length + AI_CHECKSUM_LENGTH];
            if (Connected == false)
                return -5;

            /* Build packet */
            TxPacket[AI_DEST_ADDR_OFFSET] = (byte)AI_DEST_ADDR;
            TxPacket[AI_LENGTH_OFFSET] = (byte)((AI_HEADER_LENGTH + Param.Length) >> 24);
            TxPacket[AI_LENGTH_OFFSET + 1] = (byte)((AI_HEADER_LENGTH + Param.Length) >> 16);
            TxPacket[AI_LENGTH_OFFSET + 2] = (byte)((AI_HEADER_LENGTH + Param.Length) >> 8);
            TxPacket[AI_LENGTH_OFFSET + 3] = (byte)((AI_HEADER_LENGTH + Param.Length));
            TxPacket[AI_SOURCE_ADDR_OFFSET] = (byte)AI_SOURCE_ADDR;
            TxPacket[AI_COMMAND_OFFSET] = Command;
            /* load parameter data */
            for (int Index = 0; Index < Param.Length; Index++)
            {
                TxPacket[AI_PARAMETER_OFFSET + Index] = Param[Index];
            }
            /* Checksum packet */
            for (int Index = 0; Index < AI_HEADER_LENGTH + Param.Length; Index++)
            {
                Checksum = (ushort)(Checksum + (ushort)TxPacket[Index]);
            }
            Checksum = (ushort)(0 - (ushort)Checksum);
            TxPacket[AI_HEADER_LENGTH + Param.Length] = (byte)(Checksum >> 8);
            TxPacket[AI_HEADER_LENGTH + Param.Length + 1] = (byte)(Checksum & 0xff);
            /* Send Packet */
            try
            {
                HostConnection.Write(TxPacket, 0, TxPacket.Length);
            }
            catch (Exception e)
            {
                return -4;
            }
            /* Get reply */

            BufferPointer = 0;
            /* we start the reception and the timeout for the command */
            StartCommandTimer();

            Queue.Clear();
            while ((CommandTimedOut == false) && (PacketFound == false))
            {/* recieve bytes looking for start of packet */
                if (HostConnection.BytesToRead > 0)
                    Queue.Add((byte)HostConnection.ReadByte()); /* add to header queue */

                while ((Queue.Count > 0) &&
                    (Queue[(int)AI_DEST_ADDR_OFFSET] != AI_SOURCE_ADDR))
                {/* make sure packet starts with our address */
                    Queue.RemoveAt(0);
                }

                if ((Queue.Count >= AI_HEADER_LENGTH))
                {/* we have enough bytes for a header so lets check the source address */
                    if (Queue[(int)AI_SOURCE_ADDR_OFFSET] != AI_DEST_ADDR)
                    {/* this is not a valid packet so chop off destination address to trigger shuffle */
                        Queue.RemoveAt(0);
                    }
                    else
                    {
                        PacketFound = true;
                        /* calculate how much data we expect */
                        DataLength = DataLength + ((UInt32)Queue[(int)AI_LENGTH_OFFSET]) << 24;
                        DataLength = DataLength + ((UInt32)Queue[(int)AI_LENGTH_OFFSET + 1]) << 16;
                        DataLength = DataLength + ((UInt32)Queue[(int)AI_LENGTH_OFFSET + 2]) << 8;
                        DataLength = DataLength + ((UInt32)Queue[(int)AI_LENGTH_OFFSET + 3]);
                        int Index = 0;
                        while ((Index < ((DataLength - AI_HEADER_LENGTH) + AI_CHECKSUM_LENGTH)) &&
                            (CommandTimedOut == false))
                        {/* This is a bit weird as it was adapted from the RTT implementation */
                            if (HostConnection.BytesToRead > 0)
                            {
                                Queue.Add((byte)HostConnection.ReadByte()); /* add to header queue */
                                Index++;
                            }
                        }
                    }
                }
            }

            if (CommandTimedOut == true)
            {// not enough bytes returned
                Queue.Clear();
                return -3;
            }
            else
            {/* enough bytes were recieved, so check the checksum */
                int Index = 0;
                Checksum = 0;
                for (Index = 0; Index < (Queue.Count - AI_CHECKSUM_LENGTH); Index++)
                {
                    Checksum = (ushort)(Checksum + (ushort)Queue[Index]);
                }
                UInt16 RecCheck = (UInt16)Queue[Queue.Count() - 2];
                RecCheck = (UInt16)(RecCheck << 8);
                RecCheck = (UInt16)(RecCheck | (UInt16)Queue[Queue.Count() - 1]);
                Checksum = (UInt16)(Checksum + RecCheck);
                if (Checksum != 0)
                {// checksum of packet doesn't match transmitted checksum
                    Queue.Clear();
                    return -2;
                }
                /* now define and load the down buffer */
                DownBuffer = new byte[Queue.Count];
                Index = 0;
                for (Index = 0; Index < Queue.Count; Index++)
                {
                    DownBuffer[Index] = Queue[Index];
                }
                /* was the command acked or nacked */
                if ((uint)Queue[(int)AI_REPLY_ACK_OFFSET] == 0)
                {
                    Queue.Clear();
                    return -1;
                }
            }
            Queue.Clear();
            /* Reply was recieved successfully */
            return 0;
        }











        public bool CheckForActivity()
        {
            if (Activity == true)
            {
                Activity = false;
                return (true);
            }
            else
                return false;
        }

        public void GetLastCommand(ref byte[] Command)
        {
            Command = LastCommand;
        }

        private void RecordingHandler(object sender, EventArgs e)
        {


            // wait for
        }
        #region HandsetFunctions
        const byte HANDSET_FUNCTION_DOCK = 0x1;
        const byte HANDSET_FUNCTION_UNDOCK = 0x2;
        const int HANDSET_FUNCTION_DOCK_UNDOCK_DATA_LEN = 0x01;

        const byte HANDSET_FUNCTION_SHEATH_ON = 0x3;
        const byte HANDSET_FUNCTION_SHEATH_OFF = 0x4;
        const int HANDSET_FUNCTION_SHEATH_ON_OFF_DATA_LEN = 0x01;

        const byte HANDSET_FUNCTION_PRESS_KEY = 0x5;
        const int HANDSET_FUNCTION_KEY_PRESS_DATA_LEN = 0x02;


        const byte HANDSET_FUNCTION_GET_SCREEN = 0x6;
        const byte HANDSET_FUNCTION_GRAB_SCREEN_DATA_LEN = 0x1;

        const byte HANDSET_FUNCTION_CREATE_READING = 0x7;

        const byte HANDSET_FUNCTION_SET_BOUNDARY_STATE = 0x8;
        const byte HANDSET_FUNCTION_SET_BOUNDARY_STATE_LEN = 0x2;

        const byte HANDSET_FUNCTION_SET_BATTERY = 0x9;
        const byte HANDSET_FUNCTION_SET_BATTERY_LEN = 0x2;

        const byte HANDSET_FUNCTION_GET_SYSTEM_STATE = 0xa;
        const byte HANDSET_FUNCTION_GET_SYSTEM_STATE_DATA_LEN = 0x1;

        const byte HANDSET_LEFT_KEY = 0x01;
        const byte HANDSET_ENTER_KEY = 0x02;
        const byte HANDSET_RIGHT_KEY = 0x03;
        const byte HANDSET_SCAN_KEY = 0x04;

        const byte HANDSET_FUNCTION_GET_LED_STATE = 0xb;
        const byte HANDSET_FUNCTION_GET_LED_STATE_DATA_LEN = 0x1;

        /* State machine driven FPGA control */
        const byte HANDSET_FUNCTION_DISABLE_FPGA_INTERFACE = 12;
        const byte HANDSET_FUNCTION_DISABLE_FPGA_INTERFACE_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_ENABLE_FPGA_INTERFACE = 13;
        const byte HANDSET_FUNCTION_ENABLE_FPGA_INTERFACE_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_TAKE_MEASUREMENT = 14;
        const byte HANDSET_FUNCTION_TAKE_MEASUREMENT_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_CONFIGURE_MATRIX_SWITCH = 15;
        const byte HANDSET_FUNCTION_CONFIGURE_MATRIX_SWITCH_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_CONFIGURE_FPGA_FREQ = 16;
        const byte HANDSET_FUNCTION_CONFIGURE_FPGA_FREQ_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_GET_FPGA_RESULT = 17;
        const byte HANDSET_FUNCTION_GET_FPGA_RESULT_DATA_LEN = 1;
        /* unmanaged individual control functions */

        const byte HANDSET_FUNCTION_GET_FPGA_VERSION = 18;
        const byte HANDSET_FUNCTION_GET_FPGA_VERSION_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_WRITE_FPGA_SINE = 19;
        const byte HANDSET_FUNCTION_WRITE_FPGA_SINE_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_WRITE_FPGA_COSINE = 20;
        const byte HANDSET_FUNCTION_WRITE_FPGA_COSINE_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_READ_FPGA_SINE = 21;
        const byte HANDSET_FUNCTION_READ_FPGA_SINE_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_READ_FPGA_COSINE = 22;
        const byte HANDSET_FUNCTION_READ_FPGA_COSINE_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_SET_MATRIX_SWITCH = 23;
        const byte HANDSET_FUNCTION_SET_MATRIX_SWITCH_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_READ_MATRIX_SWITCH = 24;
        const byte HANDSET_FUNCTION_READ_MATRIX_SWITCH_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_SET_FPGA_FREQUENCY = 25;
        const byte HANDSET_FUNCTION_SET_FPGA_FREQUENCY_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_SET_FPGA_SAMPLES = 26;
        const byte HANDSET_FUNCTION_SET_FPGA_SAMPLES_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_READ_FPGA_FREQUENCY = 27;
        const byte HANDSET_FUNCTION_READ_FPGA_FREQUENCY_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_READ_FPGA_SAMPLES = 28;
        const byte HANDSET_FUNCTION_READ_FPGA_SAMPLES_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_SET_ANALOGUE_PWR = 29;
        const byte HANDSET_FUNCTION_SET_ANALOGUE_PWR_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_SET_ANALOGUE_STATE = 30;
        const byte HANDSET_FUNCTION_SET_ANALOGUE_STATE_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_SET_FPGA_PWR = 31;
        const byte HANDSET_FUNCTION_SET_FPGA_PWR_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_SET_FPGA_CLOCK = 32;
        const byte HANDSET_FUNCTION_SET_FPGA_CLOCK_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_SET_FPGA_RESET = 33;
        const byte HANDSET_FUNCTION_SET_FPGA_RESET_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_SET_FPGA_GO = 34;
        const byte HANDSET_FUNCTION_SET_FPGA_GO_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_FPGA_READY_STATE = 35;
        const byte HANDSET_FUNCTION_FPGA_READY_STATE_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_MEASUREMENT_DRV_STATE = 36;
        const byte HANDSET_FUNCTION_MEASUREMENT_DRV_STATE_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_SET_MATRIX_RESET = 37;
        const byte HANDSET_FUNCTION_SET_MATRIX_RESET_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_GET_RAW_FPGA_RESULT = 38;
        const byte HANDSET_FUNCTION_GET_RAW_FPGA_RESULT_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_RESET_FPGA_DRIVER = 39;
        const byte HANDSET_FUNCTION_RESET_FPGA_DRIVER_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_LCD_SET_BACKLIGHT = 40;
        const byte HANDSET_FUNCTION_LCD_SET_BACKLIGHT_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_LCD_SET_DIM = 41;
        const byte HANDSET_FUNCTION_LCD_SET_DIM_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_LCD_SET_STATE = 42;
        const byte HANDSET_FUNCTION_LCD_SET_STATE_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_LCD_TEST_SCREEN = 43;
        const byte HANDSET_FUNCTION_LCD_TEST_SCREEN_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_DISPLAY_SYSTEM_SCREEN = 44;
        const byte HANDSET_FUNCTION_DISPLAY_SYSTEM_SCREEN_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_SET_RED_LED = 50;
        const byte HANDSET_FUNCTION_SET_RED_LED_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_SET_GREEN_LED = 51;
        const byte HANDSET_FUNCTION_SET_GREEN_LED_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_GET_KEYBOARD = 52;
        const byte HANDSET_FUNCTION_GET_KEYBOARD_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_SET_SPEAKER_FREQ = 53;
        const byte HANDSET_FUNCTION_SET_SPEAKER_FREQ_DATA_LEN = 3;

        const byte HANDSET_FUNCTION_GET_ADC_VOLTAGES = 54;
        const byte HANDSET_FUNCTION_GET_ADC_VOLTAGES_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_GET_SNOUT = 55;
        const byte HANDSET_FUNCTION_GET_SNOUT_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_SET_LED_PATTERN = 56;
        const byte HANDSET_FUNCTION_SET_LED_PATTERN_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_SET_TONE_PATTERN = 57;
        const byte HANDSET_FUNCTION_SET_TONE_PATTERN_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_GET_CHARGE_CHIP_STATUS = 60;
        const byte HANDSET_FUNCTION_GET_CHARGE_CHIP_STATUS_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_SET_CHARGE_CHIP_STATUS = 61;
        const byte HANDSET_FUNCTION_SET_CHARGE_CHIP_STATUS_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_SET_CHARGE_CHIP_POWER = 62;
        const byte HANDSET_FUNCTION_SET_CHARGE_CHIP_POWER_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_SET_BACKLIGHT_PWM_PWR = 63;
        const byte HANDSET_FUNCTION_SET_BACKLIGHT_PWM_PWR_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_RESET_HANDSET = 70;
        const byte HANDSET_FUNCTION_RESET_HANDSET_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_START_SESSION = 71;
        const byte HANDSET_FUNCTION_START_SESSION_DATA_LEN = 3;

        const byte HANDSET_FUNCTION_GET_NV_RAM_STATUS = 72;
        const byte HANDSET_FUNCTION_GET_NV_RAM_STATUS_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_NV_ERASE_PROGRAM_IMAGE = 73;
        const byte HANDSET_FUNCTION_NV_ERASE_PROGRAM_IMAGE_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_NV_ERASE_ERROR_RECORD = 74;
        const byte HANDSET_FUNCTION_NV_ERASE_ERROR_RECORD_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_NV_ERASE_UID = 75;
        const byte HANDSET_FUNCTION_NV_ERASE_UID_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_NV_CORRUPT_PROG_IMAGE = 76;
        const byte HANDSET_FUNCTION_NV_CORRUPT_PROG_IMAGE_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_SET_DEVICE_UID = 77;
        const byte HANDSET_FUNCTION_SET_DEVICE_UID_DATA_LEN = 1 + 50;

        const byte HANDSET_FUNCTION_GET_DEVICE_UID = 78;
        const byte HANDSET_FUNCTION_GET_DEVICE_UID_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_ERASE_LANGUAGE_PACK = 79;
        const byte HANDSET_FUNCTION_ERASE_LANGUAGE_PACK_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_TEST_HARD_FAULT = 80;
        const byte HANDSET_FUNCTION_TEST_HARD_FAULT_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_TEST_CRITICAL_ERROR = 81;
        const byte HANDSET_FUNCTION_TEST_CRITICAL_ERROR_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_GET_LIFE_COUNTER = 82;
        const byte HANDSET_FUNCTION_GET_LIFE_COUNTER_DATA_LEN = 5;

        const byte HANDSET_FUNCTION_CLEAR_LIFE_COUNTERS = 83;
        const byte HANDSET_FUNCTION_CLEAR_LIFE_COUNTERS_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_TEST_WATCHDOG = 84;
        const byte HANDSET_FUNCTION_TEST_WATCHDOG_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_SET_UART_BUFF_PWR = 85;
        const byte HANDSET_FUNCTION_SET_UART_BUFF_PWR_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_SET_UART_BUFF_OE = 86;
        const byte HANDSET_FUNCTION_SET_UART_BUFF_OE_DATA_LEN = 2;
        
        const byte HANDSET_FUNCTION_GET_POINT_DATA = 90;
        const byte HANDSET_FUNCTION_GET_POINT_DATA_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_GET_CAL_DATA = 91;
        const byte HANDSET_FUNCTION_GET_CAL_DATA_DATA_LEN = 1;

        const byte HANDSET_FUNCTION_CORRUPT_DATA_CHECKSUM = 92;
        const byte HANDSET_FUNCTION_CORRUPT_DATA_CHECKSUM_DATA_LEN = 2;

        const byte HANDSET_FUNCTION_SET_SYSTEM_STATE = 93;
        const byte HANDSET_FUNCTION_SET_SYSTEM_STATE_DATA_LEN = 2;


        public int DockHandset()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_DOCK_UNDOCK_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_DOCK;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_DOCK_UNDOCK_DATA_LEN, Buffer));
        }
        public int UnDockHandset()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_DOCK_UNDOCK_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_UNDOCK;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_DOCK_UNDOCK_DATA_LEN, Buffer));
        }
        public int FitSheath()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SHEATH_ON_OFF_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SHEATH_ON;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SHEATH_ON_OFF_DATA_LEN, Buffer));
        }
        public int RemoveSheath()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SHEATH_ON];
            Buffer[0] = HANDSET_FUNCTION_SHEATH_OFF;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SHEATH_ON_OFF_DATA_LEN, Buffer));
        }
        public int LeftKey()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_KEY_PRESS_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_PRESS_KEY;
            Buffer[1] = HANDSET_LEFT_KEY;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_KEY_PRESS_DATA_LEN, Buffer));
        }
        public int EnterKey()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_KEY_PRESS_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_PRESS_KEY;
            Buffer[1] = HANDSET_ENTER_KEY;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_KEY_PRESS_DATA_LEN, Buffer));
        }
        public int RightKey()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_KEY_PRESS_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_PRESS_KEY;
            Buffer[1] = HANDSET_RIGHT_KEY;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_KEY_PRESS_DATA_LEN, Buffer));
        }
        public int ScanKey()
        {
            int Result = 0;
            byte[] Buffer = new byte[HANDSET_FUNCTION_KEY_PRESS_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_PRESS_KEY;
            Buffer[1] = HANDSET_SCAN_KEY;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_KEY_PRESS_DATA_LEN, Buffer));

        }
        public int GrabScreen()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GRAB_SCREEN_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_SCREEN;
            CommandTimer.Enabled = false;
            CommandTimer.Interval = 3000;
            CommandTimer.Enabled = true;
            int Result = SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GRAB_SCREEN_DATA_LEN, Buffer);
            CommandTimer.Enabled = false;
            CommandTimer.Interval = 1000;
            CommandTimer.Enabled = true;
            return (Result);

        }
        public int SetBattery(int Value)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_BATTERY_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_BATTERY;
            Buffer[1] = (byte)Value;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_BATTERY_LEN, Buffer));
        }

        public int GetSMState()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GET_SYSTEM_STATE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_SYSTEM_STATE;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GET_SYSTEM_STATE_DATA_LEN, Buffer));
        }

        public int SetBoundary(int Value)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_BOUNDARY_STATE_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_BOUNDARY_STATE;
            Buffer[1] = (byte)Value;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_BOUNDARY_STATE_LEN, Buffer));
        }

        public int GetLEDState()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GET_LED_STATE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_LED_STATE;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GET_LED_STATE_DATA_LEN, Buffer));
        }

        public int DisableFPGAInterface()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_DISABLE_FPGA_INTERFACE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_DISABLE_FPGA_INTERFACE;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_DISABLE_FPGA_INTERFACE_DATA_LEN, Buffer));
        }
        public int EnableFPGAInterface()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_ENABLE_FPGA_INTERFACE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_ENABLE_FPGA_INTERFACE;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_ENABLE_FPGA_INTERFACE_DATA_LEN, Buffer));
        }
        public int RunFPGAMeasurement()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_TAKE_MEASUREMENT_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_TAKE_MEASUREMENT;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_TAKE_MEASUREMENT_DATA_LEN, Buffer));
        }

        public int ConfigureMatrix(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_CONFIGURE_MATRIX_SWITCH_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_CONFIGURE_MATRIX_SWITCH;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_CONFIGURE_MATRIX_SWITCH_DATA_LEN, Buffer));
        }

        public int StartSession(int Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_START_SESSION_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_START_SESSION;
            Buffer[1] = (byte)(Setting / 256);
            Buffer[2] = (byte)Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_START_SESSION_DATA_LEN, Buffer));
        }

        public int ConfigureFrequency(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_CONFIGURE_FPGA_FREQ_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_CONFIGURE_FPGA_FREQ;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_CONFIGURE_FPGA_FREQ_DATA_LEN, Buffer));
        }


        public int GetFPGA_Version()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GET_FPGA_VERSION_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_FPGA_VERSION;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GET_FPGA_VERSION_DATA_LEN, Buffer));
        }

        public int WriteSineWave()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_WRITE_FPGA_SINE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_WRITE_FPGA_SINE;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_WRITE_FPGA_SINE_DATA_LEN, Buffer));
        }

        public int WriteCoSineWave()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_WRITE_FPGA_COSINE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_WRITE_FPGA_COSINE;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_WRITE_FPGA_COSINE_DATA_LEN, Buffer));
        }
        public int ReadSineWave()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_READ_FPGA_SINE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_READ_FPGA_SINE;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_READ_FPGA_SINE_DATA_LEN, Buffer));
        }

        public int ReadCoSineWave()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_READ_FPGA_COSINE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_READ_FPGA_COSINE;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_READ_FPGA_COSINE_DATA_LEN, Buffer));
        }
        public int SetMatrix(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_MATRIX_SWITCH_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_MATRIX_SWITCH;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_MATRIX_SWITCH_DATA_LEN, Buffer));
        }

        public int SetFrequency(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_FPGA_FREQUENCY_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_FPGA_FREQUENCY;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_FPGA_FREQUENCY_DATA_LEN, Buffer));
        }

        public int SetSamples(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_FPGA_SAMPLES_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_FPGA_SAMPLES;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_FPGA_SAMPLES_DATA_LEN, Buffer));
        }

        public int ReadMatrix()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_READ_MATRIX_SWITCH_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_READ_MATRIX_SWITCH;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_READ_MATRIX_SWITCH_DATA_LEN, Buffer));
        }

        public int ReadFrequency()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_READ_FPGA_FREQUENCY_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_READ_FPGA_FREQUENCY;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_READ_FPGA_FREQUENCY_DATA_LEN, Buffer));
        }

        public int ReadSamples()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_READ_FPGA_SAMPLES_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_READ_FPGA_SAMPLES;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_READ_FPGA_SAMPLES_DATA_LEN, Buffer));
        }

        public int SetAnaloguePower(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_ANALOGUE_PWR_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_ANALOGUE_PWR;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_ANALOGUE_PWR_DATA_LEN, Buffer));
        }

        public int SetAnalogueState(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_ANALOGUE_STATE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_ANALOGUE_STATE;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_ANALOGUE_STATE_DATA_LEN, Buffer));
        }

        public int SetFPGAPower(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_FPGA_PWR_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_FPGA_PWR;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_FPGA_PWR_DATA_LEN, Buffer));
        }

        public int SetFPGAClock(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_FPGA_CLOCK_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_FPGA_CLOCK;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_FPGA_CLOCK_DATA_LEN, Buffer));
        }
        public int SetFPGAReset(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_FPGA_RESET_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_FPGA_RESET;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_FPGA_RESET_DATA_LEN, Buffer));
        }
        public int SetFPGAGo(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_FPGA_GO_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_FPGA_GO;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_FPGA_GO_DATA_LEN, Buffer));
        }
        public int ReadFPGA_ReadyState()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_FPGA_READY_STATE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_FPGA_READY_STATE;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_FPGA_READY_STATE_DATA_LEN, Buffer));
        }
        public int ReadMeasurementDrvState()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_MEASUREMENT_DRV_STATE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_MEASUREMENT_DRV_STATE;

            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_MEASUREMENT_DRV_STATE_DATA_LEN, Buffer));
        }

        public int SetLCD_Backlight(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_LCD_SET_BACKLIGHT_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_LCD_SET_BACKLIGHT;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_LCD_SET_BACKLIGHT_DATA_LEN, Buffer));
        }
        public int SetLCD_Dim(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_LCD_SET_DIM_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_LCD_SET_DIM;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_LCD_SET_DIM_DATA_LEN, Buffer));
        }
        public int SetLCD_State(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_LCD_SET_STATE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_LCD_SET_STATE;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_LCD_SET_STATE_DATA_LEN, Buffer));
        }
        public int SetLCD_TestScreen()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_LCD_TEST_SCREEN_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_LCD_TEST_SCREEN;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_LCD_TEST_SCREEN_DATA_LEN, Buffer));
        }


        public int SetRedLed(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_RED_LED_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_RED_LED;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_RED_LED_DATA_LEN, Buffer));
        }
        public int SetGreenLed(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_GREEN_LED_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_GREEN_LED;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_GREEN_LED_DATA_LEN, Buffer));
        }
        public int GetKeyboardState()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GET_KEYBOARD_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_KEYBOARD;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GET_KEYBOARD_DATA_LEN, Buffer));
        }
        public int GetSnoutState()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GET_SNOUT_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_SNOUT;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GET_SNOUT_DATA_LEN, Buffer));
        }
        public int SetSpeakerFrequency(UInt16 Frequency)
        {

            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_SPEAKER_FREQ_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_SPEAKER_FREQ;
            Buffer[1] = (byte)(Frequency >> 8);
            Buffer[2] = (byte)(Frequency & 0x00ff);
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_SPEAKER_FREQ_DATA_LEN, Buffer));
        }
        public int GetADC_Voltages()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GET_ADC_VOLTAGES_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_ADC_VOLTAGES;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GET_ADC_VOLTAGES_DATA_LEN, Buffer));
        }
        public int GetNV_RAM_Status()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GET_NV_RAM_STATUS_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_NV_RAM_STATUS;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GET_NV_RAM_STATUS_DATA_LEN, Buffer));
        }
        public int GetChargeChipStatus()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GET_CHARGE_CHIP_STATUS_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_CHARGE_CHIP_STATUS;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GET_CHARGE_CHIP_STATUS_DATA_LEN, Buffer));
        }
        public int SetChargeChipStatus(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_CHARGE_CHIP_STATUS_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_CHARGE_CHIP_STATUS;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_CHARGE_CHIP_STATUS_DATA_LEN, Buffer));
        }
        public int SetChargeChipPower(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_CHARGE_CHIP_POWER_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_CHARGE_CHIP_POWER;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_CHARGE_CHIP_POWER_DATA_LEN, Buffer));
        }
        public int SetMatrixReset(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_MATRIX_RESET_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_MATRIX_RESET;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_MATRIX_RESET_DATA_LEN, Buffer));
        }

        public int GetFPGAResults()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GET_RAW_FPGA_RESULT_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_RAW_FPGA_RESULT;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GET_RAW_FPGA_RESULT_DATA_LEN, Buffer));
        }
        public int RequestFPGAResults()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GET_FPGA_RESULT_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_FPGA_RESULT;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GET_FPGA_RESULT_DATA_LEN, Buffer));
        }

        public int ResetFPGADriver()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_RESET_FPGA_DRIVER_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_RESET_FPGA_DRIVER;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_RESET_FPGA_DRIVER_DATA_LEN, Buffer));
        }
        public int ResetHandset()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_RESET_HANDSET_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_RESET_HANDSET;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_RESET_HANDSET_DATA_LEN, Buffer));
        }

        public int EraseProgramImage()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_NV_ERASE_PROGRAM_IMAGE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_NV_ERASE_PROGRAM_IMAGE;
            CommandTimer.Enabled = false;
            CommandTimer.Interval = 10000;
            CommandTimer.Enabled = true;
            int Result = SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_NV_ERASE_PROGRAM_IMAGE_DATA_LEN, Buffer);
            CommandTimer.Enabled = false;
            CommandTimer.Interval = 1000;
            CommandTimer.Enabled = true;
            return (Result);
        }
        public int EraseErrorRecord()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_NV_ERASE_ERROR_RECORD_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_NV_ERASE_ERROR_RECORD;
            CommandTimer.Enabled = false;
            CommandTimer.Interval = 10000;
            CommandTimer.Enabled = true;
            int Result = SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_NV_ERASE_ERROR_RECORD_DATA_LEN, Buffer);
            CommandTimer.Enabled = false;
            CommandTimer.Interval = 1000;
            CommandTimer.Enabled = true;
            return (Result);
        }
        public int EraseUID()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_NV_ERASE_UID_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_NV_ERASE_UID;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_NV_ERASE_UID_DATA_LEN, Buffer));
        }

        public int CorruptProgramImage()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_NV_CORRUPT_PROG_IMAGE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_NV_CORRUPT_PROG_IMAGE;
            CommandTimer.Enabled = false;
            CommandTimer.Interval = 60000;
            CommandTimer.Enabled = true;
            int Result = SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_NV_CORRUPT_PROG_IMAGE_DATA_LEN, Buffer);
            CommandTimer.Enabled = false;
            CommandTimer.Interval = 1000;
            CommandTimer.Enabled = true;
            return (Result);
        }


        public int CauseHardFault()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_TEST_HARD_FAULT_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_TEST_HARD_FAULT;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_TEST_HARD_FAULT_DATA_LEN, Buffer));
        }

        public int CauseCriticalError()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_TEST_CRITICAL_ERROR_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_TEST_CRITICAL_ERROR;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_TEST_CRITICAL_ERROR_DATA_LEN, Buffer));
        }
        public int SetUID(String UID)
        {
            int Index = 0;
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_DEVICE_UID_DATA_LEN];

            Buffer[0] = HANDSET_FUNCTION_SET_DEVICE_UID;
            for (Index = 0; (Index < 50); Index++)
            {
                if (Index < UID.Length)
                {// copy string char
                    Buffer[Index + 1] = Convert.ToByte(UID[Index]);
                }
                else
                {// pad with string end marker
                    Buffer[Index + 1] = 0;
                }
            }
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_DEVICE_UID_DATA_LEN, Buffer));
        }
        public int GetUID()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GET_DEVICE_UID_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_DEVICE_UID;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GET_DEVICE_UID_DATA_LEN, Buffer));
        }


        public int GetLifeCounter(UInt32 Counter)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GET_LIFE_COUNTER_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_LIFE_COUNTER;
            Buffer[1] = (byte)(Counter >> 24);
            Buffer[2] = (byte)(Counter >> 16);
            Buffer[3] = (byte)(Counter >> 8);
            Buffer[4] = (byte)Counter;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GET_LIFE_COUNTER_DATA_LEN, Buffer));
        }


        public int ClearLifeCounters()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_CLEAR_LIFE_COUNTERS_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_CLEAR_LIFE_COUNTERS;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_CLEAR_LIFE_COUNTERS_DATA_LEN, Buffer));
        }

        public int TestWatchdogReset()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_TEST_WATCHDOG_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_TEST_WATCHDOG;
            CommandTimer.Enabled = false;
            CommandTimer.Interval = 5000;
            CommandTimer.Enabled = true;
            int Result = SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_TEST_WATCHDOG_DATA_LEN, Buffer);
            CommandTimer.Enabled = false;
            CommandTimer.Interval = 1000;
            CommandTimer.Enabled = true;
            return (Result);
        }

        public int SetUARTBufferPwr(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_UART_BUFF_PWR_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_UART_BUFF_PWR;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_UART_BUFF_PWR_DATA_LEN, Buffer));
        }

        public int SetUARTBufferOE(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_UART_BUFF_OE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_UART_BUFF_OE;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_UART_BUFF_OE_DATA_LEN, Buffer));
        }

        public int SetBacklightPWM_Pwr(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_BACKLIGHT_PWM_PWR_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_BACKLIGHT_PWM_PWR;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_BACKLIGHT_PWM_PWR_DATA_LEN, Buffer));
        }

        public int SetLED_Pattern(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_LED_PATTERN_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_LED_PATTERN;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_LED_PATTERN_DATA_LEN, Buffer));
        }
        public int SetTone_Pattern(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_TONE_PATTERN_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_TONE_PATTERN;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_TONE_PATTERN_DATA_LEN, Buffer));
        }
        public int GetPointData()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GET_POINT_DATA_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_POINT_DATA;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GET_POINT_DATA_DATA_LEN, Buffer));
        }

        public int DisplaySystemScreen(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_DISPLAY_SYSTEM_SCREEN_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_DISPLAY_SYSTEM_SCREEN;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_DISPLAY_SYSTEM_SCREEN_DATA_LEN, Buffer));
        }


        public int SetSystemState(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_SET_SYSTEM_STATE_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_SET_SYSTEM_STATE;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_SET_SYSTEM_STATE_DATA_LEN, Buffer));
        }

        public int CorruptDataChecksum(byte Setting)
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_CORRUPT_DATA_CHECKSUM_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_CORRUPT_DATA_CHECKSUM;
            Buffer[1] = Setting;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_CORRUPT_DATA_CHECKSUM_DATA_LEN, Buffer));
        }

        public int GetCalData()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_GET_CAL_DATA_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_GET_CAL_DATA;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_GET_CAL_DATA_DATA_LEN, Buffer));
        }

        public int EraseLanguagePack()
        {
            byte[] Buffer = new byte[HANDSET_FUNCTION_ERASE_LANGUAGE_PACK_DATA_LEN];
            Buffer[0] = HANDSET_FUNCTION_ERASE_LANGUAGE_PACK;
            return (SendExtCommand((byte)AI_EXECUTE_FUNCTION, HANDSET_FUNCTION_ERASE_LANGUAGE_PACK_DATA_LEN, Buffer));
        }

        
        #endregion

        public string ErrorToText(int Error)
        {
            string Message = "Unknown Error";
            switch (Error)
            {
                case 0:
                    Message = "ACK";
                    break;
                case -1:
                    Message = "NACK";
                    break;
                case -2:
                    Message = "Faulty Checksum";
                    break;
                case -3:
                    Message = "Message Timeout";
                    break;
                case -4:
                    Message = "Failed to transmit";
                    break;
            }
            return Message;
        }

        // End of class
    }
    [Serializable()]
    public class ScriptLoadSaveObject
    {
        public List<string[]> Script = new List<string[]>();
    }
}
