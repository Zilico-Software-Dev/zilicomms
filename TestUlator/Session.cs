﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;

namespace ZiliCator
{
    public class Session : ZiliComms.Handset.Session
    {
        #region field

        private string _NHSnumber;
        private string _PatientName;
        private DateTime _DateOfBirth;

        #endregion

        #region properties

        /// <summary>
        /// NHS number
        /// </summary>
        public string NHSnumber
        {
            get { return _NHSnumber; }
            internal set { _NHSnumber = value; }
        }

        /// <summary>
        /// Patient name
        /// </summary>
        public string PatientName
        {
            get { return _PatientName; }
            internal set { _PatientName = value; }
        }

        /// <summary>
        /// Patient date of birth
        /// </summary>
        public DateTime DateOfBirth
        {
            get { return _DateOfBirth; }
            internal set { _DateOfBirth = value; }
        }

        #endregion



        /// <summary>
        /// Fully populated Session data object
        /// </summary>
        /// <param name="patientName"></param>
        /// <param name="patientId"></param>
        /// <param name="dateOfBirth"></param>
        /// <param name="nhsNumber"></param>
        /// <param name="operatorId"></param>
        /// <param name="referralType"></param>
        /// <param name="currentTime"></param>
        /// <param name="trainingMode"></param>
        public Session(string patientName, string patientId, DateTime dateOfBirth, string nhsNumber,
                       string operatorId, ZiliComms.Handset.ReferralType referralType, bool trainingMode)
        : base(patientId, operatorId, referralType, DateTime.Now, trainingMode)

        {
            _NHSnumber = nhsNumber;
            _PatientName = patientName;
            _DateOfBirth = dateOfBirth;
        }


    }
}
// Copyright (c) 2022 Zilico Limited
