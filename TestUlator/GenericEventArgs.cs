﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;

namespace ZiliCator
{
    internal class GenericEventArgs<T> : EventArgs
    {
		private T _value;

		public T Value
		{ get { return _value; } }

		public GenericEventArgs(T value)
		{
			_value = value;
		}
	}
}
// Copyright (c) 2022 Zilico Limited
