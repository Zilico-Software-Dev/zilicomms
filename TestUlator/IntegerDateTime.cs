﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZiliCator
{
    internal class IntegerDateTime
    {
        private DateTime _storedTimeDt = DateTime.Now;
        private UInt32 storedTimeI = ConvertToInteger(DateTime.Now);

        public DateTime DateTime
        {
            get { return _storedTimeDt; }

            set
            {
                _storedTimeDt = value;
                storedTimeI = ConvertToInteger(value);
            }
        }

        public UInt32 Integer
        {
            get { return storedTimeI; }

            set
            {
                _storedTimeDt = ConvertToDateTime(value);
                storedTimeI = value;
            }
        }

        /*Integer Time Format*/
        /*============================
         * bits 0 - 5 Seconds 0 - 59
         * bits 6 - 10 Minutes 0 - 59
         * bits 12 - 16 Hours 0 - 23
         * bits 17 - 21 Day 1 - 31
         * bits 22 - 25 Month 1 - 12
         * bits 26 - 31 Year 0 - 63 - Witha an assumed offset of 2012
         */
        public static UInt32 ConvertToInteger(DateTime dt)
        {
            return
                (UInt32)
                ((dt.Second & 0x3F) + ((dt.Minute & 0x3F) << 6) + ((dt.Hour & 0x1F) << 12) + ((dt.Day & 0x1F) << 17) +
                 ((dt.Month & 0x0F) << 22) + (((dt.Year - 2012) & 0x3F) << 26));
        }

        public static DateTime ConvertToDateTime(UInt32 i)
        {
            var second = (int)(i & 0x3F);
            var minute = (int)((i >> 6) & 0x3F);
            var hour = (int)((i >> 12) & 0x1F);
            var day = (int)((i >> 17) & 0x1F);
            var month = (int)((i >> 22) & 0x0F);
            int year = (int)((i >> 26) & 0x3F) + 2012;
            try
            {
                return new DateTime(year, month, day, hour, minute, second);
            }
            catch
            {
                throw new Exception("Date and Time received is invalid.");
            }
        }

    }
}
