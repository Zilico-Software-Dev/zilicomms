﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestComms
{
    /// <summary>
    /// ZedScan Constructor tests
    /// </summary>
    [TestClass]
    public class UnitTestPort
    {
        // Port not set
        // Port set
        // Port set after instantiation

        private ZiliComms.ZedScan _ZedScan;

        /// <summary>
        /// Check a missing port assignment is detected
        /// </summary>
        [TestMethod]
        public void ConstructUninitialisedPort()
        {
            _ZedScan = new ZiliComms.ZedScan();

            if (_ZedScan.GetDeviceInformation())
                Assert.Fail("Uninitialised port did not throw error");

            StringAssert.Contains(_ZedScan.Message, "Port not found");
        }

        /// <summary>
        /// Check port assignment upon construction
        /// </summary>
        [TestMethod]
        public void ConstructWithEmulatedPort()
        {
            ZiliComms.IUSBPort port = new PortEmulator();

            _ZedScan = null;

            try
            {
                _ZedScan = new ZiliComms.ZedScan(port);

                Assert.AreEqual(port, _ZedScan.Port,"Port not set in constructor");
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        /// <summary>
        /// Check  port assignment after instantiation
        /// </summary>
        [TestMethod]
        public void LatePortAssignment()
        {
            ZiliComms.IUSBPort port = new PortEmulator();

            _ZedScan = new ZiliComms.ZedScan();

            _ZedScan.Port = port;

            if (_ZedScan.GetDeviceInformation())
                Assert.Fail("Unexpected response");

            if (_ZedScan.Message.Contains("Port not found"))
                Assert.Fail(_ZedScan.Message);
        }
    }
}
