﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using ZiliComms;

namespace UnitTestComms
{
    public class PortEmulator : ZiliComms.IUSBPort
    {
        public string Message { get; private set; }

        public bool TraceEnabled { get; set; }

        public int TimeOut { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public string DevicePath { get; set; }

        public bool IsOpen { get; private set; }

        public bool SingleByteMode { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public List<byte> ReturnedPacket { get; private set; }

        public bool IsDisposed { get; private set; }



        public byte[] SentData { get; private set; }

        public PortEmulator() : this(null) { }

        public PortEmulator(List<byte> returnData)
        {
            Message = string.Empty;

            IsOpen = false;

            ReturnedPacket = returnData;
        }



        public bool Close()
        {
            IsOpen = false;

            return !IsOpen;
        }

        public void Dispose()
        {
            throw new NotImplementedException("Dispose");
        }

        public bool Open()
        {
            IsOpen = true;

            return IsOpen;
        }

        public bool SetBaudRate(int baud)
        {
            throw new NotImplementedException("SetBaudRate");
        }

        public bool SetBaudRate(Baud rate)
        {
            return SetBaudRate((int)rate);
        }

        public string ToHex(byte[] buffer)
        {
            throw new NotImplementedException("ToHex");
        }

        public bool Transceive(byte[] sendData)
        {
            SentData = sendData;

            //if (sendData != null)
            //{
            //    ReturnedPacket = new List<byte>();
            //    ReturnedPacket.AddRange(sendData);
            //}

            return sendData.Length > 0;
        }
    }
}
// Copyright (c) 2022 Zilico Limited
