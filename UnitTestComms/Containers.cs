﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using System;
using System.Collections.Generic;
using System.Linq;
//using System.Text;
using System.Threading.Tasks;

namespace UnitTestComms
{
    public class StringContainer
    {
        [ZiliComms.Handset.PropId(1)]
        public string TestProperty { get; set; }

        public override string ToString()
        {
            return TestProperty;
        }
    }





}
// Copyright (c) 2022 Zilico Limited
