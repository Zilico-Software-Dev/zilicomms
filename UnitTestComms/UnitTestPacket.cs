﻿// Company : Zilico Limited
// Copyright (c) 2022 Zilico Limited
// Author : P Kaye
// Date : 

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace UnitTestComms
{
    [TestClass]
    public class UnitTestPacket
    {
        // No response
        // Partial response
        // Corrupt checksum

        // Encode / Decode
        //      String
        //      Integer
        //      Float
        //      Boolean

        private ZiliComms.ZedScan _ZedScan;



        /// <summary>
        /// Check for no returned bytes
        /// </summary>
        [TestMethod]
        public void DetectNoRespose()
        {
            _ZedScan = new ZiliComms.ZedScan(new PortEmulator());

            if (_ZedScan.GetDeviceInformation())
                Assert.Fail("No returned data did not cause error");

            StringAssert.Contains(_ZedScan.Message, "No response");
        }

        /// <summary>
        /// Check for complete packet
        /// </summary>
        [TestMethod]
        public void DetectPartialResponse()
        {
            List<byte> buffer = new List<byte>() { 0xFE, 0x01 };

            _ZedScan = new ZiliComms.ZedScan(new PortEmulator(buffer));

            if (_ZedScan.GetDeviceInformation())
                Assert.Fail("Partial returned data did not cause error");

            StringAssert.Contains(_ZedScan.Message, "Incomplete packet");
        }

        /// <summary>
        /// Check corrupt checksum is detected
        /// </summary>
        [TestMethod]
        public void DetectCorruptChecksum()
        {
            List<byte> buffer = new List<byte>() { 0xFE, 0x01, 0x0A, 0x04, 0x31, 0x2E, 0x30, 0x2E,
                                                   0xFE, 0xFD, 0x00, 0x01, 0x03, 0x03 };

            _ZedScan = new ZiliComms.ZedScan(new PortEmulator(buffer));

            if (_ZedScan.GetDeviceInformation())
                Assert.Fail("Partial returned data with corrupt checksum did not cause error");

            StringAssert.Contains(_ZedScan.Message, "Failed checksum");
        }


        //[TestMethod]
        //public void PacketEncodeDecode()
        //{
        //    List<byte> buffer = new List<byte>();
        //    for (int i = 0; i < 0x100; i++)
        //    {
        //        buffer.Add((byte)i);
        //    }

        //    // check encoding
        //    Assert.IsTrue(ZiliComms.PacketWrapper.Encode(buffer.ToArray()), ZiliComms.PacketWrapper.Message);

        //    // check decoding
        //    Assert.IsTrue(ZiliComms.PacketWrapper.Decode(new List<byte>(ZiliComms.PacketWrapper.PayLoad)), 
        //                  ZiliComms.PacketWrapper.Message);

        //    // check integrity
        //    Assert.AreEqual(buffer.Count, ZiliComms.PacketWrapper.PayLoad.Length, 
        //                    "Incorrect number of bytes returned");

        //    for (int i = 0; i < buffer.Count; i++)
        //    {
        //        Assert.AreEqual(buffer[i], ZiliComms.PacketWrapper.PayLoad[i], $"Incorrect value [{i}]");
        //    }
        //}


        /// <summary>
        /// Check packet encoding of an object containing a string property
        /// </summary>
        [TestMethod]
        public void EncodeString()
        {
            StringContainer sc = new StringContainer
            {
                TestProperty = "ZSEm-SW-001"
            };

            ZiliComms.Commands.OpCode opCode = ZiliComms.Commands.OpCode.DEVICE_INFORMATION;
            
            // build  expected packet
            List<byte> expectedBuffer = new List<byte>() { };
            expectedBuffer.Add((byte)opCode);
            expectedBuffer.Add(0x0A);                               // First property, with wire type LengthDelimit
            expectedBuffer.Add((byte)sc.TestProperty.Length);
            expectedBuffer.AddRange(System.Text.Encoding.UTF8.GetBytes(sc.TestProperty));

            UInt32 checkSum = ZiliComms.CheckSum.CalculateCRC(expectedBuffer);
            byte[] checkSumBytes = BitConverter.GetBytes(checkSum);
            Array.Reverse(checkSumBytes, 0, checkSumBytes.Length);

            expectedBuffer.Insert(0, 0xFE);                         // Escape start of packet
            expectedBuffer.Add(0xFE);                               // Escape end of packet
            expectedBuffer.Add(0xFD);                               // Escape checksum start

            expectedBuffer.AddRange(checkSumBytes);


            // trace ((PortEmulator)zedScan.Port).SentData


            _ZedScan = new ZiliComms.ZedScan();

            List<byte> encodedBuffer = _ZedScan.Encode(opCode, sc); // only exposed for Zilulator

            if ((encodedBuffer != null) && (encodedBuffer.Count > 0))
            {
                Assert.AreEqual(expectedBuffer.Count, encodedBuffer.Count, "Wrong number of bytes");

                for (int i = 0; i < expectedBuffer.Count; i++)
                {
                    Assert.AreEqual(expectedBuffer[i], encodedBuffer[i], $"Wrong value [{i}]");
                }
            }
            else
            {
                Assert.Fail($"String Container object not  encoded, {_ZedScan.Message}");
            }
        }

        /// <summary>
        /// Check packet decoding of an object containing a string property
        /// </summary>
        [TestMethod]
        public void DecodeString()
        {
            StringContainer sc = new StringContainer
            {
                TestProperty = "ZSEm-SW-001"
            };

            ZiliComms.Commands.OpCode opCode = ZiliComms.Commands.OpCode.DEVICE_INFORMATION;

            // build  expected packet
            List<byte> expectedBuffer = new List<byte>() { };
            expectedBuffer.Add((byte)opCode);
            expectedBuffer.Add(0x0A);                               // First property, with wire type LengthDelimit
            expectedBuffer.Add((byte)sc.TestProperty.Length);
            expectedBuffer.AddRange(System.Text.Encoding.UTF8.GetBytes(sc.TestProperty));

            UInt32 checkSum = ZiliComms.CheckSum.CalculateCRC(expectedBuffer);
            byte[] checkSumBytes = BitConverter.GetBytes(checkSum);
            Array.Reverse(checkSumBytes, 0, checkSumBytes.Length);

            expectedBuffer.Insert(0, 0xFE);                         // Escape start of packet
            expectedBuffer.Add(0xFE);                               // Escape end of packet
            expectedBuffer.Add(0xFD);                               // Escape checksum start

            expectedBuffer.AddRange(checkSumBytes);



            _ZedScan = new ZiliComms.ZedScan();

            StringContainer decodedObject = new StringContainer();

            if (!_ZedScan.Decode(expectedBuffer, decodedObject))
                Assert.Fail($"String Container object not  decoded, {_ZedScan.Message}");

            Assert.AreEqual(sc, decodedObject);
        }
    }

    /*
    
    Assert.Fail("Uninitialised port did not throw error");
    Assert.AreEqual(port, _ZedScan.Port,"Port not set in constructor");
    Assert.ThrowsException<System.ArgumentOutOfRangeException>(() => zedScan = new ZiliComms.ZedScan(port));
    StringAssert.Contains(_ZedScan.Message, "Port not found");

    0xFE, 0x01, 0x0A, 0x07, 0x31, 0x2E, 0x30, 0x2E, 
    0x30, 0x2E, 0x30, 0x12, 0x0B, 0x5A, 0x53, 0x45, 
    0x6D, 0x2D, 0x53, 0x57, 0x2D, 0x30, 0x30, 0x31, 
    0x1A, 0x06, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 
    0x22, 0x00, 0x30, 0x00, 0x38, 0x00, 0xFE, 0xFD, 
    0x79, 0xD1, 0x2C, 0x5C

    */
}
// Copyright (c) 2022 Zilico Limited
